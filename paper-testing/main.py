import numpy as np
import scipy as sp 
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D

from Functions import Coarsen
from Functions import Plotter
from Functions import Embedder

emb = Embedder()
cg = Coarsen()
pttr = Plotter()

#cg.coarsen_demo()

#pttr.three_scatter(data,surface)

colors = []
diff_norms = []
spec_diffs = []
big_deg = 0
small_deg = 10000000
for i in range(10000):
	size = 4
	surface = "random-clustered"
	data = emb.embed_points(size,surface)
	adj = emb.weight_edges(data,"inverse_exp")
	#pttr.plot_adj(adj,2,[0,0,0,0,0,1,1,1,1,1])
	num = np.random.randint(2,int(size/2)+1)
	clust_dict = {0:np.arange(0,num)}
	others = np.arange(num,size)
	ind = 1
	for element in others:
		clust_dict[ind] = [element]
		ind += 1
	coarse_adj = cg.coarsen(clust_dict,adj)
	sizes_list = np.ones(size-num+1)
	sizes_list[0] = num
	sizes_list = sizes_list.astype(int)
	#print(sizes_list)
	lift_adj = cg.lift(sizes_list,coarse_adj)

	
	print( np.abs(adj - lift_adj) )
	print('----')
	print(adj)
	print(coarse_adj)
	print(lift_adj)
	print('----')
	
	'''
	print('---------')
	print(lift_adj)
	print(lift_adj[0:num,0:num])
	print(lift_adj[num:size,num:size])
	'''
	if np.max(np.sum(lift_adj[0:num,0:num])) > big_deg:
		big_deg = np.max(np.sum(lift_adj[0:num,0:num]))
		print('big deg: '+str(big_deg)+' round: '+str(i))
	if np.max(np.sum(lift_adj[num:size,num:size])) > big_deg:
		big_deg = np.max(np.sum(lift_adj[num:size,num:size]))
		print('big deg: '+str(big_deg)+' round: '+str(i))
	if np.max(np.sum(lift_adj[0:num,0:num])) < small_deg:
		small_deg = np.max(np.sum(lift_adj[0:num-1,0:num-1]))
		print('small deg: '+str(small_deg)+' round: '+str(i))
	if np.max(np.sum(lift_adj[num:size,num:size])) < small_deg:
		small_deg = np.max(np.sum(lift_adj[num:size,num:size]))
		print('small deg: '+str(small_deg)+' round: '+str(i))

	[w,lifted_w] = cg.spec_diff(adj,lift_adj,False)
	'''
	print('----')
	print(w)
	print(lifted_w)
	'''
	colors.append(num)
	diff_norms.append(np.max(np.abs(adj-lift_adj)))
	#print( np.max(np.abs(adj-lift_adj)) )
	spec_diffs.append(np.max(np.abs(w-lifted_w)))

x = []
func_big = []
func_small = []
num = 1000
for i in range(num):
	x.append(i/num)
	p = ((i/(num+1))/big_deg)
	func_big.append(big_deg*((3*p)/(1 - (2*p))) + (8*((3*p)/(1 - (2*p)))**2)/3 + (4*((3*p)/(1 - (2*p))))/3)
	p = ((i/(num+1))/small_deg)
	func_small.append(small_deg*((3*p)/(1 - (2*p))) + (8*((3*p)/(1 - (2*p)))**2)/3 + (4*((3*p)/(1 - (2*p))))/3)

plt.figure()
plt.scatter(spec_diffs,diff_norms,color='tab:red',s=5,alpha=0.5)
plt.plot(x,func_big,color='orange',linestyle = 'dashed', linewidth = 2, label='bound A')
plt.plot(x,func_small,color='blue',linestyle = 'dashed', linewidth = 2, label='bound B')
plt.legend(prop={'size':16})
plt.xlim(0,0.3)
plt.ylim(0,0.3)
plt.xlabel('max spectral difference',size=16)
plt.ylabel('max edge weight differece',size=16)

'''
weighting = 2

pttr.plot_adj(adj,weighting,[0,0,0,1,1,1])

pttr.plot_adj(coarse_adj,weighting,[0,1])

pttr.plot_adj(lift_adj,weighting,[0,0,0,1,1,1])
'''
plt.show()