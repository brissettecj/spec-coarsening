import numpy as np 
import networkx as nx
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
import scipy as sp 


class Embedder:

	def __init__( self ):
		print('')

	'''
	embed_points( int number, string surface, float xmin, float xmax, float ymin, float ymax )
	- TAKES IN:
		-- an integer number of points "number".
		-- a string for the name of the surface we are embedding into "surface".
	- RETURNS:
		-- a 3xN numpy array of points.
	'''
	def embed_points( self, number, surface):
		ret_mat = np.zeros((3,number))

		if surface == "bluffs":
			x = np.random.uniform(0,2,number)
			y = np.random.uniform(0,1,number)
			z = np.zeros((1,number))
			for i in range(number):
				if x[i] > 1:
					z[0,i] = 1
				else:
					z[0,i] = 0
			ret_mat[0,0:number] = x
			ret_mat[1,0:number] = y
			ret_mat[2,0:number] = z
			return(ret_mat)

		if surface == "cosine":
			x = np.random.uniform(0,3.1,number)
			y = np.random.uniform(0,2,number)
			z = np.cos(x)
			ret_mat[0,0:number] = x
			ret_mat[1,0:number] = y
			ret_mat[2,0:number] = z
			return(ret_mat)

		if surface == "tanh":
			x = np.random.uniform(0,2,number)
			y = np.random.uniform(0,0.5,number)
			z = np.tanh((x-1)*5)
			ret_mat[0,0:number] = x
			ret_mat[1,0:number] = y
			ret_mat[2,0:number] = z
			return(ret_mat)

		if surface == "random-clustered":
			x = np.zeros((1,number))
			y = np.zeros((1,number))
			z = np.zeros((1,number))
			if number % 2 == 0:
				z[0,0:int(number/2)] = np.random.normal(1,2,int(number/2))
				z[0,int((number/2)):number] = np.random.normal(2,2,int(number/2))
			else:
				z[0,0:int((number-1)/2)] = np.random.normal(1,0.1,int(((number-1)/2)))
				z[0,int((number-1)/2)+1:number] = np.random.normal(5,0.1,int(((number-1)/2)))
			ret_mat[0,0:number] = x
			ret_mat[1,0:number] = y
			ret_mat[2,0:number] = z
			return(ret_mat)

		if surface == "random-uniform":
			x = np.zeros((1,number))
			y = np.zeros((1,number))
			z = np.zeros((1,number))
			if number % 2 == 0:
				z[0,0:int(number/2)] = np.random.normal(1,0.5,int(number/2))
				z[0,int((number/2)):number] = np.random.normal(2,0.5,int(number/2))
			else:
				z[0,0:int((number-1)/2)] = np.random.normal(1,0.1,int(((number-1)/2)))
				z[0,int((number-1)/2)+1:number] = np.random.normal(5,0.1,int(((number-1)/2)))
			ret_mat[0,0:number] = x
			ret_mat[1,0:number] = y
			ret_mat[2,0:number] = z
			return(ret_mat)


	'''
	weight_edges( numpy array data, string function )
	- TAKES IN:
		-- our numpy array of data points "data".
		-- the name of the function we are using for weighting "weight".
	- RETURNS:
		-- an adjacency matrix of a graph with the correct weights.
	'''
	def weight_edges( self, data, function ):
		[three,data_points] = np.shape(data)
		weighted_adj = np.zeros((data_points,data_points))
		if function == "inverse":
			for i in range(data_points):
				for j in range(data_points):
					weighted_adj[i,j] = 1/(np.linalg.norm(data[:,i]-data[:,j])+1)
			return(weighted_adj)
		if function == "inverse_exp":
			for i in range(data_points):
				for j in range(data_points):
					weighted_adj[i,j] = 1/np.exp(np.linalg.norm(data[:,i]-data[:,j]))
			return(weighted_adj)
		if function == "random-uniform":
			for i in range(data_points):
				weighted_adj[i,i+1:data_points] = np.random.uniform(0,1,data_points-i)
			weighted_adj = weighted_adj + weighted_adj.tanspose()
			return(weighted_adj)

			

class Coarsen:

	def __init__( self ):
		print('')

	'''
	coarsen( dict clusters , numpy array adj )
	- TAKES IN: 
		-- a dictionary of numpy arrays. Each array consists of
	 	   integers with no overlapping elements "clusters".
	 	-- a graph adjacency matrix "adj". 
	- RETURNS: 
		-- a coarsened adjacency matrix.
	'''
	def coarsen( self, clusters , adj ):
		[x,cols] = np.shape(adj)
		rows = len(clusters)
		coarsening_matrix = np.zeros((rows,cols))
		for arr in clusters:
			for element in clusters[arr]:
				coarsening_matrix[arr,element] = 1
		#print('COARSENING')
		#print(coarsening_matrix)
		new_mat = np.matmul(coarsening_matrix,adj)
		new_mat = np.matmul(new_mat,np.transpose(coarsening_matrix))
		return(new_mat)


	'''
	lift( numpy array sizes, numpy array coarse_adj )
	- TAKES IN:
		-- a numpy array of the number of nodes within each cluster "sizes". These
		   should be ordered the same as the rows of "coarse_adj".
		-- a graph adjacency matrix "coarse_adj" with the same dimension as "sizes".
	- RETURNS:
		-- the lifted adjacency matrix of the coarsened graph.
	'''
	def lift( self, sizes, coarse_adj ):
		size = np.sum(sizes)
		new_matrix = np.zeros((size,size))
		curr_row = 0
		row_ind = 0
		for block_row in sizes:
			curr_col = 0
			col_ind = 0
			for block_col in sizes:
				if row_ind != col_ind:
					block_size = block_row*block_col
					new_matrix[curr_row:curr_row+block_row,curr_col:curr_col+block_col] = (coarse_adj[row_ind,col_ind]/block_size)*np.ones((block_row,block_col))
				if row_ind == col_ind:
					block_size = (block_row + 1)*block_col/2
					new_matrix[curr_row:curr_row+block_row,curr_col:curr_col+block_col] = (coarse_adj[row_ind,col_ind]/block_size)*np.ones((block_row,block_col))
				col_ind = col_ind + 1
				curr_col = curr_col + block_col
			row_ind = row_ind + 1
			curr_row = curr_row + block_row

		return(new_matrix)

	'''
	spec_diff( numpy array adj, numpy array lifted_adj, boolean normalized )
	- TAKES IN:
		-- two numpy arraus of the adjacency and lifted adjacency respectively "adj" and "lifted_adj".
		-- a boolean "normalized" determining if we are comparing the normalized or standard Laplacians.
	- RETURNS:
		-- the sorted spectra of each Laplacian.
	'''
	def spec_diff( self, adj, lifted_adj, normalized ):
		degs = adj.sum(axis = 1)
		lifted_degs = lifted_adj.sum(axis = 1)
		D = np.diag(degs)
		lifted_D = np.diag(lifted_degs)
		L = D - adj
		lifted_L = lifted_D - lifted_adj
		if normalized == True:
			L = np.matmul(np.diag(np.diag(D)**(-1/2)),L)
			L = np.matmul(L,np.diag(np.diag(D)**(-1/2)))
			lifted_L = np.matmul(np.diag(np.diag(lifted_D)**(-1/2)),lifted_L)
			lifted_L = np.matmul(lifted_L,np.diag(np.diag(lifted_D)**(-1/2)))
		[w,v] = np.linalg.eig(L)
		[lifted_w,lifted_v] = np.linalg.eig(lifted_L)
		w = np.sort(w)
		lifted_w = np.sort(lifted_w)
		return(w,lifted_w)


	'''
	coarsen_demo( )
	- TAKES IN:
		-- N/A
	-RETURNS:
		-- N/A
		-- prints a test adjacency output
	'''
	def coarsen_demo( self ):
		print('COARSEN TEST ---')
		print('adjacency:')
		adj = np.array([[0,1,1,0,0,1],[1,0,1,0,0,0],[1,1,0,0,0,0],[0,0,0,0,1,1],[0,0,0,1,0,1],[1,0,0,1,1,0]])
		print(adj)
		print('')
		clusters = {0:np.array([0,1,2]),1:np.array([3,4,5])}
		coarse_adj = self.coarsen(clusters,adj)
		print('coarsened adjacency:')
		print(coarse_adj)
		print('')
		lifted_adj = self.lift(np.array([3,3]),coarse_adj)
		print('lifted adjacency:')
		print(lifted_adj)
		print('')
		w_norm,lifted_w_norm = self.spec_diff(adj, lifted_adj, True)
		print('normalized spectral differences:')
		print(np.abs(np.sort(w_norm) - np.sort(lifted_w_norm)))
		print('')
		w,lifted_w = self.spec_diff(adj, lifted_adj, False)
		print('unnormalized spectral differences:')
		print(np.abs(np.sort(w) - np.sort(lifted_w)))
		print('')
		print('COARSEN TEST COMPLETE ---')
		plt.plot(np.sort(w_norm),'.')
		plt.plot(np.sort(lifted_w_norm),'.')
		#plt.show()

class Plotter:

	def __init__( self ):
		print('')

	'''
	three_scatter( numpy array data, string surface)
	- TAKES IN:
		-- a numpy array of data points "data".
		-- a name of a surface "surface"
	- RETURNS:
		-- a figure of the points as a scatter plot.
	'''
	def three_scatter_surf( self, data, surface ):
		fig = plt.figure()
		ax = Axes3D(fig)
		ax.scatter(data[0,:],data[1,:],data[2,:],c=data[2,:],cmap='coolwarm',alpha = 0.8)

		if surface == "bluffs":
			X = np.arange(0,2.1,0.1)
			Y = np.arange(0,1.1,0.1)
			X, Y = np.meshgrid(X,Y)
			Z = np.piecewise(X, [X>=1,X<=1],[lambda X: 1, lambda X: 0])
			ax.plot_surface(X,Y,Z,cmap='coolwarm',alpha=0.1)

		if surface == "cosine":
			X = np.arange(0,3.2,0.1)
			Y = np.arange(0,2.1,0.1)
			X, Y = np.meshgrid(X,Y)
			Z = np.cos(X)
			ax.plot_surface(X,Y,Z,cmap='coolwarm',alpha=0.1)

		if surface == "tanh":
			X = np.arange(0,2.1,0.1)
			Y = np.arange(0,0.6,0.1)
			X, Y = np.meshgrid(X,Y)
			Z = np.tanh(5*(X-1))
			ax.plot_surface(X,Y,Z,cmap='coolwarm',alpha=0.1)

		plt.axis('off')
		plt.grid(False)

	'''
	plot_adj( numpy array adj , numpy array data )
	- TAKES IN:
		-- a numpy array that is an adjacency matrix "adj".
		-- a numpy array of the locations of all the points "data".
	- RETURNS:
		-- a figure of the points plotted according to their weights.
	'''
	def plot_adj( self, adj, thickness, clusters ):
		adj = adj*thickness
		H = nx.from_numpy_matrix(np.diag(np.diag(adj)))
		G = nx.from_numpy_matrix(adj-np.diag(np.diag(adj)))

		fig = plt.figure()
		position = nx.circular_layout(G)
		edges = list(G.edges())

		off_widths = []
		diag_widths = []
		off_edges = []
		diag_edges = []

		for i in range(len(adj)):
			for j in range(len(adj)):
				if i == j:
					diag_edges.append((i,j))
					diag_widths.append(adj[i,j])
				elif i < j:
					off_edges.append((i,j))
					off_widths.append(adj[i,j])

		nx.draw_networkx_nodes(G, pos = position, nodelist = G.nodes(), node_size = 300, node_color = clusters, cmap='seismic')
		nx.draw_networkx_edges(G, position, edgelist = off_edges, width = np.exp(np.array(off_widths)), alpha = 0.8)
		nx.draw_networkx_edges(H, position, edgelist = diag_edges,width = np.exp(np.array(diag_widths)), alpha = 0.8)
