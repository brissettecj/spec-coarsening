/*--------------------------------------------------------------------------------------
Generally useful functions
--------------------------------------------------------------------------------------*/

/*
FUNCTION: cuda_realloc( void* a, void* b )
TAKES IN: A pointer to an array "old_arr", and two int64_t values, "old_size" and "new_size".
DOES: reallocates the array.
RETURNS: the address of the new reallocated array.
*/
__device__ cuda_realloc_int64( int64_t* old_arr, int64_t old_size, int64_t new_size )
{
	int64_t* newArray;
	cudaMalloc( &newArray, new_size * sizeof( int64_t ) );

	for( int64_t i = 0; i < oldsize; i++ )
	{
		*( newArray + i ) = *( old_arr + i );
	}

	cudaFree( old_arr );
	return( newArray );
}
__device__ cuda_realloc_double( double* old_arr, int64_t old_size, int64_t new_size )
{
	double* newArray;
	cudaMalloc( &newArray, new_size * sizeof( double ) );

	for( int64_t i = 0; i < oldsize; i++ )
	{
		*( newArray + i ) = *( old_arr + i );
	}

	cudaFree( old_arr );
	return( newArray );
}

/*
FUNCTION: ptr_swap( void* a, void* b )
TAKES IN: Two arbitrary pointers, "a" and "b".
DOES: swaps the two pointers.
RETURNS: NONE
*/
__device__ void ptr_swap( void* a, void* b )
{
	void* temp = a;
	a = b;
	b = temp;
}

/*
FUNCTION: print_array( int64_t* array, int64_t length )
TAKES IN: int64_t array "array" and the length of said array, "length".
DOES: prints the array to the console.
RETURNS: NONE
*/
__device__ void print_int64_arr( int64_t* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%ld ",*(array + i) );
	}
	printf("\n");
}
__device__ void print_double_arr( double* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%lf ",*(array + i) );
	}
	printf("\n");
}

/*
FUNCTION: merge_arr( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: merges two arrays in a sorted manner. Subroutine for merge_sort.
RETURNS: NONE
*/
__device__ void merge_arr( int64_t* array , int64_t offset, int64_t end )
{
	int64_t midpoint = offset + (end - offset)/2;
	int64_t length_fh = midpoint - offset;
	int64_t length_sh = end - midpoint;

	int64_t* arr_fh, arrsh;
	cudaMalloc( &arr_fh, length_fh * sizeof( int64_t ) ); 
	//( int64_t* )malloc( length_fh * sizeof( int64_t ) );
	cudaMalloc( &arr_sh, length_sh * sizeof( int64_t ) );

	for( int64_t i = 0; i < length_fh; i++ )
	{
		*( arr_fh + i ) = *( array + ( offset + i ) );
	}
	for( int64_t i = 0; i < length_sh; i++ )
	{
		*( arr_sh + i ) = *( array + ( midpoint + i ) );
	}

	int64_t i = 0;
	int64_t j = 0;
	int64_t k = offset;

	while( i < length_fh && j < length_sh )
	{
		if( *(arr_fh + i) <= *(arr_sh + j) )
		{
			*( array + k ) = *( arr_fh + i );
			i = i + 1;
		}
		else
		{
			*( array + k ) = *( arr_sh + j );
			j = j + 1;
		}
		k = k + 1;
	}

	while( i < length_fh )
	{
		*( array + k ) = *( arr_fh + i );
		i = i + 1;
		k = k + 1;
	}

	while( j < length_sh )
	{
		*( array + k ) = *( arr_sh + j );
		j = j + 1;
		k = k + 1;
	}

	cudaFree( arr_fh );
	cudaFree( arr_sh );
}

/*
FUNCTION: merge_sort( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: sorts "array" it in ascending order using merge-sort.
RETURNS: NONE
*/
__device__ void merge_sort( int64_t* array , int64_t offset, int64_t end )
{
	if( offset < end )
	{
		int64_t midpoint = offset + ( end - offset )/2;

		merge_sort( array, offset, midpoint );
		merge_sort( array, midpoint + 1, end );

		merge_arr( array, offset, end );
	}
}