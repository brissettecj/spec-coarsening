#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "graph.h"

/*
FUNCTION: 
TAKES IN: 
DOES: 
RETURNS: 
*/

/*
FUNCTION: greedy_coarsen( graph* g, float epsilon, int64_t samples, int normalized )
TAKES IN: A graph object "g", a float "epsilon", an int64_t "samples" denoting how many
		  times we attempt to coarsen, and a binary integer (0,1) "normalized" denoting
		  if we are doing a normalized coarsening.
DOES: Performs a greedy coarsen on the graph g with the parameters above.
RETURNS: 
*/
int greedy_coarsen( graph* g, float epsilon, int64_t samples, int normalized )
{	
	#ifdef GREEDY_DEBUG
	printf( "IN GREDY_COARSEN... \n");
	#endif

	clock_t start, end;
	double cpu_usage;

	// keep track of our level of coarsening.
	int64_t k_level = 0;

	// First we seed our random number generator.
	srand( time( NULL ) );

	// Enter our loop for sampling.
	for( int64_t i = 0; i < samples; i++ )
	{
		if( i%100 == 0 )
		{
			printf( " >> On sample: %li\n ", i );
		}
		
		// Choose a random node and get its subsumer.
		int64_t from_node = ( int64_t )( rand() % (g->node_num) );
		from_node = get_k_root( g, from_node, k_level ); // O( log( k_level ) )

		// Get the degree of the node. 
		int64_t from_degree;

		#ifdef TIME 
		start = clock();
		#endif
		get_k_length( g, from_node, k_level, &from_degree ); // O( n )
		#ifdef TIME
		end = clock();
		cpu_usage = ((double)( end - start)) / CLOCKS_PER_SEC;
		if( i%100 == 0 )
		{
			printf( " >> Time used on first get_k_length(): %f\n ", cpu_usage );
		}
		#endif

		// Get neighbors, and weights of this from_node.
		int64_t* from_neighbors = ( int64_t* )malloc( from_degree * sizeof( int64_t ) );
		double* from_weights = ( double* )malloc( from_degree * sizeof( double ) );

		#ifdef TIME
		start = clock();
		#endif
		get_k_neighbors( g, from_node, k_level, from_degree, &from_neighbors, &from_weights ); // O( from_degree * log( k_level ) )
		#ifdef TIME
		end = clock();
		cpu_usage = ((double)( end - start)) / CLOCKS_PER_SEC;
		if( i%100 == 0 )
		{
			printf( " >> Time used on first get_k_neighbors(): %f\n ", cpu_usage );
		}
		#endif

		// If 'normalized' is flagged then compute the normalization factor.
		double from_weight_sum = 0;
		if( normalized == 1 )
		{
			for( int64_t j = 0; j < from_degree; j++ )
			{
				from_weight_sum += *( from_weights + j );
			}
			for( int64_t j = 0; j < from_degree; j++ )
			{
				*( from_weights + j ) = *( from_weights + j ) / from_weight_sum;
			}
		}

		#ifdef GREEDY_DEBUG
		printf( " NODE: %li SAMPLED... \n", from_node );
		#endif

		// Define the number of nodes in merge_nodes.
		int64_t merge_number = 1;
		// Initialize an array, merge_nodes to track nodes for merging.
		int64_t* merge_nodes = ( int64_t* )malloc( sizeof( int64_t ) );

		// Now loop through every neighbor,
		for( int64_t j = 0; j < from_degree; j++ )
		{
			// Define our to_node.
			int64_t to_node = *( from_neighbors + j );
			to_node = get_k_root( g, to_node, k_level ); // O( log( k_level ) )

			// Get the degree, neighbors, and weights of this to_node.
			int64_t to_degree;

			#ifdef TIME
			start = clock();
			#endif
			get_k_length( g, to_node, k_level, &to_degree );  // O( n )
			#ifdef TIME
			end = clock();
			cpu_usage = ((double)( end - start)) / CLOCKS_PER_SEC;
			if( i%100 == 0 )
			{
				printf( " >> >> Time used on second get_k_length(): %f\n ", cpu_usage );
			}
			#endif

			int64_t* to_neighbors = ( int64_t* )malloc( to_degree * sizeof( int64_t ) );
			double* to_weights = ( double* )malloc( to_degree* sizeof( double ) );
			#ifdef TIME
			start = clock();
			#endif
			get_k_neighbors( g, to_node, k_level, to_degree, &to_neighbors, &to_weights ); // O( to_degree * log( k_level ) )
			#ifdef TIME
			end = clock();
			cpu_usage = ((double)( end - start)) / CLOCKS_PER_SEC;
			if( i%100 == 0 )
			{
				printf( " >> >> Time used on second get_k_neighbors(): %f\n ", cpu_usage );
			}
			#endif

			// If 'normalized' is flagged then compute the normalization factor.
			// This will take O( to_degree ) to compute.
			double to_weight_sum = 0;
			if( normalized == 1 )
			{
				for( int64_t j = 0; j < to_degree; j++ )
				{
					to_weight_sum += *( to_weights + j );
				}
				for( int64_t j = 0; j < to_degree; j++ )
				{
					*( to_weights + j ) = *( to_weights + j ) / to_weight_sum;
				}
			}

			#ifdef GREEDY_DEBUG
			printf( " NEIGHBOR: %li PARAMS COMPUTED... \n", to_node );
			#endif
			
			// Define a difference vector for computing the L1 norm and populate it.
			// This takes O( from_degree + to_degree )
			double* diff_vect = ( double* )calloc( g->node_num, sizeof( double ) );
			for( int64_t k = 0; k < from_degree; k++ )
			{
				*( diff_vect + *( from_neighbors + k ) ) += *( from_weights + k  );
			}
			for( int64_t k = 0; k < to_degree; k++ )
			{	
				*( diff_vect + *( to_neighbors + k ) ) -= *( to_weights + k  );
			}

			// calculate the norm from this information.
			// This takes O( n ) <-- could be faster
			double norm = 0;
			for( int64_t k = 0; k < g->node_num; k++ )
			{
				norm += fabs( *( diff_vect + k ) );
			}

			#ifdef GREEDY_DEBUG
			printf( " NORM: %lf COMPUTED... \n", norm );
			#endif

			// If the norm is less than our epsilon parameter, put it in our merge_nodes array.
			if( norm < epsilon )
			{	
				#ifdef GREEDY_DEBUG
				printf( " NODE PASSED EPSILON CRITERIA... \n" );
				#endif

				*( merge_nodes + ( merge_number - 1 ) ) = to_node;

				merge_number += 1;
				merge_nodes = realloc( merge_nodes, merge_number*sizeof( int64_t ) );

				#ifdef GREEDY_DEBUG
				printf( " >> MERGE NODES ALLOCATED... \n" );
				#endif
			}

			#ifdef GREEDY_DEBUG
			printf( " FREEING MEMORY... \n" );
			#endif

			// Free dynamically allocated memory.
			free( to_neighbors );
			free( to_weights );
			free( diff_vect );

			#ifdef GREEDY_DEBUG
			printf( " >> MEMORY FREED... \n" );
			#endif
		}

		// If any of our nodes passed the merge criteria perform our merges.
		if( merge_number > 1 )
		{	
			#ifdef GREEDY_DEBUG
			printf( " NODES BEING MERGED... \n" );
			#endif

			// Perform our loop and merge all the nodes in our array to our 'from_node'.
			// Takes O( merge_number * n )
			for( int64_t j = 0; j < merge_number-1; j++ )
			{
				#ifdef GREEDY_DEBUG
				printf( " >> MERGING: %li TO NODE: %li ...\n", *( merge_nodes + j ), from_node );
				#endif

				merge( g, from_node, *( merge_nodes + j ), k_level + 1 ); // O( n )
			}

			// Increment our level of coarsening.
			k_level += 1;
		}

		// Free our dynamically allocated memory.
		free( from_neighbors );
		free( from_weights );
		free( merge_nodes );
	}

	printf( "GREEDY_COARSEN TERMINATING...\n");
}




