#include "graph.h"

int main( int argc, char** argv )
{
	FILE *fd;
	fd = fopen( argv[1], "r" );

	// Allocate graph parameters.
	int64_t nodes, edges;
	edges = 0;
	int64_t** adj_ptr = ( int64_t** )malloc( sizeof( int64_t* ) );
	double** weight_ptr = ( double** )malloc( sizeof( double* ) );

	read_graph( fd, &adj_ptr, &weight_ptr, &nodes, &edges );

	// Define our graph structure.
	int64_t adj_size = ( ( 3 * nodes ) + ( 2 * edges ) ) * sizeof( int64_t );
	int64_t weight_size = ( nodes + ( 2 * edges ) ) * sizeof( double );
	graph* g = ( graph* )malloc( ( 2 * sizeof( int64_t ) ) + adj_size + weight_size );
	g->node_num = nodes;
	g->edge_num = edges;
	g->adjacency = adj_ptr;
	g->weights = weight_ptr;

	print_graph( g );

	merge( g, 1, 3, 1 );
	merge( g, 3, 4, 2 );
	merge( g, 3, 7, 3 );
	merge( g, 1, 7, 4 );
	merge( g, 7, 1, 4 );

	int64_t* neighbors = ( int64_t* )malloc( sizeof( int64_t ) );
	double* neighbor_weights = ( double* )malloc( sizeof( double ) );
	int64_t neighbor_num; 

	printf( "\nGETTING NEIGHBORS AND WEIGHTS OF 1...\n");
	get_k_length( g, 1, 4, &neighbor_num );
	get_k_neighbors( g, 1, 4, &neighbors, &neighbor_weights );

	printf( "\nPRINTING ARRAYS...\n");
	print_int64_arr( neighbors, neighbor_num );
	print_double_arr( neighbor_weights, neighbor_num );

	print_graph( g );

	free_graph( g );

}