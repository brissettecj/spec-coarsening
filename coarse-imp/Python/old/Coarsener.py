import numpy as np
import scipy as sp
import networkx as nx

'''
FUNCTION:
TAKES IN:
DOES:
NOTES:
'''	

'''
FUNCTION: merge_nodes( networkx graph G, int node_a, int node_b )
TAKES IN: A networkx graph "G" and two integers "node_a" and "node_b" corresponding
		  to two nodes to merge.
DOES: Alters G to merge node_a and node_b.
NOTES: Need to choose node_a such that node_a < node_b. Also G must have weighted edges
       via parameter ['weight'] and the number of nodes coarsened into it ['num'].
       This works differently from "merge_nodes()" in the sense that this method directly alters G
       as opposed to creating a copy.
'''
def merge_nodes( G, node_a, node_b, verbose ):
	
	if verbose:
		print("entered: merge_nodes()...")
	if node_a == node_b:
		return(G)
		
	# Perform our coarsening.
	#-----------------------------------------------------
	neighbors_b = list(G.neighbors(node_b))
	static_neighbors_b = []
	for i in range(len(neighbors_b)):
		static_neighbors_b.append(neighbors_b[i])
	for neighbor in static_neighbors_b:
		if neighbor in G.neighbors(node_a):
			G[node_a][neighbor]["weight"] += G[node_b][neighbor]["weight"]
		else:
			G.add_edge(node_a,neighbor,weight = G[node_b][neighbor]["weight"])
		G.remove_edge(node_b,neighbor)
	numerator_x = (G.nodes[node_a]["num"]*G.nodes[node_a]["X"]+G.nodes[node_b]["num"]*G.nodes[node_b]["X"])
	numerator_y = (G.nodes[node_a]["num"]*G.nodes[node_a]["Y"]+G.nodes[node_b]["num"]*G.nodes[node_b]["Y"])
	denom = G.nodes[node_a]["num"] + G.nodes[node_b]["num"]
	G.nodes[node_a]["X"] = numerator_x / denom
	G.nodes[node_b]["Y"] = numerator_y / denom
	G.nodes[node_a]["num"] += G.nodes[node_b]["num"] 
	G.remove_node(node_b)
	new_nodes = list(G.nodes())
	if verbose:
		print("nodes:")
		print(new_nodes)
	#-----------------------------------------------------

	return(G)

'''
FUNCTION: neighbor_search( networkx graph G, int node, float threshold )
TAKES IN: A networkx graph "G", an integer corresponding to a node; "node", and a float
		  denoting our epsilon threshold.
DOES: looks through the neighbors of a node for a node which can be merged and
	  will yield a spectral difference within the value "threshold". Returns a node
	  to merge. (Note that we stop at the first node that satisfies our condition).
	  Returns <node> if no neighbors meet our criteria.
NOTES: G must have weighted edges via parameter ['weight'].
'''
def neighbor_search( G, node, threshold ):

	neighbors = list( G.neighbors( node ) ) 
	node_weights = np.zeros( len( G.nodes() ) )
	index = 0
	for u in neighbors:
		node_weights[index] = G[node][u][ "weight" ]
		index += 1
	for vert in neighbors:
		vert_weights = np.zeros( len( G.nodes() ) )
		vert_neighbors = list( G.neighbors( vert ) )
		index = 0
		for v in vert_neighbors:
			vert_weights[index] = G[vert][v][ "weight" ]
			index += 1
		if np.linalg.norm( (node_weights - vert_weights), 1 ) < threshold:
			return( vert )
	return( node )

'''
FUNCTION: neighbor_search_fast( networkx graph G, int node, int samples, boolean normalized, float threshold )
TAKES IN: A networkx graph "G", an integer corresponding to a node; "node", and a float
		  denoting our epsilon threshold, and a number of samples to take "samples". Additionally takes in
		  a boolean "normalized" that controls how we find our new nodes to merge.
DOES: looks through a sampled set of neighbors of a node for a node which can be merged and
	  will yield a spectral difference within the value "threshold". Returns a node
	  to merge. (Note that we stop at the first node that satisfies our condition).
	  Returns <node> if no neighbors meet our criteria.
NOTES: G must have weighted edges via parameter ['weight'].
'''
def neighbor_search_fast( G, node, samples, normalized, threshold ):

	nodes = np.random.choice( list( G.neighbors( node ) ), samples, replace=False )
	weights = np.array( [ G[node][u]["weight"] for u in nodes ] )
	neighbors = np.array( list( G.neighbors( node ) ) )
	choice_neighbors = [ node for _, node in sorted( zip( weights, nodes ) ) ]

	node_weights = np.zeros( len( list(G.nodes()) ) )
	index = 0
	for u in neighbors:
		node_weights[np.where(neighbors==u)] = G[node][u][ "weight" ]
		index += 1
	for vert in choice_neighbors:
		vert_weights = np.zeros( len( G.nodes() ) )
		vert_neighbors = np.array( list( G.neighbors( vert ) ) )
		index = 0
		for v in vert_neighbors:
			vert_weights[np.where(vert_neighbors==v)] = G[vert][v][ "weight" ]
			index += 1
		if normalized:
			node_val = (node_weights/G.degree(node,weight="weight"))
			vert_val = (vert_weights/G.degree(vert,weight="weight"))
		else:	
			node_val = node_weights
			vert_val = vert_weights
		if np.linalg.norm( ( node_val - vert_val ), 1 ) < threshold:
			return( vert )
	return( node )

'''
FUNCTION: greedy_coarsen( networkx graph G, float threshold )
TAKES IN: A networkx graph "G", and a float "threshold" denoting our epsilon bound.
		  additionally this takes in a number of rounds of coarsening to attempt.
DOES: Coarsens as many nodes as possible within the threshold. It does this by
	  choosing a node at random and attempting to merge it to a neighbor until 
	  it cannot be done any more.
NOTES: greedy_coarsen() chooses nodes at random which can lead to some cycles not doing anything. 
'''
def greedy_coarsen( G, threshold, rounds, verbose ):

	H = G.copy()
	for level in range( rounds ):
		nodes = list(H.nodes())
		node = np.random.choice( nodes )
		neighbor = neighbor_search( H, node, threshold ) #<----- this seems like what may be our slow down
		if neighbor != node:
			arr = np.array([node,neighbor])
			arr = np.sort( arr )
			H = merge_nodes( H, arr[0], arr[1], False)
		if verbose and (level%10 == 0):
			print("current coarsening level: "+str(level))
	return( H )

'''
FUNCTION: greedy_coarsen_fast( networkx graph G, float threshold, int rounds, int samples )
TAKES IN: A networkx graph "G", and a float "threshold" denoting our epsilon bound.
	   additionally this takes in a number of rounds of coarsening to attempt "rounds".
	   It also takes in the number of samples to consider "samples", and a boolean, "verbose".
DOES: Coarsens as many nodes as possible within the threshold. It does this by
	  choosing a node at random and attempting to merge it to a set of random neighbors until 
	  it cannot be done any more.
NOTES: greedy_coarsen_fast() chooses nodes at random which can lead to some cycles not doing anything. 
'''
def greedy_coarsen_fast( G, threshold, rounds, samples, normalized, verbose ):

	H = G.copy()
	for level in range( rounds ):
		nodes = list(H.nodes())
		node = np.random.choice( nodes )
		neighbor_len = len( list( H.neighbors( node ) ) )
		if samples < neighbor_len:
			neighbor = neighbor_search_fast( H, node, samples, normalized, threshold )
		else:
			neighbor = neighbor_search_fast( H, node, neighbor_len, normalized, threshold )
		if neighbor != node:
			arr = np.array([node,neighbor])
			arr = np.sort( arr )
			H = merge_nodes( H, arr[0], arr[1], False )
		if verbose and (level%10 == 0):
			print("current coarsening level: "+str(level))
	return( H )

'''
FUNCTION: random_coarsen( networkx graph G, int rounds, int samples )
TAKES IN: A networkx graph "G", 
	   additionally this takes in a number of rounds of coarsening to attempt "rounds".
	   It also takes in a boolean, "verbose".
DOES: Coarsens random pairs of nodes.
NOTES: 
'''
def random_coarsen( G, rounds, verbose ):

	H = G.copy()
	for level in range( rounds ):
		nodes = list(H.nodes())
		node1 = np.random.choice( nodes )
		node2 = np.random.choice( nodes )
		if node1 != node2:
			arr = np.array([node1,node2])
			arr = np.sort( arr )
			H = merge_nodes( H, arr[0], arr[1], False)
		if verbose and (level%10 == 0):
			print("current coarsening level: "+str(level))
	return( H )

'''
FUNCTION: eigen_embeddings( np_array eig_vects )
TAKES IN: An array of eigenvectors.
DOES: Determines nodal-domain based embeddings for nodes.
RETURNS: A dictionary of numpy arrays where each numpy array has all the nodes in a cluster
NOTES:  
'''
def eigen_embeddings( eig_vects, verbose ):
	nodes = len(eig_vects[0,:].transpose())
	power = len(eig_vects[:,0])
	if verbose:
		print("eigen_embeddings...")
		print("nodes: "+str(nodes))
		print("power: "+str(power))
	embeddings = np.sign(eig_vects)
	embedding_dict = {}
	for i in range(2**power):
		embed_arr = np.array( list(np.binary_repr(i).zfill(power))).astype(int)
		embed_arr = np.where( embed_arr == 0, -1, embed_arr )
		temp_arr = []
		for j in range(nodes):
			if (embed_arr == (embeddings[:,j].flatten())).all(): #<--- this could be super wrong.
				temp_arr.append(j)
		embedding_dict[i] = np.array(temp_arr)
	return(embedding_dict)

'''
FUNCTION: merge_many( nx graph G, np array cluster )
TAKES IN: Our networkx graph G, and an array of nodes to cluster.
DOES: Alters G in a way to merge all the nodes in the numpy array.
RETURNS: An altered G.
NOTES:  
'''
def merge_many( G, cluster, verbose ):
	H = G.copy()
	cluster_size = len(cluster)
	sorted_cluster = np.sort(cluster)
	node_a = sorted_cluster[0]
	for node_b in sorted_cluster:
		if verbose:
			print("merge_many...")
			print("node_a: "+str(node_a))
			print("node_b: "+str(node_b))
		H = merge_nodes( H, node_a, node_b, verbose )
	return(H)

'''
FUNCTION: spectral_coarsen( networkx graph G, int evals )
TAKES IN: A networkx graph "G", and an integer number of eigenvalues to use.
DOES: Coarsens the graph according to overlapping nodal domains.
NOTES:  
'''
def spectral_coarsen( G, evals, verbose ):

	H = G.copy()
	L = nx.linalg.laplacianmatrix.laplacian_matrix(H,weight="weight").todense()
	w,v = np.linalg.eigh(L)

	eig_vects = v[1:evals+1,:]
	coarsen_dict = eigen_embeddings(eig_vects, verbose)
	
	clusters = []
	for i in range(len(list(coarsen_dict.keys()))):
		if len(coarsen_dict[i]) > 0:
			clusters.append(i)

	for clust in clusters:
		H = merge_many( H, coarsen_dict[clust], verbose ) 

	return( H )


'''
FUNCTION: group_coarsen( networkx graph G, float epsilon, int attemps, boolean verbose ):
TAKES IN: a networkx graph "G", a float "epsilon" that controls our threshold for
	   coarsening, also takes in a number of attemps "attempts" controlling the 
	   number of times we attempt to coarsen nodes, and a boolean variable 
	   "verbose" controlling print statements.
DOES: performs a greedy coarsening, coarsening many nodes at a time.
NOTES:
'''
def group_coarsen( G, epsilon, attempts, normalized, verbose ):
	if verbose: print( "IN group_coarsen()...")

	H = G.copy()
	A = nx.adjacency_matrix( H ).todense()
	nodes = list( H.nodes() ) 
	arr_len = np.max( nodes ) + 1

	for a in range( attempts ):

		if len( list( nodes ) ) < 2:
			return( H )

		u = np.random.choice( nodes, 1, replace=False )[0]

		if verbose: print( "node chosen: " + str( u ) )

		node_list = []
		
		A_u = np.zeros( arr_len )
		u_sum = 0
		for i in H.neighbors(u):
			A_u[i] = H[u][i]["weight"]
			u_sum += A_u[i]
		if normalized:
			A_u = A_u / u_sum
		for node in nodes:
			A_node = np.zeros( arr_len )
			node_sum = 0
			for i in H.neighbors(node):
				A_node[i] = H[node][i]["weight"]
				node_sum += A_node[i]
			if normalized:
				A_node = A_node / node_sum
			diff_vect = A_u - A_node
			#diff_vect = A[u,:] - A[node,:]
			diff = np.linalg.norm( diff_vect, 1 )
			if diff < epsilon:
				if verbose: 
					print("		node: " + str( node ) + " added for coarsening.")
					print("		-- adj difference: " + str( diff ) )
				node_list.append( node )

		for v in node_list:
			nodes = np.delete( nodes, np.argwhere( nodes == v ) )
			H = merge_nodes( H, u, v, False )

		if verbose: print( "nodes coarsened: " + str( len(node_list) ) )

	return( H )


'''
FUNCTION: multilevel_group_coarsen( networkx graph G, float low_epsilon, float high_epsilon, int attemps, int levels, boolean verbose ):
TAKES IN: a networkx graph "G", two floats "low_epsilon,high_epsilon" that controls our variable threshold for
	   coarsening, also takes in a number of attemps "attempts" controlling the number of times we attempt to coarsen nodes, 
	   and an int "levels" controlling how many levels we attempt coarsening, also takes a boolean variable "verbose" controlling 
	   print statements.
DOES: performs a series of greedy coarsenings, coarsening many nodes at a time. The threshold for coarsening is increased at each level. 
	  We also save the graph at every level. 
NOTES:
'''
def multilevel_group_coarsen( G, low_epsilon, attempts, levels, normalized, verbose ):
	if verbose: print( "IN multilevel_group_coarsen()..." )
	H_dict = {}
	H_dict[0] = G.copy()
	new_eps = low_epsilon
	for i in range( levels ):
		if verbose: print( "------------------------- running coarsen " + str(i) + "...")
		H_dict[i+1] = group_coarsen( H_dict[i], new_eps, attempts, normalized, False )
		if normalized:
			new_eps = new_eps + low_epsilon
		else:
			new_eps = 2*new_eps
	return( H_dict )

'''
FUNCTION: lift( networkx graph G, networkx graph P )
TAKES IN: A networkx graph "G" (coarsened) and its pre-coarsened counterpart "P".
DOES: Takes the ["num"] nodal parameter on G and creates a lifted graph H from it.
NOTES: G must have a nodal parameter ["num"].
'''
def lift( G, P, verbose ):

	H = nx.Graph()
	node_map = {}
	index = 0
	clust = 0

	# First we create a mapping from supernodes to lifted nodes.
	#-----------------------------------------------------
	for u in list(G.nodes()):
		num = G.nodes[ u ][ "num" ]
		node_set = np.arange( index, index+num )
		node_map[ u ] = node_set
		H.add_nodes_from( node_set )
		for node in node_set:
			H.nodes[ node ][ "num" ] = 1
			H.nodes[ node ][ "clust" ] = clust
			H.nodes[ node ][ "X" ] = P.nodes[ node ]["X"]
			H.nodes[ node ][ "Y" ] = P.nodes[ node ]["Y"]
		index += num
		clust += 1

	if verbose:
		print("IN LIFT: ")
		print("Number of retrieved nodes: "+str(index) )
		weights = []
		for node in G.nodes():
			weights.append( G.nodes[node][ "num" ] )
		print("Breakdown of subsumptions in G: "+str(weights) )
	#-----------------------------------------------------

	# Then we add all of our edges with associated weights.
	#-----------------------------------------------------
	edges = G.edges()
	for edge in edges:
		e0 = edge[0]
		e1 = edge[1]
		nodes0 = node_map[ e0 ]
		nodes1 = node_map[ e1 ]
		size0 = len( nodes0 )
		size1 = len( nodes1 )

		if e0 == e1 and size0 > 1 :
			for u in nodes0:
				for v in nodes1:
					H.add_edge( u, v, weight = ( G[ e0 ][ e1 ][ "weight" ]/( (size0*(size0)/2) ) ) )
		else:
			for u in nodes0:
				for v in nodes1:
					H.add_edge( u, v, weight = ( G[ e0 ][ e1 ][ "weight" ]/(size0*size1)) )
		


	#-----------------------------------------------------

	return( H )

'''
FUNCTION: lift_dict( dictionary graphs )
TAKES IN: A dictionary of networkx graphs
DOS: Computes the lift of each graph in the dictionary with respect to the graph at index 0.
NOTES: G must have a nodal parameter ["num"].
'''
def lift_dict( graphs, verbose ):
	K_dict = {}
	K_dict[0] = graphs[0]
	for i in range( len( graphs.keys() )-1 ):
		K_dict[ i + 1 ] = lift( graphs[ i + 1 ], graphs[ 0 ], verbose )
	return( K_dict )