import networkx as nx 
import numpy as np
import netCDF4
import matplotlib.pyplot as plt

'''
FUNCTION: 
TAKES IN: 
DOES: 
NOTES: 
'''

'''
FUNCTION: get_position( nx graph G, np array X, np array Y )
TAKES IN: a networkx graph "G", and two 1D numpy arrays "X" and "Y".
DOES: associates position with each node in "G"
NOTES: X and Y are initialized to [0.] unless otherwise defined. In this case
	   the positions will be decided by nx.springlayout().
'''	

def get_position( G, X = np.zeros(1), Y=np.zeros(1) ):
	H = G.copy()
	if len( X ) != len( Y ):
		print( "ERROR in <get_position()>: invalid parameters" )
	if len( X ) < len( list(G.nodes()) ):
		pos = nx.spring_layout( H ) 
		#pos = nx.circular_layout( H )
		#pos = nx.spectral_layout( H )
		for node in list( G.nodes() ):
			H.nodes[node]["X"] = pos[node][0]
			H.nodes[node]["Y"] = pos[node][1]
	else:
		for node in list( G.nodes() ):
			H.nodes[node]["X"] = X[node]
			H.nodes[node]["Y"] = Y[node]
	return( H )

'''
FUNCTION: load_exo( string filename, np array voltrange, string nodal_var, int mode )
TAKES IN: the name of an exodus file to be used "filename". Also takes in a list of 
		  integers "voltrange" as well as a string "nodal_var", and an integer "mode" 
		  which determines the connectivity of the graph.
DOES: Turns a .exo file into a networkx graph for coarsening.
NOTES: mode 0 is fully connected, 1 is original mesh.
'''
def load_exo( filename, voltrange=[0], nodal_var='vals_nod_var2', mode=0, verbose=False ):
	if verbose:
		print("load_exo...")

	nc = netCDF4.Dataset('./'+filename)
	X = np.array(nc.variables['coordx'])
	Y = np.array(nc.variables['coordy'])
	connect = nc.variables['connect1']
	if verbose:
		print("size: "+str(len(X)))

	weight = []
	for i in voltrange:
		#weight.append(np.array(nc.variables[nodal_var][:])[i])
		weight.append(np.array(nc.variables[nodal_var][:])[i])# - np.array(nc.variables[nodal_var][:])[0])
	weights = np.array(weight).transpose()

	G = nx.Graph()
	nodes = np.arange(len(weights)).astype(int)
	G.add_nodes_from(nodes)
	for i in nodes:
		G.nodes[i]["num"] = 1
	G = get_position( G, X, Y )

	if mode == 0:
		for i in nodes:
			if verbose and i%100 == 0:
				print("on node: ("+str(i)+")")
			for j in nodes:
				pos_i = np.array([X[i],Y[i]])
				pos_j = np.array([X[j],Y[j]])
				dist = np.linalg.norm( pos_i - pos_j )
				weight = 1/((np.linalg.norm(weights[i]-weights[j])) + 1 )
				#weight = np.exp(-np.linalg.norm(diff))
				G.add_edge(i,j,weight=weight)

	if mode == 1:
		for i in nodes:
			G.nodes[i]["X"] = X[i]
			G.nodes[i]["Y"] = Y[i]

		eps = 0.0001
		xy = np.array([X[:], Y[:]]).T
		mapping = {}
		neighbors = {key:[] for key in range(len(X))}
		for i in range(len(X[:])):
			mapping[tuple(xy[i])] = i
		for coords in xy[connect[:]-1]:
			for e in zip(coords, np.roll(coords,1,axis=0)):
				edge = [(0,0),(0,0)]
				edge[0] = tuple(e[0])
				edge[1] = tuple(e[1])
				if mapping[edge[1]] not in neighbors[mapping[edge[0]]]:
					neighbors[mapping[edge[0]]].append(mapping[edge[1]])
				if mapping[edge[0]] not in neighbors[mapping[edge[1]]]:
					neighbors[mapping[edge[1]]].append(mapping[edge[0]])

		inv_mapping = {mapping[key]:key for key in mapping.keys()}
		for node in range(len(X)):
			G.add_edge(node,node,weight = 1)
			for neighbor in sorted(neighbors[node]):
				diff = np.array(inv_mapping[node])-np.array(inv_mapping[neighbor])
				weight = 1/((np.linalg.norm(diff)) + eps )
				#weight = np.exp(-np.linalg.norm(diff))
				G.add_edge(node,neighbor,weight = weight)

	return( G )

'''
FUNCTION: random_sigma_connected( np array blocks, float threshold ) 
TAKES IN: A numpy array of integers "blocks", whose sum should add to the number of desired nodes.
		  Additionally takes in a parameter "threshold" denoting the maximum weight between blocks.
DOES: Generates a networkx graph that is random-sigma connected with between-block adjacencies of no 
	  vertex being greater than sigma_prop*delta where delta is the average degree of its own block.
	  Additionally this graph has block sizes given by the parameter "blocks".
NOTES: 
'''
def random_sigma_connected( blocks, threshold, verbose ):

	if verbose:
		print( "GENERATING SIGMA CONNECTED GRAPH" )

	size = np.sum( blocks )

	# First we generate a k-partite structure within the desired sigma L1 threshold.
	#----------------------------------------------------------------
	A = np.random.rand( size, size )
	A = np.matmul( A, A.transpose() )

	index = 0
	for block in blocks:
		A[ index:index+block, index:index+block ] = np.zeros( (block, block) )
		index = index + block

	row_sums = np.sum(A,axis=1)
	maximum = np.max( row_sums )
	A = ( threshold ) * ( A/maximum )

	if verbose:
		print( "K-partite adjacency-matrix (pre-scaling):" )
		print( A )

	#----------------------------------------------------------------

	# Next we fill in the in-block weights.
	#----------------------------------------------------------------
	index = 0
	B = np.zeros( (size, size) )
	for block in blocks:
		#b = np.random.rand( block, block )
		b = np.random.normal( 1, 0.1, size=( block, block ))
		b = np.matmul( b, b.transpose() )
		maximum = np.max( b )
		b = b / maximum
		b = np.identity( block ) + ( b - np.diag(np.diag(b)) )
		#b = np.ones( (block,block) )
		B[ index:index+block, index:index+block ] = b 
		index += block

	A = B + A 

	if verbose:
		print( "Full adjacency-matrix:" )
		print( A )
	#----------------------------------------------------------------

	H = nx.from_numpy_matrix( A )
			
	node = 0
	for block in blocks:
		for index in range( block ):
			H.nodes[ node ][ "num" ] = 1
			node += 1

	H = get_position( H )

	return( H )