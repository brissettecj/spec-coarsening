import sys
import numpy as np
import networkx as nx
#import matplotlib
#matplotlib.rc_params['text.usetex'] = True
import matplotlib.pyplot as plt

import Coarsener as coarse
import Plotter as plot
import Generator as gen
import netCDF4

val_diffs = []
spec_diffs = []
ratios = []


#threshold = 0.01
#w_epsilon = 0.1

bounds = {}
x_ind = {}
index = 0
num = 10000

for i in range( num ):

	if i%100 == 0:
		print( "currently performing coarsening number: " +str( i ) )

	threshold = np.random.uniform( low=0.001, high=1 )
	w_epsilon = np.random.uniform( low=0.1, high=1 )
	nombre = np.random.randint( 2, 5 )
	blocks = np.random.randint(5,10,size=nombre);
	G = gen.random_sigma_connected( blocks, threshold, False)
	pos = gen.get_position( G )

	size = len(list(G.nodes()))
	#print( size )

	av_deg = ( sum( [ G.degree( v, weight="weight" ) for v in list(G.nodes())] )/size )

	#print( "average degree "+str( av_deg ) )
	#print( "threshold "+str(threshold) )
	
	steps = np.random.randint( size )
	samples = 20
	#print(steps)

	#H = coarse.greedy_coarsen_fast( G, w_epsilon, steps, samples, False )
	H = coarse.random_coarsen( G, steps, False)

	K = coarse.lift( H, G, False )

	A1 = nx.adjacency_matrix( G ).todense()
	A2 = nx.adjacency_matrix( K ).todense()
	A_diff = np.max( np.abs( A1 - A2 ) )

	#wG = nx.linalg.spectrum.normalized_laplacian_spectrum(G, weight = 'weight')
	#wH = nx.linalg.spectrum.normalized_laplacian_spectrum(K, weight = 'weight')

	wG = nx.linalg.spectrum.laplacian_spectrum(G, weight = 'weight')
	wH = nx.linalg.spectrum.laplacian_spectrum(K, weight = 'weight')


	spec_diff = np.max( np.abs( wG - wH ) )

	val_diffs.append( A_diff )
	spec_diffs.append( spec_diff )

	delta = np.min( list( dict( G.degree( list( G.nodes() ), weight='weight' ) ).values() ) )
	clust_size = 0
	for node in K.nodes():
		if K.nodes[node]["num"] > clust_size:
			clust_size = K.nodes[node]["num"]

	q = 2*( spec_diff + 2*threshold ) / delta
	h_q = ( 3*q ) / ( 1 - (2*q) )
	bound = h_q * ( delta + ( 2 * h_q ) ) + ( ( ( 8 * ( h_q**2 ) ) + ( 4 * h_q ) ) / clust_size )

	ratios.append( A_diff / bound )
	#print("-----")
	#print( A_diff )
	#print( bound )

	#index += 1

#print( ratios )
plt.figure()
plt.scatter( spec_diffs, ratios, color='tab:red',s=10,alpha=0.8 )
plt.xlabel('max spectral difference',size=16)
plt.ylabel('max weight differece / upper bound',size=16)
plt.show()