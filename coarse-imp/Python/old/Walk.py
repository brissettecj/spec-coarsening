import numpy as np 
import networkx as nx 
from sklearn.cluster import KMeans

'''
FUNCTION: 
TAKES IN:
DOES:
RETURNS:
NOTES:
'''

'''
FUNCTION: walk_step( networkx graph G, node u )
TAKES IN: A networkx graph "G" that we are walking on and a node "u"
		  that we are walking from.
DOES: Given the graph and node we choose a random neighbor ( according to weight )
	  and jump to that node.
RETURNS: The node we are walking to.
NOTES:
'''
def walk_step( G, u, verbose = False ):

	deg = G.degree(u,weight="weight")

	if deg == 0:
		return( u )

	neighbors = list( G.neighbors(u) )
	probs = [ ((G[u][v]["weight"])/(deg)) for v in G.neighbors(u) ]
	if u in neighbors:
		probs[ np.argwhere( neighbors == u )[0,0] ] += 1 #<------- [0,0] for LFR for some reason
	else:
		probs.append( 1 )
		neighbors.append( u )

	probs = list( np.array(probs) / 2 )

	v = np.random.choice( neighbors, 1, probs )

	return( v )

'''
FUNCTION: arb_walk( networkx graph G, int samples, int walks, int steps )
TAKES IN: A networkx graph "G", a number of sample nodes "samples", a number of random
		  walks "walks", and a number of steps for each walk "steps".
DOES: Performs a set of random walks.
RETURNS: A dictionary of matrices storing the random walk information.
NOTES:
'''
def arb_walk( G, samples, walks, steps, verbose = False ):

	if verbose: 
		print( "IN arb_walk()..." )
		print( "samples: " +str( samples ) )
		print( "walks: " +str( walks ) )
		print( "steps: " +str( steps ) )

	node_num = len( list( G.nodes() ) )
	hubs = np.random.choice( list( G.nodes() ), samples, replace = False )
	ret_dict = {}

	for u in hubs:
		ret_dict[u] = np.zeros( ( steps, node_num ) )
		for walk in range( walks ):
			v = u
			for step in range( steps ):
				v = walk_step( G, v )[0]
				ret_dict[u][step,v] += 1
		ret_dict[u] = ret_dict[u] / walks
		
		if verbose: print( " finished hub: " + str( u ) )

	return( ret_dict )

'''
FUNCTION: get_sims( dict dict_RWM )
TAKES IN: A dictionary of numpy arrays "RWM" and a number of clusters "clusters"
DOES: Returns a matrix of nodal similarities
RETURNS: A matrix of nodal similarities
NOTES:
'''
def get_sims( dict_RWM, verbose = False ):

	if verbose: print( "IN get_sims()..." )

	samples = list( dict_RWM.keys() )
	nodes = len( dict_RWM[ list( dict_RWM.keys() )[ 0 ] ].transpose() )
	walk_len = len( dict_RWM[ list( dict_RWM.keys() )[ 0 ] ] )

	if verbose: 
		print( "Walk-length: "+str( walk_len ) )
		print( "Nodes: "+str( nodes ) )

	node_rw_vals = np.zeros( ( nodes, walk_len * nodes ) )
	for node in range( nodes ):
		index = 0
		for samp in samples:
			node_rw_vals[ node , index*walk_len : index*walk_len + walk_len ] = dict_RWM[ samp ][ : , node ]
			index += 1
	
	ret_mat = np.zeros( ( nodes, nodes ) )

	for node in range( nodes ):
		node_vec = node_rw_vals[ node, : ]
		for neighbor in range( nodes ): 
			neighbor_vec = node_rw_vals[ neighbor , :]
			norm = np.linalg.norm( node_vec, 2 ) * np.linalg.norm( neighbor_vec, 2 )
			ret_mat[ node, neighbor ] = np.dot( node_vec, neighbor_vec ) / norm
			diff_vect = node_vec - neighbor_vec
			#ret_mat[ node, neighbor ] = 1 / ( np.linalg.norm( diff_vect, 1 ) + 1 )**2
			#ret_mat[ node, neighbor ] = np.exp( - np.linalg.norm( diff_vect, 2 ) )
		if verbose: print( " finished node: " + str( node ) )

	return( ret_mat )

'''
FUNCTION: get_representatives( np array sim_mat, int clust_num, int samples )
TAKES IN: A numpy array "sim_mat", a number of clusters "clust_num", a number of samples ot take "samples".
DOES: Gets a representative for each of "clust_num" clusters.
RETURNS: Our list of representative nodes.
NOTES: This works in the following way. It takes a sample of nodes and places one randomly in the first 
	   bucket. Then we look for the node that has the lowest similarity to that node and place it in the 
	   next bucket. Then we repeat until all buckets have a representative, using the average similarity
	   as a comparison point. We then fill the rest of them 
'''
def get_representatives( sim_mat, clust_num, samples, verbose = False ):

	if verbose: print(" IN get_representatives()...")

	nodes = len( sim_mat )
	sample = np.random.choice( np.arange( nodes ), samples, replace = False ).astype(int)

	if verbose:
		print(" sample: " + str( sample ))

	X = sim_mat[ np.ix_( sample, sample ) ]
	kmeans = KMeans( n_clusters = clust_num, random_state = 0 ).fit( X )
	labels = kmeans.labels_
	print( labels )

	'''
	buckets = np.zeros( clust_num )
	buckets[ 0 ] = np.random.choice( sample, 1, replace = False ).astype(int)
	sample = np.delete( sample, np.argwhere( sample == buckets[0] ) )

	if verbose: print( " first node chosen: " + str( buckets[0] ) )
	index = 1

	while index < clust_num:
		next_node = 0
		low_sim = 999999999
		for samp in sample:

			av_sim = 0
			for i in range( index ):
				av_sim += sim_mat[ int(buckets[ i ]), samp ]
			av_sim = av_sim / index

			if av_sim < low_sim:
				low_sim = av_sim
				next_node = samp

		if verbose: print( " node " + str( index + 1 ) + " chosen: " + str( next_node ) )
		sample = np.delete( sample, np.argwhere( sample == next_node ) )
		buckets[ index ] = next_node
		index += 1
	'''

	buckets = {}

	for i in range( clust_num ):
		buckets[i] = list( sample[ np.argwhere( labels == i ).flatten() ]  )

	return( buckets )

'''
FUNCTION: greedy_partition( np array sims_mat, dictionary buckets )
TAKES IN: The similarity numpy matrix "sims_mat", Our filled out buckets "buckets"
DOES: Groups the nodes based on their similarity.
RETURNS: A dictionary of filled buckets.
NOTES:
'''
def greedy_partition( sim_mat, buckets, verbose = False ):
	clusters = len( buckets.keys() )
	nodes = np.arange( len( sim_mat ) )

	for bucket in buckets.keys():
		for node in buckets[ bucket ]:
			nodes = np.delete( nodes, np.argwhere( nodes == node ) )

	clusts = {}
	for bucket in range( clusters ):
		clusts[ bucket ] = []
		for node in buckets[ bucket ]:
			clusts[ bucket ].append( node )

	for node in nodes:
		sim_vect = np.zeros( clusters )
		for index in range( clusters ):
			sim_vect[ index ] = 0
			for element in buckets[ index ]:
				sim_vect[ index ] += sim_mat[ element, node ]
			sim_vect[ index ] = sim_vect[ index ] / len( buckets[ index ] )

		if verbose: print( "node: " + str( node ) + " similarities: " + str( sim_vect ) )

		if (sim_vect == np.zeros( clusters )).all():
			if verbose: print( "zero sims on node: "+str( node )+" ...skipping...")
		else:
			clusts[ np.argmax( sim_vect ) ].append( node )

	return( clusts )

