import reader

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''

pos_dat, con_dat = reader.read_raw( "./bunny_full/bunny.txt" )
points = reader.get_k_level_points( pos_dat, con_dat )
reader.k_level_to_vtk( points, "./out_bunny/raw" )

for i in range( 10 ):	
	pos_dat, con_dat = reader.read_from_text( "./bunny_full/bunny.txt", "./bunny_full/round"+str(i)+".txt" )
	
	points = reader.get_k_level_points( pos_dat, con_dat )
	reader.k_level_to_vtk( points, "./out_bunny_full/points"+str(i) )

	ownership = reader.get_k_ownership( pos_dat, con_dat, 100000 )
	reader.colors_to_vtk( pos_dat, "./out_bunny_full/colors"+str(i), ownership )

