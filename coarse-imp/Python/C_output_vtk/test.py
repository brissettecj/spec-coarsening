# -----------------------------------------------------------------
# Below is example code stolen from the internet for writing to VTK
# -----------------------------------------------------------------
'''
from pyevtk.hl import pointsToVTK
import numpy as np
npoints = 100
x = np.random.rand( npoints )
y = np.random.rand( npoints )
z = np.random.rand( npoints )
pressure = np.random.rand( npoints )
temp = np.random.rand( npoints )
pointsToVTK( "./points", x,y,z, data = { "temp:" : temp, "pressure" : pressure } )
'''

with open("./bunny/bunny.txt") as file:
	lines = file.readlines()

count = 0
pos_data = {}
for line in lines:
	curr_line = line.split()

	if count == 0:
		pos_data[ count ] = curr_line

	if count > 2:
		pos_data[ count - 2 ] = curr_line

	count = count + 1

	if (count - 2) > int( pos_data[0][0] ):
		break

with open("./bunny/round0.txt") as file:
	lines = file.readlines()

count = 0
connection_data = {}
for line in lines:
	curr_line = line.split()
	connection_data[ count ] = curr_line
	count = count + 1

	if (count - 1) > int( connection_data[0][0] ):
		break

print( connection_data )



