import numpy as np 
from pyevtk.hl import pointsToVTK

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''

'''
FUNCTION: read_raw( string position_filename )
INPUTS: Takes in the string "position_filename" to a stanford mesh.
DOES: Gets the position and connection data for a standford mesh.
RETURNS: the position data and connection data as dictionaries.
NOTES:
'''
def read_raw( position_filename ):
	with open(position_filename) as file:
		lines = file.readlines()

	count = 0
	second_count = 0
	pos_data = {}
	connection_data = {}
	for line in lines:
		curr_line = line.split()

		if count == 0:
			pos_data[ count ] = curr_line
			connection_data[ second_count ] = curr_line
			second_count = second_count + 1 

		if count > 2 and count <= int( pos_data[0][0] ) + 2:
			pos_data[ count - 2 ] = curr_line

		if count > int( pos_data[0][0] ) + 2:
			connection_data[ second_count ] = curr_line
			second_count = second_count + 1
		
		count = count + 1

	return( pos_data, connection_data )


'''
FUNCTION: read_from_txt( string position_filename, string coarsened_filename )
INPUTS: Takes in the paths to two files, the coarsened graph file "coarsened_filename"
	    and the original file holding nodal positions "position_filename".
DOES: Reads in the coarsened graph file from the path "filename" and creates
	  a numpy array of the graph.
RETURNS: Two dictionaries.
NOTES:
'''
def read_from_text( position_filename, coarsened_filename ):
	with open(position_filename) as file:
		lines = file.readlines()

	count = 0
	pos_data = {}
	for line in lines:
		curr_line = line.split()

		if count == 0:
			pos_data[ count ] = curr_line

		if count > 2:
			pos_data[ count - 2 ] = curr_line

		count = count + 1

		if (count - 2) > int( pos_data[0][0] ):
			break

	with open(coarsened_filename) as file:
		lines = file.readlines()

	count = 0
	connection_data = {}
	for line in lines:
		curr_line = line.split()
		connection_data[ count ] = curr_line
		count = count + 1

		if (count - 1) > int( connection_data[0][0] ):
			break

	return( pos_data, connection_data )

'''
FUNCTION: get_k_ownership( dictionary pos_data, dictionary connection_data, int k_level )
INPUTS: Takes in two dictionaries from either "read_from_txt()" or "read_raw()" as well as
		an int "k_level" determining the level of coarsening we want to recover.
DOES: Calculates the nodal ownership at the k_level'th level of coarsening.
RETURNS: A dictionary of nodes denoting the graph at the level of coarsening. 
		 -- Each entry is a node number in the original graph and the dictionary
		 	under it has all the nodes it has subsumed.
NOTES:
'''
def get_k_ownership( pos_data, connection_data, k_level ):
	init_nodes = int(pos_data[0][0])
	ownership = {}
	for i in range( init_nodes ):
		count = 0
		if int( connection_data[i+1][0] ) == 0 :
			curr_id = int( connection_data[i+1][1] )
			ownership[i] = {}
			while ( curr_id > 0 ) and ( curr_id // init_nodes < k_level ): 
				ownership[i][count] = ( curr_id % init_nodes ) + 1 
				count = count + 1
				curr_id = int( connection_data[ ( curr_id % init_nodes ) + 1 ][1] )
	return( ownership )


'''
FUNCTION: get_k_level_points( dictionary pos_data, dictionary connection_data )
INPUTS: Takes in a 2 dictionarties "pos_data" and "connection_data" that comes from read_from_text().
DOES: Forms the positional input needed to generate the VTK file as a dictionary.
RETURNS: A dictionary of locations of nodes.
NOTES:
'''
def get_k_level_points( pos_data, connection_data ):
	nodes = int(pos_data[0][0])
	positions = {}
	count = 0
	for i in range( nodes ):
		if int(connection_data[i+1][0]) == 0:
			positions[count] = [0,0,0]
			positions[count][0] = pos_data[i+1][0]
			positions[count][1] = pos_data[i+1][1]
			positions[count][2] = pos_data[i+1][2]
			count = count + 1
	print( "node number: "+str( len(positions) ) )
	return( positions ) 

'''
FUNCTION: k_level_to_vtk( dictionary positions, string out_filename )
INPUTS: A dictionary computed by get_k_level(), "positions".
		Also a string "out_filename" that determines where it prints to.
DOES: Turns the dictionary into a VTK
RETURNS: nothing
NOTES:
''' 
def k_level_to_vtk( positions, out_filename ):
	nodes = len( positions )
	x = np.zeros( nodes )
	y = np.zeros( nodes )
	z = np.zeros( nodes )
	for i in range( nodes ):
		x[i] = positions[i][0]
		y[i] = positions[i][1]
		z[i] = positions[i][2]
	
	pointsToVTK( out_filename, x, y, z )

'''
FUNCTION: colors_to_vtk( dictionary positions, string out_filename, dictionary ownership )
INPUTS: A dictionary computed by get_k_level(), "positions".
		Also a string "out_filename" that determines where it prints to.
		Also takes in the dictionary "ownership" from get_k_ownership().
DOES: Turns the dictionary into a colored VTK
RETURNS: nothing
NOTES:
''' 
def colors_to_vtk( positions, out_filename, ownership ):
	nodes = len( positions )
	x = np.zeros( nodes )
	y = np.zeros( nodes )
	z = np.zeros( nodes )
	for i in range( int( positions[0][0] ) ):
		x[i] = float( positions[i + 1][0] )
		y[i] = float( positions[i + 1][1] )
		z[i] = float( positions[i + 1][2] )

	owners = np.zeros( nodes )
	color = 0
	for key in ownership.keys():
		owners[ key ] = color
		for i in range( len( ownership[ key ] ) ):
			owners[ ownership[ key ][ i ] ] = color
		color += 1
	
	pointsToVTK( out_filename, x, y, z, data = { "ownership" : owners } )