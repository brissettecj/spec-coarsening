import sys
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

import Walk as walk
import Coarsener as coarse
import Plotter as plot
import Generator as gen
import netCDF4

threshold = 1
#blocks = np.array([10,25,36,100])
blocks = np.random.randint(10,200,size=4);
#print(np.sum(blocks))
#print(blocks)
G = gen.random_sigma_connected( blocks, threshold, True)
#pos = gen.get_position( G )

size = 300
#G = nx.generators.community.random_partition_graph( [200,150,100,100], 0.5, 0.05 )
#G = nx.generators.social.karate_club_graph()
#size = len( list( G.nodes() ) )
#G = nx.generators.community.LFR_benchmark_graph( size, 2, 1.5, 0.1, average_degree=4, min_community=100, seed=10 )
#G = nx.generators.lattice.hexagonal_lattice_graph( 10, 10 )
#G = nx.generators.lattice.grid_graph( dim = (size,size) )

mapping = {}
index = 0
for node in G.nodes():
	mapping[ node ] = index
	index += 1 
G = nx.relabel_nodes( G, mapping )

G = gen.get_position( G )


print( len(list(G.edges())) )
for e in G.edges():
	G[e[0]][e[1]]["weight"] = 1 
for n in G.nodes():
	G.nodes[n]["num"] = 1

'''
plt.figure(1)
ax = plt.subplot()
im = ax.imshow( nx.adjacency_matrix(G).todense(), cmap = 'viridis' )
ax.title.set_text( 'Proportional L1 adjacency error' )
plt.colorbar( im )
plt.show()
'''

#G = gen.load_exo('bjt.sg.Vb0p5.varyVc.exo',voltrange=[22],mode=0,verbose=True )
G = gen.load_exo('bjt.sg.Vb0p5.varyVc.exo',voltrange=np.arange(23),mode=1,verbose=True)

size = len(list(G.nodes()))
print( size )

av_deg = ( sum( [ G.degree( v, weight="weight" ) for v in list(G.nodes())] )/size )
print( "average degree "+str( av_deg ) )
print( "threshold "+str(threshold) )
steps = 10*size

walks = 100
clusters = 4
sample = 40#size // clusters
#diameter = nx.diameter( G )

#epsilon = 0.0046 #threshold*av_deg
#epsilon = 0.3*av_deg
#epsilon = 0.1*av_deg
epsilon = 0.2

''' RW STUFF 
ret_dict = walk.arb_walk( G, sample, walks, 5, True )
ret_mat = walk.get_sims( ret_dict, True )
buckets = walk.get_representatives( ret_mat, clusters, sample, True )
#print( buckets )
print( buckets )
clusts = walk.greedy_partition( ret_mat, buckets, True )
print( clusts )
H = G.copy( )
for clust in clusts.keys():
	H = coarse.merge_many( H, clusts[ clust ], False )
print( "Coarsened...")
'''
'''
for node in H.nodes():
	print( H.nodes[node]["X"] )
	print( H.nodes[node]["Y"] )
	print( " ")
'''


#H = coarse.greedy_coarsen( G, threshold, steps, True)
#H = coarse.greedy_coarsen_fast( G, epsilon, steps, sample, True)
#H = coarse.group_coarsen( G, epsilon, 200, True )
#H = coarse.spectral_coarsen( G, 9, False )
normalized = True
H_dict = coarse.multilevel_group_coarsen( G, epsilon, 1000, 10, normalized, True )
print( "calculating lifts..." )
K_dict = coarse.lift_dict( H_dict, False )
#K = coarse.lift( H, G, False )
A1 = nx.adjacency_matrix( G ).todense()
scale = np.max(np.max(A1))

for i in range( len( H_dict.keys() ) - 1 ):
	node_num = len(H_dict[i].nodes())
	print( "Nodes in H: "+str(len(H_dict[i].nodes())) )
	plot.show_graph( H_dict[i+1], size, scale, False, 2*i)
	plot.compare_spectrum( H_dict[0], K_dict[i+1], 2*(i+1), node_num )

plt.show()

'''
A1 = nx.adjacency_matrix( G ).todense()
A2 = nx.adjacency_matrix( K ).todense()
A_diff = np.abs( A1 - A2 )
#A = A_diff/A1
scale = np.max(np.max(A1))
#print( "DIAMETER of G: " + str( diameter ) )

print( "edges: ---------------" +str( len(G.edges()) ) )
plot.show_graph( G, size, scale, False,  1 )
print( "Nodes in G: "+str(len(G.nodes())) )
plot.show_graph( H, size, scale, False, 2 )
print( "Nodes in H: "+str(len(H.nodes())) )
plot.show_graph( K, size, scale, False, 3 )
print( "Nodes in K: "+str(len(K.nodes())) )

plt.figure(4)
ax = plt.subplot()
im = ax.imshow( A_diff, cmap = 'viridis' )
ax.title.set_text( 'Proportional L1 adjacency error' )
plt.colorbar( im )

plot.compare_spectrum( G, K, 5 )

plt.show()
'''
