import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import cm

'''
FUNCTION:
TAKES IN:
DOES:
NOTES:
'''

'''
FUNCTION: position( networkx graph G )
TAKES IN: A networkx graph "G"
DOES: determines the positions of the nodes in "G"
NOTES: Fails if nodes in G do not have an "X" and "Y" parameter
'''
def position( G ):
	pos = {}
	index = 0
	for node in G.nodes():
		pos[index] = np.array([G.nodes[node]["X"],G.nodes[node]["Y"]])
		index = index + 1
	return( pos )

'''
FUNCTION: show_graph( networkx graph G, float size, boolean "edges" int fig_num )
TAKES IN: A networkx graph "G" with weights, a normalizing constant "size", and
          a figure number "fig_num". "edges" controls if we plot edges.
DOES: Plots the graph in a circular layout with weighted edges.
NOTES: mode = 0 >> combinatorially complete , mode = 1 >> same connectivity as .exo
'''
def show_graph( G, size, scale, edges, fig_num ):
	G_size = len( G.nodes() )
	weights = np.array([G[u][v]["weight"] for u,v in G.edges()])
	max_weight = np.max(weights)

	nodes = list( G.nodes() )
	colors = np.zeros( len( nodes ) )
	index = 0
	if "clust" in list( G.nodes[ nodes[0] ].keys() ):
		for node in nodes:
			colors[ index ] = ( G.nodes[ node ][ "clust" ] )
			index += 1

	adj = nx.adjacency_matrix( G ).todense()
	H = nx.from_numpy_matrix(np.diag(np.diag(adj)))
	K = nx.from_numpy_matrix(adj-np.diag(np.diag(adj)))

	plt.figure(fig_num)
	pos = position( G )

	off_widths = []
	diag_widths = []
	off_edges = []
	diag_edges = []

	if edges:
		mapping = {}
		index = 0
		for node in G.nodes():
			mapping[ node ] = index
			index += 1

		#for i in range( len(adj) ):
		#	for j in range( len(adj) ):
		for (a,b) in list( G.edges() ):
			i = mapping[ a ]
			j = mapping[ b ]
			if i == j:
				diag_edges.append((i,j))
				diag_widths.append(adj[i,j])
			elif i < j or j < i:
				off_edges.append((i,j))
				off_widths.append(adj[i,j])

	weights_H = [(H[u][v][ 'weight' ]/(2*scale)) for u,v in list(H.edges())]
	weights_K = [(K[u][v][ 'weight' ]/(2*scale)) for u,v in list(K.edges())]
	
	nx.draw_networkx_nodes(H, pos = pos, nodelist = H.nodes(), node_size= 75*size/G_size, node_color=colors, cmap=cm.tab10 )
	#nx.draw_networkx_labels(H, pos = pos)
	if edges: nx.draw_networkx_edges(H, pos = pos, edgelist = off_edges, width = weights_H, alpha = 0.2)
	#nx.draw_networkx_edges(K, pos = pos, edgelist = diag_edges,width = weights_K, alpha = 0.2)


'''
FUNCTION: compare_spectrum( networkx graph G, networkx graph H, int fig_num )
TAKES IN: Two graphs, G and H, where H is smaller, or larger than the other.
          Additionally takes in a figure number.
DOES: Plots the difference in spectrum for the normalized laplacian and standard Laplacian.
NOTES:
'''
def compare_spectrum( G, H, fig_num, node_num = False ):
	fig,(ax1,ax2) = plt.subplots( 2, 1 )
	fig.subplots_adjust( hspace=0.5 )

	#fig, ax1 = plt.subplots()

	wG = nx.linalg.spectrum.normalized_laplacian_spectrum(G, weight = 'weight')
	wH = nx.linalg.spectrum.normalized_laplacian_spectrum(H, weight = 'weight')
	nodesG = len(wG)
	nodesH = len(wH)
	larger = nodesH
	for index in range( len(wH) ):
		if wH[index] > 1:
			larger = index
			break
	arr = np.zeros( nodesG )
	arr[0:larger] = wH[0:larger]
	arr[nodesG-(nodesH - larger):nodesG] = wH[larger:nodesH]
	arr[larger:nodesG-(nodesH - larger)] = np.ones( nodesG-(nodesH - larger)-larger )
	ax1.plot(wG,'o',color="red")
	ax1.plot(arr,'x',color="blue")
	if node_num == False:
		ax1.title.set_text( 'normalized Laplacian comparison' )
	else:
		ax1.title.set_text( 'normalized Laplacian comparison, nodes: ' + str( node_num ) )
	ax1.set_ylabel( 'eigenvalue' )
	ax1.set_xlabel( 'eigenvalue number' )
	prop_err1 = np.linalg.norm(wG-wH,2)/np.linalg.norm(wG,2)
	
	ax1.text(0.95,0.05,"prop error "+str(prop_err1),
			 horizontalalignment = "right",
			 verticalalignment = "bottom",
			 transform = ax1.transAxes)
	
	print("Proportional normalized spectral error:")
	print(prop_err1)

	
	wG = nx.linalg.spectrum.laplacian_spectrum(G, weight = 'weight')
	wH = nx.linalg.spectrum.laplacian_spectrum(H, weight = 'weight')
	nodesG = len(wG)
	nodesH = len(wH)
	larger = nodesH
	for index in range( len(wH) ):
		if wH[index] > 1:
			larger = index
			break
	arr = np.zeros( nodesG )
	arr[0:larger] = wH[0:larger]
	arr[nodesG-(nodesH - larger):nodesG] = wH[larger:nodesH]
	arr[larger:nodesG-(nodesH - larger)] = np.ones( nodesG-(nodesH - larger)-larger )
	ax2.plot(wG,'o',color="red")
	ax2.plot(wH,'x',color="blue")
	if node_num == False:
		ax2.title.set_text( 'Laplacian comparison' )
	else:
		ax2.title.set_text( 'Laplacian comparison, nodes: ' + str( node_num ) )
	ax2.set_ylabel( 'eigenvalue' )
	ax2.set_xlabel( 'eigenvalue number' )
	prop_err2 = np.linalg.norm(wG-wH,2)/np.linalg.norm(wG,2)
	'''
	'''
	ax2.text(0.95,0.05,"prop error "+str(prop_err2),
			 horizontalalignment = "right",
			 verticalalignment = "bottom",
			 transform = ax2.transAxes)
	'''
	'''
	print("Proportional unnormalized spectral error:")
	print(prop_err2)
	