#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>

#include "CoarsenAlgs.h"
#include "graph.h"

int main( int argc, char** argv )
{
	printf( "IN main()... filename: %s\n", argv[1] );

	graph* g = ( graph* )malloc( ( 2 * sizeof( int64_t ) ) + 2*sizeof( double** ) + sizeof( int64_t** ) );

	// Allocate our 'nodes' and 'edges' pointers.
	int64_t nodes, edges;

	// Loop through populating files.
	for( int i = 0; i < 10; i++ )
	{
		printf( " ROUND: %d\n", i );
		
		nodes = 0;
		edges = 0;

		FILE *fd;
		fd = fopen( argv[1], "r" );

		// Allocate our 'graph_ptr' double arrays.
		int64_t** adj_ptr = ( int64_t** )malloc( sizeof( int64_t* ) );
		double** weight_ptr = ( double** )malloc( sizeof( double* ) );
		double** pos_ptr = ( double** )malloc( sizeof( double* ) );

		printf( "IN main()... adj_ptr and weight_ptr allocated...\n" );

		// Populate it with our file.
		read_stanford_mesh( fd, &adj_ptr, &weight_ptr, &pos_ptr, &nodes, &edges );

		printf( " Allocating graph memory...\n ");

		// Define our graph structure.
		//int64_t adj_size = ( ( 3 * nodes ) + ( 2 * edges ) ) * sizeof( int64_t );
		//int64_t weight_size = ( nodes + ( 2 * edges ) ) * sizeof( double );
		//graph* g = ( graph* )malloc( ( 2 * sizeof( int64_t ) ) + adj_size + weight_size );
		g->node_num = nodes;
		g->edge_num = edges;
		g->adjacency = adj_ptr;
		g->weights = weight_ptr;
		g->positions = pos_ptr;

		printf( "IN main()... graph object populated...\n" );
		printf( "	coarsening...\n" );
		//print_graph( g );

		//greedy_coarsen( g, 1.6, 100000, 1 );
		greedy_coarsen_fullcon( g, 0.1, 20, 1 );

		printf( "	writing graph...\n" );
		char filename[1024];
		sprintf( filename, "bunny_coarsen_full/round%d.txt", i);
		write_graph( filename, g );
		printf( "	graph written...\n" );

		//free( g );
		
		for( int64_t i = 0; i < nodes; i++ )
		{
			free( *( adj_ptr + i ) );
			free( *( weight_ptr + i) );
			free( *( pos_ptr + i ) );
		}

		free( adj_ptr );
		free( weight_ptr );
		free( pos_ptr );
	}

	free( g );

}