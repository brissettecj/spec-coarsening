#ifndef GRAPH_H
#define GRAPH_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "helper.h"
/*
FUNCTION: 
TAKES IN: 
DOES: 
RETURNS: 
*/

struct graph_struct
{
	int64_t node_num;
	int64_t edge_num;
	double** positions;
	int64_t** adjacency;
	double** weights;
};

typedef struct graph_struct graph;

//----------------------------------- FUNCTIONS ---------------------------------------
//-------------------------------------------------------------------------------------
int64_t degree( graph* g, int64_t from_node_index );
double wdegree( graph* g, int64_t from_node_index );
double k_wdegree( graph* g, int64_t from_node_index, int64_t k_level );
int64_t out_node( graph* g, int64_t from_node_index, int64_t to_node_index );
double out_weight( graph* g, int64_t from_node_index, int64_t to_node_index );
int64_t interp_level( int64_t value, int64_t nodes );
int64_t interp_node( int64_t value, int64_t nodes );
int64_t get_k_root( graph* g, int64_t node_b, int64_t k_level );
int64_t get_k_leaf( graph* g, int64_t node_b, int64_t k_level );
int get_k_length( graph* g, int64_t node_a, int64_t k_level, int64_t* neighbor_num );
int get_k_neighbors( graph* g, int64_t node_a, int64_t k_level, int64_t neighbor_num, int64_t** neighbors_arr, double** weight_arr );
int merge( graph* g, int64_t node_a, int64_t node_b, int64_t k_level );
void print_graph( graph* g );
int read_graph( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges );
int read_stanford_mesh( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, double*** pos_ptr, int64_t* nodes, int64_t* edges );
int read_stanford_full( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, double*** pos_ptr, int64_t* nodes, int64_t* edges );
void write_graph( char* filename, graph* g );
//-------------------------------------------------------------------------------------

/* ------------------------------------------------------------------------------------
// Now we define basic graph functions we have access to.
-------------------------------------------------------------------------------------*/

/*
FUNCTION: degree( graph* g, int64_t from_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index"
DOES: determines the number of outgoing connections from that node.
RETURNS: the number of outgoing connections.
*/
int64_t degree( graph* g, int64_t from_node_index )
{
	return( *(*((g->adjacency) + (from_node_index) ) + 2) );
}

/*
FUNCTION: wdegree( graph* g, int64_t from_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index"
DOES: determines the total weight-sum of outgoing connections from a node.
RETURNS: the weight sum of outgoing connections from a node.
*/
double wdegree( graph* g, int64_t from_node_index )
{
	int64_t cons = degree( g, from_node_index );
	double value = 0;
	for( int64_t i = 0; i < cons; i++ )
	{
		value = value + *(*((g->weights) + (from_node_index)) + i + 1 );
	}
	return( value );
}

/*
FUNCTION: k_wdegree( graph* g, int64_t from_node_index, int64_t k_level )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index"
		  it also takes in the level of corsening "k_level".
DOES: determines the total weight-sum of outgoing connections from a node at the kth
	  level of coarsening.
RETURNS: the weight sum of outgoing connections from a node at level k.
*/
double k_wdegree( graph* g, int64_t from_node_index, int64_t k_level )
{	
	// get neighbor num.
	int64_t neighbor_num;
	get_k_length( g, from_node_index, k_level, &neighbor_num );

	// allocate memory for getting the weight and neighbors.
	int64_t* a = ( int64_t* )malloc( neighbor_num * sizeof( int64_t ) );
	double* w = ( double* )malloc( neighbor_num * sizeof( double ) );

	// fill the arrays.
	get_k_neighbors( g, from_node_index, k_level, neighbor_num, &a , &w );

	// compute our value.
	int64_t value = 0;
	for( int64_t i = 0; i < neighbor_num; i++ )
	{
		value = value + *( w + i );
	}

	free( a );
	free( w );

	return( value );

}

/*
FUNCTION: out_node( graph* g, int64_t from_node_index, int64_t to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the node at that index.
RETURNS: the node at index "to_node_index" within the adjacency of "from_node_index"
		 returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
int64_t out_node( graph* g, int64_t from_node_index, int64_t to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_node()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %ld , degree: %ld\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}
	return( *( *( g->adjacency + from_node_index) + (3 + to_node_index) ) );
}

/*
FUNCTION: out_weight( graph* g, int64_t from_node_index, int64_t to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the weight of the connection to the node at that index.
RETURNS: the weight of connection to the node node at index "to_node_index" within the adjacency 
		 of "from_node_index" returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
double out_weight( graph* g, int64_t from_node_index, int64_t to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_weight()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %ld , degree: %ld\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}

	return( *(*(g->weights + from_node_index) + (to_node_index + 1) ) );
} 

/* ------------------------------------------------------------------------------------
// Now we define coarsening functions that graphs have access to
-------------------------------------------------------------------------------------*/

/*
FUNCTION: interp_level( int64_t value, int64_t nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
		  also takes in the number of nodes "nodes".
DOES: determines what level of coarsening that identifier is associated with.
RETURNS: the level of coarsening.
*/
int64_t interp_level( int64_t value, int64_t nodes )
{
	#ifdef BIG_DEBUG_MODE
	printf( "IN <interp_level()>...\n");
	#endif
	int64_t level = 0;
	level = value / nodes;

	#ifdef BIG_DEBUG_MODE
	printf( "	completed: %li...\n", level );
	#endif

	return(level);
}

/*
FUNCTION: interp_node( int64_t value, int64_t nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
			 also takes in the number of nodes "nodes".
DOES: determines what node that 'level identifier' points to.
RETURNS: the associated node.
*/
int64_t interp_node( int64_t value, int64_t nodes )
{
	#ifdef BIG_DEBUG_MODE
	printf( "IN <interp_node()>...\n");
	#endif
	int64_t node = 0;
	node = value % nodes;

	#ifdef BIG_DEBUG_MODE
	printf( "	completed: %li...\n", node );
	#endif

	return(node);
}

/*
FUNCTION: get_k_root( graph* g, int64_t node_b, int64_t k_level )
TAKES IN: A graph struct, "g". A node "node_b". The level of coarsening we are concerned with.
DOES: Determines the node that an edge connecting to node_b in the original graph
	   connects to at the kth level of coarsening.
RETURNS: The node such an edge would connect to. Returns -1 on failure.
NOTES: "node_b" must be less than "g->node_num". This functions runtime is the same as the number
	   of hops it takes to get to our root node. This is at most O( log( k_level ) )
*/
int64_t get_k_root( graph* g, int64_t node_b, int64_t k_level )
{
	//printf("get_k_root\n");
	#ifdef BIG_DEBUG_MODE
		printf("IN <get_k_root> \n");
	#endif

	// rename parameters we need.
	int64_t** adj = g->adjacency;
	int64_t nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_root()> : invalid 'node_b' \n");
		return(-1);
	}

	// set level to be low as possible.
	int64_t level = 0;
	int64_t node = node_b;
	int flag = 0;

	// If our node has not been coarsened return the node.
	while( flag == 0 )
	{
		level = interp_level( *(*(adj + node)), nodes );
		if( level == 0 )
		{
			flag = 1;
		}
		else
		{
			node = interp_node( *(*(adj + node)), nodes );
		}
		#ifdef BIG_DEBUG_MODE
			printf("	node: %ld , level: %ld, k_level: %ld\n",node,level,k_level);
		#endif
	}

	#ifdef BIG_DEBUG_MODE
		printf("	completed <get_k_root()>... \n");
	#endif

	//printf("done with k_root...\n");
	return(node);
}

/*
FUNCTION: get_k_leaf( graph* g, int64_t node_b, int64_t k_level )
TAKES IN: a graph struct "g" and a node "node_b", also takes in the level of coarsening "k_level".
DOES: Determines the "last node" in the subsumption graph of node_b. 
RETURNS: The node at the end of the subsumption graph of node_b.
NOTES: "node_b" must be less than "g->node_num". The runtime here is bounded above by the number of nodes 
	   under any given root node. Depending on the graph this could be O( n ) <--- CAN WE DO THIS FASTER?
	   We can bound this value differently as O( maxdeg(g) ^ k_level ), but this isnt great. Instead we 
	   can do an average approximation and say this should be bounded by around O( avdeg(g) ^ k_level ).
*/
int64_t get_k_leaf( graph* g, int64_t node_b, int64_t k_level )
{
	//printf("get_k_leaf\n");
	#ifdef BIG_DEBUG_MODE
		printf("IN <get_k_leaf> \n");
	#endif

	int64_t** adj = g->adjacency;
	int64_t nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_leaf()> : invalid 'node_b' \n");
		return(-1);
	}

	int64_t level = nodes + 1;
	int64_t node = node_b;

	if( *(*(adj + node) + 1) == 0 ) { return(node); }
	else
	{ 
		while ( level > 0 && interp_level( *(*(adj + node) + 1), nodes ) < k_level )
		{
			node = interp_node( *(*(adj + node) + 1), nodes );
			level = interp_level( *(*(adj + node) + 1), nodes );
			#ifdef BIG_DEBUG_MODE
				printf("	node: %ld , level: %ld, k_level: %ld\n",node,level,k_level);
			#endif
		}
	}

	return(node);
}

/*
FUNCTION: get_k_length( graph* g, int64_t node_a, int64_t k_level, int64_t* neighbor_num )
TAKES IN: A graph pointer "g", the node we are concerned with "node_a", the level of coarsening 
		  we care about "k_level" and a pointer to an int64_t value "neighbor_num" which will be 
		  populated.
DOES:  Determines how many nodes node_a is adjacent to at the kth level of coarsening.
RETURNS: 0 on success, 1 on failure.
NOTES: Same runtime as get_k_leaf: O( n ), but realistically roughly O( avdeg ^ k_level )
*/
int get_k_length( graph* g, int64_t node_a, int64_t k_level, int64_t* neighbor_num )
{
	//printf("get_k_length\n");
	#ifdef BIG_DEBUG_MODE
		printf("IN <get_k_length>\n");
	#endif

	int64_t** adj = g->adjacency;
	int64_t nodes = g->node_num;

	int64_t value = 0;
	int64_t flag = 0;
	int64_t curr_node = node_a;
	while( flag < 1 )
	{
		#ifdef BIG_DEBUG_MODE
			printf("	curr_node: %ld , nodes: %ld\n",curr_node,nodes);
		#endif

		value = value + *(*(adj + curr_node) + 2);
		int64_t level = interp_level(*(*(adj + curr_node) + 1),nodes);
		if( level > k_level || level == 0 )
		{
			flag = 1;
			#ifdef BIG_DEBUG_MODE
				printf("	flagged \n");
			#endif
		}
		else
		{
			curr_node = interp_node(*(*(adj + curr_node) + 1),nodes);
		}
	}

	#ifdef BIG_DEBUG_MODE
		printf("	adj length: %ld \n",value);
	#endif
	
	*neighbor_num = value;
	return( 0 );
}

/*
FUNCTION: get_k_neighbors( graph* g, int64_t node_a, int64_t k_level, int64_t neighbor_num, int64_t* neighbors_arr, double* weight_arr )
TAKES IN: a graph pointer "g", the node we are computing for "node_a" level of coarsening we 
		  are concerned with "k_level" and an array we wish to populate "neighbors_arr".
		  Also populates the "weight_arr" as well. It also takes in the length of the arrays "length"
DOES: Fills an array with the neighbors of a node at the kth level of coarsening.
RETURNS: 
NOTES: THIS CAN PROBABLY MADE CONSIDERABLY FASTER. WILL REQUIRE WORKING WITH GREEDY COARSEN TO NOT MESS
	   STUFF UP.
*/
int get_k_neighbors( graph* g, int64_t node_a, int64_t k_level, int64_t neighbor_num, int64_t** neighbors_arr, double** weight_arr )
{
	int ret = 0;
	//int64_t neighbor_num;

	// get the number of neighbors.
	//ret = get_k_length( g, node_a, k_level, &neighbor_num ); // O( n )

	//printf("get_k_neighbors: %li\n", neighbor_num);

	// check if our node has no neighbors and do different stuff if so.
	if( neighbor_num < 1 )
	{
		*neighbors_arr = realloc( *neighbors_arr, sizeof( int64_t ) );
		*weight_arr = realloc( *weight_arr, sizeof( double ) );
		*( *neighbors_arr ) = node_a;
		*( *weight_arr ) = 0;
		return( 0 );
	}

	// realloc the size of neighbors_arr and weight_arr
	*neighbors_arr = realloc( *neighbors_arr, neighbor_num*sizeof( int64_t ) );
	*weight_arr = realloc( *weight_arr, neighbor_num*sizeof( double ) );

	#ifdef BIG_DEBUG_MODE
		printf("IN <get_k_neighbors>\n");
		printf("	NEIGHBORS: %li\n", neighbor_num );
	#endif 

	int64_t** adj = g->adjacency;
	int64_t nodes = g->node_num;

	int64_t value = 0;
	int64_t n_index = 0;
	int64_t level = 0;
	double temp_val = 0;

	// Get the root node of our node.
	int64_t curr_node = get_k_root( g, node_a, k_level ); // O( log( k_level ) )

	// while value is unflagged, loop.
	while( value < 1 )
	{
		// get the number of nodes in our current adjacency.
		int64_t adj_nodes = *(*(adj + curr_node) + 2);
		// loop through all the adjacent nodes.
		for( int64_t i = 0; i < adj_nodes; i++ ) 
		{
			#ifdef BIG_DEBUG_MODE
				printf("	from_node: %ld , to_node: %ld , all_nodes: %ld\n", curr_node,i,g->node_num);
			#endif

			// get the node we are pointing to at location i 
			int64_t node = out_node( g, curr_node, i ); // O( 1 )

			// set the first value of our neighbors array to be the root of that node.
			// note that this step creates self loops.
			*( *neighbors_arr + n_index ) = get_k_root( g, node, k_level ); // O( log( k_level ) )

			*( *weight_arr + n_index ) = out_weight( g, curr_node, i ); // O( 1 )

			// determine the level of coarsening we are going to.
			level = interp_level( *(*(adj + curr_node ) + 1), nodes ); // O( 1 )
			n_index += 1; 

			// if the level of coarsening in correct, exit the loop.
			if( *(*(adj + curr_node) + 1) == 0 || level > k_level ) 
			{ 
				value = 1; 
			}
		}

		// determine the next node.
		curr_node = interp_node( *(*( adj + curr_node ) + 1), nodes );
	}
	return(0);
}


/*
FUNCTION: merge( graph* g, int64_t node_a, int64_t node_b, int64_t k_level )
TAKES IN: pointer to graph struct "g", two nodes "node_a" and "node_b", and the level of coarsening
			 we care about, "k_level".
DOES: merges the two nodes. 
RETURNS: 0 on success and -1 on error.
NOTES: 
	-- Allows for merging of any node to any other node. I.E. it checks if node 
   	b has been subsumed by another node and then searches to connect that node to
   	node a.
	-- Here, node_b will be subsumed by node_a.
	-- Also "node_a" and "node_b" must be less than or equal to "g->node_num".
	-- Additionally it does a check to ensure we are not creating a subsumption loop.
*/
int merge( graph* g, int64_t node_a, int64_t node_b, int64_t k_level )
{
	//printf("merge\n");
	#ifdef BIG_DEBUG_MODE
		printf( "IN merge()... -----------------------------------------------\n");
		printf( " >> node_a: %li, node_b: %li, k_level: %li\n", node_a, node_b, k_level );  
	#endif

	int64_t nodes = g->node_num;

	int64_t node_b_root = get_k_root( g, node_b, k_level ); // O( log( k_level ) )
	
	#ifdef BIG_DEBUG_MODE
		printf("	getting nodes.\n");
	#endif

	int64_t node_a_leaf = get_k_leaf( g, node_a, k_level ); // O( n )
	int64_t node_a_root = get_k_root( g, node_a, k_level ); // O( log( k_level ) )

	#ifdef BIG_DEBUG_MODE
		printf("	leaf a: %ld , root a: %ld \n",node_a_leaf,node_a_root);
		printf("	root b: %ld \n",node_b_root);
	#endif

	// if node_b is the same as node_a in coarsening level k_level, then no merge happens.
	if( node_b_root == node_a_root )
	{
		#ifdef BIG_DEBUG_MODE
			printf("	matching roots, no merge... \n" );
		#endif
		return(0);
	}
	else
	{
		*(*((g->adjacency) + node_a_leaf) + 1) = ( nodes*k_level ) + node_b_root;
		**((g->adjacency) + node_b_root) = ( nodes*k_level ) + node_a_root;

		return(0);
	}
}


/* ------------------------------------------------------------------------------------
// Now we define generally useful functions that graphs have for debugging and whatnot
-------------------------------------------------------------------------------------*/

/*
FUNCTION: print_graph( graph* g )
TAKES IN: graph struct "g"
DOES: prints out the adjacencies and associated weights of each node.
RETURNS: NONE
*/
void print_graph( graph* g )
{
	printf("\n");
	for( int64_t i = 0; i < (g->node_num); i++ )
	{
		int64_t id_back = **(g->adjacency + i);
		int64_t id_forward = *(*(g->adjacency + i) + 1);
		int64_t id_deg = *(*(g->adjacency + i) + 2);

		printf("row (%ld) identifiers: %ld %ld %ld \n",i,id_back,id_forward,id_deg);

		int64_t arr_len = degree(g,i);
		printf("connect: ");

		for( int64_t j = 0; j < arr_len; j++ )
		{
			printf("%ld ", out_node( g, i, j ) );
		}
		printf("\n");

		printf("weights: ");
		for( int64_t j = 0; j < arr_len; j++ )
		{
			printf("%lf ", out_weight( g, i, j ) );
		}
		printf("\n");
	}
	printf("\n");
}

/*
FUNCTION: read_graph( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int64_t value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in the mesh from a .txt file.
RETURNS: The file should be set up such that the first entry is the number of nodes and the following entries are
		 the adjacency entries.
*/
int read_graph( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
{
	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... filling in nodes...\n" );
	#endif

	// fill in the value for nodes. 
	fscanf( fd, "%li", nodes );

	// initialize variables.
	int64_t buffsize = 8 * ( 2*(*nodes) + 3 );
	char line[buffsize];
	int64_t adj[buffsize];
	double wgt[buffsize];
	int64_t length,row,bytes_read,c;
	double d;
	char* line_ptr;

	row = 0;

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... nodes allocated: %li\n", *nodes );
	#endif

	// Allocate memory for the graph pointer. 
	*adj_ptr = realloc( *adj_ptr, *nodes * sizeof( int64_t ) );
	*weight_ptr = realloc( *weight_ptr, *nodes * sizeof( double ) );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// skip over the nodes row in the file.
	fgets( line, buffsize, fd );

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif

	for( int64_t i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;

		#ifdef DEBUG_MODE
			printf( "length: %li\n", buffsize );
			printf( "Row: %s\n", line );
		#endif

		// read in the first three parameters of the row.
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		#ifdef DEBUG_MODE
			printf( " >> First read: %li\n", c );
		#endif
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		#ifdef DEBUG_MODE
			printf( " >> Second read: %li\n", c );
		#endif
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		#ifdef DEBUG_MODE
			printf( " >> Third read: %li\n", c );
		#endif
		adj[length] = c;
		wgt[length-2] = c;
		length = length + 1;
		line_ptr += bytes_read;

		// read in the full rest of the array.
		while( sscanf( line_ptr, "%li %lf %ln", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			wgt[length - 2] = d;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		*( *adj_ptr + i ) = ( int64_t* )realloc( *( *adj_ptr + i ), length * sizeof( int64_t ) );
		*( *weight_ptr + i ) = ( double* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( double ) );

		// for each element in the array.
		for( int64_t j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	return(0);
}

/*
FUNCTION: read_standford_mesh( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int64_t value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in the mesh from a Stanford .ply file.
RETURNS: NONE
*/
int read_stanford_mesh( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, double*** pos_ptr, int64_t* nodes, int64_t* edges )
{
	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... filling in nodes...\n" );
	#endif

	// fill in the value for nodes. 
	fscanf( fd, "%li", nodes );

	// initialize variables.
	int64_t buffsize = 8 * ( 2*(*nodes) + 3 );
	#ifdef DEBUG_MODE
	printf( "	>> buff_size: %li\n", buffsize );
	#endif

	char* line = ( char* )malloc( buffsize * sizeof( char ) );
	int64_t* adj = ( int64_t* )malloc( buffsize * sizeof( int64_t ) );;
	double* wgt = ( double* )malloc( buffsize * sizeof( double ) );
	int64_t length,row,bytes_read,c;
	double d;
	char* line_ptr;
	row = 0;

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... nodes allocated: %li\n", *nodes );
	#endif

	// Allocate memory for the graph pointer. 
	*adj_ptr = realloc( *adj_ptr, *nodes * sizeof( int64_t* ) );
	*weight_ptr = realloc( *weight_ptr, *nodes * sizeof( double* ) );
	*pos_ptr = realloc( *pos_ptr, *nodes * sizeof( double* ) );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// skip over the positional entries in the file.
	for( int i = 0; i < 3; i++ )
	{
		fgets( line, buffsize, fd );
	}
	// read in positions.
	for( int64_t i = 0; i < (*nodes); i++ )
	{	
		*( *pos_ptr + i ) = ( double* )malloc( 3 * sizeof( double ) );
		fgets( line, buffsize, fd );
		line_ptr = line;
		sscanf( line_ptr, "%lf %lf %lf", ( *( *pos_ptr + i ) ), ( *( *pos_ptr + i ) + 1 ), ( *( *pos_ptr + i ) + 2 ) );
	}

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif

	for( int64_t i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;

		// read in the first three parameters of the row.
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		wgt[length - 2] = c;
		length = length + 1;
		line_ptr += bytes_read;

		// read in the full rest of the array.
		while( sscanf( line_ptr, "%li %lf %ln", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			wgt[length - 2] = d;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		//*( *adj_ptr + i ) = ( int64_t* )realloc( *( *adj_ptr + i ), length * sizeof( int64_t ) );
		//*( *weight_ptr + i ) = ( double* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( double ) );
		*( *adj_ptr + i ) = ( int64_t* )malloc(  length * sizeof( int64_t ) );
		*( *weight_ptr + i ) = ( double* )malloc( ( length - 2 ) * sizeof( double ) );

		// for each element in the array.
		for( int64_t j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	free( line );
	free( adj );
	free( wgt );

	return(0);
}

/*
FUNCTION: read_standford_full( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, double*** pos_ptr, int64_t* nodes, int64_t* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int64_t value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in a Stanford .ply file as a fully connected graph.
RETURNS: NONE
*/
int read_stanford_full( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, double*** pos_ptr, int64_t* nodes, int64_t* edges )
{
	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... filling in nodes...\n" );
	#endif

	// fill in the value for nodes. 
	fscanf( fd, "%li", nodes );

	// initialize variables.
	int64_t buffsize = 8 * ( 2*(*nodes) + 3 );
	#ifdef DEBUG_MODE
	printf( "	>> buff_size: %li\n", buffsize );
	#endif

	char* line = ( char* )malloc( buffsize * sizeof( char ) );
	int64_t* adj = ( int64_t* )malloc( buffsize * sizeof( int64_t ) );;
	double* wgt = ( double* )malloc( buffsize * sizeof( double ) );
	int64_t length,row,bytes_read,c;
	double d;
	char* line_ptr;
	row = 0;

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... nodes allocated: %li\n", *nodes );
	#endif

	// Allocate memory for the graph pointer. 
	*adj_ptr = realloc( *adj_ptr, *nodes * sizeof( int64_t* ) );
	*weight_ptr = realloc( *weight_ptr, *nodes * sizeof( double* ) );
	*pos_ptr = realloc( *pos_ptr, *nodes * sizeof( int64_t* ) ); 

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// skip over the positional entries in the file.
	for( int64_t i = 0; i < (*nodes + 3); i++ )
	{	
		*( pos_ptr + i ) = malloc( 3 * sizeof( double ) );
		fgets( line, buffsize, fd );
		line_ptr = line;
		sscanf( line_ptr, "%lf %lf %lf", ( *( *pos_ptr + i ) ), ( *( *pos_ptr + i ) + 1 ), ( *( *pos_ptr + i ) + 2 ) );
		//sscanf( line_ptr, "%lf %lf %lf%ln", *( *pos_ptr + i ) );
		//sscanf( line_ptr, "%lf %lf %lf%ln", *( *pos_ptr + i ) );
	}

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif

	for( int64_t i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;

		// read in the first three parameters of the row.
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		wgt[length - 2] = c;
		length = length + 1;
		line_ptr += bytes_read;

		// read in the full rest of the array.
		while( sscanf( line_ptr, "%li %lf %ln", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			wgt[length - 2] = d;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		//*( *adj_ptr + i ) = ( int64_t* )realloc( *( *adj_ptr + i ), length * sizeof( int64_t ) );
		//*( *weight_ptr + i ) = ( double* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( double ) );
		*( *adj_ptr + i ) = ( int64_t* )malloc(  length * sizeof( int64_t ) );
		*( *weight_ptr + i ) = ( double* )malloc( ( length - 2 ) * sizeof( double ) );

		// for each element in the array.
		for( int64_t j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	free( line );
	free( adj );
	free( wgt );

	return(0);
}

/*
FUNCTION: write_graph( char* filename, graph* g )
TAKES IN: A file name "filename" and a graph struct "g".
DOES: Writes it to a file
RETURNS: NONE
*/
void write_graph( char* filename, graph* g )
{
	FILE *new_fd;
	new_fd = fopen( filename, "w" );

	int64_t** adj = g->adjacency;
	double** wgt = g->weights;
	int64_t nodes = g-> node_num;

	fprintf( new_fd, "%li\n", nodes );
	// Write edges to adjancency file
	for( int64_t i = 0; i < nodes; i++ )
	{
		int64_t adjnum = degree( g, i );
		fprintf( new_fd, "%li %li %li", **( adj + i ), *( *( adj + i ) + 1 ), adjnum );
		for( int64_t j = 0; j < adjnum; j++ )
		{
			fprintf( new_fd, " %li %lf", *( *( adj + i ) + ( j + 3 ) ), *( *( wgt + i ) + ( j + 1 ) ) );
		}
		fprintf( new_fd, "\n" );
	}
	fclose( new_fd );
}

#endif