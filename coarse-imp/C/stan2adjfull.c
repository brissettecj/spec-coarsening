#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>

// Changes a stanford mesh to the associated fully weighted adjacency file.

int main( int argc, char** argv )
{
	// get our file to read from
	FILE *fd;
	fd = fopen( argv[1], "r" );

	// form our new file for positions
	FILE *new_fd;
	new_fd = fopen( argv[2], "w" );

	// determine the weight scheme
	// 1 is based on positions, 0 is simple.
	char* weight_scheme = argv[3];

	// Go through the beginning of the file copying
	// the number of nodes and triangles to the new file.
	// we also copy over the number of edges.

	char line[1024]; 

	int64_t nodes, triangles, edges;
	fscanf( fd, "%li", &nodes );
	fscanf( fd, "%li", &triangles );
	edges = ( 3 * triangles ) / 2;
	fgets( line, 1024, fd ); // skip to the next line

	#ifdef DEBUG_MODE
	printf( "nodes: %li\n", nodes );
	printf( "traingles: %li\n", triangles );
	printf( "edges: %li\n", edges );
	#endif

	// Perform our copy.
	fprintf( new_fd, "%li\n", nodes );
	fprintf( new_fd, "%li\n", edges );
	fprintf( new_fd, "%li\n", triangles );

	// Get positions.

	float pos_arr[3];
	float positions[nodes][3];

	for( int64_t i = 0; i < nodes; i++ )
	{
		fgets( line, 1024, fd );
		char* line_ptr = line;
		sscanf( line_ptr, "%f %f %f", pos_arr, (pos_arr + 1), (pos_arr + 2) );

		#ifdef DETAILED_DEBUG_MODE
		printf( "node_num: %li \n", i );
		printf( "line pointer: %s", line_ptr );
		printf( "pos_array: %f, %f, %f\n", *pos_arr, *(pos_arr + 1), *(pos_arr + 2) );
		printf( "\n" );
		#endif

		*( *(positions + i) ) = *pos_arr;
		*( *(positions + i) + 1 ) = *(pos_arr + 1);
		*( *(positions + i) + 2 ) = *(pos_arr + 2);

		fprintf( new_fd, "%f %f %f\n", *pos_arr, *(pos_arr + 1), *(pos_arr + 2) );
	}

	#ifdef DETAILED_DEBUG_MODE
	printf( "Done with positions...\n");
	#endif
	#ifdef DEBUG_MODE
	printf( "Done with positions...\n");
	#endif

	#ifdef DETAILED_DEBUG_MODE
	printf( "Allocating %li bytes...\n", nodes * sizeof( int64_t* ));
	#endif
	#ifdef DEBUG_MODE
	printf( "Allocating %li bytes...\n", nodes * sizeof( int64_t* ));
	#endif

	// we need to keep track of adjacencies
	// so we dynamically allocate and reallocate memory for it as we go.
	//int64_t** ret = ( int64_t** )malloc( nodes * sizeof( int64_t* ) );
	int64_t** adjacencies = ( int64_t** )malloc( nodes * sizeof( int64_t* ) );
	if( adjacencies == NULL )
	{	
		printf( "We have a whoopsie on our hands! -- ");
		printf( "Could not allocate memory. \n ");
	}

	int64_t* sizes = ( int64_t* )malloc( nodes * sizeof( int64_t ) );

	#ifdef DETAILED_DEBUG_MODE
	printf( "Allocating adjacency memory...\n");
	#endif
	#ifdef DEBUG_MODE
	printf( "Allocating adjacency memory...\n");
	#endif

	for( int64_t i = 0; i < nodes; i++ )
	{
		//*( sizes + i ) = nodes;
		*( adjacencies + i ) = ( int64_t* )malloc( nodes * sizeof(int64_t) );
	}

	#ifdef DEBUG_MODE
	printf( "Adjacency memory allocated...\n");
	printf( "Populating adjacency...\n" );
	#endif
	#ifdef DETAILED_DEBUG_MODE
	printf( "Adjacency memory allocated...\n");
	printf( "Populating adjacency...\n" );
	#endif

	// Get edges with weights and populate an array
	for( int64_t i = 0; i < nodes; i++ )
	{	
		for( int64_t j = 0; j < nodes; j++ )
		{
			*( *( adjacencies + i ) + j ) = j; 
		}
		
	}

	// Write edges to adjancency file
	for( int64_t i = 0; i < nodes; i++ )
	{
		int64_t adjnum = *( sizes + i );
		fprintf( new_fd, "%li %li %li", 0, 0, adjnum );
		for( int64_t j = 0; j < adjnum; j++ )
		{
			if( atoi( weight_scheme ) == 0 )
			{
				#ifdef DETAILED_DEBUG_MODE
				printf( "adjacency element: %li\n", *(*(adjacencies + i) + j) );
				#endif

				fprintf( new_fd, " %li %li", *( *( adjacencies + i ) + j ), 1 );
			}
			else
			{
				double epsilon = 0.0001;
				float weight = 0;
				weight = weight + fabs( *(*(positions + i)) - *(*(positions + *( *(adjacencies + i ) + j) ) ) );
				weight = weight + fabs( *(*(positions + i) + 1) - *(*(positions + *( *(adjacencies + i ) + j) + 1) ) );
				weight = weight + fabs( *(*(positions + i) + 2) - *(*(positions + *( *(adjacencies + i ) + j) + 2) ) );

				weight = 1 / ( weight + epsilon );
				fprintf( new_fd, " %li %f", *( *( adjacencies + i ) + j ), weight );
			}
		}
		fprintf( new_fd, "\n" );
	}
}