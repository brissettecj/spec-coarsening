import numpy as np
import matplotlib.pyplot as plt
import os
from reader import *

# Plots the accuracy and loss for a rundata dictionary. 
# Note that this rundata should come from reader.get_full_data( filename ).
def plot_experiment( rundata, filename=None, show=False  ):

	fig,(ax1,ax2) = plt.subplots(2,1,figsize=(10,10))

	ax1.plot( rundata["terp_timings"], rundata["mean_test_acc"], linewidth=2, color="red", label="test accuracy" )
	ax1.plot( rundata["terp_timings"], rundata["mean_train_acc"], linewidth=2, linestyle=':', color="grey", label="train accuracy" )
	ax1.fill_between( rundata["terp_timings"], rundata["mean_test_acc"]-rundata["std_test_acc"], rundata["mean_test_acc"]+rundata["std_test_acc"], color="red",alpha=0.2 )
	ax1.fill_between( rundata["terp_timings"], rundata["mean_train_acc"]-rundata["std_train_acc"], rundata["mean_train_acc"]+rundata["std_train_acc"], color="grey",alpha=0.2 )
	ax1.plot( rundata["terp_timings"], rundata["mean_bench_test_acc"], linewidth=2, color="black", label=rundata["optimizer"]+" test accuracy", linestyle="--" )
	ax1.fill_between( rundata["terp_timings"], rundata["mean_bench_test_acc"]-rundata["std_bench_test_acc"], rundata["mean_bench_test_acc"]+rundata["std_bench_test_acc"], color="black",alpha=0.2 )
	ax1.legend(fontsize=20,loc="lower right")

	ax2.plot( rundata["terp_timings"], rundata["mean_test_loss"], linewidth=2, color="red", label="test loss" )
	ax2.plot( rundata["terp_timings"], rundata["mean_train_loss"], linewidth=2, linestyle=':', color="grey", label="train loss" )
	ax2.fill_between( rundata["terp_timings"], rundata["mean_test_loss"]-rundata["std_test_loss"],rundata["mean_test_loss"]+rundata["std_test_loss"], color="red",alpha=0.2 )
	ax2.fill_between( rundata["terp_timings"], rundata["mean_train_loss"]-rundata["std_train_loss"], rundata["mean_train_loss"]+rundata["std_train_loss"], color="grey", alpha=0.2 )
	ax2.plot( rundata["terp_timings"], rundata["mean_bench_test_loss"], linewidth=2, color="black", label=rundata["optimizer"]+" test loss", linestyle="--" )
	ax2.fill_between( rundata["terp_timings"], rundata["mean_bench_test_loss"]-rundata["std_bench_test_loss"], rundata["mean_bench_test_loss"]+rundata["std_bench_test_loss"], color="black",alpha=0.2 )
	ax2.legend(fontsize=20,loc="upper right")
	
	ax1.set_ylabel("accuracy",fontsize=24)
	ax2.set_ylabel(rundata["loss"]+" loss",fontsize=24)
	ax2.set_xlabel("time (s)",fontsize=24)
	fig.tight_layout()

	if filename != None:
		plt.savefig(filename+".pdf",format="pdf",bbox_inches="tight")

	if show:
		plt.show()

files = os.listdir("./results")

name = "cora_nll_adam_1_128_0.0001_4_8_3_1000_5"+".npy"
rundata = get_full_data( "results/"+name )
plot_experiment( rundata, filename="./plots/"+name, show=True )

#for file in files:
#	rundata = get_full_data( "results/"+file )
#	plot_experiment( rundata, filename="./plots/"+file, show=False )
