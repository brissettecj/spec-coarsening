import numpy as np
import os
from reader import *

# Checks for stability via the loss.
def check_stability( rundata ):

	# Initially set stability to True.
	stab = True

	# First check for nan values.
	if np.max(np.isnan(rundata["mean_test_loss"])):
		stab = False
	elif np.max(rundata["mean_test_loss"] > rundata["mean_test_loss"][0]):
		stab = False

	# Return the stability.
	return( stab )

# Get graph name.
def get_name( filename ):
	
	# Now break down the arguments.
	info = filename.split('_')[0]

	# Get the name from this and return it.
	return( info )


# Computes simple metrics.
def get_metrics( folder_name ):

	# Get all the filenames.
	files = os.listdir("./"+folder_name)

	# Set some initial parameters.
	loss_times = {"cora":[], "citeseer":[],"pubmed":[]}
	acc_times = {"cora":[], "citeseer":[],"pubmed":[]}
	av_loss_speedup = 0
	av_acc_speedup = 0
	loss_sum = 0
	acc_sum = 0 

	# Iterate through each filename.
	for file in files:

		# Get the data for our runs.
		rundata = get_full_data( folder_name+"/"+file )

		# Get the name.
		name = get_name( file )

		# Determine if the parameters are stable.
		stability = check_stability( rundata )

		# If they are, compute our speedup.
		if stability:

			# Determine if the data reaches the desired accuracy and loss.
			acc_threshold = np.max(rundata["mean_bench_test_acc"])
			loss_threshold = np.min(rundata["mean_bench_test_loss"])
			acc_thresh_pos = np.min(np.argwhere(rundata["mean_bench_test_acc"] == acc_threshold))
			loss_thresh_pos = np.min(np.argwhere(rundata["mean_bench_test_loss"] == loss_threshold))
			acc_thresh_time = rundata["terp_timings"][acc_thresh_pos]
			loss_thresh_time = rundata["terp_timings"][loss_thresh_pos]

			# If we meet our requirements for accuracy perform 'statistics'
			if np.any( rundata["mean_test_acc"][rundata["mean_test_acc"] > acc_threshold] ):
				acc = np.min(rundata["mean_test_acc"][rundata["mean_test_acc"] > acc_threshold])
				acc_pos = np.min(np.argwhere( rundata["mean_test_acc"] == acc ))
				acc_time = rundata["terp_timings"][acc_pos]
				av_acc_speedup += acc_thresh_time / acc_time
				acc_times[name].append( acc_thresh_time / acc_time )
				acc_sum += 1

			# If we meet our requirements for loss perform 'statistics'
			if np.any( rundata["mean_test_loss"][rundata["mean_test_loss"] < loss_threshold] ):
				loss = np.max(rundata["mean_test_loss"][rundata["mean_test_loss"] < loss_threshold])
				loss_pos = np.min(np.argwhere( rundata["mean_test_loss"] == loss ))
				loss_time = rundata["terp_timings"][loss_pos]
				av_loss_speedup += loss_thresh_time / loss_time
				loss_times[name].append( loss_thresh_time / loss_time )
				loss_sum += 1

	av_loss_speedup /= loss_sum
	av_acc_speedup /= acc_sum

	# print em.
	for name in loss_times.keys():

		print(name)
		print(" mean loss improvement: "+str(np.mean(loss_times[name])) )
		print(" mean accuracy improvement: "+str(np.mean(acc_times[name])))
		print(" stdev loss improvement: "+str(np.std(loss_times[name])) )
		print(" stdev accuracy improvement: "+str(np.std(acc_times[name])))
		print(" median loss improvement: "+str(np.median(loss_times[name])) )
		print(" median accuracy improvement: "+str(np.median(acc_times[name])))
		print()

get_metrics("results")