import numpy as np

import torch
from torch_geometric.nn import GCNConv
import torch.nn.functional as F

class One_Layer_Net(torch.nn.Module):
	def __init__(self, num_features, hidden, num_classes, seed=None ):

		if seed == None:
			seed = np.random.randint(0,high=999999,dtype=int)
		torch.manual_seed(seed)
		
		super(One_Layer_Net, self).__init__()
		self.conv1 = GCNConv(num_features, hidden)

	def reset_parameters(self):
		self.conv1.reset_parameters()

	def forward(self, torchG):
		x, edge_index, = torchG.x, torchG.edge_index

		x = self.conv1(x, edge_index)

		return F.log_softmax(x, dim=1)

class Two_Layer_Net(torch.nn.Module):
	def __init__(self, num_features, hidden, num_classes, seed=None ):
		
		if seed == None:
			seed = np.random.randint(0,high=999999,dtype=int)
		torch.manual_seed(seed)
		
		super(Two_Layer_Net, self).__init__()
		self.conv1 = GCNConv(num_features, hidden)
		self.conv2 = GCNConv(hidden, num_classes)

	def reset_parameters(self):
		self.conv1.reset_parameters()
		self.conv2.reset_parameters()

	def forward(self, torchG):
		x, edge_index, = torchG.x, torchG.edge_index

		x = self.conv1(x, edge_index)
		x = F.relu(x)
		x = F.dropout(x, training=self.training)
		x = self.conv2(x, edge_index)

		return F.log_softmax(x, dim=1)

class Three_Layer_Net(torch.nn.Module):
	def __init__(self, num_features, hidden, num_classes, seed=None ):

		if seed == None:
			seed = np.random.randint(0,high=999999,dtype=int)
		torch.manual_seed(seed)
		
		super(Three_Layer_Net, self).__init__()
		self.conv1 = GCNConv(num_features, hidden)
		self.conv2 = GCNConv(hidden, hidden)
		self.conv3 = GCNConv(hidden, num_classes)

	def reset_parameters(self):
		self.conv1.reset_parameters()
		self.conv2.reset_parameters()
		self.conv3.reset_parameters()

	def forward(self, torchG):
		x, edge_index, = torchG.x, torchG.edge_index

		x = self.conv1(x, edge_index)
		x = F.relu(x)
		x = F.dropout(x, training=self.training)
		x = self.conv2(x, edge_index)
		x = F.relu(x)
		x = F.dropout(x, training=self.training)
		x = self.conv3(x,  edge_index)

		return F.log_softmax(x, dim=1)