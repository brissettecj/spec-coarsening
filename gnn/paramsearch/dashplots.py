import numpy as np
import pandas as pd
import sys, os
from os import listdir
from os.path import isfile, join
from dash import Dash, html, dcc, callback, Output, Input, State
import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio
from plotly.subplots import make_subplots
from reader import *

mypath = 'results/'
global files, params, subset
files = np.array([f for f in listdir(mypath) if isfile(join(mypath, f))])
params = np.array([f[:-4].split('_') for f in files])
global df

df = pd.DataFrame(params,
            columns=["dataset","loss","opt", "layers","layer_size",
                     "lr","m","p","r","epochs","runs"])

app = Dash(__name__, prevent_initial_callbacks=True)

dropdowns = [dcc.Dropdown(df[col].unique(), placeholder=col, multi=True, id=f'dropdown-{col}') for col in df.columns]
Z_options = {
   'time_loss' : "time speedup to loss" ,
    'time_acc' : "time speedup to accuracy", 
    'time_loss_abs' : "endpoint loss speedup",
    'time_acc_abs' : "endpoint accuracy speedup"
} 
cols = df.columns
header = html.H1(children='Heatmap', style={'textAlign':'center'})
exp_counts = html.Div(id='dd-output-container')

heatmapX = html.Div(dcc.Dropdown(cols, placeholder='X', id='dropdownX'), style={'width' : '25%','display': 'inline-block'})
heatmapY = html.Div(dcc.Dropdown(cols, placeholder='Y', id='dropdownY'), style={'width' : '25%','display': 'inline-block'})
heatmapZ = html.Div(dcc.Dropdown(Z_options, placeholder='Z',id='dropdownZ'), style={'width' : '25%','display': 'inline-block'})
heatmapV = html.Div(dcc.Input(id='input-on-submit', type='text'), style={'width' : '25%','display': 'inline-block'})

heatmapButton  = html.Button('Generate Heatmap', id='gen-heatmap', n_clicks=0, style={'width' : '10%','display': 'inline-block'})
lineplotButton = html.Button('Generate Line Plot(s)', id='gen-plot', n_clicks=0, style={'width' : '10%','display': 'inline-block'})
saveFigButton  = html.Button('Save Figure', id='save-fig',n_clicks=0, style={'width' : '10%','display': 'inline-block'} )
tableButton    = html.Button('Generate Table', id='gen-table',n_clicks=0, style={'width' : '10%','display': 'inline-block'})

figLocation =html.Div(id='saved-fig', children='')

heatmap = dcc.Graph(id='heatmap')
linePlots = html.Div(id='plot')
table = dcc.Graph(id='table')

app.layout = html.Div([
    header,
    *[html.Div(drop, style={'width' : '33%','display': 'inline-block'}) for drop in dropdowns],    
    exp_counts,
    heatmapX,
    heatmapY,
    heatmapZ,
    heatmapV,
    html.Div([
        heatmapButton,
        tableButton,
        lineplotButton,
        saveFigButton,
        figLocation,
        html.Div(id='container-button-basic', children='Enter a value and press submit'),
        heatmap,
        table,
        linePlots
     ] )
])
global masks, global_mask, subset
masks = {col : np.full(df.shape[0],True) for col in cols}
global_mask = True
for v in masks.values():
    global_mask *= v
subset = {c : None for c in cols}

funcs = [f"""
@app.callback(
        Output('dd-output-container','children', allow_duplicate=True),
        Input('dropdown-{col}','value'),
        prevent_initial_call=True
)
def update_mask_{col}(values):
    print(values)
    subset['{col}'] = values
    if values == [] or values is None:
        masks['{col}'] = np.full(df.shape[0],True)
    else:
        masks['{col}'] = df['{col}'].isin(values)
    return update_mask()
""" for col in cols ]
for f in funcs:
    exec(f)

@app.callback(
    Output('input-on-submit','placeholder'),
    Input('dropdownZ','value')
)
def update_mark(value):
    return value.split("_")[1]

@app.callback(
    Output('plot','children'),
    Input('gen-plot', 'n_clicks'))
def gen_plot(n_clicks):
    exp_files = files[global_mask]
    n = len(exp_files)
    #fig = make_subplots(rows=n,cols=2)

    divs = []
    for i,file in enumerate(exp_files):
        fig = make_subplots(rows=1,cols=2,
            subplot_titles=('accuracy', 'loss') )

        rundata = get_stats(get_data("results/"+file), omit_nan=False, omit_unstable=False)

        test_line = go.Scatter(x=rundata["terp_timings"], y=rundata["mean_test_acc"], name="test accuracy", line={'color':'red'})
        train_line = go.Scatter(x=rundata["terp_timings"], y=rundata["mean_train_acc"], name="train accuracy", line={'color':'grey','dash':'dot'} )
        bench_line = go.Scatter(x=rundata['terp_timings'], y=rundata['mean_bench_test_acc'], name='adam test accuracy', line={'color':'black','dash':'dash'} )
        
        test_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_test_acc']-rundata["std_test_acc"], line=dict(width=.1), showlegend=False)
        test_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_test_acc']+rundata["std_test_acc"], fill='tonexty', line=dict(width=.1), fillcolor=to_rgb('red',.25), showlegend=False )
        
        train_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_train_acc']-rundata["std_train_acc"], line=dict(width=.1), showlegend=False)
        train_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_train_acc']+rundata["std_train_acc"], line=dict(width=.1), fillcolor=to_rgb('grey',.5), fill='tonexty', showlegend=False)

        bench_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_bench_test_acc']-rundata["std_bench_test_acc"], line=dict(width=.1), showlegend=False)
        bench_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_bench_test_acc']+rundata["std_bench_test_acc"], line=dict(width=.1), fillcolor=to_rgb('grey',.5), fill='tonexty', showlegend=False)

        fig.add_trace( test_line, row=1, col = 1 )
        fig.add_trace( train_line, row=1, col = 1 )
        fig.add_trace( bench_line, row=1, col = 1 )

        fig.add_trace(test_minus, row=1, col=1)
        fig.add_trace(test_plus, row=1, col=1)
        fig.add_trace(train_minus, row=1, col=1)
        fig.add_trace(train_plus, row=1, col=1)
        fig.add_trace(bench_minus, row=1, col=1)
        fig.add_trace(bench_plus, row=1, col=1)

        test_line = go.Scatter(x=rundata["terp_timings"], y=rundata["mean_test_loss"], name="test loss", line={'color':'red'}, showlegend=False)
        train_line = go.Scatter(x=rundata["terp_timings"], y=rundata["mean_train_loss"], name="test loss", line={'color':'grey','dash':'dot'}, showlegend=False )
        bench_line = go.Scatter(x=rundata['terp_timings'], y=rundata['mean_bench_test_loss'], name='adam test loss', line={'color':'black','dash':'dash'}, showlegend=False )
        
        test_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_test_loss']-rundata["std_test_loss"], line=dict(width=.1), showlegend=False)
        test_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_test_loss']+rundata["std_test_loss"], fill='tonexty', line=dict(width=.1), fillcolor=to_rgb('red',.25), showlegend=False )
        
        train_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_train_loss']-rundata["std_train_loss"], line=dict(width=.1), showlegend=False)
        train_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_train_loss']+rundata["std_train_loss"], line=dict(width=.1), fillcolor=to_rgb('grey',.5), fill='tonexty', showlegend=False)

        bench_minus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_bench_test_loss']-rundata["std_bench_test_loss"], line=dict(width=.1), showlegend=False)
        bench_plus = go.Scatter(x=rundata["terp_timings"], y=rundata['mean_bench_test_loss']+rundata["std_bench_test_loss"], line=dict(width=.1), fillcolor=to_rgb('grey',.5), fill='tonexty', showlegend=False)

        fig.add_trace( test_line, row=1, col = 2 )
        fig.add_trace( train_line, row=1, col = 2 )
        fig.add_trace( bench_line, row=1, col = 2 )

        fig.add_trace(test_minus, row=1, col=2)
        fig.add_trace(test_plus, row=1, col=2)
        fig.add_trace(train_minus, row=1, col=2)
        fig.add_trace(train_plus, row=1, col=2)
        fig.add_trace(bench_minus, row=1, col=2)
        fig.add_trace(bench_plus, row=1, col=2)

        title = ""
        for i,col in enumerate(file.split('_')):
            title += f"{str(df.columns[i])}: {str(col)}\t\t"

        fig.update_layout(title_text=title)

        divs.append(html.Div(dcc.Graph(figure=fig)))

    return divs
    


@app.callback(
    Output('heatmap','figure'),
    Input('gen-heatmap', 'n_clicks'),
    State('dropdownX', 'value'),
    State('dropdownY', 'value'),
    State('dropdownZ', 'value'),
    State('input-on-submit', 'value')
)
def gen_heatmap(n_clicks,x,y,z,value):
    if value is not None:
        value = float(value)
    print("GENEATING HEATMAP")
    #breakpoint()
    exp_files = files[global_mask]
    exp_params = df[global_mask]
    X = sorted(df[global_mask][x].unique(), key=lambda x: float(x))
    Y = sorted(df[global_mask][y].unique(), key=lambda x: float(x))
    #breakpoint()
    M = np.zeros([len(Y),len(X)])
    Xindex = {X[i] : i for i in range(len(X))}
    Yindex = {Y[i] : i for i in range(len(Y))}
    Mcounts = np.zeros([len(Y),len(X)])
    MScounts = np.zeros([len(Y),len(X)])
    Mnans = np.zeros([len(Y),len(X)])
    Mfailed = np.zeros([len(Y),len(X)])
    print('here')

    #breakpoint()
    for i in range(len(exp_files)):
        file = exp_files[i]
        data = get_data('results/'+file)
        stats = get_stats(data)

        xparam = stats[x]
        yparam = stats[y]
        Mcounts[Yindex[str(yparam)],Xindex[str(xparam)]] += 1

        if z == 'time_loss':
          
            speedup = stats['terp_timings'][np.argmax(stats['mean_bench_test_loss'] < value)] / stats['terp_timings'][np.argmax(stats['mean_test_loss'] < value)]
            
        elif z == 'time_acc':
            speedup = stats['terp_timings'][np.argmax(stats['mean_bench_test_acc'] > value)] / stats['terp_timings'][np.argmax(stats['mean_test_acc'] > value)]
        
        elif z == 'time_loss_abs':
            endval = (-1)*np.min( stats['mean_bench_test_loss'] )
            endloc = np.argmin( stats['mean_bench_test_loss'])
            bottom = stats['terp_timings'][np.argmax((-1)*stats['mean_test_loss'] > endval)]
            # Check if argmax is satisfied.
            if bottom == 0:
                speedup = 0
            else:
                speedup = stats['terp_timings'][endloc] / bottom

        elif z == 'time_acc_abs':
            endval = np.max( stats['mean_bench_test_acc'] )
            endloc = np.argmax( stats['mean_bench_test_acc'])
            bottom = stats['terp_timings'][np.argmax(stats['mean_test_acc'] > endval)]
            # Check if np argmax is satisfied.
            if bottom == 0:
                speedup = 0
            else:
                speedup = stats['terp_timings'][endloc] / bottom

        Mnans[Yindex[str(yparam)],Xindex[str(xparam)]] += stats['unstables']
        if speedup == 0:
            Mfailed[Yindex[str(yparam)],Xindex[str(xparam)]] += 1
        else: 
            M[Yindex[str(yparam)],Xindex[str(xparam)]] += speedup
            MScounts[Yindex[str(yparam)],Xindex[str(xparam)]] += 1

    colorscale = [[0.0, '#3D9970'], [.5, '#FFFFFF'], [1.0, '#FF4136']]
    M = M / Mcounts
    Mnans = (Mnans / Mcounts).round(2)
    Mfailed = (Mfailed / Mcounts).round(2)
    #fig = px.imshow(M, color_continuous_scale='reds', labels=dict(x=x,y=y,color='speedup', showgrid=False),x=X, y=Y)
    #fig = px.imshow(M, color_continuous_scale='reds',x=X, y=Y)


    fig = go.Figure(data=go.Heatmap(x=X,y=Y, z=M,
                    text = Mnans,
                    colorscale='reds',
                    zmin=0, zmid=1.5, zmax=3),
                    layout=go.Layout(xaxis_title=x,yaxis_title=y))

    fig.update_traces(text=Mnans, texttemplate="%{text}")
    fig.update_layout(xaxis=dict(showgrid=False),
              yaxis=dict(showgrid=False),
              font=dict(size=34))

    fig.update_layout(margin_r=5)
    print("HEATMAP COMPLETE")
    #fig.tight_layout()
    #px.tight_layout()
    return fig

@app.callback(
    Output('table', 'figure'),
    Input('gen-table', 'n_clicks')
)
def gen_table(n_clicks):
    exp_files = files[global_mask]
    exp_params = df[global_mask]
    datasets = exp_params.dataset.unique()
    d = datasets.shape[0]

    speedups = np.zeros([2,len(exp_files),d])

    iterables = [['min','avg','max'],['loss','acc']]

    columns = pd.MultiIndex.from_product(iterables)


    for j in range(d):
        exp_files = files[global_mask * (df['dataset'] == datasets[j])]

        
        
        for i in range(len(exp_files)):
            file = exp_files[i]
            data = get_data('results/'+file)
            stats = get_stats(data)

            #'time_loss_abs':
            endval = (-1)*np.min( stats['mean_bench_test_loss'] )
            endloc = np.argmin( stats['mean_bench_test_loss'])
            bottom = stats['terp_timings'][np.argmax((-1)*stats['mean_test_loss'] > endval)]
            # Check if argmax is satisfied.
            if bottom == 0:
                loss_speedup = 0
            else:
                loss_speedup = stats['terp_timings'][endloc] / bottom

            #time_acc_abs':
            endval = np.max( stats['mean_bench_test_acc'] )
            endloc = np.argmax( stats['mean_bench_test_acc'])
            bottom = stats['terp_timings'][np.argmax(stats['mean_test_acc'] > endval)]
            # Check if np argmax is satisfied.
            if bottom == 0:
                acc_speedup = 0
            else:
                acc_speedup = stats['terp_timings'][endloc] / bottom


            speedups[:,i,j] = loss_speedup, acc_speedup



    acc_speedups = speedups[1]
    print(speedups)
    results = np.zeros([d,3,2])
    """
         
          

              acc | loss
            ----------|
   dataset1 |         |     |
            -------------
   dataset2 |         |     |
            -------------


    """

    results[:,1] = speedups.mean(axis=1).T
    results[:,2] = speedups.max(axis=1).T

    result_df = pd.DataFrame(results.reshape([d,6]), columns=columns, index=datasets)
    print(result_df)
    fig = go.Figure(data=go.Table(header=dict(values=['loss', 'accuracy']), cells=dict(values=result_df.T) ) )

    return fig

@app.callback(
    Output('saved-fig','children'),
    Input('save-fig', 'n_clicks'),
    State('heatmap', 'figure'),
    State('dropdownX', 'value'),
    State('dropdownY', 'value'),
    State('dropdownZ', 'value')
)
def save_fig(n_clicks, fig, x, y, z):
    heatmap_name = ""
    for col in subset.keys():
        if col in [x,y]:
            heatmap_name += str(col) + '_'
        else:
            heatmap_name += str(subset[col]) + '_'

    heatmap_name = "heatmaps/" + heatmap_name[:-1]

    fig = go.Figure(fig)

    fig.write_image(heatmap_name+".pdf")
    return f"Heatmap saved at {heatmap_name}"
def to_rgb(color,opacity=1):
    colors = {
            'red' : 'rgba(255,0,0,{0})',
            'blue': 'rgba(0,0,255,{0})',
            'grey': 'rgba(192,192,192,{0})',
            'black': 'rgba(0,0,0,{0})',
        }

    return colors[color].format(opacity)

def update_mask():
    global global_mask
    global_mask = True
    for v in masks.values():
        global_mask *= v
    #print(masks)
    return f"Experiments: {len(df[global_mask])}"

if __name__ == '__main__':
    app.run_server(debug=True)