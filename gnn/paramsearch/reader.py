import numpy as np
import matplotlib.pyplot as plt

# Reads in the file at file_path and writes the data to a labeled dictionary for easy plotting.
# It additionally computes the means and variance of each non-time parameter.
def get_data( filename ):
	
	# First read in the file.
	outputs = np.load(filename)

	# Now break down the arguments.
	filename = filename.split('/')[1]
	info = filename.split('_')
	info[len(info)-1] = info[len(info)-1].split('.')[0]
	
	# Fill in the parameters in the rundata dict.
	rundata = {}
	rundata["dataset"] = info[0]
	rundata["loss"] = info[1]
	rundata["optimizer"] = info[2]
	rundata["layers"] = int(info[3])
	rundata["layer_size"] = int(info[4])
	rundata["lr"] = float(info[5])
	rundata["m"] = int(info[6])
	rundata["p"] = int(info[7])
	rundata["r"] = int(info[8])
	rundata["epochs"] = int(info[9])
	rundata["runs"] = int(info[10])

	# Get the outputs of each run.
	indices = 10*np.arange( rundata["runs"] )
	rundata["test_acc"] = outputs[indices,:]
	rundata["train_acc"] = outputs[indices+2,:]
	rundata["test_loss"] = outputs[indices+1,:]
	rundata["train_loss"] = outputs[indices+3,:]
	rundata["timings"] = outputs[indices+4,:]
	rundata["bench_test_acc"] = outputs[indices+5,:]
	rundata["bench_train_acc"] = outputs[indices+7,:]
	rundata["bench_test_loss"] = outputs[indices+6,:]
	rundata["bench_train_loss"] = outputs[indices+8,:]
	rundata["bench_timings"] = outputs[indices+9,:]

	# Return the dictionary.
	return( rundata )

# Gets the averages, and varainces of the parameters in a "rundata" dictionary.
def get_stats( rundata, omit_nan=True, omit_unstable=True ):

	# From timings, get the start and end times and form our time axis.
	starttime = 0

	time_arr = rundata['timings'][:,-1]
	try:
		endtime = np.min( time_arr[time_arr > 0] )
	except ValueError:
		endtime = 0
	xvals = np.linspace(starttime,endtime,1000)
	rundata["terp_timings"] = xvals

	# Now interpolate all the parameters we care about with respect to their own timing.
	names = ["test_acc","train_acc","train_loss","test_loss"]
	bench_names = ["bench_test_acc","bench_train_acc","bench_train_loss","bench_test_loss"]
	names = names + bench_names

	nan_runs = [False for _ in range(rundata['runs'])]
	unstable_runs = [False for _ in range(rundata['runs'])]

	for stat in names:
		for run in range(rundata['runs']):
			if sum(np.isnan(rundata[stat][run])) > 0:
				nan_runs[run] = True
				unstable_runs[run] = True

	for run in range(rundata['runs']):
		if sum(rundata['train_loss'][run] > (2*rundata['train_loss'][run][0])) > 0:
			unstable_runs[run] = True			

	for name in names:
		datarr = rundata[name]
		arr = []
		for index in range(rundata["runs"]):
			if nan_runs[index] and omit_nan:
				continue
			if unstable_runs[index] and omit_unstable:
				continue
			arr.append( np.interp( xvals, rundata["timings"][index], rundata[name][index]) )
		arr = np.array(arr)
		rundata["mean_"+name] = np.mean(arr,axis=0)
		rundata["std_"+name] = np.std(arr,axis=0)
	rundata['nans'] = 1 - (sum(nan_runs) / len(nan_runs))
	rundata['unstables'] = 1 - (sum(unstable_runs) / len(unstable_runs))
	# Return rundata.
	return( rundata )

# Get the data and stats for a file.
def get_full_data( filename ):

	# Get the raw data.
	full_data = get_data( filename )

	# Get the means and deviations.
	full_data = get_stats( full_data )

	# Return the dictionary.
	return( full_data )