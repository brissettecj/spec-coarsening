import numpy as np
import pandas as pd
import sys, os
from os import listdir
from os.path import isfile, join
from dash import Dash, html, dcc, callback, Output, Input, State
import plotly.express as px
import plotly.graph_objs as go
import plotly.io as pio
from reader import *

mypath = 'results/'
global files, params, subset
files = np.array([f for f in listdir(mypath) if isfile(join(mypath, f))])
params = np.array([f[:-4].split('_') for f in files])
global df

df = pd.DataFrame(params,
            columns=["dataset","loss","opt", "layers","layer_size",
                     "lr","m","p","r","epochs","runs"])

app = Dash(__name__, prevent_initial_callbacks=True)

dropdowns = [dcc.Dropdown(df[col].unique(), placeholder=col, id=f'dropdown-{col}') for col in df.columns]
Z_options = {
   'time_loss' : "time speedup to loss" ,
    'time_acc' : "time speedup to accuracy" 
} 
cols = df.columns
app.layout = html.Div([
    html.H1(children='Heatmap', style={'textAlign':'center'}),
    *[html.Div(drop, style={'width' : '33%','display': 'inline-block'}) for drop in dropdowns],    
    html.Div(id='dd-output-container'),
    html.Div(dcc.Dropdown(cols, placeholder='X', id='dropdownX'), style={'width' : '25%','display': 'inline-block'}),
    html.Div(dcc.Dropdown(cols, placeholder='Y', id='dropdownY'), style={'width' : '25%','display': 'inline-block'}),
    html.Div(dcc.Dropdown(Z_options, placeholder='Z',id='dropdownZ'), style={'width' : '25%','display': 'inline-block'}),
    html.Div(dcc.Input(id='input-on-submit', type='text'), style={'width' : '25%','display': 'inline-block'}),
    html.Div([
        html.Button('Generate Heatmap', id='submit-val', n_clicks=0, style={'width' : '10%','display': 'inline-block'}),
        html.Button('Save Figure', id='save-fig',n_clicks=0, style={'width' : '10%','display': 'inline-block'} ),
        html.Div(id='saved-fig', children=''),
        html.Div(id='container-button-basic',
                children='Enter a value and press submit'),
    dcc.Graph(id='heatmap') ] )
])
global masks, global_mask, subset
masks = {col : np.full(df.shape[0],True) for col in cols}
global_mask = True
for v in masks.values():
    global_mask *= v
subset = {c : None for c in cols}

funcs = [f"""
@app.callback(
        Output('dd-output-container','children', allow_duplicate=True),
        Input('dropdown-{col}','value'),
        prevent_initial_call=True
)
def update_mask_{col}(value):
    #print(value)
    subset['{col}'] = value
    if value is None:
        masks['{col}'] = np.full(df.shape[0],True)
    else:
        masks['{col}'] = df['{col}'] == value
    return update_mask()
""" for col in cols ]

for f in funcs:
    exec(f)
@app.callback(
    Output('input-on-submit','placeholder'),
    Input('dropdownZ','value')
)
def update_mark(value):
    return value.split("_")[1]

@app.callback(
    Output('heatmap','figure'),
    Input('submit-val', 'n_clicks'),
    State('dropdownX', 'value'),
    State('dropdownY', 'value'),
    State('dropdownZ', 'value'),
    State('input-on-submit', 'value')
)
def gen_heatmap(n_clicks,x,y,z,value):
    value = float(value)
    print("GENEATING HEATMAP")
    #breakpoint()
    exp_files = files[global_mask]
    exp_params = df[global_mask]
    X = sorted(df[global_mask][x].unique(), key=lambda x: float(x))
    Y = sorted(df[global_mask][y].unique(), key=lambda x: -float(x))
    #breakpoint()
    M = np.zeros([len(Y),len(X)])
    Xindex = {X[i] : i for i in range(len(X))}
    Yindex = {Y[i] : i for i in range(len(Y))}
    Mcounts = np.zeros([len(Y),len(X)])
    Mnans = np.zeros([len(Y),len(X)])
    print('here')

    #breakpoint()
    for i in range(len(exp_files)):
        file = exp_files[i]
        data = get_data('results/'+file)
        stats = get_stats(data)

        xparam = stats[x]
        yparam = stats[y]
        Mcounts[Yindex[str(yparam)],Xindex[str(xparam)]] += 1
        if z == 'time_loss':
          
            speedup = stats['terp_timings'][np.argmax(stats['mean_bench_test_loss'] < value)] / stats['terp_timings'][np.argmax(stats['mean_test_loss'] < value)]
            
        elif z == 'time_acc':
            speedup = stats['terp_timings'][np.argmax(stats['mean_bench_test_acc'] > value)] / stats['terp_timings'][np.argmax(stats['mean_test_acc'] > value)]
        
        Mnans[Yindex[str(yparam)],Xindex[str(xparam)]] += stats['nans']
        M[Yindex[str(yparam)],Xindex[str(xparam)]] += speedup

    M = M / Mcounts
    Mnans = (Mnans / Mcounts).round(2)
    fig = px.imshow(M, color_continuous_scale='reds',             
                        labels=dict(x=x,y=y,color='speedup', showgrid=False),
        x=X, y=Y)
    fig.update_traces(text=Mnans, texttemplate="%{text}")
    fig.update_layout(xaxis=dict(showgrid=False),
              yaxis=dict(showgrid=False))

    fig.update_layout(margin_r=5)
    print("HEATMAP COMPLETE")
    #fig.tight_layout()
    #px.tight_layout()
    return fig

@app.callback(
    Output('saved-fig','children'),
    Input('save-fig', 'n_clicks'),
    State('heatmap', 'figure'),
    State('dropdownX', 'value'),
    State('dropdownY', 'value'),
    State('dropdownZ', 'value')
)
def save_fig(n_clicks, fig, x, y, z):
    heatmap_name = ""
    for col in subset.keys():
        if col in [x,y]:
            heatmap_name += str(col) + '_'
        else:
            heatmap_name += str(subset[col]) + '_'

    heatmap_name = "plots/" + heatmap_name[:-1]
    
    fig = go.Figure(fig)
    fig.write_image(heatmap_name+".svg")
    return f"Heatmap saved at {heatmap_name}"

def update_mask():
    global global_mask
    global_mask = True
    for v in masks.values():
        global_mask *= v
    #print(masks)
    return f"Experiments: {len(df[global_mask])}"
if __name__ == '__main__':
    app.run_server(debug=True)