import numpy as np
import cupy as cp
import time
import torch
from torch.nn.utils import parameters_to_vector,vector_to_parameters
from forecaster import *

# Performs one epoch of training.
def train_epoch(model, data, optimizer, loss_fn, mask=True):
	model.train()
	optimizer.zero_grad()
	yhat = model(data)
	loss = loss_fn(yhat[mask], data.y[mask])
	loss.backward()
	optimizer.step()
	return loss

# Performs validation for the model.
def validate(model, data, crit, mask=True):
	model.eval()
	with torch.no_grad():
		yhat = model(data)[mask]
		pred = yhat.argmax(dim=1)
		correct = (pred == data.y[mask])
		accuracy = correct.sum() / len(correct)
		loss = crit(yhat, data.y[mask])
		return (accuracy, loss)

# Performs the full training, changing the parameters every 20 epochs.
def train_full( model, graph, optimizer, loss_fn, its, r=3, track=0, pred=0, cupy=False, output=False, printem=False ):

	# Set up the timer.
	timer = 0
	starter,ender = torch.cuda.Event(enable_timing=True),torch.cuda.Event(enable_timing=True)

	# If output is true, initialize data for storage.
	if output:
		out = cp.zeros((5,its+1))
		algstart = time.time()
		acc, loss = validate(model, graph, loss_fn, mask=graph.test_mask)
		acc_train, loss_train = validate(model, graph, loss_fn, mask=graph.train_mask)
		valarr = cp.array([acc.item(), loss.item(), acc_train.item(), loss_train.item(), 0])
		out[:,0] = valarr

	# Get the breakdown of our parameter dimensions for flattening.
	if track > 0:
		flat_params = parameters_to_vector(model.parameters())
		param_num = flat_params.size()[0]
		appended_param_num = param_num + (32 - param_num%32)
		W = cp.zeros((track,appended_param_num))

	# Loop through our epochs, randomly changing the weights every 20 steps.
	for epoch in range(its):

		# If the iteration is correct, predict weights.
		if track > 0 and epoch%track == 0 and epoch > 0:

			# Collect time.
			starter.record()

			# Temporarily reshape W.
			W = W.transpose().reshape((appended_param_num//32,32,track))

			# Forecast using DMD.
			if cupy:
				new_weights = cupy_forecast( W, pred, r, times=printem )
			else:
				new_weights = torch_forecast( W, pred, r, times=printem )
			
			# Reshape W.
			W = W.transpose().reshape(track,appended_param_num)

			# Set the model parameters.
			vector_to_parameters( torch.flatten(new_weights), model.parameters() )

			# Get the ending times.
			ender.record()

		# Otherwise perform some standard adam steps!
		else:
			# Collect time.
			starter.record()

			# Perform an epoch.
			train_epoch(model, graph, optimizer, loss_fn, mask=graph.train_mask)

			# Get the ending times.
			ender.record()

		# synchronize for timings.
		torch.cuda.synchronize()
		timing = starter.elapsed_time(ender)

		# If output is true perform our important validations.
		if output:
			timer = timer + timing
			acc, loss = validate(model, graph, loss_fn, mask=graph.test_mask)
			acc_train, loss_train = validate(model, graph, loss_fn, mask=graph.train_mask)
			valarr = cp.array([acc.item(), loss.item(), acc_train.item(), loss_train.item(), timer*0.001])
			out[:,epoch+1] = valarr
			# Check if we nan. If we do, get out of there man.
			if np.isnan(valarr[1]) or np.isnan(valarr[3]):
				print("		!! nanland...")
				break
			
		# Get the parameters and populate W.
		if track > 0:
			W[epoch%track,0:param_num] = cp.array( torch.tensor(parameters_to_vector(model.parameters()),device="cuda:0"), dtype=cp.float32 )

		# Print the end time of our iteration.
		if printem and epoch > track+1:
			if epoch%track == 0:
				print()
				print( "THIS STEP SHOULD BE SLOWER " )
			if epoch%track == 1:
				print( "THIS IS WHERE THINGS GENERALLY BREAK " )
			print( "iteration: "+str(epoch)+" time: "+str(timing) )
			if epoch%track == 1:
				print( )

	# Return.
	if output:
		return( cp.asnumpy(out) )
	else:
		return( None )