import torch
import cupy as cp
import time

# Performs dmd forecasting using torch tensors.
def torch_forecast( data, pred_step, r, times=False ):

    if times:
        print("torch forecast...")
        print("     data shape: "+str(data.shape))

    # Seperate our data.
    inputs = data[:,:,:-1]
    outputs = data[:,:,1:]

    # Change inputs to torch for svd.
    inputs = torch.as_tensor(inputs,device="cuda",dtype=torch.float32)
    
    # Perform the svd
    if times:
        start = time.time()
    u,s,vt = torch.linalg.svd( inputs, full_matrices=False, driver="gesvdj" )
    if times:
        print( "        svd time: "+str(time.time()-start) )

    # Back to cupy.
    s = cp.asarray(s)
    vt = cp.asarray(vt)
    u = cp.asarray(u)
    
    # Take the recirpocal of s.
    s = cp.reciprocal(s)[:,:r]
    vt = vt.transpose(0,2,1)[:,:,:r]
    u = u.transpose(0,2,1)[:,:r,:]

    # Compute A for predictions.
    A = outputs @ vt @ ( s[...,None] * u )
    
    # Get the last rows.
    outs = outputs[:,:,outputs.shape[2]-1:]

    # Forecast.
    if times:
        start = time.time()
    for i in range(pred_step):
        outs = cp.matmul( A, outs )
    if times:
        print( "        predict time: "+str(time.time()-start) )
    
    # Reshape outs and turn it into a torch tensor.
    outs = torch.tensor( outs.reshape( 1,outs.shape[0]*outs.shape[1] ), device="cuda:0", dtype=torch.float32 )
    
    # Return outs.
    return( outs )


# Performs dmd forecasting using cupy arrays.
def cupy_forecast( data, pred_step, r, times=False ):

    if times:
        print("cupy forecast...")
        print("     data shape: "+str(data.shape))

    # Seperate our data.
    inputs = data[:,:,:-1]
    outputs = data[:,:,1:]

    # Perform the svd
    if times:
        start = time.time()
    u,s,vt = cp.linalg.svd( inputs, full_matrices=False )
    if times:
        print( "        svd time: "+str(time.time()-start) )

    # Back to cupy.
    s = cp.asarray(s)
    vt = cp.asarray(vt)
    u = cp.asarray(u)
    
    # Take the recirpocal of s.
    s = cp.reciprocal(s)[:,:r]
    vt = vt.transpose(0,2,1)[:,:,:r]
    u = u.transpose(0,2,1)[:,:r,:]

    # Compute A for predictions.
    A = outputs @ vt @ ( s[...,None] * u )
    
    # Get the last rows.
    outs = outputs[:,:,outputs.shape[2]-1:]

    # Forecast.
    if times:
        start = time.time()
    for i in range(pred_step):
        outs = cp.matmul( A, outs )
    if times:
        print( "        predict time: "+str(time.time()-start) )
    
    # Reshape the outs array and change it to torch.
    outs = torch.tensor( outs.reshape( 1,outs.shape[0]*outs.shape[1] ), device="cuda:0", dtype=torch.float32 )

    # Return outs.
    return( outs )