import cupy as cp
import numpy as np

import copy
import time
from itertools import product

import torch
from torch.nn.utils import parameters_to_vector,vector_to_parameters
from torch import optim
from torch.nn import CrossEntropyLoss, NLLLoss
import torch_geometric.datasets as tds

from netobjs import *
from trainer import *

#import matplotlib.pyplot as plt

# Set our parameters for our search.
parameters = {}
parameters["dataset"] = ['cora'] #["pubmed"] #["pubmed","citeseer","cora"] #["cora","pubmed","citeseer"]
parameters["loss"] = ["nll"] #["nll","cel"]
parameters["optimizer"] = ["adam"] #,"sgd"] #["adam","sgd","adadelta"]
parameters["layers"] = [1] #[2,1] #[1,2,3]
parameters["layer_size"] = [128] #[ 16, 32, 64, 128 ]
parameters["lr"] = [0.0001] #[0.001,0.0001,0.01]
parameters["m"] = [4] #[4,8,16,32]
parameters["p"] = [8] #[4,8,16,32]
parameters["r"] = [3] #[1,2,3,4,5]
parameters["epochs"] = [1000]
parameters["runs"] = [5]

# Get a dictionary of all possible combinations of parameters.
keys, values = zip(*parameters.items())
combinations = [dict(zip(keys, p)) for p in product(*values)]

# Begin the parameter search.
for comb_dict in combinations:

	# Get our parameters for the run.
	dat = comb_dict["dataset"]
	loss = comb_dict["loss"]
	layers = comb_dict["layers"]
	layer_size = comb_dict["layer_size"]
	lr = comb_dict["lr"]
	m = comb_dict["m"]
	p = comb_dict["p"]
	r = comb_dict["r"]
	epochs = comb_dict["epochs"]
	runnum = comb_dict["runs"]
	opt = comb_dict["optimizer"]

	# Create a name for our output file.
	filename = '_'.join([str(val) for val in comb_dict.values()])
	
	# Initialize our dataset.
	if dat == "cora":
		data = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')[0].to('cuda')
	elif dat == "pubmed":
		data = tds.Planetoid(root="tmp/PubMed//", name='PubMed')[0].to('cuda')
	elif dat == "citeseer":
		data = tds.Planetoid(root="tmp/PubMed//", name='Citeseer')[0].to('cuda')

	# Set our loss function.
	if loss == "nll":
		loss_fn = NLLLoss()
	elif loss == "cel":
		loss_fn = CrossEntropyLoss()

	# Get our features, classes, and network seeds.
	num_features = data.x.shape[1]
	num_classes  = data.y.unique().shape[0]
	seeds = [np.random.randint(0,high=999999,dtype=int) for i in range(runnum) ]

	# Initialize a numpy matrix for tracking results.
	tracker = np.zeros((10*runnum,epochs+1))

	print("Collecting for parameters: "+filename)


	# Loop.
	it = 0
	for curr_seed in seeds:

		# Define our network topology.
		if layers == 1:
			model = One_Layer_Net( num_features, layer_size, num_classes, curr_seed ).to('cuda')
		elif layers == 2:
			model = Two_Layer_Net( num_features, layer_size, num_classes, curr_seed ).to('cuda')
		elif layers == 3:
			model = Three_Layer_Net( num_features, layer_size, num_classes, curr_seed ).to('cuda')

		# Define our optimizer.
		if opt == "adam":
			optimizer = optim.Adam
		elif opt == "sgd":
			optimizer = optim.SGD
		elif opt == "adadelta":
			optimizer = optim.Adadelta

		optimizer = optimizer( model.parameters(), lr=lr )

		# Run train_full once to warm it up.
		if it == 0:
			train_full( model, data, optimizer, loss_fn, 3, r=1, track=2, pred=1, 
											cupy=False, output=False, printem=False )

		temp_params = copy.deepcopy(model.state_dict())
		
		# Get the time we start the training round.
		starttime = time.time()

		# Get the benchmark results
		tracker[it*10+5:(it*10)+10,:] = train_full( model, data, optimizer, loss_fn, epochs, track=0, pred=0, 
											cupy=False, output=True, printem=False )

		model.load_state_dict(temp_params)
		# Run train_full once to warm it up.

		# Run train_full again and print resulting times.
		tracker[it*10:(it*10)+5,:] = train_full( model, data, optimizer, loss_fn, epochs, r=r, track=m, pred=p, 
											cupy=False, output=True, printem=False )

		print( "		training round time ("+str(it)+"): "+str(time.time()-starttime)+" model device: "+str(next(model.parameters()).is_cuda) )

		# increment it.
		it += 1

	# write to file.
	#np.save("/home/brissc/Desktop/gnnresultspm/"+filename, tracker, allow_pickle=False)
	np.save("./results/"+filename, tracker, allow_pickle=False)