import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import networkx as nx
#import metis
import math

import util
import time 

'''------------------------------------------------
merge(G,a,b,outmap)
- INPUT: A graph 'G', and two nodes 'a' and 'b'. Also takes in the outmap array 'outmap'. 
- DOES: Merges nodes 'a' and 'b' into a single node. The larger index node is merged into the smaller one.
- RETURNS: Nothing
- NOTES:
  If the graph is labeled and has features. 'label' and 'features' will both be added in the new graph.
------------------------------------------------'''
def merge(G,a,b,outmap):
	# check if graph has features and is labeled. This is used for coarsening graphs
	# for GNN training.
	labeled = False
	node_info = G.nodes[a]
	if node_info.get('features') is not None:
		labeled = True

	if a != b:
		# order our nodes for merging.
		[root_node,merge_node] = np.sort( [a,b] )
		# update outmap
		for map_index in range( len( outmap ) ):
			entry = outmap[map_index]
			if entry == merge_node:
				outmap[map_index] = root_node
		
		# if the graph is labeled, add "label" vectors and add "features" vectors.
		if labeled:
			G.nodes[root_node]["features"] = (G.nodes[root_node]["features"]+G.nodes[merge_node]["features"]).astype(np.float64)
			G.nodes[root_node]["label"] = (G.nodes[root_node]["label"]+G.nodes[merge_node]["label"]).astype(np.int32)
			
			
		# increment the associated node num.
		G.nodes[root_node]["num"] += G.nodes[merge_node]["num"]
		# loop through neighbors and remove edges.
		weights = {}
		neighbors = list( G.neighbors( merge_node ) )
		for u in neighbors:
			weights[u] = G[u][merge_node]["weight"]
			G.remove_edge( merge_node, u )
		G.remove_node( merge_node )
		# add edge weights.
		neighbors = list( G.neighbors( root_node ) )
		# go through every neighbor 
		for v in weights.keys():
			# if the neighbor is the node we merged.
			if v == merge_node:
				# check if a self loop already exsists
				if G.has_edge( root_node, root_node ):
					# if it does, add the merge_nodes self loop weight to the loop
					G[root_node][root_node]["weight"] += weights[v]
				else:
					# if it does not, create the edge and then add our self loop weight to the loop
					G.add_edge(root_node,root_node,weight=weights[v])
			elif G.has_edge( root_node, v ):
				G[root_node][v]["weight"] += weights[v]
			else:
				G.add_edge( root_node, v, weight=weights[v] )
	'''
	G_sum = 0
	for node in G.nodes():
		G_sum += G.degree( node, weight="weight" )
	print( "node_a: "+str(a)+" node_b: "+str(b)+" -- volume: "+str(G_sum))
	'''


'''------------------------------------------------
mapping_root(mapping,a)
- INPUT: A node mapping 'mapping', and a node 'a'.
- DOES: Finds the node that now represents 'a'
- RETURNS: The root node of 'a' according to the mapping.
------------------------------------------------'''
def mapping_root(mapping,a):
	node = a
	while mapping[node] != node:
		node = mapping[node]
	return( node )

'''------------------------------------------------
merge_many_edges(G,mergelist,num,outmap)
- INPUT: A graph 'G', and a list of merges (tuples), 'mergelist', and a
		 number of merges to perform 'num'. Also takes in the 'outmap'.
- DOES: Performs every merge in the mergelist.
- RETURNS: Nothing
- NOTES: all edges must be disjoint. If a node is at the intersection of two edges in 'mergelist'
		 undefined behavior may happen.
------------------------------------------------'''
def merge_many_edges(G,mergelist,num,outmap):
	# get our number of nodes.
	nodes = list(G.nodes())
	# define a mapping for node coarsening.
	mapping = {i:i for i in nodes}
	# count our merges.
	merge_num = 0
	# for each merge available to us.
	for [A,B] in mergelist:
		#u = mapping_root(mapping,A)
		#v = mapping_root(mapping,B)
		[a,b] = np.sort( [A,B] )
		# if the merge is reasonable, perform it and increment merge_num.
		if a != b:
			merge( G, a, b, outmap )
			mapping[b] = a
			merge_num += 1
		# if we are past our merge number, break.
		if merge_num == num:
			break

'''------------------------------------------------
merge_many_nodes(G,nodelist,num,outmap)
- INPUT: A graph 'G', and a list of nodelists, 'nodelist', and a
		 number of merges to perform 'num'. Also takes in the mapping "outmap"
- DOES: Performs every merge in the mergelist.
- RETURNS: Nothing
------------------------------------------------'''
def merge_many_nodes(G,nodelist,num,outmap):
	# get our number of nodes.
	nodes = list(G.nodes())
	# define a mapping for node coarsening.
	mapping = {i:i for i in nodes}
	# count our merges.
	merge_num = 0
	# for each merge available to us.
	for arr in nodelist:
		sorted_arr = np.sort( arr )
		a = sorted_arr[0]
		# if the merge is reasonable, perform it and increment merge_num.
		for index in range( len(arr)-1 ):
			#b = mapping_root( mapping, sorted_arr[index+1] )
			b = sorted_arr[index+1]
			if a != b:
				merge( G, a, b, outmap )
				mapping[b] = a
				merge_num += 1
			# if we are past our merge number, break.
			if merge_num >= num:
				break
				
		if merge_num >= num:
				break

'''------------------------------------------------
relabel(G,outmap)
- INPUT: A graph 'G', and the lsit 'outmap'.
- DOES: relabels the graph 'G' with integer nodes and changes 'outmap' accordingly.
- RETURNS: Nothing
------------------------------------------------'''
def relabel(G,H,outmap):
	# get the list of nodes in 'G'.
	G_nodes = sorted( list ( set( list( G.nodes() ) ) ) )
	H_nodes = sorted( list ( set( list( H.nodes() ) ) ) )

	# get the label dictionary.
	relabel_dict = {}
	index = 0
	for u in H_nodes:
		relabel_dict[u] = index
		index += 1

	# adjust outmap. <---- I dont think this needs to happen...
	index = 0
	for i in range( len(outmap) ):
		outmap[i] = relabel_dict[ outmap[i] ]

	# relabel H.
	K = nx.relabel_nodes( H, relabel_dict )

	# return our relabeled graph.
	return ( K )



'''------------------------------------------------
HWC(G,p,greedy=True)
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. It also takes in a boolean 'greedy' which controls how HWC coarsening proceeds.
- DOES: Performs merges according to the heavy-weight-clustering scheme.
- RETURNS: The coarsened graph H, and a mapping array 'outmap'.
- NOTES: If greedy is set to True it coarsens the optimal choice at every step according to W(u,v)/max(d_u,d_v). 
		 If it is false it performs coarsenings according to maximum weight matchings with respect to W(u,v).
- TODO: Putting in the approximate maximum / minimum matching.
------------------------------------------------'''
def HWC(G,p,greedy=True):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*(1-p) )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	if greedy == True:
		# for the defined number of coarsening levels.
		for i in range( levels ):
			edges = []
			weights = []
			# obtain all weights.
			for e in H.edges():
				denom = np.max([H.degree(e[0],weight="weight"),H.degree(e[1],weight="weight")])
				if e[0] != e[1]:
					edges.append( e )
					weights.append( H[e[0]][e[1]]["weight"]/denom )
			# sort weights.
			weights = np.array(weights)
			edges = np.array(edges)
			idx = weights.argsort()[::-1]
			weights = weights[idx]
			edges = edges[idx]
			# perform the optimal merge.
			merge(H,edges[0][0],edges[0][1],outmap)
	else:
		# initialize the remaining number of merges.
		rem = levels
		# while we still have nodes to coarsen
		while rem > 0:
			# set H parameters
			for e in H.edges():
				denom = np.max([H.degree(e[0],weight="weight"),H.degree(e[1],weight="weight")])
				H[e[0]][e[1]]["heavy"] = H[e[0]][e[1]]["weight"]/denom
			rem_nodes = len( H.nodes() )
			# perform a maximum matching
			# matching = sorted( nx.max_weight_matching( H, weight="heavy" ) )
			matching = util.fast_match( H, weight_name="heavy", mtype="max")
			# merge edges based on this matching
			# merge_many_edges(H,matching,rem,outmap)
			merge_many_nodes(H,matching,rem,outmap)
			rem = rem - ( rem_nodes - len( H.nodes() ) )

	H = relabel(G,H,outmap)

	return( H, outmap )

'''------------------------------------------------
ADC(G,p,k,vec_num,greedy=True)
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in the number of relaxation steps 'k', the number of vectors
		 to consider 'vec_num', and the boolean 'greedy', which controls how ADC proceeds.
- DOES: Performs merges according to the algebraic-distance-clustering scheme.
- RETURNS: The coarsened graph H.
- NOTES: If greedy is set to True it coarsens the optimal choice at every step according to the minimum
	     algebraic distance. If it is false it performs coarsenings according to minimum weight matchings 
	     with respect to algebraic distance. Currently this is very slow because computing the algebraic 
	     distance takes a long time and the networkx max_weight_matching function is n^3. 
- TODO: Add approximate maximum matching function.
------------------------------------------------'''
def ADC(G,p,k,vec_num,greedy=True):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*(1-p) )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	# if greedy is true.
	if greedy == True:
		rem = levels
		while rem > 0:
			# get nodes in H
			H_nodes = len( H.nodes() )
			X = np.zeros( ( H_nodes, vec_num ) )
			mapping = {}
			index = 0
			for node in H.nodes():
				mapping[node] = index
				index += 1
			# get our algebraic distances
			for vec in range( vec_num ):
				# initialize random vector.
				x = ( np.random.rand( H_nodes ) - 0.5 )
				x = x / np.linalg.norm( x, 2 )
				for i in range( k ):
					for u in H.nodes():
						x_tilde = 0
						for v in H.neighbors( u ):
							x_tilde += H[u][v]["weight"]*x[ mapping[v] ]
						x_tilde /= H.degree( u, weight="weight" ) 
						X[ mapping[u], vec ] = (x[ mapping[u] ]/2) + (x_tilde/2)
			# get our optimal edge for merging.
			edges = []
			weights = []
			for e in H.edges():
				if e[0] != e[1]:
					edges.append(e)
					weights.append( np.max( np.abs(X[ mapping[e[0]],:]-X[ mapping[e[1]],:]) ) )
			# sort weights and edges
			weights = np.array(weights)
			edges = np.array(edges)
			idx = weights.argsort()[::1]
			weights = weights[idx]
			edges = edges[idx]
			# perform the optimal merge.
			merge(H,edges[0][0],edges[0][1],outmap)
			rem -= 1

	# if greedy is false.
	else:
		# initialize the remaining number of merges.
		rem = levels
		# initialize a node mapping.
		mapping = {i:i for i in H.nodes()}
		# while we still have nodes to coarsen
		while rem > 0:
			# get nodes in H
			H_nodes = len( H.nodes() )
			X = np.zeros( ( H_nodes, vec_num ) )
			mapping = {}
			index = 0
			for node in H.nodes():
				mapping[node] = index
				index += 1
			# get our algebraic distances
			for vec in range( vec_num ):
				# initialize random vector.
				x = ( np.random.rand( H_nodes ) - 0.5 )
				x = x / np.linalg.norm( x, 2 )
				for i in range( k ):
					for u in H.nodes():
						x_tilde = 0
						for v in H.neighbors( u ):
							x_tilde += H[u][v]["weight"]*x[ mapping[v] ]
						x_tilde /= H.degree( u, weight="weight" ) 
						X[ mapping[u], vec ] = (x[ mapping[u] ]/2) + (x_tilde/2)

			# label edges with algebraic distances
			for e in H.edges():
				H[e[0]][e[1]]["algdist"] = np.max( np.abs(X[ mapping[e[0]],:]-X[ mapping[e[1]],:]) )
			rem_nodes = len( H.nodes() )
			# perform a maximum matching
			# matching = sorted( nx.min_weight_matching( H, weight="algdist" ) )
			matching = util.fast_match( H, weight_name="algdist", mtype="min")
			# merge edges based on this matching
			#merge_many_edges(H,matching,rem,outmap)
			merge_many_nodes(H,matching,rem,outmap)
			rem = rem - ( rem_nodes - len( H.nodes() ) )

	H = relabel(G,H,outmap)

	return( H, outmap )

'''------------------------------------------------
ASC(G,p,variant="greedy")
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in a parameter 'variant' controlling the version of ASC we do.
- DOES: Performs merges according to the adjacency-spectral-coarsening scheme.
- RETURNS: The coarsened graph H.
- NOTES: The 'variant' options are as follows:
		 - variant = "greedy" -- greedy method from Jin and Loukas
		 - variant = "matching" -- minimum matching variant
		 - variant = "means" -- k-means variant from Jin and Loukas 
- TODO: Putting in the approximate maximum / minimum matching. Also need to add "means" variant, but requires
		a k-means implementation first.
------------------------------------------------'''
def ASC(G,p,variant="greedy"):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*(1-p) )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	if variant == "greedy":
		# initialize remaining merges.
		rem = levels
		# perform our loop.
		while rem > 0:
			# initialize the minimum difference.
			min_diff = 3
			edge = (0,0)
			# compute the adjacency weights.
			for e in H.edges():
				if e[1] != e[0]:
					u = e[1]
					v = e[0]
					vect = {}
					for a in H.neighbors( u ):
						vect[a] = 0
					for b in H.neighbors( v ):
						vect[b] = 0
					for a in H.neighbors( u ):
						vect[a] += H[u][a]["weight"]/H.degree( u, weight="weight" )
					for b in H.neighbors( v ):
						vect[b] -= H[v][b]["weight"]/H.degree( v, weight="weight" )
					arr = np.array( list(vect.values()) )
					H[u][v]["adjdiff"] = np.linalg.norm( arr, 1 )
					# update our edge and min_diff (possibly)
					if H[u][v]["adjdiff"] < min_diff:
						edge = e
						min_diff = H[u][v]["adjdiff"]
			# perform our merge
			merge( H, edge[0], edge[1], outmap )
			rem -= 1

	elif variant == "matching":
		# initialize remaining merges.
		rem = levels
		# perform our loop.
		while rem > 0:
			# compute the adjacency weights.
			for e in H.edges():
				if e[1] != e[0]:
					u = e[1]
					v = e[0]
					vect = {}
					for a in H.neighbors( u ):
						vect[a] = 0
					for b in H.neighbors( v ):
						vect[b] = 0
					for a in H.neighbors( u ):
						vect[a] += H[u][a]["weight"]/H.degree( u, weight="weight" )
					for b in H.neighbors( v ):
						vect[b] -= H[v][b]["weight"]/H.degree( v, weight="weight" )
					arr = np.array( list(vect.values()) )
					H[u][v]["adjdiff"] = np.linalg.norm( arr, 1 )
			rem_nodes = len( H.nodes() )
			# perform a maximum matching
			# matching = sorted( nx.min_weight_matching( H, weight="adjdiff" ) )
			matching = util.fast_match( H, weight_name="adjdiff", mtype="min")
			# merge edges based on this matching
			#merge_many_edges(H,matching,rem,outmap)
			merge_many_nodes(H,matching,rem,outmap)
			rem = rem - ( rem_nodes - len( H.nodes() ) )

	elif variant == "means":
		print( "Still to do...")

	else:
		print( "Invalid variant... continuing..." )

	H = relabel(G,H,outmap)

	return( H, outmap )

'''------------------------------------------------
fiedler(G)
- INPUT: A graph 'G'.
- DOES: Splits G into two portions according to its Fiedler decomposition
- RETURNS: An array containing both partitions.
- NOTES: sub-function for GFSC. Has trouble returning correct clusters for "star-like" graphs.
------------------------------------------------'''
def fiedler(G):

	term_a = 0
	term_b = 0
	Gnodes = list( G.nodes() )
	evals = np.array([])

	# get the Laplacian of our graph.
	lap = nx.normalized_laplacian_matrix(G,nodelist=Gnodes,weight="weight").asfptype()
	# if the subgraph is large enough, use sparse matrices.
	if len( Gnodes ) > 3:
		evals,evecs = sp.sparse.linalg.eigs( lap, 2, which="SM", ncv=25 )
	# otherwise use dense operations.
	else:
		lap = lap.todense()
		evals,evecs = np.linalg.eig( lap )
	# compute the Fiedler vector.
	idx = evals.argsort()[::1]
	evals = evals[idx]
	evecs = evecs[:,idx]
	vect = evecs[:,1]

	# define a mapping.
	mapping = { Gnodes[i]:i for i in range(len(G.nodes())) }

	# get our nodal sets.
	clust1 = []
	clust2 = []
	for i in range( len(Gnodes) ):
		if vect[i] > 1e-10:
			clust1.append( Gnodes[i] )
		else:
			clust2.append( Gnodes[i] )

	# get subgraphs based on nodal sets.
	K1 = G.subgraph( clust1 )
	K2 = G.subgraph( clust2 )

	# if either subgraph is disconnected return an indicator as such.
	if nx.is_connected( K1 ) != True:
		term_a = 1
	if nx.is_connected( K2 ) != True:
		term_b = 1

	return( [ clust1, clust2 ], [ term_a, term_b ], vect, mapping )

'''------------------------------------------------
GFSC(G,p,objective="node")
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in a parameter 'objective', which controls how we greedily
		 choose our next partition to refine.
- DOES: Coarsens the graph G with respect to the greedy-fiedler-spectral-partitioning method.
- RETURNS: The coarsened graph H.
- NOTES: 'objective' can be the following.
		 - 'nodes' -- coarsen the partition with the most nodes in it.
		 - 'weight' -- coarsen the partition with the largest subgraph volume.
-- TODO: Sometimes generates disconnected graphs somehow. FIX THIS
------------------------------------------------'''
def GFSC(G,p,objective="node"):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.floor( nodes*p )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	# get our component list
	comp_list = [list(H.nodes())]
	# loop through our number of merges
	i = 0
	while i < ( levels-1 ):
		# if our objective is nodes...
		if objective == "node":
			# sort our component list based on lengths
			lengths = []
			for comp in comp_list:
				lengths.append( len(comp) )
			comp_list = [ comp for _,comp in sorted(zip(lengths,comp_list),reverse=True)]
			# form our next subgraph
			K = G.subgraph( comp_list[0] )

		# if our objective is subgraph weight...
		elif objective == "weight":
			break
		# if we have an invalid objective...
		else:
			break 

		[comp_one,comp_two],[term_a,term_b], vect, fied_map = fiedler( K )
		# if the components are disconnected, adjust the component lists accordingly.
		if term_a == 1:
			# get our subgraph and the connected components therin.
			W = K.subgraph( comp_one )
			a_comps = [ list(c) for c in nx.connected_components(W) ]

			# compute the average value of each connected component wrt "vect".
			comp_avs = []
			for component in a_comps:
				num = len( component )
				curr_sum = 0
				for node in component:
					curr_sum += vect[fied_map[node]]
				comp_avs.append( curr_sum/num )

			# redefine components.
			arg = np.argmax( comp_avs )
			comp_one = a_comps[ arg ]
			index = 0
			for comp in a_comps:
				if index != arg:
					comp_two = comp_two + comp
				index += 1

		# if the components are disconnected, adjust the component lists accordingly.
		if term_b == 1:
			# get our subgraph and the connected components therin.
			W = K.subgraph( comp_two )
			b_comps = [ list(c) for c in nx.connected_components(W) ]

			# compute the average value of each connected component wrt "vect".
			comp_avs = []
			for component in a_comps:
				num = len( component )
				curr_sum = 0
				for node in component:
					curr_sum += vect[fied_map[node]]
				comp_avs.append( curr_sum/num )

			# redefine components.
			arg = np.argmax( comp_avs )
			comp_two = a_comps[ arg ]
			index = 0
			for comp in a_comps:
				if index != arg:
					comp_one = comp_one + comp
				index += 1

		# change our component list.
		comp_list[0] = comp_one
		comp_list.append( comp_two )

		i += 1

	merge_many_nodes(H,comp_list,nodes,outmap)

	H = relabel(G,H,outmap)

	return( H, outmap )

'''------------------------------------------------
NDC(G,p,k,method="explicit",asc="greedy")
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in a parameter 'k', which controls the number of greedy refinements.
		 Also takes in a parameter 'method', which controls how we compute the nodal domains. Additionally
		 takes in a parameter 'asc' which controls the whether greedy, or minimum matching is used in the 
		 coarsening phase.
- DOES: Coarsens the graph G with respect to the nodal-partitioning method. 
- RETURNS: The coarsened graph H.
- NOTES: 'method' can take the following forms
		 - 'explicit' -- compute nodal domains using GMRES.
		 - 'fft' -- compute nodal domains using FFT method. Sahai et al. 2011
		 - 'dmd' -- compute nodal domains using DMD method. Sahai et al. 2022
		 'asc' can take the following forms
		 - 'greedy' -- performs the filtered greedy clustering
		 - 'matching' -- performs the clustering via minimum matching
- TODO: 
		 - asc='greedy' could be considerably faster, by not looping over all edges in each iteration.
		 - method='fft' needs to be added.
		 - method='dmd' needs to be added.

------------------------------------------------'''
def NDC(G,p,k,method="explicit",asc="greedy"):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*(1-p) )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	node_list = list( H.nodes() )
	# get the Laplacian of our graph.
	lap = nx.normalized_laplacian_matrix(H,nodelist=node_list,weight="weight").asfptype()
	# initialize the signs array.
	signs = np.array([])

	# if method is set to "explicit", compute the signs array using GMRES
	if method == "explicit":
		print( "	In NDC: computing eigenvalues..." )
		evals,evecs = sp.sparse.linalg.eigs( lap, k+1, which="SM", ncv=4*k+20, tol=1e-10 )
		idx = evals.argsort()[::1]
		evals = evals[idx]
		evecs = evecs[:,idx]
		signs = np.sign( np.array(evecs) )
		print( "	In NDC: eigenvalues computed..." )

	# compute the connected components of each nodal domain as "new_dict"
	nodal_dict = {}
	row_names = []
	count = 0
	for row in signs:
		row_name = np.array2string( row.astype(int) )
		if row_name in row_names:
			nodal_dict[ row_name ].append( count )
		else:
			row_names.append( row_name )
			nodal_dict[ row_name ] = [ count ]
		count += 1

	new_dict = {}
	curr_key = 0
	for key in nodal_dict:
		components = [ list(c) for c in sorted(nx.connected_components(H.subgraph(nodal_dict[key])), key=len, reverse=True) ]
		for comp in components:
			new_dict[curr_key] = sorted( comp )
			curr_key += 1

	# if we have too many nodal domains for the number of merges, we reduce the number of eigenvectors.
	val_num = k-1
	poss_merges = 0
	for key in new_dict.keys():
		comp = new_dict[key]
		poss_merges += (len(comp) - 1)

	#while len(new_dict) > (nodes-levels):
	while poss_merges < (levels):

		print(" 	too many nodal sets, refining: k="+str(val_num))
		evecs = evecs[:,0:val_num]
		signs = np.sign( np.array(evecs) )
		nodal_dict = {}
		row_names = []
		count = 0
		for row in signs:
			row_name = np.array2string( row.astype(int) )
			if row_name in row_names:
				nodal_dict[ row_name ].append( count )
			else:
				row_names.append( row_name )
				nodal_dict[ row_name ] = [ count ]
			count += 1

		new_dict = {}
		curr_key = 0
		for key in nodal_dict:
			components = [ list(c) for c in sorted(nx.connected_components(H.subgraph(nodal_dict[key])), key=len, reverse=True) ]
			for comp in components:
				new_dict[curr_key] = sorted( comp )
				curr_key += 1
		val_num -= 1

		poss_merges = 0
		for key in new_dict.keys():
			comp = new_dict[key]
			poss_merges += (len(comp) - 1)


	print( " 	labels set...")

	# if asc is set to "greedy", loop through our greedy coarsening.
	if asc == "greedy":
		rem = levels
		while rem > 0:
			# initialize the minimum difference and initial nodes.
			min_diff = 3
			edge = (0,0)
			# for every nodal intersection
			for key in new_dict.keys():
				# get the component 
				comp = new_dict[key]
				# If the component has enough nodes in it 
				if len( comp ) > 1:
					# form a subgraph from those nodes
					K = H.subgraph( comp )
					# go through every edge in  K
					for e in K.edges():
						# if an edge is not a self loop
						if e[1] != e[0]:
							u = e[1]
							v = e[0]
							vect = {}
							# compute the L1 norm along the edge.
							for a in H.neighbors( u ):
								vect[a] = 0
							for b in H.neighbors( v ):
								vect[b] = 0
							for a in H.neighbors( u ):
								vect[a] += H[u][a]["weight"]/H.degree( u, weight="weight" )
							for b in H.neighbors( v ):
								vect[b] -= H[v][b]["weight"]/H.degree( v, weight="weight" )
							arr = np.array( list(vect.values()) )
							diff = np.linalg.norm( arr, 1 )
							# update our edge and min_diff (possibly)
							if diff < min_diff:
								min_diff = diff
								edge = e

			merge( H, edge[0], edge[1], outmap )
			rem -= 1
	# if asc is set to "matching", perform the minimum matching variant.
	elif asc == "matching":
		# set the levels
		rem = levels
		# while we have levels to go
		while rem > 0: 
			# initialize an edge array
			edges = []
			# fill the edge array with possible merge edges
			for comp in new_dict.values():
				K = H.subgraph( comp )
				Ke = []
				for e in K.edges():
					if e[0] != e[1]:
						Ke.append( e ) 
				edges.extend(Ke)

			# create a new disconnected graph for coarsening.
			K = nx.Graph()
			K.add_nodes_from( list( H.nodes() ) )
			K.add_edges_from( edges )
			index = 0
			for (u,v) in edges:
				if u != v:
					(u,v) = tuple( sorted( (u,v) ) )
					vect = {}
					# compute the L1 norm along the edge.
					for a in H.neighbors( u ):
						vect[a] = 0
					for b in H.neighbors( v ):
						vect[b] = 0
					for a in H.neighbors( u ):
						vect[a] += H[u][a]["weight"]/H.degree( u, weight="weight" )
					for b in H.neighbors( v ):
						vect[b] -= H[v][b]["weight"]/H.degree( v, weight="weight" )
					arr = np.array( list(vect.values()) )
					K[u][v]["weight"] = np.linalg.norm( arr, 1 )
				index += 1
			# get the remaining number of nodes
			rem_nodes = len( H.nodes() )
			# perform a maximum matching
			matching = util.fast_match( K, weight_name="weight", mtype="min")
			# merge edges based on this matching
			# merge_many_edges(H,matching,rem,outmap)
			merge_many_nodes(H,matching,rem,outmap)
			rem = rem - ( rem_nodes - len( H.nodes() ) )
			print( rem )

	H = relabel(G,H,outmap)
	print( type(outmap) )
	return( H, outmap )

'''------------------------------------------------
METIS(G,p)
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph.
- DOES: Coarsens the graph G with respect to the METIS method. 
- RETURNS: The coarsened graph H.
- NOTES: If p is too large, the metis partition will be smaller than expected.
- TODO: Need to figure out / fix why the metis partition is wrong for large p -> 1. 
		- seems to be mentioned in the documentation (metis.readthedocs.io/en/latest)
------------------------------------------------'''
'''
def METIS(G,p):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*(p) )
	# copy our graph.
	H = G.copy()
	# initialize out_map.
	outmap = [i for i in range(len(G.nodes()))]

	# change the graph to metis.
	K = metis.networkx_to_metis( H )
	# partition the metis graph.
	(edgecuts,parts) = metis.part_graph( G,nparts=levels,objtype="vol" )

	# obtain a mapping to define our partitions for coarsening.
	part_list = list( set( parts ) )
	part_num = len( set(parts) )
	part_list_relabel = list( np.arange( part_num ) )
	mapping = {part_list[i]:part_list_relabel[i] for i in range(part_num)}
	
	# define our clusters.
	clusters = []
	for i in range( part_num ):
		clusters.append([])
	index = 0
	for node in parts:
		clusters[mapping[node]].append(index)
		index += 1

	# merge our clusters.
	merge_many_nodes(H,clusters,nodes,outmap)

	H = relabel(G,H,outmap)

	return( H, outmap )
'''
'''------------------------------------------------
KRON(G,p)
- INPUT: A graph 'G', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph.
- DOES: Coarsens the graph G with respect to the Kron reduction method. 
- RETURNS: The coarsened graph H.
- NOTES: Computes intersections of signs of eigenvectors to decide the candidate nodes 
		 for coarsening. 
- TODO: Need to see if we can come up with any theory reguarding how this method preserves
		the normalized spectrum, all the theory is for the combinatorial Laplacian.
		- Additionally there is no way to map this to the lift. This makes spectral comparisons
		  a little tough.
		  -- maybe we can just manually align these eigenvalues.
------------------------------------------------'''
def KRON(G,p):
	# get our number of nodes.
	nodes = len( G.nodes() )
	# define the number of partitions we want.
	levels = math.ceil( nodes*p )
	# copy our graph.
	H = G.copy()
	L = nx.laplacian_matrix( H, weight="weight" ).asfptype()

	# if method is set to "explicit", compute the signs array using GMRES
	val,vec = sp.sparse.linalg.eigs( L, 1, which="LM", ncv=25, tol=1e-10 )
	signs = np.sign( np.array(vec) )
	#L = L.todense()

	# compute the nodal domains
	pos_nodes = []
	neg_nodes = []
	for i in range( len(signs) ):
		if signs[i] < 0:
			neg_nodes.append( i )
		else:
			pos_nodes.append( i )

	# select the smallest one to be the one we coarsen wrt.
	if len(neg_nodes) < len(pos_nodes):
		temp_nodes = [ val for val in neg_nodes ]
		neg_nodes = [ val for val in pos_nodes ]
		pos_nodes = [val for val in temp_nodes ]


	# reduce the nodes in the smallest one to be the correct size.
	if len(pos_nodes) > levels:
		temp_nodes = list( p.array(pos_nodes)[levels:len(pos_nodes)] )
		pos_nodes = list( np.array(pos_nodes)[0:levels] )
		neg_nodes = neg_nodes + temp_nodes

	''' SPARSE MATRIX VERSION '''
	# form our submatrices
	L_11 = L[pos_nodes,:][:,pos_nodes]
	L_22 = L[neg_nodes,:][:,neg_nodes]
	L_d = L[pos_nodes,:][:,neg_nodes]

	# Get our Laplacian and adjacency matrices
	print( "	Obtaining Laplacian...")
	red_L = L_11 - ( L_d.dot( sp.sparse.linalg.inv(L_22) ) ).dot( L_d.transpose() )
	print( "	Obtaining adjacency...")
	red_A = (-(red_L - sp.sparse.diags( sp.sparse.csr_matrix.diagonal(red_L) ))).todense()

	# compute the new graph from the numpy matrix
	print("		Forming networkx graph...")
	H = nx.from_numpy_matrix( red_A )

	# give each node a node label "num", unfortunately this is devoid of meaning for KRON reduction
	for u in H.nodes():
		H.nodes[u]["num"] = 1

	# initialize a fill in outmap (mapping doesnt naturally make sense for Kron reduction)
	outmap = sorted( list(G.nodes()) )
	H = nx.convert_node_labels_to_integers( H )

	return( H, outmap )


if __name__ == "__main__":


	print( "Coarsening a random partition graph with p=0.5...")
	print()
	G = nx.random_partition_graph( [1000,1000,1000],0.1,0.05 )
	for u in G.nodes():
		G.nodes[u]["num"] = 1
	for e in G.edges():
		G[e[0]][e[1]]["weight"] = 1 
	size = 0.5

	start = time.time()
	H,omap = HWC(G,size,greedy=False)
	end = time.time()
	print("HWC time is: "+str(end-start))

	print("HWC:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	'''
	H,omap = ADC(G,size,20,20,greedy=True)
	print("ADC:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	H,omap = ASC(G,size,variant="matching")
	print("ASC:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	H,omap = GFSC(G,size,objective="weight")
	print("GFSC:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	H,omap = NDC(G,size,1,method="explicit",asc="matching")
	print("NDC:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	H,omap = METIS(G,size)
	print("METIS:")
	print("-- mapping --")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()

	H,omap = KRON(G,size)
	print("KRON:")
	print("-- mapping (meaningless)--")
	print( omap )
	print("-- nodes --")
	print( list(H.nodes()) )
	print()
	'''