import numpy as np
import cupy as cp

import dmd
import dmdCUDA

import time

def warm_up():
	A = cp.array([[i for i in range(j,j+500000)] for j in range(10)], dtype=cp.float64)
	dmdCUDA.DMD4cast(A,2,1)

warm_up()
#C = A.copy()
m = 10
d = 128
A = cp.empty([m]+ [d,d])
for i in range(m):
	A[i] = cp.arange(0,d**2).reshape([d,d])


B = cp.asnumpy(A)
print(A.shape)

#breakpoint()

N=100
start = time.time()
for x in range(N):
	dmdCUDA.DMD4cast(A.reshape([m,d**2]),2,1)
#ub, sb, vb = cp.linalg.svd(A, full_matrices = False)
dur_gpu = time.time() - start

start = time.time()
for x in range(N):
	dmd.DMD4cast(B.reshape([m,d**2]),2,1)
#ua, sa, va = np.linalg.svd(B, full_matrices = False)
dur_cpu = time.time() - start



print(f"CPU: {dur_cpu}, GPU:{dur_gpu}")