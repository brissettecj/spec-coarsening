import scipy as sp
import scipy.io
import numpy as np
import networkx as nx

names = ["08blocks", "662_bus", "685_bus", "1138_bus", "ABACUS_shell_hd", "Alemdar", "bloweybq", "bp_0", "bp_1200","cage", "cage10", "CAG_mat364", "cavity01", "cavity05", "cavity14", "Chebyshev1", "crack", "CSphd", "delaunay_n10","dw1024", "dw4096", "dwt_307", "EVA", "flowmeter0", "fs_541_3", "jagmesh2", "jagmesh5", "jagmesh9", "Kaufhold","laser", "mahindas", "Reuters911", "SmaGri", "SmallW", "Trefethen_150","Trefethen_2000", "TSOPF_FS_b9_c6", "TSOPF_RS_b162_c1"]

for name in names:

	print(name)
	filename = "./data/mtx/"+name+".mtx"
	fh = open( filename, "r" )
	A = sp.io.mmread( fh )
	print( np.shape(A) )
	fh.close()

	G = nx.from_scipy_sparse_matrix( A )
	G = nx.convert_node_labels_to_integers( G )
	outpath = "./data/"+name+".txt"
	fh = open( outpath, "wb" )
	nx.write_edgelist( G, fh, data=False )
	fh.close()
	print()

