import cupy as cp 

def DMD4cast( weights ):

	# Get the dimension of weights.
	d = weights.shape
	
	# First reorganize the weights.
	data = weights.reshape()