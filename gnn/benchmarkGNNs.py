import sys, os
from os import path
cur = os.path.dirname(os.path.dirname(path.realpath(__file__)))
sys.path.append( cur)

import random
import time

import torch
from torch import optim
import torch.nn.functional as F
from torch.nn import CrossEntropyLoss, NLLLoss
import torch_geometric.datasets as tds

import numpy as np
import pandas as pd

import yaml
import argparse
from tqdm import tqdm
import matplotlib.pyplot as plt
import dominate
from dominate import tags

import gnnutils as utils
from gnn import *
import gnntraindmd, gnntraindmdCUDA


def main(args):

	dataset = args.dataset
	cratio  = args.cratio
	method  = args.method
	levels  = args.levels
	epochs  = args.epochs#[int(e) for e in args.epochs]
	lr 		= args.lr
	weight_decay=args.weight_decay
	device = args.device
	opt = args.optimizer
	loss 	= args.loss
	hidden	= args.hidden
	m 		= args.m
	r 		= args.r
	pred_step= args.pred_step
	model   = args.model
	train_level=0
	runs = args.runs

	# SELECT DEVICE IF NOT PROVIDED
	device = ('cuda' if torch.cuda.is_available() else 'cpu') if device is None else device
	#device = 'cuda'
	args.device = device
	torch.device(device)
	### LOAD AND SELECT THE GRAPH ###
	if dataset == 'ALL':
		cora = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')
		print(f"CORA loaded:\n\t{cora[0].__repr__()}")
		pubmed = tds.Planetoid(root="tmp/PubMed//", name='PubMed')
		print(f"PUBMED loaded:\n\t{pubmed[0].__repr__()}")
		citeseer = tds.Planetoid(root="tmp/PubMed//", name='Citeseer')
		print(f"CITESEER loaded:\n\t{citeseer[0].__repr__()}")
		TGG = [cora[0], pubmed[0], citeseer[0]]
	elif dataset == 'cora':
		cora = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')
		print(f"CORA loaded:\n\t{cora[0].__repr__()}")
		TGG = [cora[0]]
	elif dataset == 'pubmed':
		pubmed = tds.Planetoid(root="tmp/PubMed//", name='PubMed')
		print(f"PUBMED loaded:\n\t{pubmed[0].__repr__()}")
		TGG = [pubmed[0]]
	elif dataset == 'citeseer':
		citeseer = tds.Planetoid(root="tmp/PubMed//", name='Citeseer')
		print(f"CITESEER loaded:\n\t{citeseer[0].__repr__()}")
		TGG = [citeseer[0]]
	#--------------------------------------------------------------------#
	#PICK THE OPTIMIZER
	if opt == 'adam':
		optimizer = optim.Adam#(model.parameters(), lr=lr, weight_decay=5e-10)
	else:
		raise Exception(f"optimizer {opt} not supported.")

	### PICK THE LOSS ###
	if loss.lower() in ['crossentropyloss','cel']:
		loss = CrossEntropyLoss()
	else:
		loss = NLLLoss()
	#----------------------------------------------#
	random.seed(400)

	seeds = [random.randint(0,10000) for _ in range(runs)]

	### FIRST RUN ###
	start = time.time()
	benchmark(TGG[0], model, cratio, method, levels, train_level, epochs, lr, weight_decay, 
			  device, optimizer, loss, hidden,m,r,pred_step,verbose=True, seed=42)
	time.sleep(.1)
	print(f"FIRST RUN DURATION: {time.time()-start}")
	### PERFORM RUN(S) ###
	dfs = []
	for i in range(runs):
		print(f"\nRUN #: {i+1}")
		df = benchmark(TGG[0], model, cratio, method, levels, train_level, epochs, lr, weight_decay, 
			  device, optimizer, loss, hidden,m,r,pred_step,verbose=runs<2, seed=seeds[i])
		df['level'] = df['level'].astype(int)
		dfs.append(df)
	#--------------------------------------------#
	var_cols = ['test_accuracy','test_loss','train_accuracy','train_loss',
				'step_time','level_time']
	### BENCHMARK RUN ###
	bdfs = []
	
	if args.benchmark:
		print("\n---------  BENCHMARKING  ---------")

		for i in range(runs):
			print(f"\nRUN #: {i+1}")
			bdf = benchmark(TGG[0], model, cratio, method, levels, train_level, epochs, lr, weight_decay, 
				  device, optimizer, loss, hidden,None,None,None,verbose=runs<2, seed=seeds[i])
			bdfs.append(bdf)
		sum_bdf = pd.DataFrame(0, columns=dfs[0].columns, index=df.index)
		for i in range(runs):
			sum_bdf += bdfs[i][var_cols]

		sum_bdf = sum_bdf / runs
	#----------------------------------------------#
	extremes = pd.DataFrame(0, columns=dfs[0].columns, index=['max','min']).drop(columns=['optim','level'], axis=0)
	for i in range(runs):
		extremes.loc['max'] += dfs[i].max(axis=0).drop(columns=['optim','level'], axis=0)
		extremes.loc['min'] += dfs[i].min(axis=0).drop(columns=['optim','level'], axis=0)

	#breakpoint()
	
	sum_df = pd.DataFrame(0, columns=dfs[0].columns, index=df.index)
	for i in range(runs):
		sum_df += dfs[i][var_cols]

	sum_df = sum_df / runs

	sum_df['level'] = df['level']
	sum_df['optim'] = df['optim']

	avg_df = sum_df[['level']+var_cols+['optim']]

	extremes = extremes
	extremes = extremes / runs
	extremes.index = ['--max--', '--min--']
	#Write results to .html file

	doc = dominate.document()

	run_id = ""
	for param in [dataset, model, cratio, method, levels, train_level, epochs,
			 lr, weight_decay, device, opt, type(loss).__name__, hidden,
			 m,r,pred_step]:
		run_id += (str(param)) + '_'

	try:
		os.mkdir("results")
	except FileExistsError:
		pass
	try:
		os.mkdir(f"results/{run_id}")
	except FileExistsError:
		pass

	### MAKE PLOTS ###
	fig, ax = plt.subplots()
	df = sum_df
	for col, color in [('test_accuracy','red'),( 'train_accuracy','blue')]:
		ax.plot(df["level_time"],df[col].round(3), label=col, c=color)

	if args.benchmark:
		ax.plot(sum_bdf["level_time"],sum_bdf['test_accuracy'], label=opt, c='maroon')

	### PLOT DMD LINES ###	
	dmd_df = df[df['optim']=='DMD']
	print(dmd_df['step_time'])
	#breakpoint()
	for i in range(len(dmd_df)):
		ax.axvspan(dmd_df.iloc[i]['level_time']-dmd_df.iloc[i]['step_time'],
						dmd_df.iloc[i]['level_time'],color = 'green', alpha=.2)

	handles, labels = ax.get_legend_handles_labels()
	by_label = dict(zip(labels, handles))
	ax.legend(by_label.values(), by_label.keys())
	plt.savefig(f"results/{run_id}/acc_fig.png")

	fig, ax = plt.subplots()
	for col, color in [('test_loss','red'),( 'train_loss','blue')]:
		ax.plot(df["level_time"],df[col], label=col, c=color)

	for i in range(len(dmd_df)):
		ax.axvspan(dmd_df.iloc[i]['level_time']-dmd_df.iloc[i]['step_time'],
						dmd_df.iloc[i]['level_time'],color = 'green', alpha=.2)
	if args.benchmark:
		ax.plot(sum_bdf["level_time"],sum_bdf['test_loss'], label=opt, c='maroon')
	


	handles, labels = ax.get_legend_handles_labels()
	by_label = dict(zip(labels, handles))
	ax.legend(by_label.values(), by_label.keys())
	plt.savefig(f"results/{run_id}/loss_fig.png")

	### END MAKE PLOTS ###


	with open(f"results/{run_id}/df.yaml", 'w') as file:
	    yaml.dump({'result': df.to_dict(orient='records')}, file, default_flow_style=False)


	doc = dominate.document(title='Title!')

	with doc.head:
		tags.style("""\
			#plots {
			display: table;
			clear: both;
			}
			""")
	info = vars(args)

	header = tags.div('Hyper-Params')

	i = 0
	for key, value in info.items():
		header.add(tags.div( f"{key}: {value}"))
		i += 1
	imgs = [ 
		tags.img(src=f'{run_id}/acc_fig.png'),
		tags.img(src=f'{run_id}/loss_fig.png'),

		]



	plots = tags.div(id_='plots')

	data = tags.div(df.to_html())

	doc.add(header)
	plots.add(imgs)
	doc.add(plots)
	#doc.add(tags.table(df.to_html()))
	#print(df.to_html())
	def highlight_max(s):
		is_max = s == s.max()
		return ['color: green' if cell else '' for cell in is_max]
	def highlight_min(s):
		is_min = s == s.min()
		return ['color: green' if cell else '' for cell in is_min]		

	max_subset = ['test_accuracy', 'train_accuracy']
	min_subset = ['test_loss', 'train_loss', 'step_time',]
	df_view = avg_df.style.highlight_max(
					color='lightgreen', axis=0, subset=max_subset).highlight_min(
					color='red', axis=0, subset=max_subset).highlight_max(
					color='red', axis=0, subset=min_subset).highlight_min(
					color='lightgreen', axis=0, subset=min_subset).set_table_styles(
				    [{"selector": "", "props": [("border", "1px solid grey")]},
				      {"selector": "tbody td", "props": [("border", "1px solid grey")]},
				     {"selector": "th", "props": [("border", "1px solid grey")]}
				    ])

	table_html = df_view.to_html(border=1)
	with open(f"results/{run_id}.html",'w') as fd:
		fd.write(doc.__str__()+extremes.to_html()+table_html)
		print(f"file://{os.getcwd()}/results/{run_id}.html")

def benchmark(TGG, model_type, cratio, method, levels, train_level, epochs, 
			  lr, weight_decay, device, optimizer, loss, hidden,m,r,p,verbose=True,seed=None):	

	#Coarsening
	TGG_D,_,t = utils.geomat_to_multilevel(TGG, cratio, method, levels, timing=True)

	gtd = gnntraindmd if device == 'cpu' else gnntraindmdCUDA

	#Initialize Model, Optimizer, and Loss
	num_features = TGG_D[0].x.shape[1]
	num_classes  = TGG_D[0].y.unique().shape[0]

	print("SEEDWORLD: "+str(seed))
	if model_type == 'GNN':
		model = GNN(num_features, hidden, num_classes,seed=seed).to(device)
	elif model_type == 'Net':
		model = Net(num_features, hidden, num_classes,seed=seed).to(device)

	optimizer = optimizer(model.parameters(), lr=lr,weight_decay=weight_decay)
	optims, stats = gtd.train_level(model, TGG_D, optimizer, loss, train_level,
							epochs=epochs, device=device, verbose=verbose,
							m=m,pred_step=p,r=r)
	df = pd.DataFrame(stats, columns=gtd.train_level.headers)
	df['optim'] = pd.Series(optims)

	return df




if __name__ == '__main__':
	global model
	parser = argparse.ArgumentParser()
	parser.add_argument('-d','--dataset',choices=['cora','pubmed','citeseer', 'ALL'], default='cora')
	parser.add_argument('-c','--cratio',type=float)
	parser.add_argument('--method',choices=['NONE','HWC','ADC','ASC','NDC'], default='NONE')
	parser.add_argument('-l','--levels',type=int, default=1)
	parser.add_argument('-e','--epochs',type=int, default=30)
	parser.add_argument('-lr','--lr',type=float, default=.001)
	parser.add_argument('-w','--weight_decay', type=float,default=0)
	parser.add_argument('-v','--device', choices=['cpu','cuda'], default=None)
	parser.add_argument('-o','--optimizer',choices=['adam'], default='adam')
	parser.add_argument('--loss',choices=['cel',"nll"], default='cel')
	parser.add_argument('--hidden',type=int,default=64)
	parser.add_argument('-m','--m', type=int)
	parser.add_argument('-r','--r', type=int)
	parser.add_argument('-p','--pred_step', type=int)
	parser.add_argument('--train_level', type=int, default=0)
	parser.add_argument('--model', type=str, default='GNN')
	parser.add_argument('--runs', type=int, default=1)
	parser.add_argument('-B', '--benchmark', action='store_true')


	args = parser.parse_args()
	print(args)

	main(args)