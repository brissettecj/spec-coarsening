# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 13:06:26 2022

"@author: William
"""

import numpy as np

def DMD2(W, ft=None, r=None):

    global Wminus,Wplus,Lambda,Y,psi,w, S_r_inv, U,S,A,V
    Wminus = W[:, :-1]
    Wplus = W[:, 1:]
    
    # Comput lowcost SVD decomposition
    U,S,V = np.linalg.svd(Wminus, full_matrices=False)
    V = V.T
    
    #calculate r
    if r is None:
        r = 0
        for r in range(1,S.shape[0]):
            if S[r-1,r-1]/S[r,r] > ft:
                break
    #Build reduced koopman operator (3)
    S_r_inv = np.reciprocal(S[:r])
    A = U[:r].T@Wplus
    A = A@V[:r]
    A = A@S_r_inv
    
    # Perform eigendecomposition of the reduced 
    # Koopman operator with (4)
    Lambda,Y = np.linalg.eig(A)
    
    #compute the matrix of weights modes
    psi = Wplus @ V[:r] @ np.diag(S_r_inv) @ Y
    
    w = psi @ np.diag(Lambda) @ np.linalg.pinv(psi)
    
       
    return w, Lambda, A
"""
def DMD(data, r):
    Dynamic Mode Decomposition (DMD) algorithm.
    
    global X1,X2,u,s,v, A_tilde,Phi,Q,Psi
    
    ## Build data matrices
    X1 = data[:, : -1]
    X2 = data[:, 1 :]
    ## Perform singular value decomposition on X1
    u, s, v = np.linalg.svd(X1, full_matrices = False)
    s = np.matrix(s)
    ## Compute the Koopman matrix
    A_tilde = u[:, : r].conj().T @ X2 
    A_tilde = A_tilde @ v[: r, :].conj().T 
    A_tilde = np.multiply(A_tilde, np.reciprocal(s[: r]))
    ## Perform eigenvalue decomposition on A_tilde
    Phi, Q = np.linalg.eig(A_tilde)
    ## Compute the coefficient matrix
    Psi = X2 @ v[: r, :].conj().T @ np.diag(np.reciprocal(s[: r])) @ Q
    A = Psi @ np.diag(Phi) @ np.linalg.pinv(Psi)
    
    return A_tilde, Phi, A
"""
def DMD(data, r):
    """Dynamic Mode Decomposition (DMD) algorithm."""
    
    ## Build data matrices
    X1 = data[:, : -1]
    X2 = data[:, 1 :]
    ## Perform singular value decomposition on X1
    u, s, v = np.linalg.svd(X1, full_matrices = False)
    ## Compute the Koopman matrix
    A_tilde = u[:, : r].conj().T @ X2 @ v[: r, :].conj().T * np.reciprocal(s[: r])
    ## Perform eigenvalue decomposition on A_tilde
    Phi, Q = np.linalg.eig(A_tilde)
    ## Compute the coefficient matrix
    Psi = X2 @ v[: r, :].conj().T @ np.diag(np.reciprocal(s[: r])) @ Q
    A = Psi @ np.diag(Phi) @ np.linalg.pinv(Psi)
    
    return A_tilde, Phi, A


def DMD4cast(data, r, pred_step):
    N, T = data.shape
    _, _, A = DMD(data, r)
    mat = np.append(data, np.zeros((N, pred_step)), axis = 1)
    for s in range(pred_step):
        mat[:, T + s] = (A @ mat[:, T + s - 1]).real
    return mat[:, - pred_step :]

def DMDstep(model, weights, r, pred_step, params=None, verbose=True):
    if params is None:
        params = [i for i in range(len(weights))]
        
    for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
        if i in params:
            start = time.time()
            M = W.reshape(W.shape[0], np.prod(W.shape[1:])).T
            new_weight = DMD4cast(M, r, pred_step)[:,-1].T
            new_weight = new_weight.reshape(W[0].shape)

            param.data = Tensor(new_weight)
            if verbose:
                print(f"Step {i}: {time.time()-start}")