import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import scipy.spatial as spat
import networkx as nx
import math

import util
import time


'''------------------------------------------------
l1_approx(M,delta)
- INPUT: A tall numpy matrix 'M', where each row is an embedding of a node in the graph (for ADC and ASC coarsening).
		 Also takes in a real number 'delta', which upper bounds the size of the distance approximation.
		 Additionally takes in the parameter 'prop', which upper bounds the number of performed merges.
- DOES: Finds nodes which are approximately close given this embedding. The method is described in more detail in
		the NOTES section.
- RETURNS: A mask from the given 
- NOTES: 
	This method works be dividing a portion of $R^n$ into a grid of hypercubes with side length 'delta', or less.
	The mask that is returned is the assignment of each node to one of these hypercubes. The idea is that the L1 
	distance between nodes within each hypercube is bounded above by 'delta*n'. The benefit is that if M.shape = (n,k)
	this can be performed in O(nk) as opposed to an explicit distance between all nodes which is O(n^2).
------------------------------------------------'''
def l1_approx(M,delta):

	# perform integer division on all of M by delta.
	M_div = np.floor_divide(M.copy(),delta).astype(int)

	# turn each row into a hashable datatype.
	hashable = [ a.tobytes() for a in M_div ]
	indices = np.arange(len(hashable)).astype(int)

	# get the unique entries in hashable.
	vals,inverse,counts = np.unique(hashable,return_counts=True,return_inverse=True)

	# fill a dictionary with the correct nodes for each bytestring. (Removing the for loop would be good.)
	merge_dict = { vals[i]:(np.zeros(counts[i]).astype(int)) for i in range(len(counts)) }
	counts = np.zeros(len(vals)).astype(int)
	for ind in range(len(inverse)):
		merge_dict[vals[inverse[ind]]][counts[inverse[ind]]] = ind 
		counts[inverse[ind]] += 1

	# now form the mask matrix. (Again, removing the for loop here would be good.)
	mask = np.zeros((M.shape[0],M.shape[0]))
	for key in merge_dict.keys():
		vect = np.zeros(M.shape[0])
		vect[merge_dict[key]] = 1
		mask = mask + np.outer(vect,vect)

	# return mask
	return(mask)

'''------------------------------------------------
matching(M,minimize=True)
- INPUT: A symmetric scipy csr matrix 'M', and a boolean value 'minimize', which controls what kind of 
		 matching we perform.
- DOES: Finds a mask of the M matrix. Finds a minimum or maximum spanning tree and then performs a minimum or
		maximum matching to obtain the mask.
- RETURNS: the bipartite matching as an array.
- NOTES: 
	-- if 'minimum' is True : minimize the weight sum in our matching according to metric matrix M.
	-- if 'minimum' is False : maximize the weight sum in our matching according to metric matrix M.
------------------------------------------------'''
def matching(M,minimize=True):

	if minimize:

		# Take the upper triangle of M to compute the mst in the next step.
		T = sp.sparse.triu(M)

	else:

		# Construct a matrix for inverting metric values.
		maxval = 2*np.amax(M)
		Q = M.copy()
		Q[M.nonzero()] = maxval

		# now we alter P to make the smallest elements the largest, and visa versa. 
		# We also take the upper triangle of it to compute the mst in the next step.
		T = sp.sparse.triu(Q - M)

	# now we obtain a minimum spanning tree, and represent it as an adjacency matrix.
	mst = sp.sparse.csgraph.minimum_spanning_tree(T)
	mst = mst + mst.transpose()

	# Mask the values of the mst.
	Mt = M.multiply(mst)

	# now we perform a maximum bipartite matching.
	if minimize:

		# Construct a matrix for inverting values of the mst masked metrics.
		maxval = 2*np.amax(Mt)
		Q = Mt.copy()
		Q[Mt.nonzero()] = maxval

		# Invert the values of the mst masked matrix.
		Mt = Q - Mt

	mbm = sp.sparse.csgraph.maximum_bipartite_matching(Mt,perm_type='row')

	return( mbm )

'''------------------------------------------------
get_coarsening_matrix(dim,mbm,rem)
- INPUT: The matching given by matching, 'mbm', and the 
		 number of remaining merges 'rem'.
- DOES: Obtains a coarsening matrix with the desired properties.
- RETURNS: A scipy csr coarsening matrix (left coarsening matrix; can be transposed for right matrix).
------------------------------------------------'''
def get_coarsening_matrix(mbm,rem):
	
	# from the mbm, we form the coarsening matrix and apply it to P.
	I = sp.sparse.csr_matrix( sp.sparse.identity(len(mbm)) ) 

	# alter mbm to be in a better format for our uses.
	indices = np.arange(len(mbm))
	where = np.argwhere(mbm == -1)
	mbm[where] = indices[where]

	# get our base node and merge node arrays.
	bases = np.argwhere(mbm >= indices).flatten()
	merges = mbm[bases]

	# form the left coarsening matrix.
	Cl = I[bases] + I[merges]
	Cl[Cl.nonzero()] = 1

	return( Cl )

'''------------------------------------------------
HWC(A,p)
- INPUT: A scipy graph adjacency 'A', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph.
- DOES: Coarsens the graph G with respect to the heavy weight coarsening method. 
- RETURNS: The coarsened graph matrix W.
------------------------------------------------'''
def HWC(A,p,timeit=True):

	B = A.copy()
	Clold = sp.sparse.identity(B.shape[0])

	outer_start = 0 
	inner_start = 0
	outer_end = 0 
	inner_end = 0

	if timeit:
		print("HWC: outer loop starting... ")
		outer_start = time.time()

	while (B.shape[0]) > (A.shape[0]*p):

		if timeit:
			inner_start = time.time()

		# now we get the row sums and turn them into a matrix.
		row_sums = np.reciprocal(np.sqrt(np.array(B.sum(axis=0)).astype(float).flatten()))
		d = sp.sparse.diags(row_sums)

		# now we apply this matrix to A to get our transition matrix, and we get weights from this.
		P = d.dot(B).dot(d)
		mbm = matching(P,minimize=True)

		# get our left-coarsening matrix, and form the right coarsening matrix as well.
		Cl = get_coarsening_matrix(mbm,0)
		Clold = Cl*Clold
		Cr = Cl.transpose()

		# coarsen.
		B = Cl*B*Cr

		if timeit:
			inner_end = time.time()
			print("	>> HWC: inner loop time: "+str( inner_end - inner_start ))

		if np.all( mbm == np.arange(len(mbm)) ):
			break

	if timeit:
		outer_end = time.time()
		print("HWC: outer loop time: "+str( outer_end - outer_start ))

	return(B,Clold)

'''------------------------------------------------
ADC(A,p)
- INPUT: A scipy graph adjacency 'A', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in a parameter 'k', which controls the number of iterations. Also
		 takes in a number of random vectors 'vec_num'. Also takes in a parameter 'delta', which, if not zero
		 forces the use of an approximate L1 instead of true L1.
- DOES: Coarsens the graph G with respect to the algebraic distance metric. 
- RETURNS: The coarsened graph matrix W.
------------------------------------------------'''
def ADC(A,p,k,vec_num,timeit=True,delta=0):

	B = A.copy()
	Clold = sp.sparse.identity(B.shape[0])

	outer_start = 0 
	inner_start = 0
	outer_end = 0 
	inner_end = 0

	if timeit:
		print("ADC: outer loop starting... ")
		outer_start = time.time()

	while (B.shape[0]) > (A.shape[0]*p):

		if timeit:
			inner_start = time.time()

		# now we get the row sums and turn them into a matrix.
		row_sums = np.reciprocal(np.array(B.sum(axis=0)).astype(float).flatten())
		d = sp.sparse.diags(row_sums)
		T = d.dot(B)	

		# Now we compute the ADC on the graph. 
		# Initialize a random numpy matrix of the correct dimension.
		R = sp.sparse.csr_matrix(np.random.rand(B.shape[0],vec_num))
		
		# Iterate k times.
		for i in range(k):
			S = T.dot(R)
			R = (R + S)/2	

		# Compute distance metrics.
		dist_start = 0
		dist_end = 0

		if delta == 0:
			# compute the distances.
			distances = sp.sparse.csr_matrix( spat.distance_matrix(R.todense(),R.todense()) )
			
			# mask it.
			mask = B.copy()
			mask[mask > 0] = 1
			P = sp.sparse.csr_matrix(distances.multiply(mask))
		else:
			# compute our approximate distance mask.
			dist_mask = l1_approx(np.array(R.todense()),delta)

			# apply our mask.
			P = sp.sparse.csr_matrix(B.multiply(dist_mask))

		# now we apply this matrix to A to get our transition matrix, and we get weights from this.
		mbm = matching(P,minimize=False) 

		# get our left-coarsening matrix, and form the right coarsening matrix as well.
		Cl = get_coarsening_matrix(mbm,0)
		Clold = Cl*Clold
		Cr = Cl.transpose()
			
		# coarsen.
		B = Cl*B*Cr

		if timeit:
			inner_end = time.time()
			print("	>> ADC: inner loop time: "+str( inner_end - inner_start ))

		if np.all( mbm == np.arange(len(mbm)) ):
			break

	if timeit:
		outer_end = time.time()
		print("ADC: outer loop time: "+str( outer_end - outer_start ))

	return(B,Clold)

'''------------------------------------------------
ASC(A,p)
- INPUT: A scipy graph adjacency 'A', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in two booleans, 'approx', which determined if we use an approximate 
		 computation, and 'timeit', which prints timing results. Also takes in a paramter 'delta', which, if 
		 not zero, uses an approximate L1 distance.
- DOES: Coarsens the graph G with respect to the adjacency similarity method. 
- RETURNS: The coarsened graph matrix W.
------------------------------------------------'''
def ASC(A,p,approx=True,timeit=True,delta=0):

	B = A.copy()
	Clold = sp.sparse.identity(B.shape[0])

	outer_start = 0 
	inner_start = 0
	outer_end = 0 
	inner_end = 0

	if timeit:
		print("ASC: outer loop starting... ")
		outer_start = time.time()

	while (B.shape[0]) > (A.shape[0]*p):

		if timeit:
			inner_start = time.time()

		# now we get the row sums and turn them into a matrix.
		row_sums = np.reciprocal(np.array(B.sum(axis=0)).astype(float).flatten())
		d = sp.sparse.diags(row_sums)
		T = d.dot(B)

		P = B.copy()

		# Now we compute the adjacency similarities on the graph. 
		if approx:
			# Form a random binary matrix
			bin_mat = sp.sparse.csr_matrix( np.random.choice([0,1],size=(B.shape[0],5),p=[1/3,2/3]) )
			# Apply our T matrix to it.
			vecs = T.dot(bin_mat)
			# obtain distances.
			if delta != 0:
				dist_mask = l1_approx(vecs.todense(),0.5)
			else:
				distances = sp.sparse.csr_matrix( spat.distance_matrix(vecs.todense(),vecs.todense()) )
		else:
			# Compute distances metrics on the original graph.
			if delta != 0:
				dist_mask = l1_approx(T.todense(),0.5)
			else:
				distances = sp.sparse.csr_matrix( spat.distance_matrix(T.todense(),T.todense()) )

		if delta != 0:
			# mask it.
			P = sp.sparse.csr_matrix(B.multiply(dist_mask))

			# now we apply this matrix to A to get our transition matrix, and we get weights from this.
			mbm = matching(P,minimize=False)
		else:
			# mask it.
			mask = B.copy()
			mask[mask > 0] = 1
			P = distances.multiply(mask)

			# now we apply this matrix to A to get our transition matrix, and we get weights from this.
			mbm = matching(P,minimize=True)

		# get our left-coarsening matrix, and form the right coarsening matrix as well.
		Cl = get_coarsening_matrix(mbm,0)
		Clold = Cl*Clold
		Cr = Cl.transpose()
		
		# coarsen.
		B = Cl*B*Cr

		if timeit:
			inner_end = time.time()
			print("	>> ASC: inner loop time: "+str( inner_end - inner_start ))

		if np.all( mbm == np.arange(len(mbm)) ):
			break

	if timeit:
		outer_end = time.time()
		print("ASC: outer loop time: "+str( outer_end - outer_start ))

	return(B,Clold)

'''------------------------------------------------
fiedler(A)
- INPUT: A Laplacian of the graph 'A'.
- DOES: Computes the Fiedler partition of the matrix.
- RETURNS: The partition matrix.
------------------------------------------------'''
def fiedler(A):

	# obtain a subgraph of a and a mapping
	nnzx,nnzy = A.nonzero()
	map_from = np.unique(nnzx)
	M = A[np.ix_(map_from,map_from)]

	# obtain eigenvector.
	flag = True
	badA = False
	if M.shape[0] > 3:
		vals,vecs = sp.sparse.linalg.eigs(M,k=2)
	elif M.shape[0] > 0:
		flag = False
		vals,vecs = np.linalg.eig(M.todense())
		vals = np.flip(vals,axis=0)
		vecs = np.flip(vecs,axis=1)
	else:
		badA = True
		sg_a = 0
		sg_b = 0
		return(sg_a,sg_b,badA)

	# get the fiedler partition
	fied_part_a = np.real( np.array(vecs[:,1].copy()) )
	fied_part_b = np.real( np.array(vecs[:,1].copy()) )

	fied_part_a[fied_part_a >= 0] = 1
	fied_part_b[fied_part_b >= 0] = 0 
	fied_part_a[fied_part_a < 0] = 0
	fied_part_b[fied_part_b < 0] = 1

	# take the outer products to get our subgraph masks.
	vect_a = np.zeros(A.shape[0])
	vect_b = np.zeros(A.shape[0])

	if flag:
		vect_a[map_from] = fied_part_a
		vect_b[map_from] = fied_part_b
	else:
		vect_a[map_from] = fied_part_a[:,0]
		vect_b[map_from] = fied_part_b[:,0]

	sg_a = sp.sparse.csr_matrix(np.outer(vect_a,vect_a))
	sg_b = sp.sparse.csr_matrix(np.outer(vect_b,vect_b))

	return(sg_a,sg_b,badA)

'''------------------------------------------------
GFSC(A,p,timeit)
- INPUT: A scipy graph adjacency 'A', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph.
- DOES: Coarsens the graph G with respect to the nodal-partitioning method. 
- RETURNS: The coarsened graph matrix W.
------------------------------------------------'''
def GFSC(A,p,timeit=True):

	B = A.copy()

	outer_start = 0 
	inner_start = 0
	outer_end = 0 
	inner_end = 0

	if timeit:
		print("GFSC: outer loop starting... ")
		outer_start = time.time()

	# initialize a 'sizes' array, and a 'partition' dictionary.
	sizes = {0:sp.sparse.csr_matrix.sum(B)}
	partition = {0:B}
	coarsen_mats = {0:B}

	# keep track of iterations
	iters = 0
	index = 0
	while iters < (A.shape[0]*(1-p)):

		if timeit:
			inner_start = time.time()

		# form the normalized Laplacian from B
		row_sums = np.array(B.sum(axis=0)).astype(float).flatten()
		row_sums[row_sums > 0] = np.reciprocal(np.sqrt(row_sums[row_sums > 0]))
		dh = sp.sparse.csr_matrix( sp.sparse.diags(row_sums) )
		ident = dh.copy()
		ident[ident > 0] = 1
		L = ident - dh.dot(B).dot(dh)

		# get the fiedler-partitions and add them to our dictionary.
		sg_a,sg_b,badA = fiedler(L)
		if badA:
			print("	>>:: badA flagged...")
			print("	>>:: sizes at the time of flagging:")
			print("	>>:: "+str(sizes.values()))
			iters = A.shape[0]
		else:
			partition[index] = B.multiply(sg_a)
			partition[iters+1] = B.multiply(sg_b)
			sizes[index] = np.sum(partition[index])
			sizes[iters+1] = np.sum(partition[iters+1])
			coarsen_mats[index] = sg_a
			coarsen_mats[iters+1] = sg_b

			index = np.argmax(list(sizes.values()))
			B = partition[index]
			iters += 1

		if timeit:
			inner_end = time.time()
			print("	>> GFSC: inner loop time: "+str( inner_end - inner_start ))

	# Using our dictionary, form our coarsening matrix.
	Cl = np.identity(A.shape[0])
	for mat in list(coarsen_mats.values()):
		Cl = Cl + mat
	Cl[Cl > 0] = 1
	Cl = np.unique(Cl,axis=0)
	Cl = np.flip(Cl,0)

	Cl = sp.sparse.csr_matrix( Cl )
	Cr = sp.sparse.csr_matrix( Cl.transpose() )
	
	# coarsen.
	B = Cl*A*Cr

	if timeit:
		outer_end = time.time()
		print("GFSC: outer loop time: "+str( outer_end - outer_start ))

	return(B,Cl)

'''------------------------------------------------
nodal_domain_mask(L,k)
- INPUT: A scipy graph Laplacian 'L', and an integer 'k' controlling the number of eigenvectors we use.
- DOES: Forms the nodal domain masking matrix. 
- RETURNS: The masking matrix based on the nodal domains.
------------------------------------------------'''
def nodal_domain_mask(L,k):
	
	# obtain the eigenvectors.
	if L.shape[0] > 3:
		try:
			vals,vecs = sp.sparse.linalg.eigs(L,k=k)
		except sp.sparse.linalg.ArpackNoConvergence as eigs:
			vals = eigs.eigenvalues
			vecs = eigs.eigenvectors
		vals,vecs = sp.sparse.linalg.eigs(L,k=k,which="SM")
	elif L.shape[0] > 0:
		flag = False
		vals,vecs = np.linalg.eig(M.todense())
		vals = np.flip(vals,axis=0)
		vecs = np.flip(vecs,axis=1)
		vecs = vecs[0:k,:]

	# turn the eigenvector embeddings into sign embeddings.
	vecs = np.sign(vecs)

	# obtain the mask from the embeddings.
	# first we get the distances between nodes to see which ones have 0 difference.
	mask = spat.distance_matrix(vecs,vecs)
	# get the nonzero indices
	nnz = np.nonzero(mask)
	# alter distances by zeroing out nonzero elements and setting zero elements to 1.
	mask[mask == 0] = 1
	mask[nnz] = 0

	return(mask)

'''------------------------------------------------
NDC(G,p)
- INPUT: A scipy graph adjacency 'A', and a real number 'p', which is the desired proportional size of the 
		 coarsened graph. Also takes in a parameter 'k', which controls the maximum number of eigenvectors.
- DOES: Coarsens the graph G with respect to the nodal-partitioning method. 
- RETURNS: The coarsened graph matrix W.
------------------------------------------------'''
def NDC(A,p,k,timeit=True):
	
	B = A.copy()
	Clold = sp.sparse.identity(B.shape[0])

	if timeit:
		print("NDC: outer loop starting... ")
		outer_start = time.time()

	# first we get the Laplacian matrix.
	row_sums = np.array(B.sum(axis=0)).astype(float).flatten()
	row_sums[row_sums > 0] = np.reciprocal(np.sqrt(row_sums[row_sums > 0]))
	dh = sp.sparse.csr_matrix( sp.sparse.diags(row_sums) )
	ident = dh.copy()
	ident[ident > 0] = 1
	L = ident - dh.dot(B).dot(dh)

	# estimate what k SHOULD be for the given graph.
	numset = np.arange(k-1)+1
	for num in numset:
		if 2**(num-1) < (A.shape[0]*p):
			k = num
	
	# now we get our nodal domain mask.
	print("	>> chosen k: "+str(k))
	mask = nodal_domain_mask(L,k)

	# Turn the mask into a coarsening matrix.
	Cl = np.unique(mask,axis=0)
	Cl = sp.sparse.csr_matrix(np.flip(Cl,0))

	Clold = Cl*Clold
	Cr = Cl.transpose()

	# coarsen B.
	B = Cl*B*Cr

	if timeit:
		outer_end = time.time()
		print("NDC: outer loop time: "+str( outer_end - outer_start ))

	return(B,Clold)

'''------------------------------------------------
coarse_test(G,p)
- INPUT: A networkx graph 'G', and a coarsening ratio 'p'.
- DOES: Coarsens the graph G with respect to each partitioning method and outputs metrics
		to determine if the coarsening is valid.
- RETURNS: nothing
------------------------------------------------'''
def coarse_test(G,p):

	# First we get the sparse adjacency from G.
	A = nx.adjacency_matrix(G)

	# Perform each coarsening
	M = {}
	Cl = {}
	M[0],Cl[0] = HWC(A,p)
	M[1],Cl[1] = ADC(A,p,20,10)
	M[2],Cl[2] = ASC(A,p,approx=True)
	M[3],Cl[3] = NDC(A,p,20)

	# set the names array.
	names = ["HWC","ADC","ASC","NDC"]

	# get the position of A.
	Apos = np.array([emb for emb in nx.spring_layout(G).values()])

	# print coarsenings and plot the resulting graph.
	for i in range(4):

		# Get our H graph, and its weights.
		H = nx.from_numpy_matrix(M[i].todense())
		weights = [np.sqrt(H[u][v]["weight"]) for u,v in H.edges()]

		# Get the position of nodes in H based on the positions in Apos.
		row_sums = np.reciprocal(np.array(Cl[i].sum(axis=1)).astype(float).flatten())
		d = sp.sparse.diags(row_sums)
		meanmat = (d*Cl[i]).todense()
		Hposmat = np.matmul(meanmat,Apos)
		Hpos = {}
		for j in range(Hposmat.shape[0]):
			Hpos[j] = np.array(Hposmat[j]).flatten()
		
		# Form our plot.
		fig,(ax1,ax2,ax3) = plt.subplots(1,3,figsize=(16,9))
		fig.suptitle(names[i],fontsize=20)
		fig.tight_layout()
		nx.draw_networkx_nodes(G,pos=Apos,ax=ax3)
		nx.draw_networkx_labels(G,pos=Apos,ax=ax3)
		nx.draw_networkx_edges(G,pos=Apos,ax=ax3,alpha=0.5)
		nx.draw_networkx_nodes(H,pos=Hpos,ax=ax2)
		nx.draw_networkx_labels(H,pos=Hpos,ax=ax2)
		nx.draw_networkx_edges(H,pos=Hpos,ax=ax2,width=weights,alpha=0.5)
		ax1.imshow(Cl[i].todense().transpose())
		plt.show()


if __name__ == "__main__":


	print( "Coarsening a random partition graph with p=0.5...")
	print()
	G = nx.random_partition_graph( [1000,1000],0.01,0.005)
	#G = nx.karate_club_graph()
	coarse_test(G,0.5)
	'''
	for u in G.nodes():
		G.nodes[u]["num"] = 1
	for e in G.edges():
		G[e[0]][e[1]]["weight"] = 1
	''' 
	#size = 0.5
	#A = nx.adjacency_matrix(G)

	'''
	print("HWC time...")
	M,Cl = HWC(A,size)
	H = nx.from_numpy_array(M.todense())
	print(M.shape)

	print("ADC time...")
	M,Cl = ADC(A,size,10,5)
	H = nx.from_numpy_array(M.todense())
	print(M.shape)

	print("ASC time...")
	M,Cl = ASC(A,size,approx=True)
	H = nx.from_numpy_array(M.todense())
	print(M.shape)

	print("NDC time...")
	M,Cl = NDC(A,size,2)
	H = nx.from_numpy_array(M.todense())
	print(M.shape)
	'''

