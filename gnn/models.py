import torch
import torch.nn.functional as F
import torch.nn as nn

class MLP(nn.Module):
	def __init__(self):
		super(MLP, self).__init__()
		self.layer1 = nn.Linear(784,20)
		self.layer2 = nn.Linear(20,20)
		self.layer3 = nn.Linear(20,20)
		self.layer4 = nn.Linear(20,10)

	def forward(self, x):
		breakpoint()
		x = self.layer1(x)
		x = self.layer2(x)
		x = self.layer3(x)
		x = self.layer4(x)

		return nn.Softmax(x)