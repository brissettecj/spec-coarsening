from dmdCUDA import DMD4cast

import numpy as np
import cupy as cp
import time
from tqdm import tqdm

import torch
from torch import Tensor
import torch.nn.functional as F


def DMDstep(model, weights, r, pred_step, params=None, verbose=True):
	"""
	Performs a DMD step and updates the model.

	Parameters

		model (nn.Module): The model to be updated.
		weights (list[Tensor]): For each layer, the matrix associated with the weight history.
		r (int): the r-value for dmd
		pred_step (int): number of steps forward DMD predicts.

		verbose (bool): Progress Bar for DMD (optional).

	Returns
		None
	"""
	if params is None:
		params = [i for i in range(len(weights))]
	#breakpoint()
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			start = time.time()
			breakpoint()
			M = W.reshape([W.shape[0], np.prod(W.shape[1:])]).T

			#breakpoint()
			new_weight = DMD4cast(M, r, pred_step)[:,-1].T
			new_weight = new_weight.reshape(W[0].shape)

			param.data = torch.as_tensor(new_weight, device='cuda', dtype=torch.float32)
			#breakpoint()
			if verbose:
				print(f"Step {i}: {time.time()-start}")

def multilevel_train(model, TGG_D, optimizer, loss_fn, epochs=None,
					 m=None, pred_step=None, r=2,  
					 verbose=True, dmd_verbose=False, device=None):
	pass



def train_level(model, data, optimizer, loss_fn, level,
				m=None, pred_step=None, r=2, epochs=20, 
				verbose=True, dmd_verbose=False, device=None):

	"""

	Parameters:
		model (nn.Module): model to be trained. Assumed that the model is already 
		on the correct device.

		TGG_D

	"""

	start = time.time()
	accuracy = []
	model.train()
	do_dmd = True
	if m is None:
		do_dmd = False
		m = epochs+1
	optims = []
	train = data[0]
	test = data[1]

	print(f"do_dmd: {do_dmd}")
	global weights
	if do_dmd:
		weights = [ cp.empty([m]+ list(param.shape)) for param in model.parameters()]
		for i,param in enumerate(model.parameters()):
			weights[i][0] = cp.asarray(param.data)

	#breakpoint()
	for epoch in tqdm(range(1,epochs+1),disable=not(verbose) ):

		#DMD step
		if epoch % m == 0:
			if verbose:
				print("DMD")

			step = time.time()
			DMDstep(model, weights, r, pred_step, params=None, verbose=dmd_verbose)
			elapsed = time.time() - start
			step_time = time.time() - step

			if do_dmd:
				#clear weights
				weights = [ cp.empty([m]+ list(param.shape)) for param in model.parameters()]
				#update weights history
				for i,param in enumerate(model.parameters()):
					weights[i][0] = cp.asarray(param.data, dtype=cp.float32)
				optims.append('DMD')
			#breakpoint()


			
		else: #Normal Training Step
			step = time.time()
			train_epoch(model, train, optimizer, loss_fn)
			elapsed = time.time() - start
			step_time = time.time() - step
			optims.append(type(optimizer).__name__)


			if do_dmd:
				#update weights history
				for i,param  in enumerate(model.parameters()):
					weights[i][epoch %m] = cp.asarray(param.data)

		acc, loss = validate(model, test, loss_fn)
		acc_train, loss_train = validate(model, train, loss_fn)
		val = [level, acc.item(), loss.item(), acc_train.item(), loss_train.item(), step_time, elapsed]
		accuracy.append(val)
		
	"""
	|DMD?|test_accuracy|test_loss|step_time|level_time|          
	"""
	#breakpoint()
	return optims, np.array(accuracy)
train_level.headers = ["level", "test_accuracy", "test_loss","train_accuracy", "train_loss", "step_time", "level_time"]

"""def validate(model, dataloader, crit):
	model.eval()
	accuracy = 0
	loss = 0
	with torch.no_grad():
		for X,y in dataloader:
			yhat = model(X)
			pred = yhat.argmax(dim=1)
			correct = (pred == y)
			accuracy += correct.sum() / len(correct)
			loss += crit(yhat, y)
		return (accuracy, loss)
def train_epoch(model, dataloader, optimizer, loss_fn):
	for batchnum, (X,y) in enumerate(dataloader):
		model.train()
		optimizer.zero_grad()
		yhat = model(X)
		loss = loss_fn(yhat, y)
		loss.backward()
		optimizer.step()
	return loss
"""
def validate(model, data, crit, mask=True):
	model.eval()
	with torch.no_grad():
		yhat = model(data)[mask]
		pred = yhat.argmax(dim=1)
		correct = (pred == data.y[mask])
		accuracy = correct.sum() / len(correct)
		loss = crit(yhat, data.y[mask])
		return (accuracy, loss)
def train_epoch(model, data, optimizer, loss_fn, mask=True):
	model.train()
	optimizer.zero_grad()
	yhat = model(data)
	loss = loss_fn(yhat[mask], data.y[mask])
	loss.backward()
	optimizer.step()
	return loss