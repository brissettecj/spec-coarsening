import sys, os
from os import path

cur = os.path.dirname(os.path.dirname(path.realpath(__file__)))

sys.path.append( cur)

from data.molecules import MoleculeDatasetpyg
from data.planetoids import PlanetoidDataset
from data.ogbn import ogbnDatasetpyg
from data.data import LoadData

from gnn import GNN





get_zinc = False
get_planetoids = False

zinc = None
cora = None
citeseer = None
pubmed = None

if get_zinc:
	zinc = LoadData("ZINC", use_node_embedding=True, framework='pyg')
	print("ZINC loaded")

if get_planetoids == True:
	cora = LoadData("Cora", use_node_embedding=True, framework='pyg')
	print("CORA loaded")
	citeseer = LoadData("Citeseer", use_node_embedding=True, framework='pyg')
	print("CITESEER loaded")
	pubmed = LoadData("Pubmed", use_node_embedding=True, framework='pyg')
	print("PUBMED loaded")

