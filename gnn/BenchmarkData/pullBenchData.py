import numpy as np
import pickle
import time
import os
import matplotlib.pyplot as plt
import zipfile

"""
Run this file inside gnn/BenchmarkData
"""


if not os.path.isfile('molecules.zip'):
    print('downloading... molecules')
    os.system('curl https://www.dropbox.com/s/feo9qle74kg48gy/molecules.zip?dl=1 -o molecules.zip -J -L -k')
    os.system('unzip molecules.zip -d ../')
    # !tar -xvf molecules.zip -C ../
else:
    print('molecules already downloaded')