import networkx as nx 
import numpy as np
import scipy as sp 
import matplotlib.pyplot as plt

import iolib as iol 
import coarselib as crl
import analyzelib as anl

# define a get-volume function.
def getvol( G ):
	vol = 0
	for e in G.edges():
		vol += G[e[0]][e[1]]["weight"]
	return( str(vol) )

# Perform a dummy coarsening and get the lift after each merge.
G = iol.read_edgelist("zachary_karate")

vol = getvol( G )
print( "volume of G: "+vol )

# coarsen each edge and output along the way.
H = G.copy()
outmap = [ i for i in sorted( G.nodes() ) ]
for i in range( len(G.nodes())-4 ):
	print("------------------------------------------------------")
	# choose an edge to coarsen at random.
	edges = list( H.edges() )
	ind = np.random.choice( [i for i in range(len(edges))], 1 )
	e = edges[ind[0]]
	(u,v) = (e[0],e[1])
	while u == v:
		ind = np.random.choice( [i for i in range(len(edges))], 1 )
		e = edges[ind[0]]
		(u,v) = (e[0],e[1])

	# print out the edge.
	print(" 	edge chosen: " + "(" + str(u) + "," + str(v) + ")" )

	# coarsen with respect to this edge. 
	crl.merge( H, u, v, outmap )

	# print the volume of H.
	vol = getvol( H )
	print( "	volume of H: "+vol )

	# get the lift from analyzelib.
	L = anl.lift(H,outmap)

	# get the volume of the lift and print it.	
	vol = getvol( L )
	print( "	volume of L: "+vol )
	
# get the lift from analyzelib.
L = anl.lift(H,outmap)

# get the volume of the lift and print it.
vol = getvol( L )
print( "	nodes in L: "+str(len(L.nodes())))
print( "	volume of L: "+vol )

plt.figure(1)
nx.draw(H)

plt.figure(2)
nx.draw(L)

plt.show()