from  dmd import DMD4cast

import numpy as np
import time
from tqdm import tqdm

import torch
from torch import Tensor
import torch.nn.functional as F


def DMDstep(model, weights, r, pred_step, params=None, verbose=True):
	"""
	Performs a DMD step and updates the model.

	Parameters

		model (nn.Module): The model to be updated.
		weights (list[Tensor]): For each layer, the matrix associated with the weight history.
		r (int): the r-value for dmd
		pred_step (int): number of steps forward DMD predicts.
		params: ???
		verbose (bool): Progress Bar for DMD (optional).

	Returns
		None
	"""
	if params is None:
		params = [i for i in range(len(weights))]
		
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			start = time.time()
			M = W.reshape(W.shape[0], np.prod(W.shape[1:])).T
			new_weight = DMD4cast(M, r, pred_step)[:,-1].T
			new_weight = new_weight.reshape(W[0].shape)

			param.data = Tensor(new_weight)
			if verbose:
				print(f"Step {i}: {time.time()-start}")

def multilevel_train(model, TGG_D, optimizer, loss_fn, epochs=None,
					 m=None, pred_step=None, r=2,  
					 verbose=True, dmd_verbose=False, device=None):
	pass



def train_level(model, TGG_D, optimizer, loss_fn, level,
				m=None, pred_step=None, r=2, epochs=20, 
				verbose=True, dmd_verbose=False, device=None):

	"""

	Parameters:
		model (nn.Module): model to be trained. Assumed that the model is already 
		on the correct device.

		TGG_D

	"""

	if device is None:
		device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

	start = time.time()
	accuracy = []
	model.train()
	do_dmd = True
	if m is None:
		do_dmd = False
		m = epochs+1
	optims = []
	data = TGG_D[level].to(device)
	data.x = F.normalize(data.x,p=1)
	global weights
	if do_dmd:
		weights = [ np.empty(np.append(1, param.shape)) for param in model.parameters()]
		for i,param in enumerate(model.parameters()):
			weights[i][0] = param.detach()


	for epoch in tqdm(range(1,epochs+1),disable=not(verbose) ):

		#DMD step
		if epoch % m == 0:
			if verbose:
				print("DMD")

			step = time.time()
			DMDstep(model, weights, r, pred_step, params=None, verbose=dmd_verbose)
			elapsed = time.time() - start
			step_time = time.time() - step

			if do_dmd:
				#clear weights
				weights = [ np.empty(np.append(1, param.shape)) for param in model.parameters()]
				#update weights history
				for i,param in enumerate(model.parameters()):
					weights[i][0] = param.detach()
					optims.append('DMD')


			
		else: #Normal Training Step
			step = time.time()
			train_epoch(model, data, optimizer, loss_fn, mask=data.train_mask)
			elapsed = time.time() - start
			step_time = time.time() - step
			optims.append(type(optimizer).__name__)


			if do_dmd:
				#update weights history
				for i,param  in enumerate(model.parameters()):
					weights[i] = np.append(weights[i], param.detach().reshape(np.append(1,weights[i].shape[1:]).tolist()),axis=0)

		acc, loss = validate(model, data, loss_fn, mask=data.test_mask)
		acc_train, loss_train = validate(model, data, loss_fn, mask=data.train_mask)
		val = [level, acc.item(), loss.item(), acc_train.item(), loss_train.item(), step_time, elapsed]
		accuracy.append(val)
		
	"""
	|DMD?|test_accuracy|test_loss|step_time|level_time|          
	"""
	return optims, np.array(accuracy)
train_level.headers = ["level", "test_accuracy", "test_loss","train_accuracy", "train_loss", "step_time", "level_time"]

def validate(model, data, crit, mask=True):
	model.eval()
	with torch.no_grad():
		yhat = model(data)[mask]
		pred = yhat.argmax(dim=1)
		correct = (pred == data.y[mask])
		accuracy = correct.sum() / len(correct)
		loss = crit(yhat, data.y[mask])
		return (accuracy, loss)
def train_epoch(model, data, optimizer, loss_fn, mask=True):
	model.train()
	optimizer.zero_grad()
	yhat = model(data)
	loss = loss_fn(yhat[mask], data.y[mask])
	loss.backward()
	optimizer.step()
	return loss