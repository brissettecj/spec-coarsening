import cupy as cp
import numpy as np

import time

import torch
from torch.nn.utils import parameters_to_vector,vector_to_parameters
from torch import optim
from torch.nn import NLLLoss
from torch_geometric.nn import GCNConv
import torch_geometric.datasets as tds
import torch.nn.functional as F

import matplotlib.pyplot as plt

class Net(torch.nn.Module):
	def __init__(self, num_features, hidden, num_classes ):

		seed = np.random.randint(0,high=999999,dtype=int)
		torch.manual_seed(seed)
		
		super(Net, self).__init__()
		self.conv1 = GCNConv(num_features, hidden)
		self.conv2 = GCNConv(hidden, num_classes)

	def reset_parameters(self):
		self.conv1.reset_parameters()
		self.conv2.reset_parameters()

	def forward(self, torchG):
		x, edge_index, = torchG.x, torchG.edge_index

		x = self.conv1(x, edge_index)
		x = F.relu(x)
		x = F.dropout(x, training=self.training)
		x = self.conv2(x, edge_index)

		return F.log_softmax(x, dim=1)

# Performs one epoch of training.
def train_epoch(model, data, optimizer, loss_fn, mask=True):
	model.train()
	optimizer.zero_grad()
	yhat = model(data)
	loss = loss_fn(yhat[mask], data.y[mask])
	loss.backward()
	optimizer.step()
	return loss

# Performs dmd forecasting using torch tensors.
def torch_forecast( data, pred_step, r, times=False ):

    if times:
        print("torch forecast...")
        print("     data shape: "+str(data.shape))

    # Seperate our data.
    inputs = data[:,:,:-1]
    outputs = data[:,:,1:]

    # Change inputs to torch for svd.
    inputs = torch.as_tensor(inputs,device="cuda",dtype=torch.float32)
    
    # Perform the svd
    if times:
        start = time.time()
    u,s,vt = torch.linalg.svd( inputs, full_matrices=False, driver="gesvdj" )
    if times:
        print( "        svd time: "+str(time.time()-start) )

    # Back to cupy.
    s = cp.asarray(s)
    vt = cp.asarray(vt)
    u = cp.asarray(u)
    
    # Take the recirpocal of s.
    s = cp.reciprocal(s)[:,:r]
    vt = vt.transpose(0,2,1)[:,:,:r]
    u = u.transpose(0,2,1)[:,:r,:]

    # Compute A for predictions.
    A = outputs @ vt @ ( s[...,None] * u )
    
    # Get the last rows.
    outs = outputs[:,:,outputs.shape[2]-1:]

    # Forecast.
    if times:
        start = time.time()
    for i in range(pred_step):
        outs = cp.matmul( A, outs )
    if times:
        print( "        predict time: "+str(time.time()-start) )
    
    # Reshape outs and turn it into a torch tensor.
    outs = torch.tensor( outs.reshape( 1,outs.shape[0]*outs.shape[1] ), device="cuda:0", dtype=torch.float32 )
    
    # Return outs.
    return( outs )


# Performs dmd forecasting using cupy arrays.
def cupy_forecast( data, pred_step, r, times=False ):

    if times:
        print("cupy forecast...")
        print("     data shape: "+str(data.shape))

    # Seperate our data.
    inputs = data[:,:,:-1]
    outputs = data[:,:,1:]

    # Perform the svd
    if times:
        start = time.time()
    u,s,vt = cp.linalg.svd( inputs, full_matrices=False )
    if times:
        print( "        svd time: "+str(time.time()-start) )

    # Back to cupy.
    s = cp.asarray(s)
    vt = cp.asarray(vt)
    u = cp.asarray(u)
    
    # Take the recirpocal of s.
    s = cp.reciprocal(s)[:,:r]
    vt = vt.transpose(0,2,1)[:,:,:r]
    u = u.transpose(0,2,1)[:,:r,:]

    # Compute A for predictions.
    A = outputs @ vt @ ( s[...,None] * u )
    
    # Get the last rows.
    outs = outputs[:,:,outputs.shape[2]-1:]

    # Forecast.
    if times:
        start = time.time()
    for i in range(pred_step):
        outs = cp.matmul( A, outs )
    if times:
        print( "        predict time: "+str(time.time()-start) )
    
    # Reshape the outs array and change it to torch.
    outs = torch.tensor( outs.reshape( 1,outs.shape[0]*outs.shape[1] ), device="cuda:0", dtype=torch.float32 )

    # Return outs.
    return( outs )


# Performs the full training, changing the parameters every 20 epochs.
def train_full( model, graph, optimizer, loss_fn, its, r=3, track=0, pred=0, cupy=False, printem=False ):

	# Get the breakdown of our parameter dimensions for flattening.
	if track > 0:
		flat_params = parameters_to_vector(model.parameters())
		param_num = flat_params.size()[0]
		appended_param_num = param_num + (32 - param_num%32)
		W = cp.zeros((track,appended_param_num))

	for param in model.parameters():
		print(param)
	# Loop through our epochs, randomly changing the weights every 20 steps.
	for epoch in range(its):

		# Get the start time for our iteration.
		start = time.time()

		# If the iteration is correct, predict weights.
		if track > 0 and epoch%track == 0 and epoch > 0:

			# Temporarily reshape W.
			W = W.transpose().reshape((appended_param_num//32,32,track))

			# Forecast using DMD.
			if cupy:
				new_weights = cupy_forecast( W, pred, r )
			else:
				new_weights = torch_forecast( W, pred, r )
			
			# Reshape W.
			W = W.transpose().reshape(track,appended_param_num)

			# Set the model parameters.
			vector_to_parameters( torch.flatten(new_weights), model.parameters() )

		# Otherwise perform some standard adam steps!
		else:
			train_epoch(model, graph, optimizer, loss_fn, mask=graph.train_mask)

		# Get the parameters and populate W.
		if track > 0:
			W[epoch%track,0:param_num] = cp.array( torch.tensor(parameters_to_vector(model.parameters()),device="cuda:0"), dtype=cp.float32 )

		# Print the end time of our iteration.
		if printem and epoch > track+1:
			if epoch%track == 0:
				print()
				print( "THIS STEP SHOULD BE SLOWER " )
			if epoch%track == 1:
				print( "THIS IS WHERE THINGS GENERALLY BREAK " )
			print( "iteration: "+str(epoch)+" time: "+str(time.time()-start) )
			if epoch%track == 1:
				print( )



# Get the torch_geometric graph.
cora = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')[0].to('cuda')

# Define our model.
num_features = cora.x.shape[1]
num_classes  = cora.y.unique().shape[0]
model = Net( num_features, 32, num_classes ).to('cuda')

# Set our loss function.
loss = NLLLoss()

# Define our optimizer.
optimizer = optim.Adam
optimizer = optimizer( model.parameters(), lr=0.0001 )

# Run train_full once to warm it up.
train_full( model, cora, optimizer, loss, 3 )
# Run train_full again and print resulting times.
train_full( model, cora, optimizer, loss, 100, track=20, pred=10, cupy=True, printem=True )