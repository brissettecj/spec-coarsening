import sys, os
from os import path
cur = os.path.dirname(os.path.dirname(path.realpath(__file__)))
sys.path.append( cur)

import torch
from torch import optim
import torch_geometric.datasets as tds
import torch.nn.functional as F
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader
import torchvision.datasets as datasets
from torchvision import transforms
import numpy as np
import pandas as pd
from tqdm import tqdm
import matplotlib.pyplot as plt
import dominate
from dominate import tags
import gnnutils as utils
from gnn import *
from models import *
import traindmdCUDA
import random
import time
import argparse

import yaml

class Data:
	def __init__(self, X,y, train=True, test=False, device='cuda'):
		self.X = torch.as_tensor(X, device=device, dtype=X.dtype)
		self.y = torch.as_tensor(y, device=device, dtype=y.dtype)
		self.train_mask = train
		self.test_mask = test
def main(args):

	dataset = args.dataset
	epochs  = args.epochs#[int(e) for e in args.epochs]
	lr 		= args.lr
	weight_decay=args.weight_decay
	device = args.device
	opt = args.optimizer
	loss 	= args.loss
	hidden	= args.hidden
	m 		= args.m
	r 		= args.r
	pred_step= args.pred_step
	model   = args.model
	train_level=0
	runs = args.runs

	# SELECT DEVICE IF NOT PROVIDED
	device = ('cuda' if torch.cuda.is_available() else 'cpu') if device is None else device
	#device = 'cuda'
	args.device = device
	torch.device(device)

	#LOAD AND SELECT THE GRAPH
	if dataset == 'ALL':
		cora = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')
		print(f"CORA loaded:\n\t{cora[0].__repr__()}")
		pubmed = tds.Planetoid(root="tmp/PubMed//", name='PubMed')
		print(f"PUBMED loaded:\n\t{pubmed[0].__repr__()}")
		citeseer = tds.Planetoid(root="tmp/PubMed//", name='Citeseer')
		print(f"CITESEER loaded:\n\t{citeseer[0].__repr__()}")
		TGG = [cora[0], pubmed[0], citeseer[0]]
	elif dataset == 'cora':
		cora = tds.Planetoid(root="tmp/Cora//", name='Cora', split='full')
		print(f"CORA loaded:\n\t{cora[0].__repr__()}")
		TGG = [cora[0]]
	elif dataset == 'pubmed':
		pubmed = tds.Planetoid(root="tmp/PubMed//", name='PubMed')
		print(f"PUBMED loaded:\n\t{pubmed[0].__repr__()}")
		TGG = [pubmed[0]]
	elif dataset == 'citeseer':
		citeseer = tds.Planetoid(root="tmp/PubMed//", name='Citeseer')
		print(f"CITESEER loaded:\n\t{citeseer[0].__repr__()}")
		TGG = [citeseer[0]]
	elif dataset == 'mnist':
		mnist_train = datasets.MNIST(root='tmp/mnist//', train=True, download=True, transform=transforms.ToTensor())
		mnist_test  = datasets.MNIST(root='tmp/mnist//', train=False, download=True, transform=transforms.ToTensor())
		mnist_train.data = mnist_train.data.to(device)
		mnist_train.targets = mnist_train.targets.to(device)
		
		mnist_test.data = mnist_test.data.to(device)
		mnist_test.targets = mnist_test.targets.to(device)

		breakpoint()

		mnist_train_loader = DataLoader(mnist_train, shuffle = True, batch_size=128 )
		mnist_test_loader = DataLoader(mnist_test, shuffle = True, batch_size=128 )

		data = [mnist_train_loader, mnist_test_loader]
	#PICK THE OPTIMIZER
	if opt == 'adam':
		optimizer = optim.Adam#(model.parameters(), lr=lr, weight_decay=5e-10)
	else:
		raise Exception(f"optimizer {opt} not supported.")

	#PICK THE LOSS
	if loss.lower() in ['crossentropyloss','cel']:
		loss = CrossEntropyLoss()

	#perform run(s)
	dfs = []
	for i in range(runs):
		df = benchmark(data, model, train_level, epochs, lr, weight_decay, 
			  device, optimizer, loss, hidden,m,r,pred_step,verbose=runs<2)

		df['level'] = df['level'].astype(int)
		
		dfs.append(df)

	extremes = pd.DataFrame(0, columns=dfs[0].columns, index=['max','min']).drop(columns=['optim','level'], axis=0)
	for i in range(runs):
		extremes.loc['max'] += dfs[i].max(axis=0).drop(columns=['optim','level'], axis=0)
		extremes.loc['min'] += dfs[i].min(axis=0).drop(columns=['optim','level'], axis=0)


	var_cols = ['test_accuracy','test_loss','train_accuracy','train_loss',
				'step_time','level_time']
	sum_df = pd.DataFrame(0, columns=dfs[0].columns, index=df.index)
	for i in range(runs):
		sum_df += dfs[i][var_cols]

	sum_df = sum_df / runs

	sum_df['level'] = df['level']
	sum_df['optim'] = df['optim']

	avg_df = sum_df[['level']+var_cols+['optim']]

	extremes = extremes
	extremes = extremes / runs
	extremes.index = ['--max--', '--min--']
	#Write results to .html file

	doc = dominate.document()

	run_id = ""
	for param in [dataset, model, train_level, epochs, lr, weight_decay, 
			  device, opt, type(loss).__name__, hidden,m,r,pred_step]:
			  if param is not None:
			  	run_id += (str(param)) + '_'

	try:
		os.mkdir("results")
	except FileExistsError:
		pass
	try:
		os.mkdir(f"results/{run_id}")
	except FileExistsError:
		pass

	#Make Plots
	breakpoint()
	### ACCURACY PLOT ###
	fig, ax = plt.subplots()
	df = sum_df#dfs[i]
	for col, color in [('test_accuracy','red'),( 'train_accuracy','blue')]:
		ax.plot(df[col].round(3), label=col, c=color)

	handles, labels = ax.get_legend_handles_labels()
	by_label = dict(zip(labels, handles))
	ax.legend(by_label.values(), by_label.keys())
	plt.savefig(f"results/{run_id}/acc_fig.png")

	### LOSS PLOT ###
	fig, ax = plt.subplots()
	#for i in range(runs):
		#df = dfs[i]
	for col, color in [('test_loss','red'),( 'train_loss','blue')]:
		ax.plot(df[col], label=col, c=color)

	handles, labels = ax.get_legend_handles_labels()
	by_label = dict(zip(labels, handles))
	ax.legend(by_label.values(), by_label.keys())
	plt.savefig(f"results/{run_id}/loss_fig.png")

	### END PLOTS ###


	with open(f"results/{run_id}/df.yaml", 'w') as file:
	    yaml.dump({'result': df.to_dict(orient='records')}, file, default_flow_style=False)


	doc = dominate.document(title='Title!')

	with doc.head:
		tags.style("""\
			#plots {
			display: table;
			clear: both;
			}
			""")
	info = vars(args)

	header = tags.div('Hyper-Params')

	i = 0
	for key, value in info.items():
		header.add(tags.div( f"{key}: {value}"))
		i += 1
	imgs = [ 
		tags.img(src=f'{run_id}/acc_fig.png'),
		tags.img(src=f'{run_id}/loss_fig.png'),

		]
	plots = tags.div(id_='plots')

	doc.add(header)
	plots.add(imgs)
	doc.add(plots)
	#doc.add(tags.table(df.to_html()))
	#print(df.to_html())
	def highlight_max(s):
		is_max = s == s.max()
		return ['color: green' if cell else '' for cell in is_max]
	def highlight_min(s):
		is_min = s == s.min()
		return ['color: green' if cell else '' for cell in is_min]		

	max_subset = ['test_accuracy', 'train_accuracy']
	min_subset = ['test_loss', 'train_loss', 'step_time',]
	df_view = avg_df.style.highlight_max(
					color='lightgreen', axis=0, subset=max_subset).highlight_min(
					color='red', axis=0, subset=max_subset).highlight_max(
					color='red', axis=0, subset=min_subset).highlight_min(
					color='lightgreen', axis=0, subset=min_subset).set_table_styles(
				    [{"selector": "", "props": [("border", "1px solid grey")]},
				      {"selector": "tbody td", "props": [("border", "1px solid grey")]},
				     {"selector": "th", "props": [("border", "1px solid grey")]}
				    ])

	table_html = df_view.to_html(border=1)
	with open(f"results/{run_id}.html",'w') as fd:
		fd.write(doc.__str__()+extremes.to_html()+table_html)
		print(f"file://{os.getcwd()}/results/{run_id}.html")

def benchmark(data, model_type, train_level, epochs, 
			  lr, weight_decay, device, optimizer, loss, hidden,m,r,p,verbose=True):	

	gtd = traindmdCUDA

	#Initialize Model, Optimizer, and Loss
	breakpoint()
	num_features = data[0].dataset.data.shape[1]
	num_classes  = data[0].dataset.targets.unique().shape[0]

	if model_type == 'GNN':
		model = GNN(num_features, hidden, num_classes).to(device)
	elif model_type == 'Net':
		model = Net(num_features, hidden, num_classes).to(device)
	elif model_type == 'MLP':
		model = MLP().to(device)

	optimizer = optimizer(model.parameters(), lr=lr,weight_decay=weight_decay)
	optims, stats,final_cm = gtd.train_level(model, data, optimizer, loss, train_level,
							epochs=epochs, device=device, verbose=verbose,
							m=m,pred_step=p,r=r)
	df = pd.DataFrame(stats, columns=gtd.train_level.headers)
	df['optim'] = pd.Series(optims)

	return df




if __name__ == '__main__':
	global model
	parser = argparse.ArgumentParser()
	parser.add_argument('-d','--dataset',choices=['cora','pubmed','citeseer', 'ALL', 'mnist'], default='cora')
	parser.add_argument('-e','--epochs',type=int, default=30)
	parser.add_argument('-lr','--lr',type=float, default=.001)
	parser.add_argument('-w','--weight_decay', type=float,default=0)
	parser.add_argument('-v','--device', choices=['cpu','cuda'], default=None)
	parser.add_argument('-o','--optimizer',choices=['adam'], default='adam')
	parser.add_argument('--loss',choices=['cel'], default='cel')
	parser.add_argument('--hidden',type=int,default=64)
	parser.add_argument('-m','--m', type=int)
	parser.add_argument('-r','--r', type=int)
	parser.add_argument('-p','--pred_step', type=int)
	parser.add_argument('--train_level', type=int, default=0)
	parser.add_argument('--model', type=str, default='GNN')
	parser.add_argument('--runs', type=int, default=1)


	args = parser.parse_args()
	print(args)

	main(args)