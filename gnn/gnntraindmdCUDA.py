from dmdCUDA import DMD4cast,DMD5cast,DMD6cast,DMDBIGcast

import numpy as np
import cupy as cp
import time
from tqdm import tqdm

import torch
from torch import Tensor
import torch.nn.functional as F
import matplotlib.pyplot as plt


def DMDstep(model, weights, r, pred_step, params=None, verbose=True):
	"""
	Performs a DMD step and updates the model.

	Parameters

		model (nn.Module): The model to be updated.
		weights (list[Tensor]): For each layer, the matrix associated with the weight history.
		r (int): the r-value for dmd
		pred_step (int): number of steps forward DMD predicts.
		params: ???
		verbose (bool): Progress Bar for DMD (optional).

	Returns
		None
	"""
	subweights = 32

	if params is None:
		params = [i for i in range(len(weights))]

	# Get the dimensions for our new weight matrix.
	dim_a = 0 
	dim_b = 0 
	W_a = np.zeros(len(params)).astype(int)
	W_b = np.zeros(len(params)).astype(int)
	dim_arr = np.zeros(len(params)).astype(int)
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			dim_a = int(W.shape[0])
			W_a[i] = int(W.shape[1])
			if len(W.shape) > 2:
				W_b[i] = int(W.shape[2])
			dim_arr[i] = int(np.prod(W.shape[1:]))
			dim_b += int(dim_arr[i])

	# Pad dim_b so that everything breaks up evenly for batched svd.
	if dim_b % subweights != 0:
		dim_b = dim_b + (subweights-dim_b%subweights)

	# Form our large data array.
	M = cp.zeros((dim_a,dim_b))
	prev = 0
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			M[0:dim_a,prev:prev+dim_arr[i]] = W.reshape(W.shape[0], np.prod(W.shape[1:])).copy()
			prev += dim_arr[i]

	# Now reshape for DMDBIGcast.
	M = M.transpose().reshape((dim_b//subweights,subweights,dim_a))
	#print(model.)

	# Get the weights
	new_weights = DMDBIGcast( M, r, pred_step,times=False)

	# Iterate through and set the params.
	prev = 0
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			arr = new_weights[0,prev:prev+dim_arr[i]]
			if W_b[i] != 0:
				arr = arr.reshape(int(W_a[i]),int(W_b[i]))
			#dstart = time.time()
			param.data = torch.tensor( arr, device='cuda:0', dtype=torch.float32 )
			#print("Param updated: "+str(time.time()-dstart))
			prev += dim_arr[i] 


	#plt.imshow( cp.asnumpy(new_weights), aspect="auto" )
	#plt.plot(cp.asnumpy(new_weights))
	#plt.show()

	#for i in range(new_weights.shape[0]):
	#	plt.imshow(cp.asnumpy(new_weights[i]),aspect="auto")
	#	plt.show()

	# Iterate through and set the params.
	#for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
	#	if i in params:
	#		param.data = torch.as_tensor(new_weights[], device='cuda', dtype=torch.float32)

	#arr = cp.asnumpy(M)
	#plt.imshow(arr,aspect="auto")
	#plt.show()


	'''
	#breakpoint()
	for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
		if i in params:
			start = time.time()

			new_weight = DMD5cast(W,r,pred_step,times=True)
			param.data = torch.as_tensor(new_weight, device='cuda', dtype=torch.float32)
			
			#breakpoint()
			if len(W.shape) <= 2:
				print(type(W))
				arr = cp.asnumpy(W)
				plt.imshow(arr)
				plt.show()
				print(W)
				dstart=time.time()
				print("nonbatched W size: "+str(W.shape))
				M = W.reshape(W.shape[0], np.prod(W.shape[1:])).T
				new_weight = DMD4cast(M, r, pred_step)[:,-1].T
				new_weight = new_weight.reshape(W[0].shape)
				print("nonbatched M size: "+str(M.shape))
				print("nonbatched time: "+str(time.time()-dstart))
			else:
				#M = W.reshape(W.shape[0], np.prod(W.shape[1:])).T
				#print(M.shape)
				dstart = time.time()
				new_weight = DMD5cast(W,r,pred_step)
				#new_weight = DMD7cast(W,r,pred_step)
				print("dmdcast time: "+str(time.time()-dstart))
	
			param.data = torch.as_tensor(new_weight, device='cuda', dtype=torch.float32)
			#breakpoint()
			if verbose:
				print(f"Step {i}: {time.time()-start}")
	'''

def multilevel_train(model, TGG_D, optimizer, loss_fn, epochs=None,
					 m=None, pred_step=None, r=2,  
					 verbose=True, dmd_verbose=False, device=None):
	pass



def train_level(model, TGG_D, optimizer, loss_fn, level,
				m=None, pred_step=None, r=2, epochs=20, inittrack=10,
				verbose=True, dmd_verbose=False, device=None):

	"""

	Parameters:
		model (nn.Module): model to be trained. Assumed that the model is already 
		on the correct device.

		TGG_D

	"""
	start = time.time()
	accuracy = []
	model.train()
	do_dmd = True
	if m is None:
		do_dmd = False
		m = epochs+1
	optims = []
	data = TGG_D[level].to(device)
	data.x = F.normalize(data.x,p=1)
	print(f"do_dmd: {do_dmd}")
	global weights
	if do_dmd:
		weights = [ cp.empty([m]+ list(param.shape)) for param in model.parameters()]
		for i,param in enumerate(model.parameters()):
			weights[i][0] = cp.asarray(torch.clone(param.data),dtype=cp.float32)

	#breakpoint()
	for epoch in tqdm(range(1,epochs+1),disable=not(verbose) ):

		#DMD step
		if epoch % m == 0 and (epoch >= inittrack + m):
			#if verbose:
				#print("DMD")

			step = time.time()
			DMDstep(model, weights, r, pred_step, params=None, verbose=dmd_verbose)
			elapsed = time.time() - start
			step_time = time.time() - step

			if do_dmd:
				#clear weights
				weights = [ cp.empty([m]+ list(param.shape)) for param in model.parameters()]
				#update weights history
				for i,param in enumerate(model.parameters()):
					weights[i][0] = cp.asarray(torch.clone(param.data), dtype=cp.float32)
				optims.append('DMD')
			#breakpoint()

		else: #Normal Training Step
			step = time.time()
			train_epoch(model, data, optimizer, loss_fn, mask=data.train_mask)
			elapsed = time.time() - start
			step_time = time.time() - step
			optims.append(type(optimizer).__name__)


			if do_dmd and (epoch > inittrack):
				#update weights history
				for i,param  in enumerate(model.parameters()):
					weights[i][epoch %m] = cp.asarray(torch.clone(param.data), dtype=cp.float32)

		#if epoch % m == 0 and (epoch > inittrack + m):
		#	print("HAHAHAHAHAHAHA------------------------")
		#dstart = time.time()
		acc, loss = validate(model, data, loss_fn, mask=data.test_mask)
		#print("validate time: "+str(time.time()-dstart))
		acc_train, loss_train = validate(model, data, loss_fn, mask=data.train_mask)
		val = [level, acc.item(), loss.item(), acc_train.item(), loss_train.item(), step_time, elapsed]
		accuracy.append(val)
		
	"""
	|DMD?|test_accuracy|test_loss|step_time|level_time|          
	"""
	#breakpoint()
	return optims, np.array(accuracy)
train_level.headers = ["level", "test_accuracy", "test_loss","train_accuracy", "train_loss", "step_time", "level_time"]

def validate(model, data, crit, mask=True):
	model.eval()
	with torch.no_grad():
		yhat = model(data)[mask]
		pred = yhat.argmax(dim=1)
		correct = (pred == data.y[mask])
		accuracy = correct.sum() / len(correct)
		loss = crit(yhat, data.y[mask])
		return (accuracy, loss)

def train_epoch(model, data, optimizer, loss_fn, mask=True):
	model.train()
	optimizer.zero_grad()
	yhat = model(data)
	loss = loss_fn(yhat[mask], data.y[mask])
	loss.backward()
	optimizer.step()
	return loss