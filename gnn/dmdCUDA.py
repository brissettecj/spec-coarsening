# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 13:06:26 2022

"@author: William
"""

import cupy as cp
import torch
import time
import matplotlib.pyplot as plt

def DMD3( data, r ):

    inputs = data[:, : -1]
    outputs = data[:, 1 :]

    # First we perform the svd on the inputs.
    u,s,vt = cp.linalg.svd( inputs, full_matrices=False )

    # limit the dimension
    u = u[:, : r]
    s = s[ 0: r ]
    vt = vt[: r, :]

    # Compute the inverse of s.
    invs = cp.linalg.inv(cp.diag(s))

    # Now compute our A matrix
    A = outputs.dot(vt.transpose()).dot(invs).dot(u.transpose())

    # Return our matrix.
    return None, None, A


def DMD2(W, ft=None, r=None):

    global Wminus,Wplus,Lambda,Y,psi,w, S_r_inv, U,S,A,V
    Wminus = W[:, :-1]
    Wplus = W[:, 1:]
    
    # Comput lowcost SVD decomposition
    U,S,V = cp.linalg.svd(Wminus, full_matrices=False)
    V = V.T
    
    #calculate r
    if r is None:
        r = 0
        for r in range(1,S.shape[0]):
            if S[r-1,r-1]/S[r,r] > ft:
                break
    #Build reduced koopman operator (3)
    S_r_inv = cp.reciprocal(S[:r])
    A = U[:r].T@Wplus
    A = A@V[:r]
    A = A@S_r_inv
    
    # Perform eigendecomposition of the reduced 
    # Koopman operator with (4)
    Lambda,Y = cp.linalg.eigh(A)
    
    #compute the matrix of weights modes
    psi = Wplus @ V[:r] @ cp.diag(S_r_inv) @ Y
    
    w = psi @ cp.diag(Lambda) @ cp.linalg.pinv(psi)
    
       
    return w, Lambda, A
"""
def DMD(data, r):
    Dynamic Mode Decomposition (DMD) algorithm.
    
    global X1,X2,u,s,v, A_tilde,Phi,Q,Psi
    
    ## Build data matrices
    X1 = data[:, : -1]
    X2 = data[:, 1 :]
    ## Perform singular value decomposition on X1
    u, s, v = cp.linalg.svd(X1, full_matrices = False)
    s = cp.matrix(s)
    ## Compute the Koopman matrix
    A_tilde = u[:, : r].conj().T @ X2 
    A_tilde = A_tilde @ v[: r, :].conj().T 
    A_tilde = cp.multiply(A_tilde, cp.reciprocal(s[: r]))
    ## Perform eigenvalue decomposition on A_tilde
    Phi, Q = cp.linalg.eig(A_tilde)
    ## Compute the coefficient matrix
    Psi = X2 @ v[: r, :].conj().T @ cp.diag(cp.reciprocal(s[: r])) @ Q
    A = Psi @ cp.diag(Phi) @ cp.linalg.pinv(Psi)
    
    return A_tilde, Phi, A
"""
def DMD(data, r):
    """Dynamic Mode Decomposition (DMD) algorithm."""
    
    ## Build data matrices
    X1 = data[:, : -1]
    X2 = data[:, 1 :]
    #breakpoint()
    ## Perform singular value decomposition on X1
    u, s, v = cp.linalg.svd(X1, full_matrices = False)
    ## Compute the Koopman matrix
    A_tilde = u[:, : r].conj().T @ X2 @ v[: r, :].conj().T * cp.reciprocal(s[: r])
    ## Perform eigenvalue decomposition on A_tilde
    Phi, Q = cp.linalg.eigh(A_tilde)
    ## Compute the coefficient matrix
    Psi = X2 @ v[: r, :].conj().T @ cp.diag(cp.reciprocal(s[: r])) @ Q
    try:
        A = Psi @ cp.diag(Phi) @ cp.linalg.pinv(Psi)
    except:
        breakpoint()
    return A_tilde, Phi, A

def DMDBIGcast( data, r, pred_step, times=False ):

    if times:
        print("DMD BIG CAST")
        print("     data shape: "+str(data.shape))

    # Seperate our data.
    inputs = data[:,:,:-1]
    outputs = data[:,:,1:]

    # Change inputs to torch for svd.
    inputs = torch.as_tensor(inputs,device="cuda",dtype=torch.float32)

    #for i in range(inputs.shape[0]):
    #    plt.imshow( cp.asnumpy(inputs[i]),aspect="auto")
    #    plt.show()
    
    # Perform the svd
    if times:
        start = time.time()
    #u,s,vt = cp.linalg.svd( inputs, full_matrices=False )
    u,s,vt = torch.linalg.svd( inputs, full_matrices=False, driver="gesvdj" )
    if times:
        print( "        svd time: "+str(time.time()-start) )

    # Back to cupy.
    s = cp.asarray(s)
    vt = cp.asarray(vt)
    u = cp.asarray(u)
    
    # Take the recirpocal of s.
    s = cp.reciprocal(s)[:,:r]
    vt = vt.transpose(0,2,1)[:,:,:r]
    u = u.transpose(0,2,1)[:,:r,:]

    # Compute A for predictions.
    A = outputs @ vt @ ( s[...,None] * u )
    
    # Get the last rows.
    outs = outputs[:,:,outputs.shape[2]-1:]

    # Forecast.
    if times:
        start = time.time()
    for i in range(pred_step):
        outs = cp.matmul( A, outs )
    if times:
        print( "        predict time: "+str(time.time()-start) )
    
    outs = outs.reshape( 1,outs.shape[0]*outs.shape[1] )
    # Return outs.

    return( outs )

def DMD5cast( data, r, pred_step, times=False ):

    # Get the length of data to determine how the svd should be done.
    data_len = len(data.shape)

    if times:
        print("DMD 5 CAST >> data shape: "+str(data.shape))

    if data_len == 3:

        # Seperate our data.
        inputs = data[:-1,:,:].transpose(2,1,0)
        outputs = data[1:,:,:].transpose(2,1,0)

        # Perform the batched svd.
        if times:
            start = time.time()
        u,s,vt = cp.linalg.svd( inputs, full_matrices=False )
        if times:
            end = time.time()
            print("     3D SVD time: "+str(end-start))

        # Change back to cupy.
        u = cp.array(u)
        s = cp.array(s)
        vt = cp.array(vt)

        # Take the reciprocal of s.
        s = cp.reciprocal(s)[:,:r]
        vt = vt.transpose(0,2,1)[:,:,:r]
        u = u.transpose(0,2,1)[:,:r,:]

        # Compute A for predictions.
        if times:
            start = time.time()
        A = outputs @ vt @ ( s[...,None] * u )
        if times:
            end = time.time()
            print("     3D A prod time: "+str(end-start))

        # Get the last rows.
        outs = outputs[:,:,outputs.shape[2]-1:]

        # Forecast.
        if times:
            start = time.time()
        for i in range(pred_step):
            outs = cp.matmul( A, outs )
        if times:    
            end = time.time()
            print("     forecast: "+str(end-start))

        # Reshape outs to a new shape.
        outs = outs.reshape(outs.shape[0],outs.shape[1]).transpose()

    if data_len == 2:
        
        inputs = data[: -1,:].transpose()
        outputs = data[1 :,:].transpose()
        print(inputs.shape)
        # First we perform the svd on the inputs.
        if times:
            start = time.time()
        u,s,vt = cp.linalg.svd( inputs, full_matrices=False )
        if times:
            end = time.time()
            print("     2D SVD time: "+str(end-start))

        # limit the dimension
        u = u[:, : r]
        s = s[ 0 : r ]
        vt = vt[: r, :]

        # Compute the inverse of s.
        invs = cp.linalg.inv(cp.diag(s))

        # Now compute our A matrix
        if times:
            start = time.time()
        A = outputs.dot(vt.transpose()).dot(invs).dot(u.transpose())
        if times:
            end = time.time()
            print("     2D A prod time: "+str(end-start))
        #A = outputs.dot(vt.transpose()).dot(invs).dot(u.transpose())

        # Get the last row.
        outs = outputs[:,outputs.shape[1]-1]

        # Now predict.
        if times:
            start = time.time()
        for i in range(pred_step):
            outs = cp.matmul( A, outs )
        if times:    
            end = time.time()
            print("     forecast: "+str(end-start))   

        # Reshape outs.
        outs = outs.transpose()    

    # Return outs.
    return( outs )

def DMD6cast( data, r, pred_step ):

    # Seperate our data.
    X = data[:-1,:,:].transpose(1,2,0)
    Y = data[1:,:,:].transpose(1,2,0)

    # Perform the svd
    U, s, Vh = cp.linalg.svd(X, full_matrices=False) 

    s = cp.reciprocal(s)

    A_tilde = U.conj().transpose(0,2,1) @ Y @ ( s[...,None] * Vh.conj().transpose(0,2,1) )

    lam, w = cp.linalg.eigh((A_tilde @ A_tilde.transpose(0,2,1)))

    phi = ((1./cp.sqrt(lam[:,-1].reshape(lam.shape[0],1)))[...,None] * Y) @ ( s[...,None] * Vh.conj().transpose(0,2,1) ) @ w[...,None,-1]

    A = phi @ cp.linalg.pinv(phi)

    # Get the last rows.
    outs = Y[:,:,Y.shape[2]-1:]

    # Forecast.
    for i in range(pred_step):
        outs = cp.matmul( A, outs )

    # Reshape outs to a new shape.
    outs = outs.reshape(outs.shape[0],outs.shape[1])

    # Return outs.
    return( outs )

def DMD4cast(data, r, pred_step):

    N, T = data.shape
    dstart = time.time()
    _, _, A = DMD3(data, r=r)
    print("     dmd3 time: "+str(time.time()-dstart))

    mat = cp.append(data, cp.zeros((N, pred_step)), axis = 1)
    for s in range(pred_step):
        mat[:, T + s] = (A @ mat[:, T + s - 1]).real

    return mat[:, - pred_step :]

def DMDstep(model, weights, r, pred_step, params=None, verbose=True):
    if params is None:
        params = [i for i in range(len(weights))]
        
    for i,(W,param) in tqdm(enumerate(zip(weights,model.parameters())), total=len(params), disable=~verbose):
        if i in params:
            start = time.time()
            M = W.reshape(W.shape[0], cp.prod(W.shape[1:])).T
            new_weight = DMD4cast(M, r, pred_step)[:,-1].T
            new_weight = new_weight.reshape(W[0].shape)

            param.data = Tensor(new_weight)
            if verbose:
                print(f"Step {i}: {time.time()-start}")