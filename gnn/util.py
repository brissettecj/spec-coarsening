import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import networkx as nx
import math

'''----------------------------------------------------------------
atoi(string)
- INPUT: A numerical string
- DOES: Changes it to its integer value. 
- RETURNS: The integer value.
----------------------------------------------------------------'''
def atoi(string):
	res = 0
	for i in range( len(string) ):
		res = res*10 + (ord(string[i])-ord('0'))
	return( res )

'''----------------------------------------------------------------
fast_match(G,weight_name,mtype)
- INPUT: A networkx graph 'G'. Additionally takes in the string 'weight_name' 
		 controlling which weight we use. Also takes in a 'mtype' which should be
		 either "max", or "min"
- DOES: Uses Kruskal's algorithm to find a minimum spanning tree assuming 'G'
	    is connected. Then greedily finds a minimum matching on this tree.
- RETURNS: An edge list of matches.
----------------------------------------------------------------'''
def fast_match(G, weight_name="weight", mtype="max"):

	# obtain a matching on that spanning tree
	if mtype == "max":
		H = nx.maximum_spanning_tree( G, weight=weight_name )
		matching = nx.max_weight_matching( H, weight=weight_name )
	elif mtype == "min":
		H = nx.minimum_spanning_tree( G, weight=weight_name )
		matching = nx.min_weight_matching( H, weight=weight_name )
	else:
		print( "invalid 'mtype' in fast_match()...")

	# return the matching
	return( matching )

if __name__ == "__main__":

	print( "Generating partition graph..." )
	G = nx.random_partition_graph( [1000,1000,1000],0.6,0.2 )
	for u in G.nodes():
		G.nodes[u]["num"] = 1
	for e in G.edges():
		G[e[0]][e[1]]["weight"] = 1 

	print( "Getting matching..." )
	matching = fast_minmatch( G )
	print( matching )
	H = G.subgraph(matching)
	plt.figure(1)
	nx.draw(H,with_labels=True)
	plt.show(  )


