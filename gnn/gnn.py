import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv
import numpy as np

class GNN(torch.nn.Module):
	
	def __init__(self,num_features,hidden,num_classes, seed=None):
		super(GNN,self).__init__()

		seed = np.random.randint(0,high=999999,dtype=int) if seed is None else seed
		torch.manual_seed(seed)

		self.conv1 		= GCNConv(num_features,hidden)
		self.conv2		= GCNConv(hidden,hidden)
		self.conv3		= GCNConv(hidden,hidden)
		self.lay3 		= nn.Linear(hidden, hidden)
		self.final_layer= nn.Linear(hidden, num_classes)
	
	def reset_parameters(self):
		self.conv1.reset_parameters()
		self.conv2.reset_parameters()
		self.lay3.reset_parameters()
		self.final_layer.reset_parameters()

	def forward(self,torchG):

		x, edge_index = torchG.x, torchG.edge_index
		x = x.float()

		x = self.conv1(x,edge_index)
		x = F.relu(x)
		x = F.dropout(x,training=self.training)
		x = self.conv2(x,edge_index)
		#x = F.relu(x)
		#x = self.conv3(x, edge_index)
		x = self.lay3(x)
		x = F.relu(x)
		x = self.final_layer(x)

		return F.log_softmax(x, dim=1)


class Net(torch.nn.Module):
	def __init__(self, num_features, hidden, num_classes, seed=None):

		seed = np.random.randint(0,high=999999,dtype=int) if seed is None else seed
		torch.manual_seed(seed)
		
		super(Net, self).__init__()
		self.conv1 = GCNConv(num_features, hidden)
		self.conv2 = GCNConv(hidden, num_classes)

	def reset_parameters(self):
		self.conv1.reset_parameters()
		self.conv2.reset_parameters()

	def forward(self, torchG):
		x, edge_index, = torchG.x, torchG.edge_index

		x = self.conv1(x, edge_index)
		x = F.relu(x)
		x = F.dropout(x, training=self.training)
		x = self.conv2(x, edge_index)

		return F.log_softmax(x, dim=1)