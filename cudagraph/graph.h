#ifndef GRAPH_H
#define GRAPH_H

// GRAPH.H 
/*------------------------------------------------------------------------------
defines the bascis structure and functions of the graph_struct.
------------------------------------------------------------------------------*/

struct graph_struct
{
	int64_t node_num;
	int64_t edge_num;
	int64_t** adjacency;
	double** weights;
};

typedef struct graph_struct graph;

#endif
