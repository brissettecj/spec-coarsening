import tools
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.sparse as sparse
import networkx as nx 

#nodes1, edges1, mesh_dict1 = tools.read_general_mesh( "../bunny_17000.txt" )
#nodes2, edges2, mesh_dict2 = tools.read_general_mesh( "../bunny_8500.txt" )
#nodes3, edges3, mesh_dict3 = tools.read_general_mesh( "../bunny_4750.txt" )

#nodes1, edges1, mesh_dict1 = tools.read_general_mesh( "../facebook_2000.txt" )
#nodes2, edges2, mesh_dict2 = tools.read_general_mesh( "../facebook_1000.txt" )
#nodes3, edges3, mesh_dict3 = tools.read_general_mesh( "../facebook_500.txt" )

nodes1, edges1, mesh_dict1 = tools.read_general_mesh( "../bjt_2000.txt" )
nodes2, edges2, mesh_dict2 = tools.read_general_mesh( "../bjt_1000.txt" )
nodes3, edges3, mesh_dict3 = tools.read_general_mesh( "../bjt_500.txt" )

A,B1 = tools.get_lifted_matrices( nodes1, edges1, mesh_dict1 )
A,B2 = tools.get_lifted_matrices( nodes2, edges2, mesh_dict2 )
A,B3 = tools.get_lifted_matrices( nodes3, edges3, mesh_dict3 )
print("matrices formed... ")

'''
G = nx.from_numpy_matrix( A )
print("original graph formed...")
H1 = nx.from_numpy_matrix( B1 )
print("first graph formed...")
H2 = nx.from_numpy_matrix( B2 )
print("second graph formed...")
H3 = nx.from_numpy_matrix( B3 )
print("third graph formed...")

H = [H1,H2,H3]
'''

#L = np.identity( len(B3) ) - np.matmul( np.matmul( np.linalg.inv( np.sqrt( np.diag( np.sum( B3, axis=1) ) ) ), B3 ), np.linalg.inv( np.sqrt( np.diag( np.sum( B3, axis=1) ) ) ) )

I = sparse.csr_matrix( np.identity( len(A) ) )
Q = np.sqrt( [np.sum( A, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( A )
X = D.dot( W )
Y = X.dot( D ) 
L = I - Y
print( "first laplacian done..." )
I = sparse.csr_matrix( np.identity( len(B1) ) )
Q = np.sqrt( [np.sum( B1, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B1 )
X = D.dot( W )
Y = X.dot( D ) 
L1 = I - Y
print( "second laplacian done..." )
I = sparse.csr_matrix( np.identity( len(B2) ) )
Q = np.sqrt( [np.sum( B2, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B2 )
X = D.dot( W )
Y = X.dot( D ) 
L2 = I - Y
print( "third laplacian done..." )
I = sparse.csr_matrix( np.identity( len(B3) ) )
Q = np.sqrt( [np.sum( B3, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B3 )
X = D.dot( W )
Y = X.dot( D ) 
L3 = I - Y
print("laplacians formed...")

L_coarse = [L1,L2,L3]

tools.compare_spectra_triple( L, L_coarse, 50 )