#import reader
import tools
import numpy as np
import matplotlib.pyplot as plt
import scipy
import networkx as nx 
'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''

#nodes, edges, mesh_dict = tools.read_general_mesh( "../facebook_500.txt" )
nodes, edges, mesh_dict = tools.read_general_mesh( "../bunny_17000.txt" )
#A,B = tools.get_matrices( nodes, edges, mesh_dict )
A,B = tools.get_lifted_matrices( nodes, edges, mesh_dict )
print( np.shape(A) )
print( np.shape(B) )
G = nx.from_numpy_matrix( A )
H = nx.from_numpy_matrix( B )

tools.compare_spectra( G, H, 100 )

'''
edge_dict = tools.to_edgelist( nodes, edges, mesh_dict )
M = tools.get_matrix( nodes, edge_dict )
G = nx.from_numpy_matrix( M )


sums = M.sum(axis=1)
for i in range( nodes ):
	if sums[i] == 0:
		sums[i] = 1

normM = M/sums[:,np.newaxis]

print( normM )

edge_fitness = tools.get_weights( edge_dict, normM )

H = tools.perform_merges( G, edge_dict, edge_fitness, 15000 )
tools.compare_spectra( G, H )
'''





	

