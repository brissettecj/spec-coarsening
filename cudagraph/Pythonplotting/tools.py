import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import networkx as nx

def read_general_mesh( filename ):
	with open(filename) as file:
		lines = file.readlines()

	connections = {}

	index = 0
	for line in lines:
		curr_line = line.split()
		if index == 0:
			nodes = int( curr_line[0] )
		elif index == 1:
			edges = int( curr_line[0] )
		else:
			connections[ index-2 ] = []
			pos = 0
			for element in curr_line:
				if pos%2 == 0:
					if pos != 0 and pos != 2:
						connections[ index-2 ].append( float( element ) )
					else:
						connections[ index-2 ].append( int( element ) )
				else:
					connections[ index-2 ].append( int( element ) )
				pos += 1
		index = index + 1 

	# prune all degree 0 nodes
	keys = [ key for key in connections.keys() ]
	for key in keys:
		if connections[ key ][ 2 ] == 0:
			connections.pop( key )
			#nodes = nodes - 1

	return( nodes, edges, connections )

def get_root( conn_dict, nodes, node ):
	root_node = node
	value = conn_dict[ root_node ][ 0 ]
	while value != 0:
		root_node = conn_dict[ root_node ][ 0 ]%nodes
		value = conn_dict[ root_node ][ 0 ]
	return( root_node )



def to_edgelist( nodes, edges, conn_dict ):
	edge_list = {}
	index = 0
	for i in conn_dict.keys():
		degree = conn_dict[i][2]
		for j in range( degree ):
			new_edge = np.sort( [i,conn_dict[i][3 + (2*j)]] )
			edge_list[ index ] = [new_edge,conn_dict[i][4 + (2*j)]]
			index += 1

	return( edge_list )

def get_matrices( nodes, edges, conn_dict ):
	orig_node_map = {}
	coarse_node_map = {}
	orig_index = 0
	coarse_index = 0
	for i in conn_dict.keys():
		orig_node_map[ i ] = orig_index
		orig_index += 1
		if conn_dict[ i ][ 0 ] == 0:
			coarse_node_map[ i ] = coarse_index
			coarse_index += 1

	A = np.zeros((len(conn_dict.keys()),len(conn_dict.keys())))
	B = np.zeros((len(coarse_node_map.keys()),len(coarse_node_map.keys())))

	edgelist = to_edgelist( nodes, edges, conn_dict )

	for i in edgelist.keys():
		nod_a = orig_node_map[ edgelist[i][0][0] ]
		nod_b = orig_node_map[ edgelist[i][0][1] ]
		A[nod_a][nod_b] += edgelist[i][1]
		A[nod_b][nod_a] += edgelist[i][1]

		root_a = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][0] ) ]
		root_b = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][1] ) ]
		B[root_a][root_b] += edgelist[i][1]
		B[root_b][root_a] += edgelist[i][1]

	return( A,B )

def get_lifted_matrices( nodes, edges, conn_dict ):
	orig_node_map = {}
	coarse_node_map = {}
	orig_index = 0
	coarse_index = 0
	for i in conn_dict.keys():
		orig_node_map[ i ] = orig_index
		orig_index += 1
		if conn_dict[ i ][ 0 ] == 0:
			coarse_node_map[ i ] = coarse_index
			coarse_index += 1

	A = np.zeros((len(conn_dict.keys()),len(conn_dict.keys())))
	B = np.zeros((len(coarse_node_map.keys()),len(coarse_node_map.keys())))
	#A = sparse.csr_matrix((len(conn_dict.keys()),len(conn_dict.keys())),dtype=np.float64 )
	#B = sparse.csr_matrix((len(coarse_node_map.keys()),len(coarse_node_map.keys())),dtype=np.float64 )
	edgelist = to_edgelist( nodes, edges, conn_dict )

	clusters = {}
	nums = {}
	for i in range( len( coarse_node_map.keys() ) ):
		clusters[ i ] = []
		nums[i] = 0
	
	for node in conn_dict.keys():
		actual_node = orig_node_map[ node ]
		node_assignment = coarse_node_map[ get_root( conn_dict, nodes, node ) ]
		clusters[ node_assignment ].append( actual_node )
		nums[ node_assignment ] += 1

	lift_mat = np.zeros( (len(orig_node_map.keys()), len(coarse_node_map.keys()) ) )
	#lift_mat = sparse.csr_matrix( (len(orig_node_map.keys()), len(coarse_node_map.keys()) ) ) 

	for i in clusters.keys():
		for j in clusters[i]:
			#lift_mat[j,i] = 1/nums[i]
			lift_mat[j][i] = 1/nums[i]

	for i in edgelist.keys():
		nod_a = orig_node_map[ edgelist[i][0][0] ]
		nod_b = orig_node_map[ edgelist[i][0][1] ]
		#A[nod_a,nod_b] += edgelist[i][1]
		#A[nod_b,nod_a] += edgelist[i][1]
		A[nod_a][nod_b] += edgelist[i][1]
		A[nod_b][nod_a] += edgelist[i][1]

		root_a = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][0] ) ]
		root_b = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][1] ) ]
		#B[root_a,root_b] += edgelist[i][1]
		#B[root_b,root_a] += edgelist[i][1]
		B[root_a][root_b] += edgelist[i][1]
		B[root_b][root_a] += edgelist[i][1]

	B = np.matmul(np.matmul(lift_mat, B), lift_mat.transpose())
	#B = ( lift_mat.dot(B) ).dot( lift_mat.transpose() )
	out_map = {orig_node_map[key]:key for key in orig_node_map.keys()}
	return( A,B,out_map )

def get_matrix( nodes, edge_list ):

	M = np.zeros((nodes,nodes))

	for i in edge_list.keys():
		nod_a = edge_list[i][0]
		nod_b = edge_list[i][1]
		M[nod_a][nod_b] = 1
		M[nod_b][nod_a] = 1

	return( M )

def get_weights( edge_dict, M ):
	weight_dict = {}
	for i in edge_dict.keys():
		node_a = edge_dict[i][0]
		node_b = edge_dict[i][1]
		norm = np.linalg.norm( M[node_a] - M[node_b], 1 )
		weight_dict[ i ] = norm
	sorted_dict = {k: v for k, v in sorted(weight_dict.items(), key=lambda item: item[1])}
	return( sorted_dict )

def perform_merges( G, edge_dict, edge_fitness, threshold ):
	H = G.copy()
	contractions = 0
	index = 0
	mapping = {i:i for i in G.nodes()}
	for i in edge_fitness.keys():
		a = edge_dict[ i ][0]
		b = edge_dict[ i ][1]
		node_a = mapping[ a ]
		node_b = mapping[ b ]
		if node_a != node_b:
			print( "performing contraction..."+str(index))
			index += 1
			H = nx.contracted_nodes( H, node_a, node_b )
			for i in mapping.keys():
				if mapping[i] == node_b or mapping[i] == b:
					mapping[i] = node_a
			contractions += 1
		if contractions > threshold:
			break

	return( H )

def compare_spectra( G, H, k ):
	xg = range( k )
	xh = range( k )
	lap_G = nx.normalized_laplacian_matrix(G,weight="weight")
	lap_H = nx.normalized_laplacian_matrix(H,weight="weight")
	print( "IN: compare_spectra() -- computing eigen values / vectors..." )
	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k, which="SM" )
	evals_H,evecs_H = sp.sparse.linalg.eigs( lap_H, k, which="SM" )

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.scatter( xg, evals_G, c='r', marker='.', label='original')
	ax1.scatter( xh, evals_H, c='b', marker='+', label='coarsened')
	plt.legend(loc='upper left')
	plt.title("BJT coarsened spectra")
	plt.xlabel("eigenvalue index")
	plt.ylabel("value")

	arr_comp = np.abs( np.matmul( evecs_G.transpose(), evecs_H ) )
	
	fig2 = plt.figure()
	ax2 = fig2.add_subplot(111)
	ax2.imshow(arr_comp,cmap="YlGnBu")
	plt.title("BJT eigenvector comparison")
	

	plt.show()

def compare_spectra_triple( L, L_coarse, k ):
	xg = range( k )
	xh = range( k )
	'''
	lap_G = nx.normalized_laplacian_matrix(G,weight="weight")
	lap_H1 = nx.normalized_laplacian_matrix(H[0],weight="weight")
	lap_H2 = nx.normalized_laplacian_matrix(H[1],weight="weight")
	lap_H3 = nx.normalized_laplacian_matrix(H[2],weight="weight")
	print( "IN: compare_spectra() -- computing eigen values / vectors..." )
	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k, which="SM" )
	evals_H1,evecs_H1 = sp.sparse.linalg.eigs( lap_H1, k, which="SM" )
	print( "	first eigenvalues computed...")
	evals_H2,evecs_H2 = sp.sparse.linalg.eigs( lap_H2, k, which="SM" )
	print( "	second eigenvalues computed...")
	evals_H3,evecs_H3 = sp.sparse.linalg.eigs( lap_H3, k, which="SM" )
	print( "	third eigenvalues computed...")
	'''
	print( "IN: compare_spectra() -- computing eigen values / vectors..." )
	evals_G,evecs_G = sp.sparse.linalg.eigs( L, k, which="SM" )
	evals_H1,evecs_H1 = sp.sparse.linalg.eigs( L_coarse[0], k, which="SM" )
	print( "	first eigenvalues computed...")
	evals_H2,evecs_H2 = sp.sparse.linalg.eigs( L_coarse[1], k, which="SM" )
	print( "	second eigenvalues computed...")
	evals_H3,evecs_H3 = sp.sparse.linalg.eigs( L_coarse[2], k, which="SM" )
	print( "	third eigenvalues computed...")

	idx1 = evals_H1.argsort()[::1]
	evals_H1 = evals_H1[idx1]
	evecs_H1 = evecs_H1[:,idx1]
	idx2 = evals_H2.argsort()[::1]
	evals_H2 = evals_H2[idx2]
	evecs_H2 = evecs_H2[:,idx2]
	idx3 = evals_H3.argsort()[::1]
	evals_H3 = evals_H3[idx3]
	evecs_H3 = evecs_H3[:,idx3]

	print( "plotting..." )
	fig, ( ax1, ax2, ax3 ,ax4 ) = plt.subplots(1,4)
	ax1.plot( xg, evals_G, '--', c='c', label='original (n)')
	ax1.scatter( xh, evals_H1, c='firebrick', marker='.', label='coarsened (n/2)')
	ax1.scatter( xh, evals_H2, c='yellowgreen', marker='x', label='coarsened (n/4)')
	ax1.scatter( xh, evals_H3, c='goldenrod', marker='+', label='coarsened (n/8)')
	ax1.legend(loc='upper left',fontsize=14)
	ax1.set_title("BJT coarsened spectra",fontsize=20)
	ax1.set_xlabel("eigenvalue index",fontsize=18)
	ax1.set_ylabel("value",fontsize=18)

	arr_comp1 = np.abs( np.matmul( evecs_G.transpose(), evecs_H1 ) )
	arr_comp2 = np.abs( np.matmul( evecs_G.transpose(), evecs_H2 ) )
	arr_comp3 = np.abs( np.matmul( evecs_G.transpose(), evecs_H3 ) )
	
	ax2.set_title("eigenvector comparison (n/2)",fontsize=20)
	ax2.imshow(arr_comp1,cmap="YlGnBu")
	ax3.set_title("eigenvector comparison (n/4)",fontsize=20)
	ax3.imshow(arr_comp2,cmap="YlGnBu")
	ax4.set_title("eigenvector comparison (n/8)",fontsize=20)
	ax4.imshow(arr_comp3,cmap="YlGnBu")

	ax1.set_box_aspect(1)
	ax2.set_box_aspect(1)
	ax3.set_box_aspect(1)
	ax4.set_box_aspect(1)
	

	plt.show()

def comp_spectrum( L, L_coarse, k ):

	print( "IN: compare_spectra() -- computing eigen values / vectors..." )
	evals_G,evecs_G = sp.sparse.linalg.eigs( L, k, which="SM" )
	evals_H1,evecs_H1 = sp.sparse.linalg.eigs( L_coarse[0], k, which="SM" )
	print( "	first eigenvalues computed...")
	evals_H2,evecs_H2 = sp.sparse.linalg.eigs( L_coarse[1], k, which="SM" )
	print( "	second eigenvalues computed...")
	evals_H3,evecs_H3 = sp.sparse.linalg.eigs( L_coarse[2], k, which="SM" )
	print( "	third eigenvalues computed...")
	evals = {0:evals_G, 1:evals_H1, 2:evals_H2, 3:evals_H3}
	evecs = {0:evecs_G, 1:evecs_H1, 2:evecs_H2, 3:evecs_H3}

	return( evals, evecs )