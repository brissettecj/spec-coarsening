import numpy as np 

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''
def read_general_mesh( filename ):
	with open(filename) as file:
		lines = file.readlines()

	connections = {}

	index = 0
	for line in lines:
		curr_line = line.split()
		if index == 0:
			nodes = int( curr_line[0] )
		if index == 1:
			edges = int( curr_line[0] )
		else:
			connections[ index-2 ] = [ int( float( k ) ) for k in curr_line ]
		index += 1

	return( connections )


'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''
def get_k_level_matrix( connection_dict ):
	nodes = len( connection_dict )-1
	mapping = {}
	index = 0
	# first get a mapping
	for i in range( nodes ):
		if connection_dict[i][0] == 0:
			mapping[i] = index
			index += 1
	# now allocate and fill our array
	A = np.zeros((nodes,nodes))
	M = np.zeros((len(mapping),len(mapping)))
	for u in range( nodes ):
		for v in range( connection_dict[u][2] ):
			u_root = get_root( connection_dict, u )
			v_root = get_root( connection_dict, connection_dict[u][3+(2*v)] )
			print( u_root )
			print( v_root )
			print("")
			M[ mapping[u_root], mapping[v_root] ] += connection_dict[u][3+(2*v)+1]
			A[ u, v ] = connection_dict[ u ][ 3 + (2*v) + 1 ]
			A[ v, u ] = connection_dict[ u ][ 3 + (2*v) + 1 ]
	return( A, M )

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''
def get_root( connection_dict, node ):
	nodes = len( connection_dict )-1
	index = node
	value = connection_dict[ index ][ 0 ]
	while value != 0:
		index = value % nodes
		value = connection_dict[ index ][ 0 ] 
	return( index )

