import numpy as np
import netCDF4

def load_exo( filename, new_filename, voltrange=[0], nodal_var='vals_nod_var2', verbose=False ):
	if verbose:
		print("load_exo...")

	nc = netCDF4.Dataset('./'+filename)
	# read in positions
	X = np.array(nc.variables['coordx'])
	Y = np.array(nc.variables['coordy'])
	# read in connections
	connect = nc.variables['connect1']

	if verbose:
		print("size: "+str(len(X)))

	# initialize weights array.
	weight = []
	for i in voltrange:
		weight.append(np.array(nc.variables[nodal_var][:])[i])# - np.array(nc.variables[nodal_var][:])[0])
	weights = np.array(weight).transpose()

	nodes = np.arange(len(weights)).astype(int)
	node_num = len( nodes )
	edge_num = 0
	for i in nodes:
		eps = 0.0001
		xy = np.array([X[:], Y[:]]).T
		mapping = {}
		neighbors = {key:[0,0,0] for key in range(len(X))}
	for j in range(len(X[:])):
		mapping[tuple(xy[j])] = j
	for coords in xy[connect[:]-1]:
		for e in zip(coords, np.roll(coords,1,axis=0)):
			edge_num += 1
			edge = [(0,0),(0,0)]
			edge[0] = tuple(e[0])
			edge[1] = tuple(e[1])
			diff = weights[mapping[edge[0]]] - weights[mapping[edge[1]]]
			weight = 1/((np.linalg.norm(diff)) + eps )
			if mapping[edge[1]] not in neighbors[mapping[edge[0]]]:
				neighbors[mapping[edge[0]]].append(mapping[edge[1]])
				neighbors[mapping[edge[0]]].append( weight )
				neighbors[mapping[edge[0]]][2] += 1
			if mapping[edge[0]] not in neighbors[mapping[edge[1]]]:
				neighbors[mapping[edge[1]]].append(mapping[edge[0]])
				neighbors[mapping[edge[1]]].append( weight )
				neighbors[mapping[edge[1]]][2] += 1

	file = open( new_filename, "w" )
	# write to file
	file.write("%i \n" % node_num )
	file.write("%i \n" % edge_num )
	for node in neighbors.keys():
		file.write("%li %li %li " % (neighbors[node][0], neighbors[node][1], neighbors[node][2]) )
		degree = neighbors[node][2]
		for i in range( degree ):
			file.write( "%i %f " % (neighbors[node][3+(2*i)], neighbors[node][4+(2*i)]) )
		file.write( "\n" )
	file.close( )

load_exo( "../bjt.sg.Vb0p5.varyVc.exo", "bjt_adjlist.txt", np.arange(22) )