import original_reader as reader
import tools
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import networkx as nx 

'''
FUNCTION:
INPUTS:
DOES:
RETURNS:
NOTES:
'''

'''
pos_dat, con_dat = reader.read_raw( "./bunny/bunny.txt" )
points = reader.get_k_level_points( pos_dat, con_dat )
reader.k_level_to_vtk( points, "./out_bunny/raw" )

for i in range( 100 ):	
	pos_dat, con_dat = reader.read_from_text( "./bunny/bunny.txt", "./bunny/round"+str(i)+".txt" )
	
	points = reader.get_k_level_points( pos_dat, con_dat )
	reader.k_level_to_vtk( points, "./out_bunny/points"+str(i) )

	ownership = reader.get_k_ownership( pos_dat, con_dat, 100000 )
	reader.colors_to_vtk( pos_dat, "./out_bunny/colors"+str(i), ownership )
'''
print("reading positions... ")
pos_dat1, con_dat1 = reader.read_from_text( "../nets/bunny.txt", "../bunny_17000.txt" )
pos_dat2, con_dat2 = reader.read_from_text( "../nets/bunny.txt", "../bunny_8500.txt" )
pos_dat3, con_dat3 = reader.read_from_text( "../nets/bunny.txt", "../bunny_4750.txt" )

print("reading meshes... ")
nodes1, edges1, mesh_dict1 = tools.read_general_mesh( "../bunny_17000.txt" )
nodes2, edges2, mesh_dict2 = tools.read_general_mesh( "../bunny_8500.txt" )
nodes3, edges3, mesh_dict3 = tools.read_general_mesh( "../bunny_4750.txt" )

print("forming adjacencies...")
A,B1,outmap = tools.get_lifted_matrices( nodes1, edges1, mesh_dict1 )
A,B2,outmap = tools.get_lifted_matrices( nodes2, edges2, mesh_dict2 )
A,B3,outmap = tools.get_lifted_matrices( nodes3, edges3, mesh_dict3 )
print("matrices formed... ")

I = sparse.csr_matrix( np.identity( len(A) ) )
Q = np.sqrt( [np.sum( A, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( A )
X = D.dot( W )
Y = X.dot( D ) 
L = I - Y
print( "first laplacian done..." )
'''
evals_G,evecs_G = sp.sparse.linalg.eigs( L, 11, which="SM" )
print( evecs_G[:,1] )
print( np.real(evecs_G[:,1]) )
reader.eigen_to_vtk( pos_dat1, "raw_bunny.vtk", np.real(evecs_G), outmap )

'''
I = sparse.csr_matrix( np.identity( len(B1) ) )
Q = np.sqrt( [np.sum( B1, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B1 )
X = D.dot( W )
Y = X.dot( D ) 
L1 = I - Y
print( "second laplacian done..." )
I = sparse.csr_matrix( np.identity( len(B2) ) )
Q = np.sqrt( [np.sum( B2, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B2 )
X = D.dot( W )
Y = X.dot( D ) 
L2 = I - Y
print( "third laplacian done..." )
I = sparse.csr_matrix( np.identity( len(B3) ) )
Q = np.sqrt( [np.sum( B3, axis=1)] )
Q = np.reciprocal( Q, where = Q != 0 )
D = sparse.diags( Q, [0], format="csr" )
#D = sparse.csr_matrix( np.reciprocal( Q, where = Q!=0 ) )
W = sparse.csr_matrix( B3 )
X = D.dot( W )
Y = X.dot( D ) 
L3 = I - Y
print("laplacians formed...")
L_coarse = [L1,L2,L3]

print("computing spectra...")
enum = 11
evals,evecs = tools.comp_spectrum( L, L_coarse, enum )

for i in range( enum ):
	cannonical_sign = evecs[0][0,i]
	for j in range( len(evecs) - 1 ):
		other_sign = evecs[j+1][0,i]
		if cannonical_sign != other_sign:
			evecs[j+1][:,i] = (-1)*evecs[j+1][:,i]
			
print("outputting vtk...")
reader.eigen_to_vtk( pos_dat1, "raw_bunny.vtk", np.real(evecs[0]), outmap  )
reader.eigen_to_vtk( pos_dat1, "bunny_17000.vtk", np.real(evecs[1]), outmap  )
reader.eigen_to_vtk( pos_dat1, "bunny_8500.vtk", np.real(evecs[2]), outmap  )
reader.eigen_to_vtk( pos_dat1, "bunny_4750.vtk", np.real(evecs[3]), outmap  )
