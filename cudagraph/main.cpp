#include "utils.h"

void main( int argc, char** argv )
{
	// define our input variables.
	char* filename = argv[0];
	int64_t reduced_nodes = ( int64_t )atoi( argv[1] );
	int block_num = atoi( argv[2] );
	int thread_num = atoi( argv[3] );

	// read in our graph from input file. 

	// allocate our graph structures and merge_values array in unified memory.
	int64_t** adjacency;
	double** weights, merge_vals;
	int64_t n,m;

	int64_t degree = 0;

	cudaMallocManaged( &adjacency, n*sizeof( int64_t* ) );
	cudaMallocManaged( &weights, n*sizeof( double* ) );
	cudaMallocManaged( &merge_vals, n*sizeof( double* ) );

	for( int64_t i = 0; i < n; i++ )
	{
		degree = *( *( g<-adjacency + i ) + 2 );
		cudaMallocManaged( &( adjacency + i ), (degree + 3)*sizeof( int64_t ) );
		cudaMallocManaged( &( weights + i ), degree*sizeof( double ) );
		cudaMallocManaged( &( merge_vals + i ), n*sizeof( double* ) );
	}

	// fill merge_values in parallel.
	par_utils::get_coarsen_vals<<< block_num, thread_num >>>( adjacency, weights, merge_vals, n );

	// sort merge_nodes.


	// merge nodes sequentially by sort.
}