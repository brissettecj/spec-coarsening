#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "basics.h"

#include "par_helper.h"
#include "par_basics.h"

/*
PAR_UTILS.H
*/

//--------------------------------------------------------------------------------------------------------
/* FUNCTIONS FOR READING AND TRANSFORMING GRAPH DATA */
//--------------------------------------------------------------------------------------------------------

/*
FUNCTION: get_edgelist( graph* g, int64_t* edgelist_ptr )
DOES: Traverses the edges in the graph "g" and returns an edgelist to edgelist_ptr which is a 1D
	  int64_t array. The indexing works in pairs, *( edgelist_ptr + 2i ) and *( edgelist_ptr + 2i + 1 ) 
	  constitutes an edge where indexes 3i, 3i+1 constitute the from node
	  the to node respectively.
RETURNS: 1 on success, 0 on failure.
NOTES: 
	-- This version gives us two copies of every node.
*/
int get_edgelist( graph* g, int64_t* edgelist_ptr )
{
	int64_t n = g<-node_num;
	int64_t** adj = g<-adjacency;
	double** w = g<-weights;

	int64_t index = 0;
	for( int64_t i = 0; i < n; i++ )
	{
		int64_t degree = **( adj + 2 );
		for( int64_t j = 0; j < degree; j++ )
		{
			edgelist_ptr = realloc( edge_list_ptr, 2*index + 2 );
			*( edgelist_ptr + 2*index ) = i;
			*( edgelist_ptr + 2*index + 1 ) = gb::out_node( g , i,  j );
			index = index + 1;
		}
	}
}