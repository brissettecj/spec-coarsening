#include "utils.h"
#include "graph.h"


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

int main( int argc, char** argv )
{
	// define our input variables.
	char* filename = argv[1];
	int64_t reduced_nodes = ( int64_t )atoi( argv[2] );
	int block_num = atoi( argv[3] );
	int thread_num = atoi( argv[4] );
	char* new_filename = argv[5];
	printf( "%li %d %d\n", reduced_nodes, block_num, thread_num );

	// allocate host memory for our graph object.
	double** host_weight = ( double** )malloc( sizeof( double* ) );
	int64_t** host_adj = ( int64_t** )malloc( sizeof( int64_t* ) );
	int64_t n = 0;
	int64_t m = 0;

	printf("readtime\n");
	read_edgelist( filename, &host_adj, &host_weight, &n, &m );
	//read_stanford_mesh( filename, &host_adj, &host_weight, &n, &m );
	//read_adjlist( filename, &host_adj, &host_weight, &n, &m );
	printf("donezo\n");
	
	/*
	printf( " 	nodes: %li edges: %li\n", n, m );
	for( int64_t i = 0; i < n; i++ )
	{
	 	int64_t degree = *( *( host_adj + i ) + 2 );
	 	printf( " 	node: %li :: degree: %li :: adj: ", i, degree );
	 	for( int64_t j = 0; j < degree + 3; j++ )
	 	{
	 		printf("%li ", *( *( host_adj + i ) + j ) );
	 	}
	 	printf( "\n" );
	}

	if( ret != 0 )
	{
		printf("read spaghettios\n");
	}
	*/
	sort_adj( &host_adj, &host_weight, n );
	
	// allocate our graph structures and merge_values array in unified memory.
	int64_t **adjacency;
	double **weights, **merge_vals;
	int64_t degree = 0;

	printf( "allocating memory\n" );
	cudaMallocManaged( &adjacency, n*sizeof( int64_t* ) );
	cudaMallocManaged( &weights, n*sizeof( double* ) );
	cudaMallocManaged( &merge_vals, n*sizeof( double* ) );
	printf( "base memory allocated\n" );
	for( int64_t i = 0; i < n; i++ )
	{
		degree = *( *( host_adj + i ) + 2 );
		//cudaMallocManaged( &( adjacency + i ), (degree + 3)*sizeof( int64_t ) );
		//cudaMallocManaged( &( weights + i ), degree*sizeof( double ) );
		//cudaMallocManaged( &( merge_vals + i ), degree*sizeof( double* ) );
		gpuErrchk( cudaMallocManaged( &adjacency[i], (degree + 3) * sizeof( int64_t ) ) );
		gpuErrchk( cudaMallocManaged( &weights[i], ( degree + 1 ) * sizeof( double ) ) );
		gpuErrchk( cudaMallocManaged( &merge_vals[i], ( degree + 1 ) * sizeof( double* ) ) );

		adjacency[i][0] = host_adj[i][0];
		adjacency[i][1] = host_adj[i][1];
		adjacency[i][2] = host_adj[i][2];

		for( int64_t j = 0; j < degree; j++ )
		{
			adjacency[i][j+3] = host_adj[i][j+3];
			weights[i][j] = host_weight[i][j];
			merge_vals[i][j] = 0;
		}
	}

	printf( "memory allocated\n");

	// fill merge_values in parallel.
	par_utils::get_coarsen_vals<<< block_num, thread_num >>>( adjacency, weights, merge_vals, n );
	cudaDeviceSynchronize();
	/*
	for( int64_t i = 0; i < n; i++ )
	{
		degree = *(*(adjacency + i) + 2);
		for( int64_t j = 0; j < degree; j++ )
		{
			printf(" nodes: (%li,%li), merge_val: %lf\n", i, *(*(adjacency + i)+j+3), *(*(merge_vals + i) + j) );
		}
	}
	*/
	
	// sort merge_vals.
	double* mvals = (double*)malloc( 2 * m * sizeof( double ) );
	int64_t** new_adj = (int64_t**)malloc( 2 * m * sizeof( int64_t* ) );
	int64_t* indices = (int64_t*)malloc( 2 * m * sizeof( int64_t ) );
	for( int64_t i = 0; i < 2 * m; i++ )
	{
		indices[i] = i;
		*( new_adj + i ) = ( int64_t* )malloc( 2 * sizeof( int64_t ) ); 
	}
	
	
	printf( "flatten time... \n" );
	flatten_to_edges( &mvals, &new_adj, merge_vals, adjacency, n, m );
	
	/*
	for( int64_t i = 0; i < 2*m; i++ )
	{
		printf( "edge: (%li,%li) value: %lf\n", new_adj[i][0], new_adj[i][1], mvals[i] );
	}
	*/

	printf( "sort time... \n" );
	helper::merge_sort_double( mvals, indices, 0, (2*m)-1 );
	
	graph* g = ( graph* )malloc( sizeof( void* ) );
	g->node_num = n;
	g->edge_num = m;
	g->adjacency = host_adj;
	g->weights = host_weight;

	// merge nodes sequentially by sort.
	printf( "merge_time\n");

	
	int64_t merged = 0;
	int64_t ind = 0;
	while( merged < (n - reduced_nodes) )
	{
		merged += graphbasics::merge( g, new_adj[ indices[ ind ] ][0], new_adj[ indices[ ind ] ][1], 1 );
		ind += 2;
	}

	int64_t nodes = 0;
	for( int64_t i = 0; i < n; i++ )
	{
		if( host_adj[i][0] == 0 )
		{
			nodes += 1;
		}
	}

	//graphbasics::print_graph( g );

	printf( "you've got nodes! original:%li, updated:%li\n", n, nodes );
	//graphbasics::print_graph( g );
	
	printf( "outputting to file...\n");
	//char new_filename[] = "facebook_coarsened_1000.txt";
	graphbasics::write_graph( new_filename, g );
	printf( "done\n" );
	
	// free everything
	for( int64_t i = 0; i < n; i++ )
	{
		free( *( host_weight + i ) );
		free( *( host_adj + i ) );

		cudaFree( *( adjacency + i) );
		cudaFree( *( weights + i ) );
		cudaFree( *( merge_vals + i ) ); 
	}
	free( host_weight );
	free( host_adj );
	//free( mvals );
	//free( indices );

	cudaFree( adjacency );
	cudaFree( weights );
	cudaFree( merge_vals );

	for( int64_t i = 0; i < 2 * m; i++ )
	{
		//free( *( new_adj + i ) );
	}
	//free( new_adj );

	return( 0 );
}

/*
TODO:
	-- allocation is slow. Need to remedy this. Perhaps a memcopy or something like that?
	-- perhaps collapse edges so there are no repeat (can do this by sorting and checking)
	-- get scaling and spectral results
*/