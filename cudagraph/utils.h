#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

#include "helper.h"
#include "basics.h"
#include "graph.h"

__device__ double adjacency_norm( int64_t** adjacency, double** weights, int64_t node_a, int64_t node_b );
__global__ void get_coarsen_vals( int64_t** adjacency, double** weights, double** merge_vals, int64_t n );

/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/

//--------------------------------------------------------------------------------------------------------
/* FUNCTIONS FOR READING AND TRANSFORMING GRAPH DATA */
//--------------------------------------------------------------------------------------------------------

/*
FUNCTION: read_standford_mesh( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int64_t value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in the mesh from a Stanford .ply file.
RETURNS: NONE
*/
int read_adjlist( char* filename, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
{
	FILE* fd = fopen( filename, "r" );

	#ifdef DEBUG_MODE
	printf( "IN read_adjlist()... filling in nodes...\n" );
	#endif
	// fill in the value for nodes. 
	fscanf( fd, "%li", nodes );
	// initialize variables.
	int64_t buffsize = 8 * ( 2*(*nodes) + 3 );
	#ifdef DEBUG_MODE
	printf( "	>> buff_size: %li\n", buffsize );
	#endif
	char* line = ( char* )malloc( buffsize * sizeof( char ) );
	int64_t* adj = ( int64_t* )malloc( buffsize * sizeof( int64_t ) );;
	double* wgt = ( double* )malloc( buffsize * sizeof( double ) );
	int64_t length,bytes_read,c;
	double d;
	char* line_ptr;
	// Allocate memory for the graph pointer. 
	*adj_ptr = (int64_t**)realloc( *adj_ptr, *nodes * sizeof( int64_t* ) );
	*weight_ptr = (double**)realloc( *weight_ptr, *nodes * sizeof( double* ) );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif
	fgets( line, buffsize, fd );
	for( int64_t i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;
		// read in the first three parameters of the row.
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		wgt[length - 2] = c;
		length = length + 1;
		line_ptr += bytes_read;
		// read in the full rest of the array.
		while( sscanf( line_ptr, "%li %lf %ln", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			wgt[length - 2] = d;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		//*( *adj_ptr + i ) = ( int64_t* )realloc( *( *adj_ptr + i ), length * sizeof( int64_t ) );
		//*( *weight_ptr + i ) = ( double* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( double ) );
		*( *adj_ptr + i ) = ( int64_t* )malloc(  length * sizeof( int64_t ) );
		*( *weight_ptr + i ) = ( double* )malloc( ( length - 2 ) * sizeof( double ) );

		// for each element in the array.
		for( int64_t j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	free( line );
	free( adj );
	free( wgt );

	return(0);
}

/*
FUNCTION: read_standford_mesh( FILE* fd, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int64_t value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in the mesh from a Stanford .ply file.
RETURNS: NONE
*/
int read_stanford_mesh( char* filename, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
{
	FILE* fd = fopen( filename, "r" );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... filling in nodes...\n" );
	#endif

	// fill in the value for nodes. 
	fscanf( fd, "%li", nodes );

	// initialize variables.
	int64_t buffsize = 8 * ( 2*(*nodes) + 3 );
	#ifdef DEBUG_MODE
	printf( "	>> buff_size: %li\n", buffsize );
	#endif

	char* line = ( char* )malloc( buffsize * sizeof( char ) );
	int64_t* adj = ( int64_t* )malloc( buffsize * sizeof( int64_t ) );;
	double* wgt = ( double* )malloc( buffsize * sizeof( double ) );
	int64_t length,bytes_read,c;
	double d;
	char* line_ptr;

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... nodes allocated: %li\n", *nodes );
	#endif

	// Allocate memory for the graph pointer. 
	*adj_ptr = (int64_t**)realloc( *adj_ptr, *nodes * sizeof( int64_t* ) );
	*weight_ptr = (double**)realloc( *weight_ptr, *nodes * sizeof( double* ) );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// skip over the positional entries in the file.
	for( int64_t i = 0; i < (*nodes + 3); i++ )
	{
		fgets( line, buffsize, fd );
	}

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif

	for( int64_t i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;

		// read in the first three parameters of the row.
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &c, &bytes_read );
		adj[length] = c;
		wgt[length - 2] = c;
		length = length + 1;
		line_ptr += bytes_read;

		// read in the full rest of the array.
		while( sscanf( line_ptr, "%li %lf %ln", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			wgt[length - 2] = d;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		//*( *adj_ptr + i ) = ( int64_t* )realloc( *( *adj_ptr + i ), length * sizeof( int64_t ) );
		//*( *weight_ptr + i ) = ( double* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( double ) );
		*( *adj_ptr + i ) = ( int64_t* )malloc(  length * sizeof( int64_t ) );
		*( *weight_ptr + i ) = ( double* )malloc( ( length - 2 ) * sizeof( double ) );

		// for each element in the array.
		for( int64_t j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	free( line );
	free( adj );
	free( wgt );

	return(0);
}

/*
FUNCTION: get_edgelist( graph* g, int64_t* edgelist_ptr )
DOES: Traverses the edges in the graph "g" and returns an edgelist to edgelist_ptr which is a 1D
	  int64_t array. The indexing works in pairs, *( edgelist_ptr + 2i ) and *( edgelist_ptr + 2i + 1 ) 
	  constitutes an edge where indexes 3i, 3i+1 constitute the from node
	  the to node respectively.
RETURNS: -1 on failure, and the number of edges on success.
NOTES: 
	-- This version gives us two copies of every node.
*/
int get_edgelist( int64_t** adj, double** w, int64_t n, int64_t* edgelist_ptr )
{
	int64_t index = 0;
	for( int64_t i = 0; i < n; i++ )
	{
		int64_t degree = **( adj + 2 );
		for( int64_t j = 0; j < degree; j++ )
		{
			edgelist_ptr = (int64_t* )realloc( edgelist_ptr, 2*index + 2 );
			*( edgelist_ptr + 2*index ) = i;
			*( edgelist_ptr + 2*index + 1 ) = *( *(adj + i ) + j );
			index = index + 1;
		}
	}

	return( index );
}

/*
FUNCTION: read_edgelist( char* filepath, int64_t** adj_ptr, double** weight_ptr, int64_t* nodes, int64_t edges )
DOES: Reads in an edgelist file where the first two entries are node number and edge number.
RETURNS: -1 on failure, 0 otherwise.
NOTES: This reads in an unweighted edgelist, but can be easily changed for the weighted case.
*/
int read_edgelist( char* filepath, int64_t*** adj_ptr, double*** weight_ptr, int64_t* nodes, int64_t* edges )
{
	printf( "%s\n",filepath);
	FILE *fd;
	fd = fopen( filepath, "r" );

	int buffsize = 2 * sizeof( int64_t );
	char* line = ( char* )malloc( buffsize * sizeof( char ) );

	// get nodes 
	fscanf( fd, "%li", nodes );
	fgets( line, buffsize, fd );
	// get edges
	fscanf( fd, "%li", edges );
	fgets( line, buffsize, fd );

	// reallocate pointer memory
	*adj_ptr = ( int64_t** )realloc( *adj_ptr, *nodes * sizeof( int64_t* ) );
	*weight_ptr = ( double** )realloc( *weight_ptr, *nodes * sizeof( double* ) );
	int* lengths = ( int* )malloc( *nodes * sizeof( int ) ); 

	// traverse every node and allocate memory for the adj_ptr, and eight_ptr. Also set initial lengths to 1. 
	for( int64_t i = 0; i < *nodes; i++ )
	{
		*( *adj_ptr + i ) = ( int64_t* )malloc( 3 * sizeof( int64_t ) );
		**( *adj_ptr + i ) = 0;
		*(*( *adj_ptr + i ) + 1 ) = 0;
		*(*( *adj_ptr + i ) + 2 ) = 0;

		*( *weight_ptr + i ) = ( double* )malloc( sizeof( double* ) );
		*( lengths + i ) = 1;
	}
	// perform our loop and read
	int64_t a,b,bytes_read;

	for( int64_t i = 0; i < *edges; i++ )
	{
		fgets( line, buffsize, fd );

		char* line_ptr = line;

		sscanf( line_ptr, "%li%ln", &a, &bytes_read );
		line_ptr += bytes_read;
		sscanf( line_ptr, "%li%ln", &b, &bytes_read );

		int add_it = 0;
		for( int64_t j = 0; j < *( lengths + a ); j++ )
		{
			if( b == *( *( *adj_ptr + a ) + j + 3 ) )
			{
				add_it = 1;
			}
		}

		if( add_it == 0 )
		{
			*( *adj_ptr + a ) = ( int64_t* )realloc( *( *adj_ptr + a ), (3 + *( lengths + a ))*sizeof( int64_t ) );
			*( *weight_ptr + a ) = ( double* )realloc( *( *weight_ptr + a ), (1 + *( lengths + a ))*sizeof( double ) );
			*( *( *adj_ptr + a ) + 2 + *( lengths + a ) ) = b;
			*( *( *weight_ptr + a ) + *( lengths + a ) - 1 ) = 1;
			*( *( *adj_ptr  + a ) + 2 ) = *( *( *adj_ptr  + a ) + 2 ) + 1;
			*( lengths + a ) += 1;
		}

		add_it = 0;
		for( int64_t j = 0; j < *( lengths + b ); j++ )
		{
			if( a == *( *( *adj_ptr + b ) + j + 3 ) )
			{
				add_it = 1;
			}
		}

		if( add_it == 0 )
		{
			*( *adj_ptr + b ) = ( int64_t* )realloc( *( *adj_ptr + b ), (3 + *( lengths + b ))*sizeof( int64_t ) );
			*( *weight_ptr + b ) = ( double* )realloc( *( *weight_ptr + b ), (1 + *( lengths + b ))*sizeof( double ) );
			*( *( *adj_ptr + b ) + 2 + *( lengths + b ) ) = a;
			*( *( *weight_ptr + b ) + *( lengths + b ) - 1 ) = 1;
			*( *( *adj_ptr  + b ) + 2 ) = *( *( *adj_ptr  + b ) + 2 ) + 1;
			*( lengths + b ) += 1;
		}
	}
	printf( "donezo...\n");
	return( 0 );
}

/*
FUNCTION: read_edgelist( char* filepath, int64_t** adj_ptr, double** weight_ptr, int64_t* nodes, int64_t edges )
DOES: Reads in an edgelist file where the first two entries are node number and edge number.
RETURNS: -1 on failure, 0 otherwise.
NOTES: This reads in an unweighted edgelist, but can be easily changed for the weighted case.
*/
int sort_adj( int64_t*** adj_ptr, double*** weight_ptr, int64_t nodes )
{
	
	int64_t* temp_array = ( int64_t* )malloc( nodes * sizeof( int64_t ) );
	for( int64_t i = 0; i < nodes; i++ )
	{
		for( int64_t j = 0; j < *( *( *adj_ptr + i) + 2 ); j++ )
		{
			*( temp_array + j ) = *( *( *adj_ptr + i ) + 3 + j );
		}
		//helper::merge_sort_double( temp_array, *( weight_ptr + i ) , 0, nodes-1 );
		helper::merge_sort( temp_array, 0, *( *( *adj_ptr + i) + 2 ) - 1 );

		for( int64_t j = 0; j < *( *( *adj_ptr + i) + 2 ); j++ )
		{
			*( *( *adj_ptr + i ) + j + 3 ) = *( temp_array + j );
		}
	}
	free( temp_array );
	return( 0 );
}

/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/
void flatten_to_edges( double** new_weight, int64_t*** new_adj, double** weight_array, int64_t** adj_arr, int64_t n, int64_t m )
{
	int64_t index = 0;
	int64_t degree = 0;
	for( int64_t i = 0; i < n; i++ )
	{
		degree = *( *( adj_arr + i ) + 2 );

		for( int64_t j = 0; j < degree; j++ )
		{
			*( *new_weight + index ) = weight_array[i][j];
			**( *new_adj + index ) = i;
			*( *( *new_adj + index ) + 1 ) = adj_arr[i][j+3];
			index += 1;
		}
	}
}
//--------------------------------------------------------------------------------------------------------
/* FUNCTIONS FOR PARALLEL COMPUTING */
//--------------------------------------------------------------------------------------------------------

namespace par_utils{

/*
FUNCTION: adjacency_norm( graph* g, int64_t node_a. int64_t node_b )
DOES: Determines the 1-norm difference between the adjacencies of node_a and node_b in the graph "g". 
RETURNS: -1 on failure, the norm on success.
NOTES: This is built to find the norm at the zeroth level.
	   -- It requires that the entried in adjacency are sorted in ascending order of index, and 
	   	  weights is sorted accordingly.
*/
__device__ double adjacency_norm( int64_t** adjacency, double** weights, int64_t node_a, int64_t node_b )
{
{
	double norm = 0;
	double deg_a = 0;//(double)*( *( adjacency + node_a ) + 2 );
	double deg_b = 0;//(double)*( *( adjacency + node_b ) + 2 );
	int64_t a_len = *( *( adjacency + node_a ) + 2 );
	int64_t b_len = *( *( adjacency + node_b ) + 2 );
	for( int k = 0; k < a_len; k++ )
	{
		deg_a += weights[ node_a ][ k ];
	}
	for( int k = 0; k < b_len; k++ )
	{
		deg_b += weights[ node_b ][ k ];
	}
	int64_t ind_a = 0;
	int64_t ind_b = 0;
	int64_t tot_len = a_len + b_len;
	double wa,wb;

	while( tot_len > 0 )
	{
		wa = ( (double)*( *( weights + node_a ) + ind_a ) / deg_a );
		wb = ( (double)*( *( weights + node_b ) + ind_b ) / deg_b );

		if ( adjacency[node_a][3+ind_a] > adjacency[node_b][3+ind_b] || ind_a == a_len )
		{
			norm += fabs( wb );
			ind_b += 1;
			tot_len -= 1; 
		}
		else if ( adjacency[node_b][3+ind_b] > adjacency[node_a][3+ind_a] || ind_b == b_len )
		{
			norm += fabs( wa );
			ind_a += 1;
			tot_len -= 1; 
		}
		else // if ( adjacency[node_b][3+ind_b] == adjacency[node_a][3+ind_a] )
		{
			norm += fabs( wa - wb );
			ind_a += 1;
			ind_b += 1;
			tot_len -= 2; 
		}
	}
	return( norm );
}
}

/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/
__global__ void get_coarsen_vals( int64_t** adjacency, double** weights, double** merge_vals, int64_t n )
{
	// get both the index and our stride for parallel oeprations. 
	int index = blockIdx.x * blockDim.x + threadIdx.x;
  	int stride = blockDim.x * gridDim.x;

  	// perform our compute loop
  	int64_t degree = 0;
  	for( int64_t i = index; i < n; i += stride )
  	{
  		degree = *( *( adjacency + i ) + 2 );
  		
  		if ( degree == 0 )
  		{
  			*( *( merge_vals + i ) + 0 ) = 100000;
  		}
  		else
  		{
  			for( int64_t j = 0; j < degree; j++ )
  			{
  				*( *( merge_vals + i ) + j ) = adjacency_norm( adjacency, weights, i, *( *( adjacency + i ) + j + 3 ) );
  			}
  		}
  	}
}

}

