#include "graph.h"

namespace graphbasics{
/*
FUNCTION: degree( graph* g, int from_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index"
DOES: determines the number of outgoing connections from that node.
RETURNS: the number of outgoing connections.
*/
int degree( graph* g, int from_node_index )
{
	return( *(*((g->adjacency) + (from_node_index) ) + 2) );
}

/*
FUNCTION: out_node( graph* g, int from_node_index, int to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the node at that index.
RETURNS: the node at index "to_node_index" within the adjacency of "from_node_index"
		 returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
int out_node( graph* g, int from_node_index, int to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_node()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %d , degree: %d\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}
	return( *( *( g->adjacency + from_node_index) + (3 + to_node_index) ) );
}

/*
FUNCTION: out_weight( graph* g, int from_node_index, int to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the weight of the connection to the node at that index.
RETURNS: the weight of connection to the node node at index "to_node_index" within the adjacency 
		 of "from_node_index" returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
float out_weight( graph* g, int from_node_index, int to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_weight()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %d , degree: %d\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}

	return( *(*(g->weights + from_node_index) + (to_node_index + 1) ) );
} 

/*
FUNCTION: interp_level( int value, int nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
		  also takes in the number of nodes "nodes".
DOES: determines what level of coarsening that identifier is associated with.
RETURNS: the level of coarsening.
*/
int interp_level( int value, int nodes )
{
	#ifdef DEBUG_MODE
	printf( "IN <interp_level()>...\n");
	#endif
	int level = 0;
	level = value / nodes;

	#ifdef DEBUG_MODE
	printf( "	completed: %d...\n", level );
	#endif

	return(level);
}

/*
FUNCTION: interp_node( int value, int nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
			 also takes in the number of nodes "nodes".
DOES: determines what node that 'level identifier' points to.
RETURNS: the associated node.
*/
int interp_node( int value, int nodes )
{
	#ifdef DEBUG_MODE
	printf( "IN <interp_node()>...\n");
	#endif
	int node = 0;
	node = value % nodes;

	#ifdef DEBUG_MODE
	printf( "	completed: %d...\n", node );
	#endif

	return(node);
}

/*
FUNCTION: get_k_root( graph* g, int node_b, int k_level )
TAKES IN: A graph struct, "g". A node "node_b". The level of coarsening we are concerned with.
DOES: Determines the node that an edge connecting to node_b in the original graph
	   connects to at the kth level of coarsening.
RETURNS: The node such an edge would connect to. Returns -1 on failure.
NOTES: "node_b" must be less than "g->node_num". This functions runtime is the same as the number
	   of hops it takes to get to our root node. This is at most O( log( k_level ) )
*/
int get_k_root( graph* g, int node_b, int k_level )
{
	//printf("get_k_root\n");
	#ifdef DEBUG_MODE
		printf("IN <get_k_root> \n");
	#endif

	// rename parameters we need.
	int** adj = g->adjacency;
	int nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_root()> : invalid 'node_b' \n");
		return(-1);
	}

	// set level to be low as possible.
	int level = 0;
	int node = node_b;
	int flag = 0;

	// If our node has not been coarsened return the node.
	while( flag == 0 )
	{
		level = interp_level( *(*(adj + node)), nodes );
		if( level == 0 )
		{
			flag = 1;
		}
		else
		{
			node = interp_node( *(*(adj + node)), nodes );
		}
		#ifdef DEBUG_MODE
			printf("	node: %d , level: %d, k_level: %d\n",node,level,k_level);
		#endif
	}

	#ifdef DEBUG_MODE
		printf("	completed <get_k_root()>... \n");
	#endif

	//printf("done with k_root...\n");
	return(node);
}

/*
FUNCTION: get_k_leaf( graph* g, int node_b, int k_level )
TAKES IN: a graph struct "g" and a node "node_b", also takes in the level of coarsening "k_level".
DOES: Determines the "last node" in the subsumption graph of node_b. 
RETURNS: The node at the end of the subsumption graph of node_b.
NOTES: "node_b" must be less than "g->node_num". The runtime here is bounded above by the number of nodes 
	   under any given root node. Depending on the graph this could be O( n ) <--- CAN WE DO THIS FASTER?
	   We can bound this value differently as O( maxdeg(g) ^ k_level ), but this isnt great. Instead we 
	   can do an average approximation and say this should be bounded by around O( avdeg(g) ^ k_level ).
*/
int get_k_leaf( graph* g, int node_b, int k_level )
{
	//printf("get_k_leaf\n");
	#ifdef DEBUG_MODE
		printf("IN <get_k_leaf> \n");
	#endif

	int** adj = g->adjacency;
	int nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_leaf()> : invalid 'node_b' \n");
		return(-1);
	}

	int level = nodes + 1;
	int node = node_b;

	if( *(*(adj + node) + 1) == 0 ) { return(node); }
	else
	{ 
		while ( level > 0 && interp_level( *(*(adj + node) + 1), nodes ) < k_level )
		{
			node = interp_node( *(*(adj + node) + 1), nodes );
			level = interp_level( *(*(adj + node) + 1), nodes );
			#ifdef DEBUG_MODE
				printf("	node: %d , level: %d, k_level: %d\n",node,level,k_level);
			#endif
		}
	}

	return(node);
}

/*
FUNCTION: get_k_length( graph* g, int node_a, int k_level, int* neighbor_num )
TAKES IN: A graph pointer "g", the node we are concerned with "node_a", the level of coarsening 
		  we care about "k_level" and a pointer to an int value "neighbor_num" which will be 
		  populated.
DOES:  Determines how many nodes node_a is adjacent to at the kth level of coarsening.
RETURNS: 0 on success, 1 on failure.
NOTES: Same runtime as get_k_leaf: O( n ), but realistically roughly O( avdeg ^ k_level )
*/
int get_k_length( graph* g, int node_a, int k_level, int* neighbor_num )
{
	#ifdef DEBUG_MODE
		printf("IN <get_k_length>\n");
	#endif

	int** adj = g->adjacency;
	int nodes = g->node_num;

	int value = 0;
	int flag = 0;
	int curr_node = node_a;
	while( flag < 1 )
	{
		#ifdef DEBUG_MODE
			printf("	curr_node: %d , nodes: %d\n",curr_node,nodes);
		#endif

		value = value + *(*(adj + curr_node) + 2);
		int level = interp_level(*(*(adj + curr_node) + 1),nodes);
		if( level > k_level || level == 0 )
		{
			flag = 1;
			#ifdef DEBUG_MODE
				printf("	flagged \n");
			#endif
		}
		else
		{
			curr_node = interp_node(*(*(adj + curr_node) + 1),nodes);
		}
	}

	#ifdef DEBUG_MODE
		printf("	adj length: %d \n",value);
	#endif
	
	*neighbor_num = value;
	return( 0 );
}

/*
FUNCTION: merge( graph* g, int node_a, int node_b, int k_level )
TAKES IN: pointer to graph struct "g", two nodes "node_a" and "node_b", and the level of coarsening
			 we care about, "k_level".
DOES: merges the two nodes. 
RETURNS: 1 if the 
NOTES: 
	-- Allows for merging of any node to any other node. I.E. it checks if node 
   	b has been subsumed by another node and then searches to connect that node to
   	node a.
	-- Here, node_b will be subsumed by node_a.
	-- Also "node_a" and "node_b" must be less than or equal to "g->node_num".
	-- Additionally it does a check to ensure we are not creating a subsumption loop.
*/
int merge( graph* g, int node_a, int node_b, int k_level )
{
	//printf("merge\n");
	#ifdef BIG_DEBUG_MODE
		printf( "IN merge()... -----------------------------------------------\n");
		printf( " >> node_a: %d, node_b: %d, k_level: %d\n", node_a, node_b, k_level );  
	#endif

	int nodes = g->node_num;

	int node_b_root = get_k_root( g, node_b, k_level ); // O( log( k_level ) )
	int node_a_root = get_k_root( g, node_a, k_level ); // O( log( k_level ) )
	
	#ifdef BIG_DEBUG_MODE
		printf("	getting nodes.\n");
	#endif

	int node_a_leaf = get_k_leaf( g, node_a, k_level ); // O( n )
	int64_t node_b_leaf = get_k_leaf( g, node_b, k_level ); // O( n )
	
	#ifdef BIG_DEBUG_MODE
		printf("	leaf a: %d , root a: %d \n",node_a_leaf,node_a_root);
		printf("	root b: %d \n",node_b_root);
	#endif

	// if node_b is the same as node_a in coarsening level k_level, then no merge happens.
	if( node_b_root == node_a_root )
	{
		#ifdef BIG_DEBUG_MODE
			printf("	matching roots, no merge... \n" );
		#endif
		return(0);
	}
	else
	{
		printf( "performed merge on (%d,%d)\n", node_a, node_b );
		#ifdef BIG_DEBUG_MODE
			printf( "performed merge on (%d,%d)\n", node_a, node_b );
		#endif
		if( node_a_root < node_b_root )
		{
			*(*((g->adjacency) + node_a_leaf) + 1) = ( nodes*k_level ) + node_b_root;
			**((g->adjacency) + node_b_root) = ( nodes*k_level ) + node_a_root;
		}
		else
		{
			*(*((g->adjacency) + node_b_leaf) + 1) = ( nodes*k_level ) + node_a_root;
			**((g->adjacency) + node_a_root) = ( nodes*k_level ) + node_b_root;
		}
		return(1);
	}
}

/*
FUNCTION: print_graph( graph* g )
TAKES IN: graph struct "g"
DOES: prints out the adjacencies and associated weights of each node.
RETURNS: NONE
*/
void print_graph( graph* g )
{
	printf("\n");
	for( int i = 0; i < (g->node_num); i++ )
	{
		int id_back = **(g->adjacency + i);
		int id_forward = *(*(g->adjacency + i) + 1);
		int id_deg = *(*(g->adjacency + i) + 2);

		printf("row (%d) identifiers: %d %d %d \n",i,id_back,id_forward,id_deg);

		int arr_len = degree(g,i);
		printf("connect: ");

		for( int j = 0; j < arr_len; j++ )
		{
			printf("%d ", out_node( g, i, j ) );
		}
		printf("\n");

		printf("weights: ");
		for( int j = 0; j < arr_len; j++ )
		{
			printf("%f ", out_weight( g, i, j ) );
		}
		printf("\n");
	}
	printf("\n");
}

/*
FUNCTION: write_graph( char* filename, graph* g )
TAKES IN: A file name "filename" and a graph struct "g".
DOES: Writes it to a file
RETURNS: NONE
*/
void write_graph( char* filename, graph* g )
{
	FILE *new_fd;
	new_fd = fopen( filename, "w" );

	int** adj = g->adjacency;
	float** wgt = g->weights;
	int nodes = g->node_num;
	int edges = g->edge_num;

	fprintf( new_fd, "%d\n", nodes );
	fprintf( new_fd, "%d\n", edges );
	// Write edges to adjancency file
	for( int i = 0; i < nodes; i++ )
	{
		int adjnum = *(*(adj + i ) + 2);
		fprintf( new_fd, "%d %d %d", **( adj + i ), *( *( adj + i ) + 1 ), adjnum );
		for( int j = 0; j < adjnum; j++ )
		{
			fprintf( new_fd, " %d %f", *( *( adj + i ) + ( j + 3 ) ), *( *( wgt + i ) + ( j ) ) );
		}
		fprintf( new_fd, "\n" );
	}
	//fclose( new_fd );
}

}

namespace par_graphbasics{

/*
FUNCTION: degree( graph* g, int from_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index"
DOES: determines the number of outgoing connections from that node.
RETURNS: the number of outgoing connections.
*/
__device__ int degree( graph* g, int from_node_index )
{
	return( *(*((g->adjacency) + (from_node_index) ) + 2) );
}

/*
FUNCTION: out_node( graph* g, int from_node_index, int to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the node at that index.
RETURNS: the node at index "to_node_index" within the adjacency of "from_node_index"
		 returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
__device__ int out_node( graph* g, int from_node_index, int to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_node()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %d , degree: %d\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}
	return( *( *( g->adjacency + from_node_index) + (3 + to_node_index) ) );
}

/*
FUNCTION: out_weight( graph* g, int from_node_index, int to_node_index )
TAKES IN: a pointer to a graph struct g, and the index of a node "from_node_index", and the index
		  of a node in the adjacency of that node "to_node_index"
DOES: determines the weight of the connection to the node at that index.
RETURNS: the weight of connection to the node node at index "to_node_index" within the adjacency 
		 of "from_node_index" returns -1 on failure.
NOTES: "to_node_index" needs to be less than or equal to "degree( g, from_node_index )".
*/
__device__ float out_weight( graph* g, int from_node_index, int to_node_index )
{
	if( to_node_index > *(*((g->adjacency) + (from_node_index) ) + 2) )
	{
		printf("ERROR IN <out_weight()> : invalid 'to_node_index' \n");
		printf("	to_node_index: %d , degree: %d\n",to_node_index,*(*((g->adjacency) + (from_node_index) ) + 2));
		return(-1);
	}

	return( *(*(g->weights + from_node_index) + (to_node_index + 1) ) );
} 

/*
FUNCTION: interp_level( int value, int nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
		  also takes in the number of nodes "nodes".
DOES: determines what level of coarsening that identifier is associated with.
RETURNS: the level of coarsening.
*/
__device__ int interp_level( int value, int nodes )
{
	#ifdef DEBUG_MODE
	printf( "IN <interp_level()>...\n");
	#endif
	int level = 0;
	level = value / nodes;

	#ifdef DEBUG_MODE
	printf( "	completed: %d...\n", level );
	#endif

	return(level);
}

/*
FUNCTION: interp_node( int value, int nodes )
TAKES IN: one of the 'level identifiers' (0th and 1st index in adjacency of g) "value"
			 also takes in the number of nodes "nodes".
DOES: determines what node that 'level identifier' points to.
RETURNS: the associated node.
*/
__device__ int interp_node( int value, int nodes )
{
	#ifdef DEBUG_MODE
	printf( "IN <interp_node()>...\n");
	#endif
	int node = 0;
	node = value % nodes;

	#ifdef DEBUG_MODE
	printf( "	completed: %d...\n", node );
	#endif

	return(node);
}

/*
FUNCTION: get_k_root( graph* g, int node_b, int k_level )
TAKES IN: A graph struct, "g". A node "node_b". The level of coarsening we are concerned with.
DOES: Determines the node that an edge connecting to node_b in the original graph
	   connects to at the kth level of coarsening.
RETURNS: The node such an edge would connect to. Returns -1 on failure.
NOTES: "node_b" must be less than "g->node_num". This functions runtime is the same as the number
	   of hops it takes to get to our root node. This is at most O( log( k_level ) )
*/
__device__ int get_k_root( graph* g, int node_b, int k_level )
{
	//printf("get_k_root\n");
	#ifdef DEBUG_MODE
		printf("IN <get_k_root> \n");
	#endif

	// rename parameters we need.
	int** adj = g->adjacency;
	int nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_root()> : invalid 'node_b' \n");
		return(-1);
	}

	// set level to be low as possible.
	int level = 0;
	int node = node_b;
	int flag = 0;

	// If our node has not been coarsened return the node.
	while( flag == 0 )
	{
		level = interp_level( *(*(adj + node)), nodes );
		if( level == 0 )
		{
			flag = 1;
		}
		else
		{
			node = interp_node( *(*(adj + node)), nodes );
		}
		#ifdef DEBUG_MODE
			printf("	node: %d , level: %d, k_level: %d\n",node,level,k_level);
		#endif
	}

	#ifdef DEBUG_MODE
		printf("	completed <get_k_root()>... \n");
	#endif

	//printf("done with k_root...\n");
	return(node);
}

/*
FUNCTION: get_k_leaf( graph* g, int node_b, int k_level )
TAKES IN: a graph struct "g" and a node "node_b", also takes in the level of coarsening "k_level".
DOES: Determines the "last node" in the subsumption graph of node_b. 
RETURNS: The node at the end of the subsumption graph of node_b.
NOTES: "node_b" must be less than "g->node_num". The runtime here is bounded above by the number of nodes 
	   under any given root node. Depending on the graph this could be O( n ) <--- CAN WE DO THIS FASTER?
	   We can bound this value differently as O( maxdeg(g) ^ k_level ), but this isnt great. Instead we 
	   can do an average approximation and say this should be bounded by around O( avdeg(g) ^ k_level ).
*/
__device__ int get_k_leaf( graph* g, int node_b, int k_level )
{
	//printf("get_k_leaf\n");
	#ifdef DEBUG_MODE
		printf("IN <get_k_leaf> \n");
	#endif

	int** adj = g->adjacency;
	int nodes = g->node_num;

	if( node_b > nodes )
	{
		printf("ERROR IN <get_k_leaf()> : invalid 'node_b' \n");
		return(-1);
	}

	int level = nodes + 1;
	int node = node_b;

	if( *(*(adj + node) + 1) == 0 ) { return(node); }
	else
	{ 
		while ( level > 0 && interp_level( *(*(adj + node) + 1), nodes ) < k_level )
		{
			node = interp_node( *(*(adj + node) + 1), nodes );
			level = interp_level( *(*(adj + node) + 1), nodes );
			#ifdef DEBUG_MODE
				printf("	node: %d , level: %d, k_level: %d\n",node,level,k_level);
			#endif
		}
	}

	return(node);
}

/*
FUNCTION: get_k_length( graph* g, int node_a, int k_level, int* neighbor_num )
TAKES IN: A graph pointer "g", the node we are concerned with "node_a", the level of coarsening 
		  we care about "k_level" and a pointer to an int value "neighbor_num" which will be 
		  populated.
DOES:  Determines how many nodes node_a is adjacent to at the kth level of coarsening.
RETURNS: 0 on success, 1 on failure.
NOTES: Same runtime as get_k_leaf: O( n ), but realistically roughly O( avdeg ^ k_level )
*/
__device__ int get_k_length( graph* g, int node_a, int k_level, int* neighbor_num )
{
	#ifdef DEBUG_MODE
		printf("IN <get_k_length>\n");
	#endif

	int** adj = g->adjacency;
	int nodes = g->node_num;

	int value = 0;
	int flag = 0;
	int curr_node = node_a;
	while( flag < 1 )
	{
		#ifdef DEBUG_MODE
			printf("	curr_node: %d , nodes: %d\n",curr_node,nodes);
		#endif

		value = value + *(*(adj + curr_node) + 2);
		int level = interp_level(*(*(adj + curr_node) + 1),nodes);
		if( level > k_level || level == 0 )
		{
			flag = 1;
			#ifdef DEBUG_MODE
				printf("	flagged \n");
			#endif
		}
		else
		{
			curr_node = interp_node(*(*(adj + curr_node) + 1),nodes);
		}
	}

	#ifdef DEBUG_MODE
		printf("	adj length: %d \n",value);
	#endif
	
	*neighbor_num = value;
	return( 0 );
}

/*
FUNCTION: merge( graph* g, int node_a, int node_b, int k_level )
TAKES IN: pointer to graph struct "g", two nodes "node_a" and "node_b", and the level of coarsening
			 we care about, "k_level".
DOES: merges the two nodes. 
RETURNS: 0 on success and -1 on error.
NOTES: 
	-- Allows for merging of any node to any other node. I.E. it checks if node 
   	b has been subsumed by another node and then searches to connect that node to
   	node a.
	-- Here, node_b will be subsumed by node_a.
	-- Also "node_a" and "node_b" must be less than or equal to "g->node_num".
	-- Additionally it does a check to ensure we are not creating a subsumption loop.
*/
__device__ int merge( graph* g, int node_a, int node_b, int k_level )
{
	//printf("merge\n");
	#ifdef DEBUG_MODE
		printf( "IN merge()... -----------------------------------------------\n");
		printf( " >> node_a: %d, node_b: %d, k_level: %d\n", node_a, node_b, k_level );  
	#endif

	int nodes = g->node_num;

	int node_b_root = get_k_root( g, node_b, k_level ); // O( log( k_level ) )
	
	#ifdef DEBUG_MODE
		printf("	getting nodes.\n");
	#endif

	int node_a_leaf = get_k_leaf( g, node_a, k_level ); // O( n )
	int node_a_root = get_k_root( g, node_a, k_level ); // O( log( k_level ) )

	#ifdef DEBUG_MODE
		printf("	leaf a: %d , root a: %d \n",node_a_leaf,node_a_root);
		printf("	root b: %d \n",node_b_root);
	#endif

	// if node_b is the same as node_a in coarsening level k_level, then no merge happens.
	if( node_b_root == node_a_root )
	{
		#ifdef DEBUG_MODE
			printf("	matching roots, no merge... \n" );
		#endif
		return(0);
	}
	else
	{
		*(*((g->adjacency) + node_a_leaf) + 1) = ( nodes*k_level ) + node_b_root;
		**((g->adjacency) + node_b_root) = ( nodes*k_level ) + node_a_root;

		return(0);
	}
}

}
