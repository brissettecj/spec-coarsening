#ifndef GRAPH_H
#define GRAPH_H

// GRAPH.H 
/*------------------------------------------------------------------------------
defines the bascis structure and functions of the graph_struct.
------------------------------------------------------------------------------*/

struct graph_struct
{
	int node_num;
	int edge_num;
	int** adjacency;
	float** weights;
};

typedef struct graph_struct graph;

#endif
