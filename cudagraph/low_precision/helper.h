
/*--------------------------------------------------------------------------------------
Generally useful functions
--------------------------------------------------------------------------------------*/

namespace helper{
/*
FUNCTION: ptr_swap( void* a, void* b )
TAKES IN: Two arbitrary pointers, "a" and "b".
DOES: swaps the two pointers.
RETURNS: NONE
*/
void ptr_swap( void* a, void* b )
{
	void* temp = a;
	a = b;
	b = temp;
}

/*
FUNCTION: print_array( int* array, int length )
TAKES IN: int array "array" and the length of said array, "length".
DOES: prints the array to the console.
RETURNS: NONE
*/
void print_int64_arr( int* array, int length )
{	
	for( int i = 0; i < length; i++ )
	{
		printf("%d ",*(array + i) );
	}
	printf("\n");
}
void print_float_arr( float* array, int length )
{	
	for( int i = 0; i < length; i++ )
	{
		printf("%f ",*(array + i) );
	}
	printf("\n");
}

/*
FUNCTION: merge_arr( int* array , int offset, int end )
TAKES IN: int array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: merges two arrays in a sorted manner. Function for merge_sort.
RETURNS: NONE
*/
void merge_arr( int* array , int offset, int midpoint, int end )
{
	int length_fh = midpoint - offset + 1;
	int length_sh = end - midpoint;
	int arr_fh[length_fh];
	int arr_sh[length_sh];

	for( int i = 0; i < length_fh; i++ )
	{
		arr_fh[i] = *( array + ( offset + i ) );
	}
	for( int i = 0; i < length_sh; i++ )
	{
		arr_sh[i] = *( array + ( midpoint + i + 1 ) );
	}
	int i = 0;
	int j = 0;
	int k = offset;

	while( i < length_fh && j < length_sh )
	{
		if( *(arr_fh + i) <= *(arr_sh + j) )
		{
			*( array + k ) = *( arr_fh + i );
			i = i + 1;
		}
		else
		{
			*( array + k ) = *( arr_sh + j );
			j = j + 1;
		}
		k = k + 1;
	}

	while( i < length_fh )
	{
		*( array + k ) = *( arr_fh + i );
		i = i + 1;
		k = k + 1;
	}

	while( j < length_sh )
	{
		*( array + k ) = *( arr_sh + j );
		j = j + 1;
		k = k + 1;
	}
}

/*
FUNCTION: merge_sort( int* array , int offset, int end )
TAKES IN: int array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: sorts "array" it in ascending order using merge-sort.
RETURNS: NONE
*/
void merge_sort( int* array , int offset, int end )
{
	if( offset < end )
	{
		int midpoint = offset + ( end - offset )/2;

		merge_sort( array, offset, midpoint );
		merge_sort( array, midpoint + 1, end );

		merge_arr( array, offset, midpoint, end );
	}

}

/*
FUNCTION: merge_arr( int* array , int offset, int end )
TAKES IN: int array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: merges two arrays in a sorted manner. Function for merge_sort.
RETURNS: NONE
*/
void merge_arr_float( float* array , int* indices, int offset, int midpoint, int end )
{
	int length_fh = midpoint - offset + 1;
	int length_sh = end - midpoint;

	float arr_fh[length_fh];
	float arr_sh[length_sh];

	int ind_fh[length_fh];
	int ind_sh[length_sh];

	for( int i = 0; i < length_fh; i++ )
	{
		arr_fh[i] = *( array + ( offset + i ) );
		ind_fh[i] = *( indices + ( offset + i ) );
	}
	for( int i = 0; i < length_sh; i++ )
	{
		arr_sh[i] = *( array + ( midpoint + i + 1 ) );
		ind_sh[i] = *( indices + ( midpoint + i + 1) );
	}
	int i = 0;
	int j = 0;
	int k = offset;

	while( i < length_fh && j < length_sh )
	{
		if( *(arr_fh + i) <= *(arr_sh + j) )
		{
			*( array + k ) = *( arr_fh + i );
			*( indices + k ) = *( ind_fh + i );
			i = i + 1;
		}
		else
		{
			*( array + k ) = *( arr_sh + j );
			*( indices + k ) = *( ind_sh + j );
			j = j + 1;
		}
		k = k + 1;
	}

	while( i < length_fh )
	{
		*( array + k ) = *( arr_fh + i );
		*( indices + k ) = *( ind_fh + i );
		i = i + 1;
		k = k + 1;
	}

	while( j < length_sh )
	{
		*( array + k ) = *( arr_sh + j );
		*( indices + k ) = *( ind_sh + j );
		j = j + 1;
		k = k + 1;
	}
}

/*
FUNCTION: merge_sort( int* array , int offset, int end )
TAKES IN: int array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: sorts "array" it in ascending order using merge-sort.
RETURNS: NONE
*/
void merge_sort_float( float* array, int* indices, int offset, int end )
{
	if( offset < end )
	{
		int midpoint = offset + ( end - offset )/2;

		merge_sort_float( array, indices, offset, midpoint );
		merge_sort_float( array, indices, midpoint + 1, end );

		merge_arr_float( array, indices, offset, midpoint, end );
	}

}
}

namespace parhelper{

/*
FUNCTION: ptr_swap( void* a, void* b )
TAKES IN: Two arbitrary pointers, "a" and "b".
DOES: swaps the two pointers.
RETURNS: NONE
*/
__device__ void ptr_swap( void* a, void* b )
{
	void* temp = a;
	a = b;
	b = temp;
}

/*
FUNCTION: print_array( int* array, int length )
TAKES IN: int array "array" and the length of said array, "length".
DOES: prints the array to the console.
RETURNS: NONE
*/
__device__ void print_int64_arr( int* array, int length )
{	
	for( int i = 0; i < length; i++ )
	{
		printf("%d ",*(array + i) );
	}
	printf("\n");
}
__device__ void print_float_arr( float* array, int length )
{	
	for( int i = 0; i < length; i++ )
	{
		printf("%f ",*(array + i) );
	}
	printf("\n");
}

}
