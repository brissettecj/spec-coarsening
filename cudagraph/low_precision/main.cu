#include "utils.h"
#include "graph.h"


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

int main( int argc, char** argv )
{
	// define our input variables.
	char* filename = argv[1];
	int reduced_nodes = ( int )atoi( argv[2] );
	int block_num = atoi( argv[3] );
	int thread_num = atoi( argv[4] );
	char* new_filename = argv[5];
	printf( "%d %d %d\n", reduced_nodes, block_num, thread_num );

	//int ret;
	// allocate host memory for our graph object.
	float** host_weight = ( float** )malloc( sizeof( float* ) );
	int** host_adj = ( int** )malloc( sizeof( int* ) );
	int n = 0;
	int m = 0;
	//read_edgelist( filename, &host_adj, &host_weight, &n, &m );
	read_stanford_mesh(  filename , &host_adj, &host_weight, &n, &m );
	printf( "nodes:%d edges:%d \n", n, m );
	
	printf( " 	nodes: %d edges: %d\n", n, m );
	for( int i = 0; i < n; i++ )
	{
	 	int degree = *( *( host_adj + i ) + 2 );
	 	printf( " 	node: %d :: degree: %d :: adj: ", i, degree );
	 	for( int j = 0; j < degree + 3; j++ )
	 	{
	 		printf("%d ", *( *( host_adj + i ) + j ) );
	 	}
	 	printf( "\n" );
	}

	
	/*
	if( ret != 0 )
	{
		printf("read spaghettios\n");
	}

	ret = sort_adj( &host_adj, &host_weight, n );
	if( ret != 0 )
	{
		printf("sort spaghettios\n");
	}
	*/

	// allocate our graph structures and merge_values array in unified memory.
	int **adjacency;
	float **weights, **merge_vals;
	int degree = 0;

	printf( "allocating memory\n" );
	cudaMallocManaged( &adjacency, n*sizeof( int* ) );
	cudaMallocManaged( &weights, n*sizeof( float* ) );
	cudaMallocManaged( &merge_vals, n*sizeof( float* ) );
	printf( "base memory allocated\n" );
	for( int i = 0; i < n; i++ )
	{
		degree = *( *( host_adj + i ) + 2 );
		//cudaMallocManaged( &( adjacency + i ), (degree + 3)*sizeof( int ) );
		//cudaMallocManaged( &( weights + i ), degree*sizeof( float ) );
		//cudaMallocManaged( &( merge_vals + i ), degree*sizeof( float* ) );
		gpuErrchk( cudaMallocManaged( &adjacency[i], (degree + 3) * sizeof( int ) ) );
		gpuErrchk( cudaMallocManaged( &weights[i], ( degree + 1 ) * sizeof( float ) ) );
		gpuErrchk( cudaMallocManaged( &merge_vals[i], ( degree + 1 ) * sizeof( float* ) ) );

		adjacency[i][0] = host_adj[i][0];
		adjacency[i][1] = host_adj[i][1];
		adjacency[i][2] = host_adj[i][2];

		for( int j = 0; j < degree; j++ )
		{
			adjacency[i][j+3] = host_adj[i][j+3];
			weights[i][j] = host_weight[i][j];
			merge_vals[i][j] = 0;
		}
	}

	printf( "memory allocated\n");

	// fill merge_values in parallel.
	par_utils::get_coarsen_vals<<< block_num, thread_num >>>( adjacency, weights, merge_vals, n );
	cudaDeviceSynchronize();
	printf( "Coarsened!\n");

	for( int i = 0; i < n; i++ )
	{
		printf(" node: %i ", i );
		int num = *( *(adjacency + i ) + 2 );
		for( int j = 0; j < num; j++ )
		{
			printf( "%f ",*( *(merge_vals + i ) + j ) );
		}
		printf("\n");
	}



	// sort merge_vals.
	float* mvals = (float*)malloc( 2 * m * sizeof( float ) );
	int** new_adj = (int**)malloc( 2 * m * sizeof( int* ) );
	int* indices = (int*)malloc( 2 * m * sizeof( int ) );
	for( int i = 0; i < 2 * m; i++ )
	{
		indices[i] = i;
		*( new_adj + i ) = ( int* )malloc( 2 * sizeof( int ) ); 
	}
	
	
	printf( "flatten time... \n" );
	flatten_to_edges( &mvals, &new_adj, merge_vals, adjacency, n, m );
	/*
	for( int i = 0; i < 2 * m; i++ )
	{
		printf( "edge: (%d,%d)\n", new_adj[i][0], new_adj[i][1] );
	}
	*/

	
	printf( "sort time... \n" );
	helper::merge_sort_float( mvals, indices, 0, (2*m)-1 );
	int index = 0;

	for( int i = 0; i < 2 * m; i++ )
	{
		index = indices[i];
		printf( "edge: (%d,%d) value: %f\n", new_adj[index][0], new_adj[index][1], mvals[i]);
	}

	graph* g = ( graph* )malloc( sizeof( void* ) );
	g->node_num = n;
	g->edge_num = m;
	g->adjacency = host_adj;
	g->weights = host_weight;

	// merge nodes sequentially by sort.
	printf( "merge_time\n");
	
		
	int merged = 0;
	int ind = 0;
	
	
	while( merged < (n - reduced_nodes) )
	{
		index = indices[ ind ];
		merged += graphbasics::merge( g, new_adj[ index ][0], new_adj[ index ][1], 1 );
		ind += 1;
	}
	
	
	int nodes = 0;
	for( int i = 0; i < n; i++ )
	{
		if( host_adj[i][0] == 0 )
		{
			nodes += 1;
		}
	}

	printf( "you've got nodes! original:%d, updated:%d\n", n, nodes );
	//graphbasics::print_graph( g );
	
	printf( "outputting to file...\n");
	//char new_filename[] = "facebook_coarsened_1000.txt";
	graphbasics::write_graph( new_filename, g );
	printf( "done\n" );


	// free everything
	for( int i = 0; i < n; i++ )
	{
		free( *( host_weight + i ) );
		free( *( host_adj + i ) );

		cudaFree( *( adjacency + i) );
		cudaFree( *( weights + i ) );
		cudaFree( *( merge_vals + i ) ); 
	}
	free( host_weight );
	free( host_adj );
	free( mvals );
	free( indices );

	cudaFree( adjacency );
	cudaFree( weights );
	cudaFree( merge_vals );

	for( int i = 0; i < 2 * m; i++ )
	{
		free( *( new_adj + i ) );
	}
	free( new_adj );

	return( 0 );
}

/*
TODO:
	-- allocation is slow. Need to remedy this. Perhaps a memcopy or something like that?
	-- perhaps collapse edges so there are no repeat (can do this by sorting and checking)
	-- get scaling and spectral results
*/