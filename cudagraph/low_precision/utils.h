#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

#include "helper.h"
#include "basics.h"
#include "graph.h"

__device__ float adjacency_norm( int** adjacency, float** weights, int node_a, int node_b );
__global__ void get_coarsen_vals( int** adjacency, float** weights, float** merge_vals, int n );

/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/

//--------------------------------------------------------------------------------------------------------
/* FUNCTIONS FOR READING AND TRANSFORMING GRAPH DATA */
//--------------------------------------------------------------------------------------------------------

/*
FUNCTION: read_standford_mesh( FILE* fd, int*** adj_ptr, float*** weight_ptr, int* nodes, int* edges )
TAKES IN: A FILE pointer 'fd' and two empty triple pointers, adj_ptr and weight_ptr. It also takes in a pointer to 
		  an int value 'nodes' that tracks the number of nodes. The same for 'edges'.
DOES: Reads in the mesh from a Stanford .ply file.
RETURNS: NONE
*/
int read_stanford_mesh( char* filename , int*** adj_ptr, float*** weight_ptr, int* nodes, int* edges )
{
	FILE* fd = fopen( filename, "r" );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... filling in nodes...\n" );
	#endif

	// fill in the value for nodes. 
	fscanf( fd, "%d", nodes );

	// initialize variables.
	int buffsize = 8 * ( 2*(*nodes) + 3 );
	#ifdef DEBUG_MODE
	printf( "	>> buff_size: %d\n", buffsize );
	#endif

	char* line = ( char* )malloc( buffsize * sizeof( char ) );
	int* adj = ( int* )malloc( buffsize * sizeof( int ) );;
	float* wgt = ( float* )malloc( buffsize * sizeof( float ) );
	int length,bytes_read,c;
	float d;
	char* line_ptr;

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... nodes allocated: %d\n", *nodes );
	#endif

	// Allocate memory for the graph pointer. 
	*adj_ptr = ( int** )realloc( *adj_ptr, *nodes * sizeof( int* ) );
	*weight_ptr = ( float** )realloc( *weight_ptr, *nodes * sizeof( float* ) );

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... graph_ptr reallocated...\n" );
	#endif

	// skip over the positional entries in the file.
	for( int i = 0; i < (*nodes + 3); i++ )
	{
		fgets( line, buffsize, fd );
	}

	// read in adjacencies and weights.
	// go line by line in the file.

	#ifdef DEBUG_MODE
	printf( "IN read_standford_mesh()... reading in adjacencies...\n" );
	#endif

	for( int i = 0; i < *nodes; i++ )
	{
		length = 0;
		fgets( line, buffsize, fd );
		line_ptr = line;

		// read in the first three parameters of the row.
		sscanf( line_ptr, "%d%n", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%d%n", &c, &bytes_read );
		adj[length] = c;
		length = length + 1;
		line_ptr += bytes_read;
		sscanf( line_ptr, "%d%n", &c, &bytes_read );
		adj[length] = c;
		//wgt[length - 2] = c;
		length = length + 1;
		line_ptr += bytes_read;

		// read in the full rest of the array.
		while( sscanf( line_ptr, "%d %f %n", &c, &d, &bytes_read ) > 0 )
		{
			adj[length] = c;
			//wgt[length - 2] = d; //<------------------------CHANGE BACK TO THIS LATER
			wgt[length - 3] = 1;
			length = length + 1;

			line_ptr += bytes_read;
		}

		// allocate the memory for the array in graph_ptr.
		//*( *adj_ptr + i ) = ( int* )realloc( *( *adj_ptr + i ), length * sizeof( int ) );
		//*( *weight_ptr + i ) = ( float* )realloc( *( *weight_ptr + i ), ( length - 2 ) * sizeof( float ) );
		*( *adj_ptr + i ) = ( int* )malloc(  length * sizeof( int ) );
		*( *weight_ptr + i ) = ( float* )malloc( ( length - 2 ) * sizeof( float ) );

		// for each element in the array.
		for( int j = 0; j < length; j++ )
		{
			*( *(*adj_ptr + i ) + j ) = *( adj + j );
			if( j < length - 2 )
			{
				*( *(*weight_ptr + i ) + j ) = *( wgt + j );
			}
			*edges += 1;
		}
		*edges = *edges - 3;
	}

	*edges = ( *edges / 2 );

	fclose( fd );
	free( line );
	free( adj );
	free( wgt );

	return(0);
}

/*
FUNCTION: get_edgelist( graph* g, int* edgelist_ptr )
DOES: Traverses the edges in the graph "g" and returns an edgelist to edgelist_ptr which is a 1D
	  int array. The indexing works in pairs, *( edgelist_ptr + 2i ) and *( edgelist_ptr + 2i + 1 ) 
	  constitutes an edge where indexes 3i, 3i+1 constitute the from node
	  the to node respectively.
RETURNS: -1 on failure, and the number of edges on success.
NOTES: 
	-- This version gives us two copies of every node.
*/
int get_edgelist( int** adj, float** w, int n, int* edgelist_ptr )
{
	int index = 0;
	for( int i = 0; i < n; i++ )
	{
		int degree = **( adj + 2 );
		for( int j = 0; j < degree; j++ )
		{
			edgelist_ptr = (int* )realloc( edgelist_ptr, 2*index + 2 );
			*( edgelist_ptr + 2*index ) = i;
			*( edgelist_ptr + 2*index + 1 ) = *( *(adj + i ) + j );
			index = index + 1;
		}
	}

	return( index );
}

/*
FUNCTION: read_edgelist( char* filepath, int** adj_ptr, float** weight_ptr, int* nodes, int edges )
DOES: Reads in an edgelist file where the first two entries are node number and edge number.
RETURNS: -1 on failure, 0 otherwise.
NOTES: This reads in an unweighted edgelist, but can be easily changed for the weighted case.
*/
int read_edgelist( char* filepath, int*** adj_ptr, float*** weight_ptr, int* nodes, int* edges )
{
	printf( "%s\n",filepath);
	FILE *fd;
	fd = fopen( filepath, "r" );

	int buffsize = 1024;//1024 * sizeof( int );
	char* line = ( char* )malloc( buffsize * sizeof( char ) );

	// get nodes 
	fscanf( fd, "%i", nodes );
	fgets( line, buffsize, fd );

	// get edges
	fscanf( fd, "%i", edges );
	printf(" %i\n", *edges);
	fgets( line, buffsize, fd );

	printf( "nodes: %d edges: %d\n",*nodes,*edges );
	// reallocate pointer memory
	*adj_ptr = ( int** )realloc( *adj_ptr, *nodes * sizeof( int* ) );
	*weight_ptr = ( float** )realloc( *weight_ptr, *nodes * sizeof( float* ) );
	int* lengths = ( int* )malloc( *nodes * sizeof( int ) ); 
	
	// traverse every node and allocate memory for the adj_ptr, and eight_ptr. Also set initial lengths to 1. 
	for( int i = 0; i < *nodes; i++ )
	{
		*( *adj_ptr + i ) = ( int* )malloc( 4 * sizeof( int ) );
		**( *adj_ptr + i ) = 0;
		*(*( *adj_ptr + i ) + 1 ) = 0;
		*(*( *adj_ptr + i ) + 2 ) = 0;
		*(*( *adj_ptr + i ) + 3 ) = *nodes + 1;

		*( *weight_ptr + i ) = ( float* )malloc( sizeof( float* ) );
		*( lengths + i ) = 1;
	}
	// perform our loop and read
	int a,b,bytes_read;

	for( int i = 0; i < *edges; i++ )
	{
		fgets( line, buffsize, fd );

		char* line_ptr = line;

		sscanf( line_ptr, "%i%n", &a, &bytes_read );
		line_ptr += bytes_read;
		sscanf( line_ptr, "%i%n", &b, &bytes_read );

		//printf( "plop---------------------------\n" );
		int add_it = 0;
		for( int j = 0; j < *( lengths + a ); j++ )
		{
			//printf("j time: %d\n",j);
			//printf("b time: %d\n",b);
			//printf("deg: %d\n",*( *( *adj_ptr + a ) + 2 ) );
			if( *(*( *adj_ptr + a ) + 2 ) > 0 )
			{
				
			if( b == *( *( *adj_ptr + a ) + j + 3 ) )
			{
				add_it = 1;
			}
			
			}
		}
		//printf( "plop---------------------------\n" );

		//printf( "iteration: %i, edges: %i\n", i, *edges );
		if( add_it == 0 )
		{
			*( *adj_ptr + a ) = ( int* )realloc( *( *adj_ptr + a ), (3 + *( lengths + a ) + 1 )*sizeof( int ) );
			*( *( *adj_ptr + a ) + 3 + *( lengths + a ) ) = *nodes - 1;
			*( *weight_ptr + a ) = ( float* )realloc( *( *weight_ptr + a ), (1 + *( lengths + a ) )*sizeof( float ) );
			*( *( *adj_ptr + a ) + 2 + *( lengths + a ) ) = b;
			*( *( *weight_ptr + a ) + *( lengths + a ) - 1 ) = 1;
			*( *( *adj_ptr  + a ) + 2 ) = *( *( *adj_ptr  + a ) + 2 ) + 1;
			*( lengths + a ) += 1;
		}

		//printf( "pleep---------------------------\n" );
		add_it = 0;
		for( int j = 0; j < *( lengths + b ); j++ )
		{
			//printf("j time: %d\n",j);
			//printf("a time: %d\n",a);
			//printf("deg: %d\n",*( *( *adj_ptr + b ) + 2 ) );
			if( *(*( *adj_ptr + a ) + 2 ) > 0 )
			{
				//printf(" the inner circle \n" );
			
			if( a == *( *( *adj_ptr + b ) + j + 3 ) )
			{
				add_it = 1;
			}
			
			}
		}
		//printf( "pleep---------------------------\n" );

		if( add_it == 0 )
		{	
			*( *adj_ptr + b ) = ( int* )realloc( *( *adj_ptr + b ), (3 + *( lengths + b ) + 1 )*sizeof( int ) );
			*( *( *adj_ptr + b ) + 3 + *( lengths + b ) ) = *nodes - 1;
			*( *weight_ptr + b ) = ( float* )realloc( *( *weight_ptr + b ), (1 + *( lengths + b ) )*sizeof( float ) );
			*( *( *adj_ptr + b ) + 2 + *( lengths + b ) ) = a;
			*( *( *weight_ptr + b ) + *( lengths + b ) - 1 ) = 1;
			*( *( *adj_ptr  + b ) + 2 ) = *( *( *adj_ptr  + b ) + 2 ) + 1;
			*( lengths + b ) += 1;
		}
	}
	printf( "donezo...\n");
	free( lengths );
	return( 0 );
}

/*
FUNCTION: read_edgelist( char* filepath, int** adj_ptr, float** weight_ptr, int* nodes, int edges )
DOES: Reads in an edgelist file where the first two entries are node number and edge number.
RETURNS: -1 on failure, 0 otherwise.
NOTES: This reads in an unweighted edgelist, but can be easily changed for the weighted case.
*/
int sort_adj( int*** adj_ptr, float*** weight_ptr, int nodes )
{
	
	int* temp_array = ( int* )malloc( nodes * sizeof( int ) );
	for( int i = 0; i < nodes; i++ )
	{
		for( int j = 0; j < *( *( *adj_ptr + i) + 2 ); j++ )
		{
			*( temp_array + j ) = *( *( *adj_ptr + i ) + 3 + j );
		}
		//helper::merge_sort_float( temp_array, *( weight_ptr + i ) , 0, nodes-1 );
		helper::merge_sort( temp_array, 0, *( *( *adj_ptr + i) + 2 ) - 1 );

		for( int j = 0; j < *( *( *adj_ptr + i) + 2 ); j++ )
		{
			*( *( *adj_ptr + i ) + j + 3 ) = *( temp_array + j );
		}
	}
	free( temp_array );
	return( 0 );
}

/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/
void flatten_to_edges( float** new_weight, int*** new_adj, float** weight_array, int** adj_arr, int n, int m )
{
	int index = 0;
	int degree = 0;
	for( int i = 0; i < n; i++ )
	{
		degree = *( *( adj_arr + i ) + 2 );

		for( int j = 0; j < degree; j++ )
		{
			*( *new_weight + index ) = weight_array[i][j];
			**( *new_adj + index ) = i;
			*( *( *new_adj + index ) + 1 ) = adj_arr[i][j+3];
			index += 1;
		}
	}
}
//--------------------------------------------------------------------------------------------------------
/* FUNCTIONS FOR PARALLEL COMPUTING */
//--------------------------------------------------------------------------------------------------------

namespace par_utils{

/*
FUNCTION: adjacency_norm( graph* g, int node_a. int node_b )
DOES: Determines the 1-norm difference between the adjacencies of node_a and node_b in the graph "g". 
RETURNS: -1 on failure, the norm on success.
NOTES: This is built to find the norm at the zeroth level.
	   -- It requires that the entried in adjacency are sorted in ascending order of index, and 
	   	  weights is sorted accordingly.
*/
__device__ float adjacency_norm( int** adjacency, float** weights, int node_a, int node_b )
{
	float norm = 0;
	float deg_a = (float)*( *( adjacency + node_a ) + 2 );
	float deg_b = (float)*( *( adjacency + node_b ) + 2 );
	int a_len = *( *( adjacency + node_a ) + 2 );
	int b_len = *( *( adjacency + node_b ) + 2 );
	int ind_a = 0;
	int ind_b = 0;
	int tot_len = a_len + b_len;
	float wa,wb;

	while( tot_len > 0 )
	{
		wa = ( (float)*( *( weights + node_a ) + ind_a ) / deg_a );
		wb = ( (float)*( *( weights + node_b ) + ind_b ) / deg_b );

		if ( adjacency[node_a][3+ind_a] > adjacency[node_b][3+ind_b] || ind_a == a_len )
		{
			norm += fabsf( wb );
			ind_b += 1;
			tot_len -= 1; 
		}
		else if ( adjacency[node_b][3+ind_b] > adjacency[node_a][3+ind_a] || ind_b == b_len )
		{
			norm += fabsf( wa );
			ind_a += 1;
			tot_len -= 1; 
		}
		else // if ( adjacency[node_b][3+ind_b] == adjacency[node_a][3+ind_a] )
		{
			norm += fabsf( wa - wb );
			ind_a += 1;
			ind_b += 1;
			tot_len -= 2; 
		}
	}
	return( norm );
}


/*
FUNCTION: 
DOES: 
RETURNS: 
NOTES: 
*/
__global__ void get_coarsen_vals( int** adjacency, float** weights, float** merge_vals, int n )
{
	//printf("spin me up boy\n");
	// get both the index and our stride for parallel oeprations. 
	int index = blockIdx.x * blockDim.x + threadIdx.x;
  	int stride = blockDim.x * gridDim.x;

  	// perform our compute loop
  	int degree = 0;
  	for( int i = index; i < n; i += stride )
  	{
  		degree = *( *( adjacency + i ) + 2 );
  		
  		if ( degree == 0 )
  		{
  			*( *( merge_vals + i ) + 0 ) = 100000;
  		}
  		else
  		{
  			for( int j = 0; j < degree; j++ )
  			{
  				*( *( merge_vals + i ) + j ) = adjacency_norm( adjacency, weights, i, *( *( adjacency + i ) + j + 3 ) );
  				// printf( " calculating norm for: (%i,%i) -- %f\n", i, *( *( adjacency + i ) + j + 3 ), *( *( merge_vals + i ) + j ) );
  			}
  		}
  	}
}

}

