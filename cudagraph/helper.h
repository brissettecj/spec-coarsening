
/*--------------------------------------------------------------------------------------
Generally useful functions
--------------------------------------------------------------------------------------*/

namespace helper{
/*
FUNCTION: ptr_swap( void* a, void* b )
TAKES IN: Two arbitrary pointers, "a" and "b".
DOES: swaps the two pointers.
RETURNS: NONE
*/
void ptr_swap( void* a, void* b )
{
	void* temp = a;
	a = b;
	b = temp;
}

/*
FUNCTION: print_array( int64_t* array, int64_t length )
TAKES IN: int64_t array "array" and the length of said array, "length".
DOES: prints the array to the console.
RETURNS: NONE
*/
void print_int64_arr( int64_t* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%ld ",*(array + i) );
	}
	printf("\n");
}
void print_double_arr( double* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%lf ",*(array + i) );
	}
	printf("\n");
}

/*
FUNCTION: merge_arr( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: merges two arrays in a sorted manner. Function for merge_sort.
RETURNS: NONE
*/
void merge_arr( int64_t* array , int64_t offset, int64_t midpoint, int64_t end )
{
	int64_t length_fh = midpoint - offset + 1;
	int64_t length_sh = end - midpoint;
	int64_t arr_fh[length_fh];
	int64_t arr_sh[length_sh];

	for( int64_t i = 0; i < length_fh; i++ )
	{
		arr_fh[i] = *( array + ( offset + i ) );
	}
	for( int64_t i = 0; i < length_sh; i++ )
	{
		arr_sh[i] = *( array + ( midpoint + i + 1 ) );
	}
	int64_t i = 0;
	int64_t j = 0;
	int64_t k = offset;

	while( i < length_fh && j < length_sh )
	{
		if( *(arr_fh + i) <= *(arr_sh + j) )
		{
			*( array + k ) = *( arr_fh + i );
			i = i + 1;
		}
		else
		{
			*( array + k ) = *( arr_sh + j );
			j = j + 1;
		}
		k = k + 1;
	}

	while( i < length_fh )
	{
		*( array + k ) = *( arr_fh + i );
		i = i + 1;
		k = k + 1;
	}

	while( j < length_sh )
	{
		*( array + k ) = *( arr_sh + j );
		j = j + 1;
		k = k + 1;
	}
}

/*
FUNCTION: merge_sort( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: sorts "array" it in ascending order using merge-sort.
RETURNS: NONE
*/
void merge_sort( int64_t* array , int64_t offset, int64_t end )
{
	if( offset < end )
	{
		int64_t midpoint = offset + ( end - offset )/2;

		merge_sort( array, offset, midpoint );
		merge_sort( array, midpoint + 1, end );

		merge_arr( array, offset, midpoint, end );
	}

}

/*
FUNCTION: merge_arr( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: merges two arrays in a sorted manner. Function for merge_sort.
RETURNS: NONE
*/
void merge_arr_double( double* array , int64_t* indices, int64_t offset, int64_t midpoint, int64_t end )
{
	int64_t length_fh = midpoint - offset + 1;
	int64_t length_sh = end - midpoint;

	double* arr_fh = (double*)malloc(length_fh*sizeof( double ));
	double* arr_sh = (double*)malloc(length_sh*sizeof( double ));

	int64_t* ind_fh = (int64_t*)malloc(length_fh*sizeof( int64_t ));
	int64_t* ind_sh = (int64_t*)malloc(length_sh*sizeof( int64_t ));

	for( int64_t i = 0; i < length_fh; i++ )
	{
		arr_fh[i] = *( array + ( offset + i ) );
		ind_fh[i] = *( indices + ( offset + i ) );
	}
	for( int64_t i = 0; i < length_sh; i++ )
	{
		arr_sh[i] = *( array + ( midpoint + i + 1 ) );
		ind_sh[i] = *( indices + ( midpoint + i + 1) );
	}
	int64_t i = 0;
	int64_t j = 0;
	int64_t k = offset;

	while( i < length_fh && j < length_sh )
	{
		if( *(arr_fh + i) <= *(arr_sh + j) )
		{
			*( array + k ) = *( arr_fh + i );
			*( indices + k ) = *( ind_fh + i );
			i = i + 1;
		}
		else
		{
			*( array + k ) = *( arr_sh + j );
			*( indices + k ) = *( ind_sh + j );
			j = j + 1;
		}
		k = k + 1;
	}

	while( i < length_fh )
	{
		*( array + k ) = *( arr_fh + i );
		*( indices + k ) = *( ind_fh + i );
		i = i + 1;
		k = k + 1;
	}

	while( j < length_sh )
	{
		*( array + k ) = *( arr_sh + j );
		*( indices + k ) = *( ind_sh + j );
		j = j + 1;
		k = k + 1;
	}

	free( arr_fh );
	free( arr_sh );
	free( ind_fh );
	free( ind_sh );
}

/*
FUNCTION: merge_sort( int64_t* array , int64_t offset, int64_t end )
TAKES IN: int64_t array "array", the start of our array segment "offset", and the end of
		  our array segment "end"
DOES: sorts "array" it in ascending order using merge-sort.
RETURNS: NONE
*/
void merge_sort_double( double* array, int64_t* indices, int64_t offset, int64_t end )
{
	if( offset < end )
	{
		int64_t midpoint = offset + ( end - offset )/2;

		merge_sort_double( array, indices, offset, midpoint );
		merge_sort_double( array, indices, midpoint + 1, end );

		merge_arr_double( array, indices, offset, midpoint, end );
	}

}
}

namespace parhelper{

/*
FUNCTION: ptr_swap( void* a, void* b )
TAKES IN: Two arbitrary pointers, "a" and "b".
DOES: swaps the two pointers.
RETURNS: NONE
*/
__device__ void ptr_swap( void* a, void* b )
{
	void* temp = a;
	a = b;
	b = temp;
}

/*
FUNCTION: print_array( int64_t* array, int64_t length )
TAKES IN: int64_t array "array" and the length of said array, "length".
DOES: prints the array to the console.
RETURNS: NONE
*/
__device__ void print_int64_arr( int64_t* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%ld ",*(array + i) );
	}
	printf("\n");
}
__device__ void print_double_arr( double* array, int64_t length )
{	
	for( int64_t i = 0; i < length; i++ )
	{
		printf("%lf ",*(array + i) );
	}
	printf("\n");
}

}
