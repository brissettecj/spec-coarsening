#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double adjacency_norm( int64_t** adjacency, double** weights, int64_t node_a, int64_t node_b );

int main( int argc, char** argv )
{
	int64_t** adjacency = (int64_t**)malloc( 2*sizeof(int64_t*) );
	double** weights = (double**)malloc( 2*sizeof(double*) );

	*(adjacency + 0) = (int64_t*)malloc( 8*sizeof(int64_t*) );
	*(weights + 0) = (double*)malloc( 5*sizeof(double) );
	*(adjacency + 1) = (int64_t*)malloc( 11*sizeof(int64_t*) );
	*(weights + 1) = (double*)malloc( 8*sizeof(double) );


	adjacency[0][0] = 0;
	adjacency[0][1] = 0;
	adjacency[0][2] = 6;
	adjacency[0][3] = 0;
	adjacency[0][4] = 89;
	adjacency[0][5] = 95;
	adjacency[0][6] = 147;
	adjacency[0][7] = 219;
	adjacency[0][8] = 319;
	/*adjacency[0][8] = 312;
	adjacency[0][9] = 326;
	adjacency[0][10] = 333;
	adjacency[0][11] = 343;*/

	weights[0][0] = 1;
	weights[0][1] = 1;
	weights[0][2] = 1;
	weights[0][3] = 1;
	weights[0][4] = 1;
	/*weights[0][5] = 1;
	weights[0][6] = 1;
	weights[0][7] = 1;
	weights[0][8] = 1;*/

	adjacency[1][0] = 0;
	adjacency[1][1] = 0;
	adjacency[1][2] = 8;
	adjacency[1][3] = 0;
	adjacency[1][4] = 6;
	adjacency[1][5] = 19;
	adjacency[1][6] = 89;
	adjacency[1][7] = 95;
	adjacency[1][8] = 147;
	adjacency[1][9] = 219;
	adjacency[1][10] = 327;
	/*adjacency[1][11] = 343;*/

	weights[1][0] = 1;
	weights[1][1] = 1;
	weights[1][2] = 1;
	weights[1][3] = 1;
	weights[1][4] = 1;
	weights[1][5] = 1;
	weights[1][6] = 1;
	weights[1][7] = 1;
	//weights[1][8] = 1;

	int64_t node_a = 2;
	int64_t node_b = 116;

	double norm = adjacency_norm( adjacency, weights, 0, 1 );

	printf( " %lf\n", norm );

}



__device__ double adjacency_norm( int64_t** adjacency, double** weights, int64_t node_a, int64_t node_b )
{
	double norm = 0;
	double deg_a = (double)*( *( adjacency + node_a ) + 2 );
	double deg_b = (double)*( *( adjacency + node_b ) + 2 );
	int64_t a_len = *( *( adjacency + node_a ) + 2 );
	int64_t b_len = *( *( adjacency + node_b ) + 2 );
	int64_t ind_a = 0;
	int64_t ind_b = 0;
	int64_t tot_len = a_len + b_len;
	double wa,wb;

	int loopnum = 0;
	printf( "degrees-- a: %li, b:%li\n", a_len, b_len );

	while( tot_len > 0 )
	{
		wa = ( (double)*( *( weights + node_a ) + ind_a ) ) / deg_a ;
		wb = ( (double)*( *( weights + node_b ) + ind_b ) ) / deg_b ;

		printf( "loopnum: %d\n", loopnum );
		printf( "	weights -- a: %lf , b: %lf\n", wa, wb );
		printf( "	nodes: ( %li , %li )\n", adjacency[node_a][3+ind_a], adjacency[node_b][3+ind_b]);
		printf( "	indices-- a: %li, b: %li\n", ind_a, ind_b );

		if ( adjacency[node_a][3+ind_a] > adjacency[node_b][3+ind_b] || ind_a == a_len )
		{
			printf( "	a > b\n " );

			norm += fabs( wb );
			ind_b += 1;
			tot_len -= 1; 
		}
		else if ( adjacency[node_b][3+ind_b] > adjacency[node_a][3+ind_a] || ind_b == b_len )
		{
			printf( "	b > a\n " );

			norm += fabs( wa );
			ind_a += 1;
			tot_len -= 1; 
		}
		else // if ( adjacency[node_b][3+ind_b] == adjacency[node_a][3+ind_a] )
		{
			printf( "	a = b\n " );

			norm += fabs( wa - wb );
			ind_a += 1;
			ind_b += 1;
			tot_len -= 2; 
		}
		printf( "	norm: %lf\n", norm );

	}
	return( norm );
}
