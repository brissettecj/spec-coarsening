#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>

int main( int argc, char** argv )
{
	// get our file to read from
	FILE *fd;
	fd = fopen( argv[1], "r" );

	// form our new file for positions
	FILE *new_fd;
	new_fd = fopen( argv[2], "w" );
	printf( "files opened...\n" );

	// read in the node and edge numbers from the file
	int64_t nodes, edges;
	char line[1024]; 

	fscanf( fd, "%li", &nodes );
	fscanf( fd, "%li", &edges );
	fgets( line, 1024, fd );

	// initialize our adjacency
	int64_t** adjacencies = ( int64_t** )malloc( nodes * sizeof( int64_t* ) );
	if( adjacencies == NULL )
	{	
		printf( "We have a whoopsie on our hands! -- ");
		printf( "Could not allocate memory. \n ");
	}

	int64_t* sizes = ( int64_t* )malloc( nodes * sizeof( int64_t ) );

	#ifdef DETAILED_DEBUG_MODE
	printf( "Allocating adjacency memory...\n");
	#endif
	#ifdef DEBUG_MODE
	printf( "Allocating adjacency memory...\n");
	#endif

	for( int64_t i = 0; i < nodes; i++ )
	{
		*( sizes + i ) = 3;
		*( adjacencies + i ) = ( int64_t* )malloc( 3*sizeof(int64_t) );
		*( *( adjacencies + i ) + 0 ) = 0;
		*( *( adjacencies + i ) + 1 ) = 0;
		*( *( adjacencies + i ) + 2 ) = 0;
	}

	printf( "adj array allocated...\n" );
	// here we initialize a mapping array
	// NOTE THAT IF THE INDEX OF THE LARGEST NODE IS GREATER THAN "EDGES" THIS MUST BE INCREMENTED.
	// this could be chosen more cleverly by just scanning through the edges for the largest index, but is not needed for my data sets.
	int64_t* mapping = ( int64_t* )malloc( 5*edges*sizeof(int64_t) );
	for( int64_t i = 0; i < 5*edges; i++ )
	{
		*( mapping + i ) = -1;
	}

	// iterate through the file and fill our adjacency array
	// note that as we go, we are filling and using a mapping array.
	int64_t edge[2];
	int64_t e[2];
	int64_t index = 0;
	for( int64_t i = 0; i < edges; i++ )
	{
		// move to new line
		fgets( line, 1024, fd );
		char* line_ptr = line;
		// read in the edge to the edge array
		sscanf( line_ptr, "%li %li", (e + 0), (e + 1) );
		//printf("edge read in: (%li,%li)\n", *(e + 0), *(e + 1) );

		if ( *( mapping + e[0] ) == -1 )
		{
			*( mapping + e[0] ) = index;
			edge[0] = index;
			index += 1;
		}
		else
		{
			edge[0] = *( mapping + e[0] );
		}
		if ( *( mapping + e[1] ) == -1 )
		{
			*( mapping + e[1] ) = index;
			edge[1] = index;
			index += 1;
		}
		else
		{
			edge[1] = *( mapping + e[1] );
		}

		// increment the size of the adjacencies at both ends of the edge
		*( sizes + edge[0] ) += 2;
		*( sizes + edge[1] ) += 2;
		/*
		printf("index: %li\n", index );
		printf("nodes %li, new edge: (%li,%li)\n", nodes, edge[0], edge[1]);
		printf("sizes: %li, %li\n", *( sizes + edge[0] ), *( sizes + edge[1] ) );
		*/
		// reallocate the adjacencies at both ends
		*( adjacencies + edge[0] ) = realloc( *( adjacencies + edge[0] ), *( sizes + edge[0] )*sizeof( int64_t ) );
		*( adjacencies + edge[1] ) = realloc( *( adjacencies + edge[1] ), *( sizes + edge[1] )*sizeof( int64_t ) );
		// change the degree of the nodes
		*( *( adjacencies + edge[0] ) + 2 ) += 1;
		*( *( adjacencies + edge[1] ) + 2 ) += 1;
		//printf("new degrees: %li, %li\n", *( *( adjacencies + edge[0] ) + 2 ), *( *( adjacencies + edge[1] ) + 2 ) );
		// append the adjacncy elements and weights to the array.
		*( *( adjacencies + edge[0] ) + *( sizes + edge[0] ) - 2 ) = edge[1];
		*( *( adjacencies + edge[1] ) + *( sizes + edge[1] ) - 2 ) = edge[0];
		*( *( adjacencies + edge[0] ) + *( sizes + edge[0] ) - 1 ) = 1;
		*( *( adjacencies + edge[1] ) + *( sizes + edge[1] ) - 1 ) = 1;
	}
	printf( "adj array filled...\n" );
	
	/*
	for( int64_t i = 0; i < nodes; i++ )
	{
		printf( "node: %li --", i );
		for( int64_t j = 0; j < *(sizes + i); j++ )
		{
			printf( " %li", *( *(adjacencies + i) + j) ); 
		}
		printf("\n");
	}
	*/

	// write to file
	fprintf( new_fd, "%li\n", nodes );
	fprintf( new_fd, "%li\n", edges );
	for( int64_t i = 0; i < nodes; i++ )
	{
		int64_t adjnum = *( *( adjacencies + i ) + 2 );
		fprintf( new_fd, "%li %li %li", *(*(adjacencies + i) + 0), *(*(adjacencies + i) + 1), *(*(adjacencies + i) + 2) );
		for( int64_t j = 0; j < adjnum; j++ )
		{
			fprintf( new_fd, " %li %li", *( *( adjacencies + i ) + 2*j + 3 ),  *( *( adjacencies + i ) + 2*j + 4 ) );
			//printf("node: %li, adj: %li\n", i, *( *( adjacencies + i ) + 2*j + 3 ) );
		}
		fprintf( new_fd, "\n" );
	}
	printf( "written...\n" );

	fclose( fd );
	fclose( new_fd );

}