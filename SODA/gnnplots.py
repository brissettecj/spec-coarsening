import yaml
import numpy as np 
import matplotlib.pyplot as plt 

'''
FUNCTION: 
PARAMETERS: 
DOES: 
RETURNS: 
NOTES: 
'''
def plot_acc_train(graph_name):

	coarse_filename = graph_name+"_data.yml"
	raw_filename = graph_name+"_data_raw.yml"

	with open(coarse_filename,'r') as stream:
		coarse_data = yaml.safe_load(stream)

	with open(raw_filename,'r') as stream:
		raw_data = yaml.safe_load(stream)

	acc = {}
	acc_dev = {}
	time = {}
	time_dev = {}

	# get the accuracy, times, and deviations.
	for key in coarse_data.keys():
		acc[key] = []
		acc_dev[key] = []
		time[key] = []
		time_dev[key] = []
		for level in coarse_data[key].keys():
			acc[key].append(coarse_data[key][level][0])
			acc_dev[key].append(coarse_data[key][level][1])
			time[key].append(coarse_data[key][level][2])
			time_dev[key].append(coarse_data[key][level][3])

	# do the same, but for the uncoarsened counterpart.
	for key in raw_data.keys():		
		acc[key] = []
		acc_dev[key] = []
		time[key] = []
		time_dev[key] = []
		acc[key].append(raw_data[key][0])
		acc_dev[key].append(raw_data[key][1])
		time[key].append(raw_data[key][2])
		time_dev[key].append(raw_data[key][3])

	# plot our accuaracy values.
	fig,(ax1,ax2) = plt.subplots(1,2) #plt.figure(1)
	x = np.arange(4) + 1

	ax1.plot(x,acc["NONE"]*np.ones(4),color="black",linewidth=2,label="NONE")
	ax1.plot(x,(np.array(acc["NONE"])-np.array(acc_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")
	ax1.plot(x,(np.array(acc["NONE"])+np.array(acc_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")

	ax2.plot(x,time["NONE"]*np.ones(4),color="black",linewidth=2,label="NONE")
	ax2.plot(x,(np.array(time["NONE"])-np.array(time_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")
	ax2.plot(x,(np.array(time["NONE"])+np.array(time_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")

	for key in acc.keys():
		if key != "NONE":
			ax1.errorbar(x,acc[key],yerr=acc_dev[key],capsize=8,capthick=2,label=key)
			ax2.errorbar(x,time[key],yerr=time_dev[key],capsize=8,capthick=2,label=key)

	ax1.legend()
	ax2.legend()

	ax1.set_title(graph_name+" Accuracy",fontsize=18)
	ax2.set_title(graph_name+" Training Time",fontsize=18)

	ax1.set_xlabel("coarsening levels",fontsize=14)
	ax1.set_ylabel("classification accuracy",fontsize=14)
	ax2.set_xlabel("coarsening levels",fontsize=14)
	ax2.set_ylabel("training time",fontsize=14)

	fig.set_size_inches(12,5)
	fig.savefig(graph_name+"_timeacc.pdf",format="pdf")

'''
FUNCTION: 
PARAMETERS: 
DOES: 
RETURNS: 
NOTES: 
'''
def plot_at_bar(graph_name):

	coarse_filename = graph_name+"_data.yml"
	raw_filename = graph_name+"_data_raw.yml"

	with open(coarse_filename,'r') as stream:
		coarse_data = yaml.safe_load(stream)

	with open(raw_filename,'r') as stream:
		raw_data = yaml.safe_load(stream)

	acc = {}
	acc_dev = {}
	time = {}
	time_dev = {}

	# get the accuracy, times, and deviations.
	for key in coarse_data.keys():
		acc[key] = []
		acc_dev[key] = []
		time[key] = []
		time_dev[key] = []
		for level in coarse_data[key].keys():
			acc[key].append((coarse_data[key][level][0]-raw_data["NONE"][0])/raw_data["NONE"][0])
			acc_dev[key].append((coarse_data[key][level][1]-raw_data["NONE"][1])/raw_data["NONE"][1])
			time[key].append((raw_data["NONE"][2]-coarse_data[key][level][2])/raw_data["NONE"][2])
			time_dev[key].append((raw_data["NONE"][3]-coarse_data[key][level][3])/raw_data["NONE"][3])

	# plot our accuaracy values.
	fig,(ax1,ax2) = plt.subplots(1,2) #plt.figure(1)
	x = np.arange(4) + 1

	'''
	ax1.plot(x,acc["NONE"]*np.ones(4),color="black",linewidth=2,label="NONE")
	ax1.plot(x,(np.array(acc["NONE"])-np.array(acc_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")
	ax1.plot(x,(np.array(acc["NONE"])+np.array(acc_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")

	ax2.plot(x,time["NONE"]*np.ones(4),color="black",linewidth=2,label="NONE")
	ax2.plot(x,(np.array(time["NONE"])-np.array(time_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")
	ax2.plot(x,(np.array(time["NONE"])+np.array(time_dev["NONE"]))*np.ones(4),color="black",linestyle="dashed")
	'''

	ind = 0
	colors = ["goldenrod","mediumseagreen","cornflowerblue","firebrick","lightslategray"]
	for key in acc.keys():
		val = 0.1 + (ind*0.1)
		ax1.bar(x-val, acc[key], color=colors[ind] , width=0.1, edgecolor='black', label=key)
		ax2.bar(x-val, time[key], color=colors[ind] ,width=0.1, edgecolor='black', label=key)
		ind += 1
		#ax1.errorbar(x,acc[key],yerr=acc_dev[key],capsize=8,capthick=2,label=key)
		#ax2.errorbar(x,time[key],yerr=time_dev[key],capsize=8,capthick=2,label=key)

	ax1.legend()
	ax2.legend()

	ax1.set_title(graph_name+" Accuracy",fontsize=18)
	ax2.set_title(graph_name+" Training Time",fontsize=18)

	ax1.set_xlabel("coarsening levels",fontsize=14)
	ax1.set_ylabel("relative accuracy improvement",fontsize=14)
	ax2.set_xlabel("coarsening levels",fontsize=14)
	ax2.set_ylabel("relative training time improvement",fontsize=14)

	fig.set_size_inches(12,5)
	fig.savefig(graph_name+"_at_bars.pdf",format="pdf")

def plot_at_bar_av(graph_names):

	coarse_data = {}
	raw_data = {}

	labels = ["Algebraic distance",'Heavy weight','Nodal domain',"METIS",'Adjacency similarity']
	methods = ['ADC','HWC','NDC','METIS','ASC']
	acc = dict.fromkeys(methods)
	acc_dev = dict.fromkeys(methods)
	time = dict.fromkeys(methods)
	time_dev = dict.fromkeys(methods)

	for method in methods:
		acc[method] = []
		acc_dev[method] = []
		time[method] = []
		time_dev[method] = []

	for name in graph_names:

		coarse_filename = name+"_data.yml"
		raw_filename = name+"_data_raw.yml"

		with open(coarse_filename,'r') as stream:
			coarse_data = yaml.safe_load(stream)

		with open(raw_filename,'r') as stream:
			raw_data = yaml.safe_load(stream)

		# get the accuracy, times, and deviations.
		for key in coarse_data.keys():
			for level in coarse_data[key].keys():
				if key not in acc.keys() or len(acc[key]) < level:
					acc[key].append( ((coarse_data[key][level][0]-raw_data["NONE"][0])/raw_data["NONE"][0])/len(graph_names) )
					acc_dev[key].append( ((coarse_data[key][level][1]-raw_data["NONE"][1])/raw_data["NONE"][1])/len(graph_names) )
					time[key].append( ((raw_data["NONE"][2]-coarse_data[key][level][2])/raw_data["NONE"][2])/len(graph_names) )
					time_dev[key].append( ((raw_data["NONE"][3]-coarse_data[key][level][3])/raw_data["NONE"][3])/len(graph_names) )
				else:
					acc[key][level-1] += ( ((coarse_data[key][level][0]-raw_data["NONE"][0])/raw_data["NONE"][0])/len(graph_names) )
					acc_dev[key][level-1] += ( ((coarse_data[key][level][1]-raw_data["NONE"][1])/raw_data["NONE"][1])/len(graph_names) )
					time[key][level-1] += ( ((raw_data["NONE"][2]-coarse_data[key][level][2])/raw_data["NONE"][2])/len(graph_names) )
					time_dev[key][level-1] += ( ((raw_data["NONE"][3]-coarse_data[key][level][3])/raw_data["NONE"][3])/len(graph_names) )
	
	# plot our accuaracy values.

	x = np.arange(4) + 1
	xticks = np.arange(5) -0.25
	xticklabels = np.arange(5)

	fig,ax = plt.subplots()

	ind = 0
	colors = ["goldenrod","mediumseagreen","cornflowerblue","firebrick","lightslategray"]
	for key in acc.keys():
		val = 0.1 + (ind*0.1)
		ax.bar(x-val, acc[key], color=colors[ind] , width=0.1, edgecolor='black', label=labels[ind])
		ind += 1

	ax.legend()

	ax.set_title("Average Accuracy",fontsize=18)
	ax.set_xticks(xticks)
	ax.set_xticklabels(xticklabels)
	ax.set_xlabel("coarsening levels",fontsize=14)
	ax.set_ylabel("relative accuracy improvement",fontsize=14)

	plt.savefig("bars_av_acc.pdf",format="pdf")

	fig,ax = plt.subplots()

	ind = 0
	colors = ["goldenrod","mediumseagreen","cornflowerblue","firebrick","lightslategray"]
	for key in acc.keys():
		val = 0.1 + (ind*0.1)
		ax.bar(x-val, time[key], color=colors[ind] ,width=0.1, edgecolor='black', label=labels[ind])
		ind += 1


	ax.set_title("Average Training Time",fontsize=18)
	ax.set_xticks(xticks)
	ax.set_xticklabels(xticklabels)
	ax.set_xlabel("coarsening levels",fontsize=14)
	ax.set_ylabel("relative training time improvement",fontsize=14)

	ax.legend()

	plt.savefig("bars_av_time.pdf",format="pdf")

#graph_name = "PubMed"
#graph_name = "Cora"
#graph_name = "CiteSeer"

#plot_acc_train(graph_name)
#plot_at_bar(graph_name)

graph_names = ["PubMed","Cora","CiteSeer"]
plot_at_bar_av(graph_names)