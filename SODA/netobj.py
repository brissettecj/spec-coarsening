import torch
import torch.nn.functional as F
from torch_geometric.nn import GCNConv


# Defines the function of our network.
class Net(torch.nn.Module):
    # Initializes a graph convolutional network with 2 hidden layers.
    def __init__(self, args):
        super(Net, self).__init__()
        self.conv1 = GCNConv(args.num_features, args.hidden)
        self.conv2 = GCNConv(args.hidden, args.num_classes)
        print(args.num_features)
        print(args.hidden)
        print(args.num_classes)

    # Resets the learnable parameters of the graph.
    def reset_parameters(self):
        self.conv1.reset_parameters()
        self.conv2.reset_parameters()

    # Performs a forward pass of the convolutional network.
    def forward(self, x, edge_index):

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        return F.log_softmax(x, dim=1)