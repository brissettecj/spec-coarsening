SOURCES FOR NETWORKS:

STANFORD LARGE NETWORK DATASET
 -- CA-GrQc
 -- CA-HepTh
 -- email-Eu-core
 -- facebook_combined
 -- lastfm_asia_edges
 -- Wiki-Vote

KOBLENZ REPOSITORY
 -- euroroad
 -- arenas-jazz
 -- zachary_karate
 -- caida
 -- dimacs
 -- opsahl-powergrid
 -- pdz
 -- copperfield
 -- football
 -- dbpedia-similar
 -- elegans-metabolic
 -- yeast
 -- netsci
 -- hamhouse
 -- rovira
 -- proteins
 -- ham-friends
 -- erdos
 -- routeview
 -- ham-full
 -- twin-city
 
 ALECJACOBSON GITHUB
 -- alligator
 -- cheburashka
 -- rocker-arm
 -- teapot
 -- woody
 
 https://networkrepository.com/misc.php
 CITE:   @inproceedings{nr,
      title = {The Network Data Repository with Interactive Graph Analytics and Visualization},
      author={Ryan A. Rossi and Nesreen K. Ahmed},
      booktitle = {AAAI},
      url={https://networkrepository.com},
      year={2015}
  }
 -- "08blocks"
 -- "662_bus" 
 -- "685_bus" 
 -- "1138_bus"
 -- "ABACUS_shell_hd" 
 -- "Alemdar"
 -- "bloweybq" 
 -- "bp_0"
 -- "bp_1200"
 -- "cage"
 -- "cage10"
 -- "CAG_mat364"
 -- "cavity01"
 -- "cavity05"
 -- "cavity14"
 -- "Chebyshev1"
 -- "crack"
 -- "CSphd"
 -- "delaunay_n10"
 -- "dw1024", "dw4096"
 -- "dwt_307"
 -- "EVA"
 -- "flowmeter0"
 -- "fs_541_3"
 -- "jagmesh2"
 -- "jagmesh5"
 -- "jagmesh9"
 -- "Kaufhold"
 -- "laser"
 -- "mahindas"
 -- "Reuters911"
 -- "SmaGri"
 -- "SmallW"
 -- "Trefethen_150"
 -- "Trefethen_2000"
 -- "TSOPF_FS_b9_c6"
 -- "TSOPF_RS_b162_c1"
