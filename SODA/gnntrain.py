import torch.nn.functional as F
import torch
from torch import tensor
import numpy as np
from gnn import GNN
import gnnutils as util
import os
import sys
import torch_geometric.datasets as tds
from torch_geometric.utils import train_test_split_edges as se
from torch_geometric.transforms.random_node_split import RandomNodeSplit
import math
import time
import yaml

'''
FUNCTION:
PARAMETERS:
DOES:
RETURNS:
NOTES: 
'''

'''
FUNCTION: parseargs(arglist)
PARAMETERS: Takes in a list of arguments and values "args".
DOES: Initializes values for training based on inputs.
RETURNS: A dictionary of inputs for training.
NOTES: 
	-- "args": has the form ["gnntrain.py","arg1","val1",...,"argk","valk"]
'''
def parseargs(arglist):

	argnames = {"hidden":64,"layers":2,"optimizer":"adam","lr":0.01,"epochs":200,"cratio":0.5,"levels":1,"method":"NDC"}

	index = 0
	for arg in arglist:
		if arg in argnames.keys():
			argnames[arg] = arglist[index+1]
		index += 1

	hidden = int(argnames["hidden"])
	layers = int(argnames["layers"])
	optimizer = argnames["optimizer"]
	lr = float(argnames["lr"])
	epochs = int(argnames["epochs"])
	cratio = float(argnames["cratio"])
	levels = int(argnames["levels"])
	method = argnames["method"]

	return( hidden,layers,optimizer,lr,epochs,cratio,levels,method )

'''
FUNCTION: multilevel_train(TGG_D,cratio,epochs,optim,lr)
PARAMETERS: Takes in the multilevel torch geometric graph dictionary "TGG_D".
			Additionally takes in the coarsening ratio for each level "cratio". Also takes in the 
			variable "epochs" which is an integer. Also takes in string "optim". Also takes in
			the staring learning rate "lr".
DOES: Performs training on this dictionary.
RETURNS: The trained model.
NOTES: 
'''
def multilevel_train(TGG_D,cratio,epochs,optim,lr):

	# split the epochs among different levels.
	keys = len(TGG_D.keys())
	epoch_arr = np.zeros(len(TGG_D))
	ind_arr = list(np.arange(len(TGG_D)))
	ind_arr.reverse()
	rem = epochs
	val = math.ceil(epochs*cratio)

	if len(ind_arr) == 1:
		epoch_arr[0] = epochs
	else:
		for i in ind_arr:
			if i != 0:
				epoch_arr[i] = val
				rem -= val
				val = math.ceil(epoch_arr[i]*cratio)
			else:
				epoch_arr[i] = rem
	epoch_arr = epoch_arr.astype(int)

	# initialize device (cuda if available).
	device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

	# initialize our model.
	num_features = TGG_D[0].x.size()[1]
	model = GNN(num_features,hidden,layers).to(device)
	model = model.float()

	# begin our loop, going through each level of coarsening starting at the coarsest level.
	keylist = list(TGG_D.keys())
	keylist.reverse()
	print(keylist)
	for key in keylist:

		# set the learning rate for the layer.
		# rate = lr*(1/cratio)**key
		rate = lr

		# set the graph, put it on device, and normalize the data.
		torchG = TGG_D[key]
		data = torchG.to(device)
		data.x = F.normalize(data.x,p=1)

		# depending on the optimizer and learning rate set the optimizer.
		if optim == "adam": 
			optimizer = torch.optim.Adam(model.parameters(), lr=rate, weight_decay=5e-4)
		elif optim == "sgd":
			optimizer = torch.optim.SGD(model.parameters(), lr=rate, momentum=0.9)

		# train the model.
		print("made it here...")
		model.train()
		for epoch in range(epoch_arr[key]):
		    optimizer.zero_grad()
		    out = model(data)
		    loss = F.nll_loss(out[data.train_mask], data.y[data.train_mask])
		    loss.backward()
		    optimizer.step()

		print("trained...")

	return( model )


'''
FUNCTION: sample_train(TGG,arguments,sample_num)
PARAMETERS: Takes in the multilevel torch geometric graph dictionary "TGG_D". Also takes in
			the array "arguments", which contains the parameters "hidden,layers,optim,lr,epochs,cratio,levels,method".
			Also takes in an integer "sample_num" which controls how many samples we consider.
DOES: Performs multilevel training on the torch geometric graph "TGG" according to the variable "arguments".
RETURNS: The accuracy and time measurements of training.
NOTES: 
'''
def sample_train(TGG,arguments,sample_num=20):

	[hidden,layers,optim,lr,epochs,levels,method] = arguments

	num_features = TGG.x.size()[1]

	#D,M = util.geometric_to_multilevel(TGG,cratio,method,levels)

	print()
	print("---- in sample_train() ----")

	device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

	coarse_accs = []
	elapsed_times = []
	coarsen_times = []

	print()
	for i in range(sample_num):

		D,M,coarse_time = util.geometric_to_multilevel(TGG,cratio,method,levels,timing=False)
		coarsen_times.append(coarse_time)

		start = time.time()
		model = multilevel_train(D,cratio,epochs,optim,lr)
		end = time.time()

		elapsed_times.append(end-start)

		orig_data = D[0].to(device)
		model.eval()
		pred = model(orig_data).argmax(dim=1)
		correct = (pred[orig_data.test_mask] == orig_data.y[orig_data.test_mask]).sum()
		acc = int(correct) / int(orig_data.test_mask.sum())
		print(f'Original Graph Accuracy (after coarsening): {acc:.4f}')

		coarse_accs.append(acc)
		model.reset_parameters()

	mean = float(np.mean(coarse_accs))
	stdev = float(np.std(coarse_accs))
	el_mean = float(np.mean(elapsed_times))
	el_dev = float(np.std(elapsed_times))
	coarse_mean = float(np.mean(coarsen_times))
	coarse_dev = float(np.std(coarsen_times))
	print()
	print("Coarse accuracy over "+str(sample_num)+" samples: "+str(mean)+"+/-"+str(stdev))
	print("Train time: "+str(el_mean)+"+/-"+str(el_dev))
	print("Coarsen time: "+str(coarse_mean)+"+/-"+str(coarse_dev))
	print()

	return(mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev)


if __name__ == '__main__':

	args = list(sys.argv)
	hidden,layers,optim,lr,epochs,cratio,levels,method = parseargs(args)

	arguments = [hidden,layers,optim,lr,epochs,levels,method]

	print()
	print("---- PARAMETERS ----")
	print(">> hidden: "+str(hidden))
	print(">> layers: "+str(layers))
	print(">> optimizer: "+str(optim))
	print(">> learning rate (lr): "+str(lr))
	print(">> epochs: "+str(epochs))
	print(">> coarsening ratio (cratio): "+str(cratio))
	print(">> coarse levels (levels): "+str(levels))
	print(">> coarsening method (method): "+str(method))
	print()

	#TGG = tds.KarateClub()[0]
	#TGG = tds.Planetoid(root='/tmp/Cora', name='Cora')[0]
	#TGG = tds.Planetoid(root='/tmp/CiteSeer', name='CiteSeer')[0]
	TGG = tds.Planetoid(root='/tmp/PubMed', name='PubMed')[0]
	
	#filename = "Cora_data.yml"
	#filename = "CiteSeer_data.yml"
	#filename = "PubMed_data.yml"

	#filename = "Cora_data_raw.yml"
	#filename = "CiteSeer_data_raw.yml"
	filename = "PubMed_data_raw.yml"

	if method == "ALL":

		for name in names:
			param_dict[name] = {}
			arguments[6] = name
			levels = (np.arange(4) + 1).astype(int)
			for lev in levels:
				arguments[5] = int(lev)
				mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev = sample_train(TGG,arguments)
				param_dict[name][int(lev)] = [mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev]
		with open(filename,'w') as outfile:
			yaml.dump(param_dict,outfile,default_flow_style=False)

	elif method == "NONE":
		param_dict = {}
		print("training")
		mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev = sample_train(TGG,arguments)
		print("trained")
		param_dict["NONE"] = [mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev]

		with open(filename,'w') as outfile:
			yaml.dump(param_dict,outfile,default_flow_style=False)
	else:
		param_dict = {}
		param_dict[method] = {}
		mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev = sample_train(TGG,arguments,sample_num=1)
		temp_filename = method+"_"+filename
		param_dict[method][levels] = [mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev]
		param_dict[method][levels-1] = [mean,stdev,el_mean,el_dev,coarse_mean,coarse_dev]
		with open(temp_filename,'w') as outfile:
			yaml.dump(param_dict,outfile,default_flow_style=False)

	'''
	print('Performing train on original graph...')
	orig_accs = []
	elapsed_times = []
	for i in range(20):
		model = GNN(num_features,hidden,layers).to(device)
		model = model.float()
		model.reset_parameters()
		
		data = D[0].to(device)
		# normalize the features.
		data.x = F.normalize(data.x,p=1)

		if optim == "adam": 
			optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=5e-4)
		elif optim == "sgd":
			optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)

		start = time.time()
		model.train()
		for epoch in range(epochs):
		    optimizer.zero_grad()
		    out = model(data)
		    loss = F.nll_loss(out[data.train_mask], data.y[data.train_mask])
		    loss.backward()
		    optimizer.step()
		end = time.time()
		elapsed_times.append(end-start)

		model.eval()
		pred = model(data).argmax(dim=1)
		correct = (pred[data.test_mask] == data.y[data.test_mask]).sum()
		acc = int(correct) / int(data.test_mask.sum())
		print(f'Original Graph Accuracy (raw): {acc:.4f}')

		orig_accs.append(acc)

	mean = np.mean(orig_accs)
	stdev = np.std(orig_accs)
	print()
	print("Original accuracy over 20 samples: "+str(mean)+"+/-"+str(stdev))
	print("Total time: "+str(np.mean(elapsed_times))+"+/-"+str(np.std(elapsed_times)))
	print()
	'''