import os
import sys
import yaml
import numpy as np
import networkx as nx
import string 
import coarselib as cl
import iolib as iol
import matplotlib.pyplot as plt
import scipy as sp

# "half", "fifth", "tenth", "small"
loc = "fifth"
locs = ["half","fifth","tenth"]
sizes = [0.5,0.2,0.1]

if __name__ == "__main__":
	if len(sys.argv) == 1:

		#graphs = ["teapot","alligator","cheburashka","woody","rocker-arm","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		#graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		#graphs = ["caida","dimacs","opsahl-powergrid","pdz","copperfield","football","dbpedia-similar","elegans-metabolic","yeast","netsci","hamhouse","rovira","proteins","ham-friends","erdos","routeview","ham-full","twin-city"]
		#graphs = ["opsahl-powergrid","pdz","copperfield","football","dbpedia-similar","elegans-metabolic","yeast","netsci","hamhouse","rovira","proteins","ham-friends","erdos","routeview","ham-full","twin-city"]
		#graphs = ["08blocks", "662_bus", "685_bus", "1138_bus", "ABACUS_shell_hd", "Alemdar", "bp_0", "bp_1200","cage", "cage10", "CAG_mat364", "cavity01", "cavity05", "cavity14", "Chebyshev1", "crack", "CSphd", "delaunay_n10","dw1024", "dw4096", "dwt_307", "EVA", "flowmeter0", "fs_541_3", "jagmesh2", "jagmesh5", "jagmesh9", "Kaufhold","laser", "mahindas", "Reuters911", "SmaGri", "SmallW", "Trefethen_150","Trefethen_2000", "TSOPF_FS_b9_c6", "TSOPF_RS_b162_c1"]
		graphs = ["teapot","alligator","cheburashka","woody","rocker-arm","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","opsahl-powergrid","pdz","copperfield","football","dbpedia-similar","elegans-metabolic","yeast","netsci","hamhouse","rovira","proteins","ham-friends","erdos","routeview","ham-full","twin-city","08blocks", "662_bus", "685_bus", "1138_bus", "ABACUS_shell_hd", "Alemdar","bp_0", "bp_1200","cage", "cage10", "CAG_mat364", "cavity01", "cavity05", "cavity14", "Chebyshev1", "crack", "CSphd", "delaunay_n10","dw1024", "dw4096", "dwt_307", "EVA", "flowmeter0", "fs_541_3", "jagmesh2", "jagmesh5", "jagmesh9", "Kaufhold","laser", "mahindas", "SmaGri", "SmallW", "Trefethen_150","Trefethen_2000"]
		names = ["HWC","ADC","ASC","GFSC","NDC","METIS"]

		for graph_name in graphs:
			# get our graph, our coarsened graph, and our mapping.
			print("forming graph...")
			print( graph_name )
			G = iol.read_edgelist(graph_name)
			print( "nodes: " +str(len(G.nodes())) )
			print( "edges: " +str(len(G.edges())) )
			for index in range( len( locs ) ):
				
				location = locs[index]
				size = sizes[index]

				for method_name in names:

					if method_name == "HWC":
						print("performing HWC...")
						H,mapping = cl.HWC( G, size, greedy=False )
					elif method_name == "ADC":
						print("performing ADC...")
						H,mapping = cl.ADC( G, size, 20, 20, greedy=False )
					elif method_name == "ASC":
						print("performing ASC...")
						H,mapping = cl.ASC( G, size, variant="matching" )
					elif method_name == "GFSC":
						print("performing GFSC...")
						H,mapping = cl.GFSC( G, size, objective="node" )
					elif method_name == "NDC":
						print("performing NDC...")
						H,mapping = cl.NDC( G, size, 20, asc="matching" )
					elif method_name == "METIS":
						print("performing METIS...")
						H,mapping = cl.METIS( G, size )
					elif method_name == "KRON":
						print("performing KRON...")
						H,mapping = cl.KRON( G, size )
					# write them to a yaml
					print( "	Graph Name: "+str(graph_name) +" Size: "+str(location))
					print( " 	Graph has "+str(len(G.nodes()))+" nodes..." )
					print( " 	Graph has "+str(len(G.edges()))+" edges..." )
					print( " 	Coarse graph has "+str(len(H.nodes()))+" nodes..." )
					print( " 	Coarse graph has "+str(len(H.edges()))+" edges..." )

					iol.write_yaml(G,H,10,mapping,graph_name,method_name,location)

	elif len(sys.argv) == 100:

		#graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		#graphs = ["teapot","alligator","cheburashka","woody","rocker-arm"]
		graphs = ["teapot","alligator","cheburashka","woody","rocker-arm","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		#graphs = ["lastfm_asia_edges"]
		size = float(sys.argv[1])

		for graph_name in graphs:

			# get our graph, our coarsened graph, and our mapping.
			print("forming graph...")
			print( graph_name )
			G = iol.read_edgelist(graph_name)
			print( " 	Graph has "+str(len(G.nodes()))+" nodes..." )
			print( " 	Graph has "+str(len(G.edges()))+" edges..." )

			# get method names
			#names = ["HWC","ADC","ASC","GFSC","NDC","METIS","KRON"]
			names = ["HWC","ADC","ASC","GFSC","NDC","METIS"]
			for method_name in names:
				if method_name == "HWC":
					print("performing HWC...")
					H,mapping = cl.HWC( G, size, greedy=False )
				elif method_name == "ADC":
					print("performing ADC...")
					H,mapping = cl.ADC( G, size, 20, 20, greedy=False )
				elif method_name == "ASC":
					print("performing ASC...")
					H,mapping = cl.ASC( G, size, variant="matching" )
				elif method_name == "GFSC":
					print("performing GFSC...")
					H,mapping = cl.GFSC( G, size, objective="node" )
				elif method_name == "NDC":
					print("performing NDC...")
					H,mapping = cl.NDC( G, size, 20, asc="matching" )
				elif method_name == "METIS":
					print("performing METIS...")
					H,mapping = cl.METIS( G, size )
				elif method_name == "KRON":
					print("performing KRON...")
					H,mapping = cl.KRON( G, size )
				# write them to a yaml
				iol.write_yaml(G,H,10,mapping,graph_name,method_name,loc)

	if len(sys.argv) == 2 and sys.argv[1] == "NDC":

		#graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		graphs = ["teapot","alligator","cheburashka","woody","rocker-arm"]
		#size = float(sys.argv[1])

		for index in range( len( locs ) ):
			location = locs[index]
			size = sizes[index]
			for graph_name in graphs:

				# get our graph, our coarsened graph, and our mapping.
				print("forming graph...")
				print( graph_name )
				G = iol.read_edgelist(graph_name)
				print( " 	Graph has "+str(len(G.nodes()))+" nodes..." )
				print( " 	Graph has "+str(len(G.edges()))+" edges..." )

				H,mapping = cl.NDC( G, size, 20, asc="matching" )
				iol.write_yaml(G,H,10,mapping,graph_name,"NDC",location)

	elif len(sys.argv) == 3 and sys.argv[2] == "METIS":

		#graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
		#graphs = ["teapot","alligator","cheburashka","horse","stanford_bunny"]
		graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","teapot","alligator","cheburashka","woody","rocker-arm"]
		size = float(sys.argv[1])

		for graph_name in graphs:

			# get our graph, our coarsened graph, and our mapping.
			print("forming graph...")
			print( graph_name )
			G = iol.read_edgelist(graph_name)
			print( " 	Graph has "+str(len(G.nodes()))+" nodes..." )
			print( " 	Graph has "+str(len(G.edges()))+" edges..." )

			H,mapping = cl.METIS( G, size )
			print( "	--- size : "+str(len(H.nodes())/len(G.nodes()))+" ---" )
			iol.write_yaml(G,H,10,mapping,graph_name,"METIS",loc)

	elif len(sys.argv) == 4:
		# get our graph name and filepath
		graph_name = sys.argv[1]
		method_name = sys.argv[2]
		size = float(sys.argv[3])

		# get our graph, our coarsened graph, and our mapping.
		print("forming graph...")
		G = iol.read_edgelist(graph_name)
		print( " 	Graph has "+str(len(G.nodes()))+" nodes..." )
		print( " 	Graph has "+str(len(G.edges()))+" edges..." )

		print("coarsening graph...")
		if method_name == "HWC":
			print("performing HWC...")
			H,mapping = cl.HWC( G, size, greedy=False )
		elif method_name == "ADC":
			print("performing ADC...")
			H,mapping = cl.ADC( G, size, 20, 20, greedy=False )
		elif method_name == "ASC":
			print("performing ASC...")
			H,mapping = cl.ASC( G, size, variant="matching" )
		elif method_name == "GFSC":
			print("performing GFSC...")
			H,mapping = cl.GFSC( G, size, objective="node" )
		elif method_name == "NDC":
			print("performing NDC...")
			#H,mapping = cl.NDC( G, size, 20 )
			H,mapping = cl.NDC( G, size, 20, asc="matching" )
			print( len(H.nodes()) )
		elif method_name == "METIS":
			print("performing METIS...")
			H,mapping = cl.METIS( G, size )
		elif method_name == "KRON":
			print("performing KRON...")
			H,mapping = cl.KRON( G, size )
		else:
			print(" Not a valid coarsening algorithm, please use one of the following...")
			print(" -- HWC, ADC, ASC, GFSC, NDC, METIS, KRON --")

		# write them to a yaml
		iol.write_yaml(G,H,10,mapping,graph_name,method_name,loc)
		G2, H2, data = iol.read_all( graph_name, method_name,loc)
		
		for e in H.edges():
			u = e[0]
			v = e[1]
			print( H[u][v]["weight"]-H2[u][v]["weight"] )

		LG = nx.normalized_laplacian_matrix( G, weight="weight" )
		LH = nx.normalized_laplacian_matrix( H, weight="weight" )
		LH2 = nx.normalized_laplacian_matrix( H2, weight="weight" )

		g_vals,g_vecs = sp.sparse.linalg.eigs( LG, 11, which="SM", ncv=10*10+20, tol=1e-12 )
		h_vals,h_vecs = sp.sparse.linalg.eigs( LH, 11, which="SM", ncv=10*10+20, tol=1e-12 )
		h2_vals,h_vecs = sp.sparse.linalg.eigs( LH2, 11, which="SM", ncv=10*10+20, tol=1e-12 )

		idx = g_vals.argsort()[::1]
		g_vals = np.real(g_vals[idx]).tolist()

		idx = h_vals.argsort()[::1]
		h_vals = np.real(h_vals[idx]).tolist()

		idx = h2_vals.argsort()[::1]
		h2_vals = np.real(h2_vals[idx]).tolist()

		print( g_vals )
		print( h_vals )
		print( h2_vals )

		plt.figure()
		plt.plot( g_vals, label="G" )
		plt.plot( h_vals, label="H" )
		plt.plot( h2_vals, label="H2" )
		plt.legend()
		plt.show()
		

	else:
		print(" Invalid inputs, please run as follows..." )
		print(" -- python3 coarsen.py [graph_name] [method_name] [size (float)] --")