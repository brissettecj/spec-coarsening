import sys
import scipy as sp
import numpy as np
import math
import networkx as nx
import matplotlib.pyplot as plt

import iolib as iol
'''--------------------------------------------------------------------------
lift(H,mapping)
- INPUT: Takes in a coarsened graph 'H', and its mapping from yaml 'mapping'.
- DOES: Determines the lifted version of 'H' with respect to 'mapping'.
- RETURNS: The lifted graph.
--------------------------------------------------------------------------'''
def lift(H,mapping):

	# set up our node_map based on mapping.
	nodes = np.arange( len(mapping) )
	node_map = { i:[] for i in range( max(mapping) + 1 ) }
	# populate node_map.
	for node in nodes:
		node_map[ mapping[node] ].append( node )
		
	# initialize our graph.
	L = nx.Graph()
	L.add_nodes_from( nodes )

	# iterate through all keys and determine sub-node connection weights.
	keys = list( node_map.keys() )
	# iterate, obtaining our cluster origin from the node_map keys (this is a node in H)
	for clust_a_orig in node_map.keys():
		# obtain our secondary list of nodes.
		b_list = clust_a_orig + np.arange( len( node_map.keys() ) - clust_a_orig )
		# iterate through the b_list with MORE nodes in H (overlapping at clust_a_orig)
		for clust_b_orig in b_list:
			# if such an edge exsists, continue...
			if H.has_edge( clust_a_orig, clust_b_orig ):
				# get clust_a and clust_b.
				clust_a = node_map[ clust_a_orig ]
				clust_b = node_map[ clust_b_orig ]
				
				# use its weight to define the lifted weight. CURRENTLY TESTING SOME WILD WEIGHTING STUFF
				#if clust_a_orig == clust_b_orig:
				#	weight = H[clust_a_orig][clust_b_orig]["weight"] / ( ( len(clust_a) * (len(clust_a)+1) )/2 )
				#else:
				#	weight = H[clust_a_orig][clust_b_orig]["weight"] / ( len(clust_a) * len(clust_b) )
				weight = H[clust_a_orig][clust_b_orig]["weight"] / ( len(clust_a) * len(clust_b) )
				for u in clust_a:
					for v in clust_b:
						L.add_edge( u, v, weight = weight )

	# return the lifted graph
	return( L )

'''--------------------------------------------------------------------------
eig_comp(G,H,data)
- INPUT: Takes in two graphs 'H', 'G'. Also takes in the 
		 data dictionary.
- DOES: Compares the eigenvectors and eigenpairs of 'H' with the eigenpairs of 'G'
- RETURNS: The coarsened eigenvectors, and eigenvalues, as well as the lifted eigenvalues.
- NOTES: For now the coarsening of 'G' is done with respect to a simple coarsening matrix P
- TO DO: 
		* Make sure this style of eigenvector coarsening makes sense.
--------------------------------------------------------------------------'''
def eig_comp(G,H,data):
	# Read the mapping from the data dictionary.
	formap = data["mapping"]
	# Set the backwards mapping dictionary.
	backmap = { i:[] for i in range(max(formap)+1) }
	index = 0
	for element in formap:
		backmap[ element ].append( index )
		index += 1

	# Read the eigenpairs of 'G' from the data dictionary.
	g_vals = data["evals"]
	g_vecs = data["evecs"]

	# Get the number of eigenpairs to compute.
	k = data["kval"]
	# Change k if the graph is too small for sparse computations
	if k > ( len(H.nodes()) - 4 ):
		k = ( len(H.nodes()) )

	# Compute the eigenpairs of 'H'
	H_lap = nx.normalized_laplacian_matrix( H, weight="weight" ).asfptype()
	if k != len(H.nodes()):
		h_vals,h_vecs = sp.sparse.linalg.eigs( H_lap, k+1, which="SM", ncv=10*k+20, tol=1e-12 )
	else:
		H_lap = H_lap.todense()
		h_vals,h_vecs = np.linalg.eig( H_lap )

	idx = h_vals.argsort()[::1]
	h_vals = np.real(h_vals[idx]).tolist()
	h_vecs = np.real(h_vecs[:,idx]).tolist()


	# Find the lift of 'H' and compute its spectral information.
	if data["method"] == "KRON":
		print( "	KRON reduction disallows lifting, setting null lift values... ")
		hl_vals = 0
		hl_vecs = 0
	else:
		H_lift = lift(H,formap)
		HL_lap = nx.normalized_laplacian_matrix( H_lift, weight="weight" ).asfptype()
		hl_vals,hl_vecs = sp.sparse.linalg.eigs( HL_lap, k+1, which="SM", ncv=4*k+20, tol=1e-12 )
		idx = hl_vals.argsort()[::1]
		hl_vals = np.real(hl_vals[idx]).tolist()
		hl_vecs = np.real(hl_vecs[:,idx]).tolist()

	# Form the P matrix for coarsening. ( combinatorial version )
	'''
	P = np.zeros( (len(H.nodes()),len(G.nodes())) )
	if data["method"] != "KRON":
		for from_node in backmap.keys():
			for to_node in backmap[from_node]:
				P[from_node][to_node] = 1
	
		# Coarsen the eigenvectors of 'G'
		gc_vecs = np.matmul( P, g_vecs )
	else:
		gc_vecs = 0 
	'''
	
	# Form the P matrix for coarsening. ( normalized version )
	P = np.zeros( (len(H.nodes()),len(G.nodes())) )
	if data["method"] != "KRON":
		for from_node in backmap.keys():
			for to_node in backmap[from_node]:
				P[from_node][to_node] = 1/math.sqrt( len( backmap[from_node] ) )
	
		# lift the eigenvectors of 'G_c'
		gc_vecs = np.matmul( P, g_vecs )
	else:
		gc_vecs = 0 

	# return it all.
	return([g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs)

if __name__ == "__main__":
	if len(sys.argv) == 4 and sys.argv[3] == "lift":
		# set our graph and method names.
		graph_name = sys.argv[1]
		method_name = sys.argv[2]
		
		# set up two graphs.
		G, H, data = iol.read_all( graph_name, method_name )

		# lift H.
		K = lift( H, data["mapping"] )

		# get the volume of K and G and H.
		volK = 0
		volG = 0
		volH = 0 
		for e in K.edges():
			volK += K[e[0]][e[1]]["weight"]
		for e in G.edges():
			volG += G[e[0]][e[1]]["weight"]
		for e in H.edges():
			volH += H[e[0]][e[1]]["weight"]

		# print out the volumes.
		print( volK )
		print( volG )
		print( volH )

	elif len(sys.argv) == 3:
		# get our graph name and filepath
		graph_name = sys.argv[1]
		method_name = sys.argv[2]

		# get our graph, a "coarsened graph", and our mapping.
		G, H, data = iol.read_all( graph_name, method_name )

		# get our comparison values.
		[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = eig_comp( G, H, data )

		# print them out.
		print( "--- G PAIRS ---" )
		print( g_vals )
		print( g_vecs )

		print( "--- H PAIRS ---" )
		print( h_vals )
		print( h_vecs )

		print( "--- H LIFT PAIRS ---" )
		print( hl_vals )
		print( hl_vecs )

		print( "--- G COARSENED VECTS ---" )
		print( gc_vecs )

		plt.figure(1)
		plt.imshow( g_vecs )

		plt.figure(2)
		plt.imshow( h_vecs )

		plt.figure(3)
		plt.imshow( hl_vecs )

		plt.figure(4)
		plt.imshow( gc_vecs )

		plt.figure(5)
		plt.plot( g_vals, '.', label="gvals" )
		plt.plot( h_vals, 'x', label="hvals" )
		plt.plot( hl_vals, '.', label="liftvals" )
		plt.legend()

		plt.show()
	else:
		print("analyzelib.py test needs a graph_name and method_name input...")

