import numpy as np
import scipy as sp
import networkx as nx
import matplotlib.pyplot as plt
import sys

'''-------------------------------------------------------------------------------
read_mesh( filepath )
INPUTS: Takes in the string "filepath" which determines where to get our mesh from.
DOES: Reads in the mesh from the file.
OUTPUT: Outputs a weighted networkx graph G.
-------------------------------------------------------------------------------'''
def read_mesh( filepath ):
	f = open( filepath, 'r' )
	lines = f.readlines()

	G = nx.Graph()
	for line in lines:
		if line[0] == "f":
			line = [int(s) for s in line.split() if s.isdigit()]
			print( line )
			G.add_edge( line[0], line[1] )
			G.add_edge( line[0], line[2] )
			G.add_edge( line[1], line[2] ) 

	G = nx.convert_node_labels_to_integers( G )
	return( G )

if __name__ == "__main__":
	filename = sys.argv[1]
	G = read_mesh( "./data/"+filename+".obj" )
	print(	"graph has "+str(len(G.nodes()))+" nodes.")
	print(	"graph has "+str(len(G.edges()))+" edges.")
	
	print(  "writing to edgelist..." )
	nx.write_edgelist( G, "./data/"+filename+".txt")
