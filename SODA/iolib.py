import os
import sys
import yaml
import networkx as nx
import numpy as np
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import matplotlib.pyplot as plt 
import netCDF4

'''------------------------------------------------
read_edgelist(graph_name)
- INPUT: Takes in a string 'graph_name'
- DOES: Forms a graph G from the associated edgelist. 
- RETURNS: the graph G
------------------------------------------------'''
def read_edgelist(graph_name):
	# define filepath.
	filepath = "./data/"+graph_name+".txt"

	# open file and initialize the graph G.
	f = open( filepath,"rb" )
	G = nx.read_edgelist( f )
	f.close()

	# get the largest connected component.
	G = G.subgraph( max( nx.connected_components(G), key=len ) )

	# fill in nodal "num" values.
	for node in G.nodes():
		G.nodes[node]["num"] = 1

	# fill in weight values.
	for edge in G.edges():
		G[edge[0]][edge[1]]["weight"] = 1

	# make sure labels are correct.
	G = nx.convert_node_labels_to_integers( G )

	# reuturn G.
	return( G ) 

'''------------------------------------------------
read_exodus(filepath,varrange,nodal_var)
- INPUT: Takes in a string 'filepath', the number of variables to be considered 'varrange', and the name
		 of the nodal variables as a string 'nodal_var'.
- DOES: Forms a graph G with the correct attributes.
- RETURNS: the graph G
------------------------------------------------'''
def read_exodus(filepath,varrange=[0],nodal_var='vals_nod_var2',labels='vals_nod_var4'):

	# Load the dataset
	nc = netCDF4.Dataset(filepath)

	print( nc )
	
	# Get the variable names and values:
	dimensions = list(nc.dimensions)
	print( dimensions )
	for dim in dimensions:
		print( nc.dimensions[dim] )
	print()

	variables = list(nc.variables.keys())
	print( variables )
	print()

	print( len(dimensions) )
	print( len(variables) )
	print()

	# Get the X and Y positions
	X = np.array(nc.variables['coordx'])
	Y = np.array(nc.variables['coordy'])

	# Get the connections
	connect = nc.variables['connect1']

	# Get the weights
	weight = []
	for i in varrange:
		weight.append(np.array(nc.variables[nodal_var][:])[i])

	# Create an adjacency matrix based on the inputs.
	raw_adj = np.zeros((len(X),len(X)))
	xf = zip(X[:],Y[:],weight)
	xy = np.array([X[:], Y[:]]).T
	mapping = {}
	neighbors = {key:[] for key in range(len(weight))}
	for i in range(len(X[:])):
		mapping[tuple(xy[i])] = i
	for coords in xy[connect[:]-1]:
		for e in zip(coords, np.roll(coords,1,axis=0)):
			edge = [(0,0),(0,0)]
			edge[0] = tuple(e[0])
			edge[1] = tuple(e[1])
			raw_adj[mapping[edge[0]]][mapping[edge[1]]] = 1
			raw_adj[mapping[edge[1]]][mapping[edge[0]]] = 1 

	# Turn the adjacency into a graph.
	G = nx.from_numpy_array(raw_adj)

	# Populate node parameters.
	for i in range(len(X[:])):
		node = mapping[tuple(xy[i])]
		G.nodes[node]["num"] = 1
		G.nodes[node]["features"] = weight[node]

'''------------------------------------------------
make_dict(G,H)
- INPUT: Takes in the graph 'G', the coarsened graph 'H'. Also takes in an eigenvector 
		 number 'k'. Additionally takes in two strings 'graph_name', and 'method_name'.
		 Also takes in an array 'mapping', where each node of 'G' is given a numerical
		 node value in 'H'.
- DOES: All spectral information of 'G' is computed. This is then all added to a dict. 
- RETURNS: A dictionary of all the relevent information.
- NOTES: computes the smallest 'k+1' eigenvectors, this gives us the first k nontrivials.
------------------------------------------------'''
def make_dict(G,H,k,mapping,graph_name,method_name):
	print( "writing to dictionary..." )
	# initialize our dictionary.
	dictionary = {
				  "graph": "",
				  "method": "",
				  "kval": 1,
				  "evecs": [],
				  "evals": [],
				  "mapping": []
				  }

	print( "	computing eigenpairs..." )
	# compute the normalized Laplacian and eigen-pairs.
	L_G = nx.normalized_laplacian_matrix( G, weight="weight" ).asfptype()
	evals,evecs = sp.sparse.linalg.eigs( L_G, k+1, which="SM", ncv=4*k+20, tol=1e-10 )
	idx = evals.argsort()[::1]
	evals = np.real(evals[idx]).tolist()
	evecs = np.real(evecs[:,idx]).tolist()
	print( "	done computing eigenpairs..." )

	print( "	writing to dictionary..." )
	# fill in our dictionary.
	dictionary.update(graph=graph_name,method=method_name,kval=k,
		evecs=evecs,evals=evals,mapping=mapping)

	# return our dictionary.
	return( dictionary )

'''------------------------------------------------
write_yaml(G,H,k,graph_name,method_name)
- INPUT: Takes in the graph 'G', the coarsened graph 'H'. Also takes in an eigenvector 
		 number 'k'. Additionally takes in two strings 'graph_name', and 'method_name'.
		 Also takes in an array 'mapping', where each node of 'G' is given a numerical
		 node value in 'H'. Additionally takes in a string "level_directory" controlling
		 where files are saved.
- DOES: Runs make_dict() and adds the output to a yaml file.
- RETURNS: nothing
- NOTES: computes the smallest 'k+1' eigenvectors, this gives us the first k nontrivials.
------------------------------------------------'''
def write_yaml(G,H,k,mapping,graph_name,method_name,level_directory):
	# obtain our dictionary.
	dictionary = make_dict( G,H,k,mapping,graph_name,method_name )

	# define our filename and open a file for writing.
	filename = "outputs/"+level_directory+"/"+graph_name+"_"+method_name+".yml"
	weight_name = "outputs/"+level_directory+"/"+graph_name+"_"+method_name+"_weights.npy"
	f = open( filename,'w+' )

	# write to yaml and close.
	yaml.dump( dictionary,f )
	f.close()
	print("written "+graph_name+" details to yaml...")

	# Make a new graph without weights.
	'''
	K = nx.Graph()
	K.add_nodes_from( list(H.nodes()) )
	weight_list = []
	for e in H.edges():
		K.add_edge( e[0], e[1] )
		weight_list.append( H[e[0]][e[1]]["weight"] )
	weight_list = np.array(weight_list)
	'''
	# write coarsened graph to an edgelist
	nx.write_edgelist( H, "outputs/"+level_directory+"/"+graph_name+"_"+method_name+".edgelist")
	# write the weights to a list
	#np.save(weight_name,weight_list)
	print("written "+graph_name+" to .edgelist...")

'''------------------------------------------------
read_yaml(graph_name,method_name)
- INPUT: Takes in two strings 'graph_name' and 'method_name'. Also takes in the string
		 'level_dicrectory', which controls where the files are read from.
- DOES: Reads in a yaml file based on the inputs to a dictionary.
- RETURNS: A coare graph H and a dictionary of data.
- TODO: Currently weights are not read in correctly... fix this.
------------------------------------------------'''
def read_yaml(graph_name,method_name,level_directory):
	print( "reading "+graph_name+" from yaml... Method: "+method_name+"..." )
	# obtain our original dictionary
	filename = "outputs/"+level_directory+"/"+graph_name+"_"+method_name+".yml"
	f = open( filename,'r' )
	data = yaml.safe_load( f )
	f.close()
	print(" 	data read in...")

	# read in edgelist.
	edgefile = "outputs/"+level_directory+"/"+graph_name+"_"+method_name+".edgelist"
	f = open(edgefile,"rb")
	H = nx.read_edgelist( f )
	print(" 	edgelist read...")

	# read in weights.
	'''
	weightfile = "outputs/"+level_directory+"/"+graph_name+"_"+method_name+"_weights.npy"
	weight_list = np.load( weightfile )

	# set weights.
	index = 0
	for e in H.edges():
		H[e[0]][e[1]]["weight"] = weight_list[index]
		index += 1
	'''

	# relabel dictionary.
	labels = { orig:int(orig) for orig in H.nodes() }
	H = nx.relabel_nodes( H, labels )
	f.close()
	print(" 	relabeled nodes...")
	print(" 	coarse graph read in...")

	# compute the nums for H from the mapping array,
	mapping = data["mapping"]
	nums = np.zeros( len(H.nodes()) )
	if data["method"] != "KRON":
		for i in range( len(mapping) ):
			nums[ mapping[i] ] += 1
	else:
		nums = np.ones( len( H.nodes() ) )
	print(" 	nums populated...")

	# use the nums to fill in the "num" nodal variables of 'H'
	for node in H.nodes():
		H.nodes[node]["num"] = nums[int(node)]
	print(" 	nodes labeled...")

	# overwrite lists in data.
	data["evecs"] = np.array(data["evecs"])
	data["evals"] = np.array(data["evals"])
	
	# return the read in dictionary as well as the coarse graph.
	return( H, data )

'''------------------------------------------------
read_all(graph_name,method_name, level_dicrectory)
- INPUT: Takes in two strings 'graph_name' and 'method_name'. Also takes in a string "level_directory" which dictates
		 where we read the file from.
- DOES: Performs read_edgelist() and read_yaml() for a given graph/method pair.
- RETURNS: The original graph 'G', the coarsened graph 'H', and
		   the data dictionary.
------------------------------------------------'''
def read_all(graph_name,method_name, level_directory ):
	# get the coarse graph and the data dictionary.
	H, data = read_yaml( graph_name, method_name, level_directory )
	
	# get the original / matching graph 'G'.
	G = read_edgelist( graph_name )

	# return it all.
	return( G, H, data )

if __name__ == "__main__":
	if len(sys.argv) == 3:
		# get our graph name and filepath
		graph_name = sys.argv[1]
		method_name = sys.argv[2]

		# read data
		G,H,data = read_all( graph_name, method_name, "fifth" )

		LG = nx.normalized_laplacian_matrix( G, weight="weight" )
		LH = nx.normalized_laplacian_matrix( H, weight="weight" )

		g_vals,g_vecs = sp.sparse.linalg.eigs( LG, 11, which="SM", ncv=10*10+20, tol=1e-12 )
		h_vals,h_vecs = sp.sparse.linalg.eigs( LH, 11, which="SM", ncv=10*10+20, tol=1e-12 )

		idx = g_vals.argsort()[::1]
		g_vals = np.real(g_vals[idx]).tolist()

		idx = h_vals.argsort()[::1]
		h_vals = np.real(h_vals[idx]).tolist()

		print( g_vals )
		print( h_vals )

		plt.figure()
		plt.plot( g_vals, label="G" )
		plt.plot( h_vals, label="H" )
		plt.legend()
		plt.show()


	elif len(sys.argv) == 2 and sys.argv[1] == 'exo':
		filepath = "./data/exo/bjt.sg.Vb0p5.varyVc.exo"
		read_exodus( filepath, varrange=np.arange(20) )
	
	else:
		print("iolob.py test needs [grap_name][method_name]...")