import scipy as sp 
import numpy as np 
import math 
import matplotlib.pyplot as plt
import pandas as pd
import sys

import iolib as iol
import analyzelib as anl

#direct = "fifth"

'''------------------------------------------------------------------------
frobvspec_avs(graph_names,k)
- INPUT: An array of strings, 'graph_names' and an integer 'k' controlling the number of eigenpairs.
- DOES: Compares frobenius norm with the spectral differences. Does this as an average with error bars.
- RETURNS: A plot of the values.
------------------------------------------------------------------------'''
def frobvspec_avs(graph_names, k, level_directory):
	# set our methods array.
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]
	# initialize our sets of values
	frob_avs = []
	spec_avs = []
	frob_dev = []
	spec_dev = []
	for method in names:
		frobs = []
		spec_norms = []
		for graph_name in graph_names:
			G, H, data = iol.read_all( graph_name, method, level_directory )
			[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )

			# append the spectral_norm value
			G_val = data["evals"]
			G_sum = np.sum(G_val)
			av = np.sum( np.abs( np.array(G_val)[0:min(len(G_val),len(h_vals))] - np.array(h_vals)[0:min(len(G_val),len(h_vals))] ) ) / G_sum
			spec_norms.append(av)

			# take the inner product of the lift with the original eigenvectors.
			hl_vects = np.array( hl_vecs )
			(hlrows,hlcols) = hl_vects.shape
			(grows,gcols) = g_vecs.shape
			rows = min(grows,hlrows)
			cols = min(gcols,hlcols) 
			mat = np.abs( np.matmul( hl_vects[0:rows,0:cols].transpose(), g_vecs[0:rows,0:cols] ) )
			print( "	matcomp shape: "+str(mat.shape) )
			# compute the Frobenius norm.
			ident = np.identity( cols )
			norm = np.linalg.norm( ( mat[0:cols,0:cols] - ident ) ) / len(mat)

			# increment frobs
			frobs.append( norm )
		frob_avs.append( np.mean(frobs) )
		spec_avs.append( np.mean(spec_norms) )
		frob_dev.append( np.std(frobs) )
		spec_dev.append( np.std(spec_norms) )

	# plot all the values
	style = ['x','+','1','2','3','.']
	color = ["black","black","black","black","black","red"]
	ebarsize = [10,15,15,15,15,20]

	fig,ax = plt.subplots(1,1,figsize=(8,6))
	fig.set_tight_layout(True)
	ax.errorbar( spec_avs, frob_avs, xerr=spec_dev, yerr=frob_dev, fmt='o', color="white", markersize='20', markeredgecolor="black", ecolor='black', elinewidth=1, capsize=6)
	for i in range( len(style) ):
		ax.errorbar( spec_avs[i], frob_avs[i], fmt='o', color="white", markersize='20', markeredgecolor="black")
		ax.errorbar( spec_avs[i], frob_avs[i], fmt=style[i], color=color[i], markersize=ebarsize[i],label=names[i] )
	plt.yscale("log")
	plt.xscale("log")
	plt.title( level_directory+" size eigenpair comparisons", fontsize=18)

	plt.grid(True)
	plt.xlabel("spectral error", fontsize=18)
	plt.ylabel("frobenius norm", fontsize=18)
	handles,labels = ax.get_legend_handles_labels()
	handles = [h[0] for h in handles]
	ax.legend(handles,labels,loc="lower right")

	figname = "./plots/frobvspec/"+level_directory+"_avs.png"
	fig.savefig(figname)

'''------------------------------------------------------------------------
frobvspec(graph_names,k)
- INPUT: An array of strings, 'graph_names' and an integer 'k' controlling the number of eigenpairs.
- DOES: Compares frobenius norm with the spectral differences.
- RETURNS: A plot of the values.
------------------------------------------------------------------------'''
def frobvspec(graph_names, k, level_directory):
	# set our methods array.
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]
	# initialize our sets of values
	alg = []
	frobs = []
	spec_norms = []
	for graph_name in graph_names:
		for method in names:
			alg.append(method)
			G, H, data = iol.read_all( graph_name, method, level_directory )
			[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )

			# append the spectral_norm value
			G_val = data["evals"]
			G_sum = np.sum(G_val)
			av = np.sum( np.abs( np.array(G_val)[0:min(len(G_val),len(h_vals))] - np.array(h_vals)[0:min(len(G_val),len(h_vals))] ) ) / G_sum
			spec_norms.append(av)

			# take the inner product of the lift with the original eigenvectors.
			hl_vects = np.array( hl_vecs )
			(hlrows,hlcols) = hl_vects.shape
			(grows,gcols) = g_vecs.shape
			rows = min(grows,hlrows)
			cols = min(gcols,hlcols) 
			mat = np.abs( np.matmul( hl_vects[0:rows,0:cols].transpose(), g_vecs[0:rows,0:cols] ) )

			print( "	matcomp shape: "+str(mat.shape) )
			# compute the Frobenius norm.
			ident = np.identity( cols )
			norm = np.linalg.norm( ( mat[0:cols,0:cols] - ident ) ) / len(mat)

			# increment frobs
			frobs.append( norm )

	# plot all the values
	style = ['x','+','1','2','3','.']
	color = ["black","black","black","black","black","red"]
	size = [100,100,100,100,100,150]
	styles = []
	colors = []
	sizes = []
	for graph in graph_names:
		styles = styles + style 
		colors = colors + color
		sizes = sizes + size

	fig = plt.figure( figsize=(9,6) ) #plt.figure(fignum, figsize=(8,6))
	fig.set_tight_layout(True)
	for i in range( len( graph_names )*len( names ) ):
		plt.scatter( spec_norms[i], frobs[i], marker=styles[i], c=colors[i], s=sizes[i] )
	plt.yscale("log")
	plt.xscale("log")
	plt.title( level_directory+" size eigenpair comparisons", fontsize=18)

	plt.grid(True)
	plt.xlabel("spectral error", fontsize=18)
	plt.ylabel("frobenius norm", fontsize=18)
	plt.legend(names,loc="lower right")

	figname = "./plots/frobvspec/"+level_directory+".png"
	fig.savefig(figname)

'''------------------------------------------------------------------------
lower_val_comp(graph_name, k, fignum)
- INPUT: A string, 'graph_name' as well as an eigenvalue number 'k' and a figure number 'fignum'
- DOES: Compares the lowest "k" eigenvalues for all methods with the original value.
- RETURNS: A plot of the values.
------------------------------------------------------------------------'''
def lower_val_comp(graph_name, k, fignum, level_directory):
	# set our methods array.
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]
	#names = ["NDC"]

	# Get all the spectra we need.
	G, H, data = iol.read_all( graph_name, 'HWC', level_directory )
	vals = [np.array(data["evals"])[1:len(data["evals"])]]
	for method in names:
		G, H, data = iol.read_all( graph_name, method, level_directory )
		[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )
		vals.append(h_vals[1:len(h_vals)])

		print( method )
		print( "---------" )
		H_vol = 0
		for node in H.nodes():
			H_vol += H.degree(node, weight="weight")
		print( "volume: "+str(H_vol) )
		print( "---------" )
	# plot all the values
	names = ["Original"] + names
	styles = ['--','x','+','1','2','3','.']
	colors = ["black","black","black","black","black","black","red"]
	sizes = [10,10,10,10,10,10,10,10,15]
	fig = plt.figure( fignum, figsize=(9,6) ) #plt.figure(fignum, figsize=(8,6))
	fig.set_tight_layout(True)
	for index in range( len( vals ) ):
		plt.plot( vals[index], styles[index], color=colors[index], markersize=sizes[index], label=names[index])
	plt.yscale("log")
	plt.title("Truncated eigenvalues for "+graph_name, fontsize=18)

	plt.grid(True)
	plt.xlabel("eigenvalue index", fontsize=18)
	plt.ylabel("eigenvalue", fontsize=18)
	plt.legend(loc="lower right")
	figname = "./plots/trunc_spec_"+level_directory+"/"+graph_name+".png"
	fig.savefig(figname)

'''------------------------------------------------------------------------
bar_avs(fignum,graphs)
- INPUT: An integer 'fignum' and an array of graph names "graphs"
- DOES: Gets the average spectral error for each method on each graph.
- RETURNS: A barplot of the values.
------------------------------------------------------------------------'''
def bar_avs(fignum,graphs, level_directory):
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]

	# Initialize a dictionary of methods
	arrays = np.zeros((len(graphs)+1,len(names)))
	# loop through all graphs and populate arrays.
	g_index = 0
	for graph in graphs:
		# Get all the spectra we need.
		G, H, data = iol.read_all( graph, 'HWC', level_directory )
		G_val = data["evals"]
		G_sum = np.sum(G_val)
		m_index = 0
		for method in names:
			G, H, data = iol.read_all( graph, method, level_directory )
			[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )
			av = np.sum( np.abs( np.array(G_val)[0:len(h_vals)] - np.array(h_vals) ) ) / G_sum
			arrays[g_index][m_index] = av 
			m_index += 1
		g_index += 1

	# append the "averages"
	graphs = graphs + ["average"]
	for index in range( len(names) ):
		arrays[g_index][index] = np.mean( arrays[0:(g_index-1),index] )

	# place arrays into a pandas array.
	df = pd.DataFrame( arrays, index = graphs )
	hatches = ''.join(h*len(df) for h in '/-.ox ')

	ax = df.plot( kind='bar', fill=False, logy=True, figsize=(18,5) )
	bars = ax.patches
	for bar, hatch in zip(bars, hatches):
		bar.set_hatch(2*hatch)
	for bar in bars[-len(df):]:
		bar.set_fill(True)
		bar.set_fc("red")
		bar.set_ec("black")

	fig = ax.get_figure()

	plt.title("spectral error for all graphs", fontsize=18)
	plt.xlabel("graph",fontsize=18)
	plt.ylabel("spectral error",fontsize=18)
	plt.xticks(rotation="horizontal", size=12)
	plt.yticks(rotation="horizontal", size=14)
	plt.legend(ncol=len(names),prop={'size': 14},labels=names)
	plt.tight_layout()

	figname = "./plots/trunc_spec_"+level_directory+"/bar_"+str(fignum)+".png"
	fig.savefig(figname)

	print( arrays )

'''------------------------------------------------------------------------
bar_avs_wierd(fignum,graphs)
- INPUT: An integer 'fignum' and an array of graph names "graphs"
- DOES: Gets the average spectral error for each method on each graph.
- RETURNS: A barplot of the values, but formatted for a very specific purpose.
------------------------------------------------------------------------'''
def bar_avs_wierd(fignum,graphs,level_directory):
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]
	labels = ["Heavy weight","Algebraic distance","METIS","Adjacency similarity","Greedy Fiedler","Nodal domain"]
	
	# Initialize a dictionary of methods
	arrays = np.zeros((len(graphs)+1,len(names)))
	# loop through all graphs and populate arrays.
	g_index = 0
	for graph in graphs:
		# Get all the spectra we need.
		G, H, data = iol.read_all( graph, 'HWC', level_directory )
		G_val = data["evals"]
		G_sum = np.sum(G_val)
		m_index = 0
		for method in names:
			G, H, data = iol.read_all( graph, method, level_directory )
			[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )
			av = np.sum( np.abs( np.array(G_val)[0:len(h_vals)] - np.array(h_vals) ) ) / G_sum
			arrays[g_index][m_index] = av 
			m_index += 1
		g_index += 1

	# append the "averages"
	graphs = graphs + ["average"]
	for index in range( len(names) ):
		arrays[g_index][index] = np.mean( arrays[0:(g_index-1),index] )

	# place arrays into a pandas array.
	df = pd.DataFrame( arrays, index = graphs )
	hatches = ''.join(h*len(df) for h in '/-.ox ')

	ax = df.plot.barh( color={"goldenrod","mediumseagreen","cornflowerblue","firebrick","lightslategray","black"}, figsize=(10,18) )
	
	#bars = ax.patches
	#for bar, hatch in zip(bars, hatches):
	#	bar.set_hatch(2*hatch)
	#for bar in bars[-len(df):]:
	#	bar.set_fill(True)
	#	bar.set_fc("red")
	#	bar.set_ec("black")

	fig = ax.get_figure()

	plt.title("spectral error for all graphs", fontsize=24)
	plt.ylabel("graph",fontsize=24)
	plt.xlabel("spectral error",fontsize=24)
	plt.yticks(rotation="vertical", size=22)
	plt.xticks(rotation="horizontal", size=22)
	plt.legend(prop={'size': 20},labels=labels)
	plt.tight_layout()

	figname = "./plots/trunc_spec_wierd_"+level_directory+"/bar_"+str(fignum)+".png"
	fig.savefig(figname)

	print( arrays )

'''------------------------------------------------------------------------
comp_vects( graph_name, method_name, fignum )
- INPUT: A graph name 'graph_name' and the method 'method_name' as strings. Additionally
		takes in the integer 'fignum' defining the figure number.
- DOES: Compares the vectors of the coarsened graph with those of the original
		graph.
- RETURNS: A visualization of the eigenvectors.
------------------------------------------------------------------------'''
def comp_vects( graph_name, method_name, fignum, level_directory ):
	# read in our graph information and find spectral information as well.
	G, H, data = iol.read_all( graph_name, method_name, level_directory )
	[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )

	# take the inner product of the lift with the original eigenvectors.
	hl_vects = np.array( hl_vecs )
	g_vecs = np.array( g_vecs )
	g_vects = g_vecs[:,0:len(hl_vects[0,:])]

	# make sure that the sign of each column is correct.
	for ind in range(len( hl_vects[0,:] )):
		if g_vects[ind,0] != hl_vects[ind,0]:
			hl_vects[:,ind] = (-1)*hl_vects[:,ind]

	mat = np.abs( np.matmul( hl_vects.transpose(), g_vects ) )

	# Form our image.
	title = graph_name + " " + method_name + " eigenvectors"
	if method_name != "NDC":
		color = "Greys"
	else:
		color = "Reds"

	# Form the image.
	fig = plt.figure( fignum )
	plt.title( title, fontsize=16 )
	plt.imshow( mat, cmap=color )

	# return the fig
	return( fig )

'''------------------------------------------------------------------------
frob_vects( graph_name, method_name )
- INPUT: A graph name 'graph_name' and the method 'method_name' as strings. 
- DOES: Compares the vectors of the coarsened graph with those of the original
		graph.
- RETURNS: The Frobenius norm between vectors
------------------------------------------------------------------------'''
def frob_vects( graph_name, method_name, level_directory ):
	# read in our graph information and find spectral information as well.
	G, H, data = iol.read_all( graph_name, method_name, level_directory )
	[g_vals,g_vecs],[h_vals,h_vecs],[hl_vals,hl_vecs],gc_vecs = anl.eig_comp( G, H, data )

	# take the inner product of the lift with the original eigenvectors.
	hl_vects = np.array( hl_vecs )
	mat = np.abs( np.matmul( hl_vects.transpose(), g_vecs ) )

	# compute the Frobenius norm.
	ident = np.identity( len(mat) )
	norm = np.linalg.norm( ( mat[0:len(mat),0:len(mat)] - ident ) ) / len(mat)

	# return the fig
	return( norm )

'''------------------------------------------------------------------------
comp_all_vects(  graphs )
- INPUT: an array of graph names "graphs"
- DOES: Compares the vectors of the coarsened graph with those of the original
		graph for all graphs and methods (except KRON reduction).
- RETURNS: A bunch of plots of eigenvector comparisons.
------------------------------------------------------------------------'''
def comp_all_vects( graphs, fignum, level_directory ):
	# initialize our graph and method names.
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]

	# iterate through and do our thang.
	new_fignum = fignum
	for graph in graphs:
		for name in names:
			fig = comp_vects( graph, name, new_fignum, level_directory )
			figname = "./plots/lift_vecs_"+level_directory+"/"+graph+"_"+name+".png"
			fig.savefig(figname)
			new_fignum += 1

	return( new_fignum )

'''------------------------------------------------------------------------
vect_frob_diffs( graph_list, fignum )
- INPUT: An array of graph names "graph_list" and the integer "fignum"
- DOES: Compares the vectors of the coarsened graph with those of the original
		graph for all graphs and methods (except KRON reduction).
- RETURNS: A big ol bar plot.
------------------------------------------------------------------------'''
def vect_frob_diffs( graph_list, fignum, level_directory ):
	names = ["HWC","ADC","METIS","ASC","GFSC","NDC"]

	# Initialize a dictionary of methods
	arrays = np.zeros((len(graph_list)+1,len(names)))
	# loop through all graphs and populate arrays.
	g_index = 0
	for graph in graph_list:
		m_index = 0
		for method in names:
			arrays[g_index][m_index] = frob_vects( graph, method, level_directory )
			m_index += 1
		g_index += 1

	# append the "averages"
	graphs = graph_list + ["average"]
	for index in range( len(names) ):
		arrays[g_index][index] = np.mean( arrays[0:(g_index-1),index] )


	# place arrays into a pandas array.
	df = pd.DataFrame( arrays, index = graphs )
	hatches = ''.join(h*len(df) for h in '/-.ox ')

	ax = df.plot( kind='bar', fill=False, logy=True, figsize=(5,18) )
	bars = ax.patches
	for bar, hatch in zip(bars, hatches):
		bar.set_hatch(2*hatch)
	for bar in bars[-len(df):]:
		bar.set_fill(True)
		bar.set_fc("red")
		bar.set_ec("black")

	fig = ax.get_figure()
	plt.title("Frobenius vector error for all graphs", fontsize=18)
	plt.xlabel("graph",fontsize=18)
	plt.ylabel("Frobenius norm",fontsize=18)
	plt.xticks(rotation="horizontal", size=12)
	plt.yticks(rotation="horizontal", size=14)
	plt.legend(ncol=len(names),prop={'size': 14},labels=names)
	plt.tight_layout()

	figname = "./plots/lift_vecs_"+level_directory+"/frobs_"+str(fignum)+".png"
	fig.savefig(figname)

	print( arrays )

if __name__ == "__main__":
	#graphs = ["lastfm_asia_edges"]
	#graphs = ["CA-GrQc"]
	#graphs = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","teapot","alligator","cheburashka","woody","rocker-arm"]
	#graphs2 = ["CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad"]
	#graphs1 = ["teapot"]
	graphs1 = ["teapot","alligator","cheburashka","woody","rocker-arm"]
	#graphs = ["teapot","alligator","arenas-jazz"]
	#graphs = ["teapot","alligator","cheburashka","woody","rocker-arm","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","opsahl-powergrid","pdz","copperfield","football","dbpedia-similar","elegans-metabolic","yeast","netsci","hamhouse","rovira","proteins","ham-friends","erdos","routeview","ham-full","twin-city","08blocks", "662_bus", "685_bus", "1138_bus", "ABACUS_shell_hd", "Alemdar","bp_0", "bp_1200","cage", "cage10", "CAG_mat364", "cavity01", "cavity05", "cavity14", "Chebyshev1", "crack", "CSphd", "delaunay_n10","dw1024", "dw4096", "dwt_307", "EVA", "flowmeter0", "fs_541_3", "jagmesh2", "jagmesh5", "jagmesh9", "Kaufhold","laser", "mahindas", "SmaGri", "SmallW", "Trefethen_150","Trefethen_2000"]
	#raphs = ["teapot","alligator","cheburashka","woody","rocker-arm","CA-GrQc","CA-HepTh","lastfm_asia_edges","facebook_combined","Wiki-Vote","email-Eu-core","arenas-jazz","euroroad","opsahl-powergrid","pdz","copperfield","football","dbpedia-similar","elegans-metabolic","yeast","netsci","hamhouse","rovira","proteins","ham-friends","erdos","routeview","ham-full","twin-city","08blocks", "662_bus", "685_bus", "1138_bus", "ABACUS_shell_hd", "Alemdar","bp_0", "bp_1200","cage", "cage10", "CAG_mat364", "cavity01", "cavity05", "cavity14", "Chebyshev1", "crack", "CSphd", "delaunay_n10","dw1024", "dw4096", "dwt_307", "EVA", "flowmeter0", "fs_541_3", "jagmesh2", "jagmesh5", "jagmesh9", "Kaufhold","laser", "mahindas", "SmaGri", "SmallW", "Trefethen_150","Trefethen_2000"]
	#graphs = ["cage"]
	#print( "GRAPHS: "+str(len(graphs)) ) 
	if len(sys.argv) == 1:
		fignum = 1
		for graph in graphs:
			lower_val_comp(graph, 10, fignum)
			fignum += 1
		#bar_avs( fignum, graphs )
	elif len(sys.argv) == 2:
		if sys.argv[1] == 'vecav':
			comp_all_vects( graphs )
		elif sys.argv[1] == 'valav':
			bar_avs( 1, graphs1, "half" )
		elif sys.argv[1] == 'wierd':
			bar_avs_wierd(1,graphs1, "tenth")
		elif sys.argv[1] == 'frob':
			vect_frob_diffs( graphs )
		elif sys.argv[1] == 'frobav':
			directs = ["half","fifth","tenth"]
			fignum = 1
			for dire in directs:
				#vect_frob_diffs( graphs1, fignum, dire )
				#fignum += 1
				vect_frob_diffs( graphs2, fignum, dire )
				fignum += 1
		elif sys.argv[1] == "frobspec":
			directs = ["half","fifth","tenth"]
			#directs = ["fifth","tenth"]
			for direct in directs:
				frobvspec_avs(graphs, 10, direct)
				#frobvspec(graphs, 10, direct)
		elif sys.argv[1] == "all":
			# loop through each directory
			#directs = ["half","fifth","tenth","small"]
			#directs = ["fifth","tenth","small"]
			directs = ["half"]
			# initialize our figure number
			fignum = 1
			for dire in directs:

				print( "--- DOING DIRECTORY: "+dire+" ---" )
				# loop through each graph in our graphs1 array
				for graph in graphs1:
					lower_val_comp(graph, 10, fignum, dire )
					fignum += 1
				# compare all vectors
				fignum = comp_all_vects( graphs1, fignum, dire )
				# get the bar_averages
				bar_avs( fignum, graphs1, dire )
				fignum += 1
				# compare the frobenius norm of all vectors
				vect_frob_diffs( graphs1, fignum, dire )
				fignum += 1

				# loop through each graph in our graphs2 array
				for graph in graphs2:
					lower_val_comp(graph, 10, fignum, dire)
					fignum += 1
				# compare all vectors
				fignum = comp_all_vects( graphs2,fignum, dire )
				# get the bar_averages
				bar_avs( fignum, graphs2, dire )
				fignum += 1
				# compare the frobenius norm of all vectors
				vect_frob_diffs( graphs2, fignum, dire )
				fignum += 1

				# compare the spectrum and frobenius norm
				# frobvspec( graphs, 10 )
		else:
			print("eyy whoah there buddy...")
	elif len(sys.argv) == 4:
		if sys.argv[3] == "v":
			graph_name = sys.argv[1]
			method_name = sys.argv[2]
			lower_val_comp(graph_name, 10, 1)
			#comp_vects( graph_name, method_name, 1 )
			plt.show()
	else:
		print("plotlib.py test needs exactly one input 'graph_name'...")