import networkx as nx 
import numpy as np
import torch
from torch_geometric.utils.convert import from_networkx
from torch_geometric.utils.convert import to_networkx
import sys
import coarselib as coarse
import torch_geometric.datasets as tds
import matplotlib.pyplot as plt
import time 
'''
FUNCTION: 
PARAMETERS:
DOES:
RETURNS:
NOTES:
'''

'''
FUNCTION: comp_ylabs( G )
PARAMETERS: Takes in a networkx graph "G" with the associated labels.
DOES: Uses the "label" parameter to compute and update the "y" parameter for each node.
RETURNS: nothing, just changes G in place.
NOTES:
'''
def comp_ylabs(G):
	for node in G.nodes():
		G.nodes[node]["y"] = np.amax(G.nodes[node]["label"])

'''
FUNCTION: multilevel_coarsen(G,coarsening_ratio,method,k,timing=True)
PARAMETERS: networkx graph 'G', positive real number less than 1 'coarsening_ratio',
			a string 'method', and a number of levels 'k'. Also takes in a boolean paramter 'timing'
			that 
DOES: Performs multilevel coarsening of a graph for a given method.
RETURNS: A dictionary of labeled graphs 'D', and a dictionary of mappings 'M'.
NOTES:
	'method' should be one of the following.
	-- "METIS": Uses the weight-balanced metis algorithm.
	-- "HWC": Uses heavy-weight coarsening.
	-- "ADC": Uses algebraic distance.
	-- "ASC": Uses adjacency spectral coarsening.
	-- "GFSC": Uses greedy Fiedler spectral coarsening.
	-- "NDC": Uses nodal domain coarsening.
'''
def multilevel_coarsen(G,coarsening_ratio,method,k,timing=True):

	D = {0:G}
	M = {0:list(np.arange(len(G.nodes())))}

	start = time.time()

	for i in range(k):
		if method == "METIS":
			D[i+1],M[i+1] = coarse.METIS(D[i],coarsening_ratio)
		elif method == "HWC":
			D[i+1],M[i+1] = coarse.HWC(D[i],coarsening_ratio,greedy=False)
		elif method == "ADC":
			D[i+1],M[i+1] = coarse.ADC(D[i],coarsening_ratio,20,20,greedy=False)
		elif method == "ASC":
			D[i+1],M[i+1] = coarse.ASC(D[i],coarsening_ratio,variant="matching")
		elif method == "GFSC":
			D[i+1],M[i+1] = coarse.GFSC(D[i],coarsening_ratio,objective="node")
		elif method == "NDC":
			D[i+1],M[i+1] = coarse.NDC(D[i],coarsening_ratio,20,asc="matching")
		elif method == "NONE":
			print("no coarsening...")
		else:
			print("ERROR in 'multilevel_coarsen()': invalid method.")
			return()

		if method != "NONE":
			comp_ylabs(D[i+1])

	end = time.time()
	elapsed = end-start

	if timing:
		print("---- IN MULTILEVEL COARSEN ----")
		print("elapsed coarsen time: "+str(elapsed))
	    	
	return( D, M, elapsed )


'''
FUNCTION: geometric_to_nx(torchG,traintest)
PARAMETERS: Takes in a torch geometric graph "torchG". Also takes in a boolean "traintest".
		    "traintest" controls if we label the graph with the train and test sets.
DOES: Transforms it to a networkx graph which is usable with our coarsening library "coarselib"
RETURNS: A labeled NetworkX graph with features.
NOTES:
'''
def geometric_to_nx(torchG,traintest=False):

	# Create our graph with labels and features (need to change the names)

	if traintest:
		G = to_networkx(torchG,to_undirected=True,node_attrs=["x","y","train_mask","test_mask"])
	else:
		G = to_networkx(torchG,to_undirected=True,node_attrs=["x","y"])

	G = G.subgraph( max( nx.connected_components(G), key=len ) )
	G = nx.convert_node_labels_to_integers(G)

	# Get the features to get one-hot vectors.
	labels = list( nx.get_node_attributes(G,"y").values() )
	n_values = np.max(labels) + 1
	one_hots = np.eye(n_values)[labels]

	# Create a dupicalte graph and populate it with features of the correct names.
	H = nx.Graph()
	for node in G.nodes():

		if traintest:
			H.add_node(node,num=1,features=np.array(G.nodes[node]["x"]),
				label=one_hots[node],y=labels[node],
				train_mask=G.nodes[node]["train_mask"],test_mask=G.nodes[node]["test_mask"])
		else:
			H.add_node(node,num=1,features=np.array(G.nodes[node]["x"]),
				label=one_hots[node],y=labels[node])

	H.add_edges_from(list(G.edges()),weight=1)

	return(H)

'''
FUNCTION: nxdict_to_geometric(D)
PARAMETERS: Takes in a dictionary of networkx graphs "D".
DOES: Transforms a dictionary 'D' of networkx graphs to torch_geometric graphs.
RETURNS: A dictionary of torch_geometric graphs 'TGG_D'.
NOTES:
'''
def nxdict_to_geometric(D):
	
	# iterate through the dictionary and convert each.
	TGG_D = {}
	for key in D.keys():

		# make a surrogate graph with less parameters.
		H = nx.Graph()
		for node in D[key].nodes():
			feat = D[key].nodes[node]["features"]
			num = D[key].nodes[node]["num"]
			y = np.argmax(D[key].nodes[node]["label"])
			H.add_node(node,x=list(feat/num),y=y)
		H.add_edges_from(list(D[key].edges()))

		# populate the dictionary
		TGG_D[key] = from_networkx( H )

	# return the dictionary.
	return( TGG_D )

'''
FUNCTION: geometric_to_multilevel(TGG,coarsening_ratio,method,k,timing)
PARAMETERS: Takes in a pytorch geometric graph "TGG", positive real number less than 1 
			'coarsening_ratio', a string 'method', and a number of levels 'k'. Also takes in the boolean
			argument 'timing', which determines if we time our multilevel coarsening.
DOES: Transforms the graph into a dictionary of pytorch geometrid graphs.
RETURNS: A dictionary of torch_geometric graphs 'TGG_D' and the mapping dictionary 'M'.
NOTES:
	'method' should be one of the following.
	-- "METIS": Uses the weight-balanced metis algorithm.
	-- "HWC": Uses heavy-weight coarsening.
	-- "ADC": Uses algebraic distance.
	-- "ASC": Uses adjacency spectral coarsening.
	-- "GFSC": Uses greedy Fiedler spectral coarsening.
	-- "NDC": Uses nodal domain coarsening.
'''
def geometric_to_multilevel(TGG,coarsening_ratio,method,k,timing=True):

	print("starting multilevel coarsening...")
	# convert to networkx
	G = geometric_to_nx(TGG)
	print("converted to networkx graph...")
	# perform our coarsening
	print("perfoming coarsening...")
	if k == 0:
		D = G
		M = 0
		elapsed = 0
	else:
		D,M,elapsed = multilevel_coarsen(G,coarsening_ratio,method,k,timing=timing)
	# convert back to geometric dictionary
	print("changing back to pytorch geometric...")
	TGG_D = nxdict_to_geometric(D)
	# label nodes as training or testing
	for key in TGG_D.keys():
		TGG_D[key] = data_split(TGG_D[key],"random")

	return( TGG_D,M,elapsed )

'''
FUNCTION: data_split(TGG,version)
PARAMETERS: Takes in a torch geometric graph "TGG", and a split version as a string "version".
DOES: Applies train and test indices to the nodes in the torch geometric graph.
RETURNS: the labeled data.
NOTES:
	"version" can be one of the following:
	-- 'fixed','random',[other]
	Also uses the "index_to_mask" function defined beneath it.
'''
def data_split(data, version):

	num_classes = len(set(np.array(data.y)))
	if version !='fixed':
		indices = []
		for i in range(num_classes):
			index = (data.y == i).nonzero().view(-1)
			index = index[torch.randperm(index.size(0))]
			indices.append(index)

		if version == 'random':
			train_index = torch.cat([i[:20] for i in indices], dim=0)
			val_index = torch.cat([i[20:50] for i in indices], dim=0)
			test_index = torch.cat([i[50:] for i in indices], dim=0)
		else:
			train_index = torch.cat([i[:5] for i in indices], dim=0)
			val_index = torch.cat([i[5:10] for i in indices], dim=0)
			test_index = torch.cat([i[10:] for i in indices], dim=0)

		data.train_mask = index_to_mask(train_index, size=data.num_nodes)
		data.val_mask = index_to_mask(val_index, size=data.num_nodes)
		data.test_mask = index_to_mask(test_index, size=data.num_nodes)

		return data

def index_to_mask(index, size):
    mask = torch.zeros(size, dtype=torch.bool, device=index.device)
    mask[index] = 1
    return mask

if __name__ == '__main__':
	#torchG = tds.Coauthor(root='./dataset/Physics', name="Physics")[0]
	torchG = tds.KarateClub()[0]

	print(torchG)
	print("torch_geometric graph parameters -----------------")
	print("x:")
	print(torchG.x)
	print("edge_index:")
	print(torchG.edge_index)
	print("y:")
	print(torchG.y)
	print("train_mask:")
	print(torchG.train_mask)
	print("--------------------------------------------------")

	G = geometric_to_nx(torchG)
	D = {0:G}
	D,M = multilevel_coarsen( G, 0.5, "ADC", 3 )

	plt.figure()
	nx.draw(D[0])
	plt.figure()
	nx.draw(D[1])
	plt.figure()
	nx.draw(D[2])
	plt.figure()
	nx.draw(D[3])
	plt.show()

	TGGD = nxdict_to_geometric( D )

	print(TGGD)