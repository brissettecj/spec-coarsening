import tools
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import networkx as nx 

def single_merge( G, node_a, node_b ):
	H = G.copy()

	# Determine our neighbors and weights
	neighbors = {}
	for node in G.neighbors(node_a):
		if node not in neighbors.keys():
			neighbors[node] = G[node_a][node]["weight"]
		else:
			neighbors[node] += G[node_a][node]["weight"]
	for node in G.neighbors(node_b):
		if node not in neighbors.keys():
			neighbors[node] = G[node_b][node]["weight"]
		else:
			neighbors[node] += G[node_b][node]["weight"]

	# remove the neighbors of node_a
	neighbor_list = list( H.neighbors( node_a ) )
	for neighbor in neighbor_list:
		H.remove_edge( node_a, neighbor )

	# remove node_b from the graph
	H.remove_node( node_b )

	# add in our edges
	for neighbor in neighbors.keys():
		if neighbor != node_b:
			H.add_edge( node_a, neighbor, weight=neighbors[neighbor] )

	return( H )

G = nx.erdos_renyi_graph(10,0.6)
for edge in G.edges():
	a = edge[0]
	b = edge[1]
	G[a][b]["weight"] = 1

plt.figure(1)
nx.draw(G)

#print( np.array(nx.adjacency_matrix(G,weight="weight")) )
H = G.copy()
#H = nx.contracted_nodes(H,1,2,self_loops=True)
H = single_merge( H, 1, 2 )

#print( list(H.neighbors(1)) )
#print( H.degree(1,weight="weight") )

print( np.array(nx.adjacency_matrix(H,weight="weight")) )
plt.figure(2)
nx.draw(H)

plt.show()