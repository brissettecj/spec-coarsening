import tools
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import networkx as nx 

k = 10
tolerance = 0
normed = True
#file = "caida_adj.txt"
#file = "dimacs_adj.txt"
#file = "euroroad_adj.txt"

file = "CA_adj.txt"
#file = "PA_road_adj.txt"
#file = "petster_adj.txt"

#file = "bunny_2375.txt"
#file = "dragon_25000.txt"
#file = "facebook_2000.txt"
#file = "bjt_adjlist.txt"

#file = "jazz_adjlist.txt"

nodes, edges, mesh_dict = tools.read_general_mesh( "../cudagraph/"+file )

print( "Mesh dict read in..." )

#G = nx.from_numpy_matrix( A )
M = tools.get_graph( nodes, edges, mesh_dict )
print( "Gotten graph..." )
print( "nodes: "+str(nodes)+" edges: "+str(edges) )
components = [ c for c in sorted(nx.connected_components(M), key=len, reverse=True) ]
print( "component size: "+str( len(components[0]) ))
N = M.subgraph( components[0] )
relabel_map = {}
index = 0
for i in N.nodes():
	relabel_map[i] = index
	index += 1
G = nx.relabel_nodes( N, relabel_map )
print(">>>>>>>>>>> getting position of graph <<<<<<<<<<<<<<")
#pos = nx.spring_layout( G )
#pos = nx.kamada_kawai_layout( G )
#pos = nx.spectral_layout( G )

#largest_component = sorted(nx.connected_components(G), key=len, reverse=True)[0]

#print( ">>>>>>>>>>>>>>>>>> get varainces... <<<<<<<<<<<<<<<<<<<<<<")
#var = tools.get_eigen_variances( G, k, tolerance, normed )
#tools.plot_eigen_variances( k, var )

print( ">>>>>>>>>>>>>>>>>> get nodal domains... <<<<<<<<<<<<<<<<<<<<<" )
signs = tools.get_nodals( G, './recent_output.txt', k, tolerance, normed )

print( ">>>>>>>>>>>>>>>>>> get nodal_dict... <<<<<<<<<<<<<<<<<<<<<" )
#nodal_dict = tools.get_nodal_dict( signs )
nodal_dict = tools.get_connected_nodal_dict( G, signs )

'''
print( ">>>>>>>>>>>>>>>>>> getting component variances... <<<<<<<<<<<<<<<<<<<<<" )
tools.plot_nodal_components( G, nodal_dict, pos, 0, "./plots/nodal_vars/nodal_parts_"+file+"_"+str(i+1)+".png", k)
w,v = tools.get_eigens( G, k, tolerance, normed )
for i in range( k ):
	eig = v[:,i+1]
	filepath = "./plots/nodal_vars/"+file+"_"+str(i+1)+".png"
	tools.plot_component_variances( G, nodal_dict, pos, eig, i+1, filepath )
'''

print( ">>>>>>>>>>>>>>>>>> performing merges... <<<<<<<<<<<<<<<<<<<<<" )
#H = tools.fast_nodal_merge( G, nodal_dict )
p = 0.5
H, mapping = tools.nodal_filter_merges( G, nodal_dict, p )

r = round( len( H.nodes() ) / len( G.nodes() ), 3 )

print( ">>>>>>>>>>>>>>>>>> lifting graph... <<<<<<<<<<<<<<<<<<<<<" )
#K = tools.get_nodal_lifted_graph( G, H, nodal_dict )
K = tools.get_filtered_lifted_graph( G, H, mapping )
'''
for node in K.nodes():
	print( node )
	print( list( K.neighbors(node) ) )
'''
print( " >>>>>>>>>>>>>>>>>> comparing spectra... <<<<<<<<<<<<<<<<<<<<<" )
tools.compare_spectrum( G, K, k , tolerance, r, normed )
#tools.compare_spectrum( G, K, 20, tolerance, r, normed )

