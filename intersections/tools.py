import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import networkx as nx
import math

def read_general_mesh( filename ):
	with open(filename) as file:
		lines = file.readlines()

	connections = {}

	index = 0
	for line in lines:
		curr_line = line.split()
		if index == 0:
			nodes = int( curr_line[0] )
		elif index == 1:
			edges = int( curr_line[0] )
		else:
			connections[ index-2 ] = []
			pos = 0
			for element in curr_line:
				if pos%2 == 0:
					if pos != 0 and pos != 2:
						connections[ index-2 ].append( float( element ) )
					else:
						connections[ index-2 ].append( int( element ) )
				else:
					connections[ index-2 ].append( int( element ) )
				pos += 1
		index = index + 1 

	# prune all degree 0 nodes
	keys = [ key for key in connections.keys() ]
	for key in keys:
		if connections[ key ][ 2 ] == 0:
			connections.pop( key )
			#nodes = nodes - 1

	return( nodes, edges, connections )

def get_root( conn_dict, nodes, node ):
	root_node = node
	value = conn_dict[ root_node ][ 0 ]
	while value != 0:
		root_node = conn_dict[ root_node ][ 0 ]%nodes
		value = conn_dict[ root_node ][ 0 ]
	return( root_node )

# CURRENTLY SET TO UNIT WEIGHTS
def to_edgelist( nodes, edges, conn_dict ):
	edge_list = {}
	index = 0
	for i in conn_dict.keys():
		degree = conn_dict[i][2]
		for j in range( degree ):
			new_edge = np.sort( [i,conn_dict[i][3 + (2*j)]] )
			#edge_list[ index ] = [new_edge,conn_dict[i][4 + (2*j)]]
			edge_list[ index ] = [new_edge, 1 ]
			index += 1

	return( edge_list )

def get_matrices( nodes, edges, conn_dict ):
	orig_node_map = {}
	coarse_node_map = {}
	orig_index = 0
	coarse_index = 0
	for i in conn_dict.keys():
		orig_node_map[ i ] = orig_index
		orig_index += 1
		if conn_dict[ i ][ 0 ] == 0:
			coarse_node_map[ i ] = coarse_index
			coarse_index += 1

	A = np.zeros((len(conn_dict.keys()),len(conn_dict.keys())))
	B = np.zeros((len(coarse_node_map.keys()),len(coarse_node_map.keys())))

	edgelist = to_edgelist( nodes, edges, conn_dict )

	for i in edgelist.keys():
		nod_a = orig_node_map[ edgelist[i][0][0] ]
		nod_b = orig_node_map[ edgelist[i][0][1] ]
		A[nod_a][nod_b] += edgelist[i][1]
		A[nod_b][nod_a] += edgelist[i][1]

		root_a = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][0] ) ]
		root_b = coarse_node_map[ get_root( conn_dict, nodes, edgelist[i][0][1] ) ]
		B[root_a][root_b] += edgelist[i][1]
		B[root_b][root_a] += edgelist[i][1]

	return( A,B )

def get_graph( nodes, edges, conn_dict ):
	edgelist = to_edgelist( nodes, edges, conn_dict )
	edges = len( edgelist )

	new_nodes = np.arange( nodes )
	mapping = {}
	ind = 0
	# for each key in the connection dictionary 
	for key in conn_dict.keys():
		mapping[key] = ind
		ind += 1

	G = nx.Graph()
	G.add_nodes_from( new_nodes )
	for key in edgelist.keys():
		source = mapping[ edgelist[key][0][0] ]
		dest = mapping[ edgelist[key][0][1] ]
		weight = edgelist[key][1]
		#weight = 1.0
		G.add_edge( source, dest )
		G[source][dest]["weight"] = weight

	for node in new_nodes:
		if G.degree( node ) == 0:
			G.remove_node( node )

	return( G )

def get_adjacency( nodes, edges, conn_dict ):
	A = np.zeros((len(conn_dict.keys()),len(conn_dict.keys())))

	edgelist = to_edgelist( nodes, edges, conn_dict )

	for i in edgelist.keys():
		nod_a = edgelist[i][0][0]
		nod_b = edgelist[i][0][1]
		A[nod_a][nod_b] += edgelist[i][1]
		A[nod_b][nod_a] += edgelist[i][1]

	return( A )

def get_matrix( nodes, edge_list ):

	M = np.zeros((nodes,nodes))

	for i in edge_list.keys():
		nod_a = edge_list[i][0]
		nod_b = edge_list[i][1]
		M[nod_a][nod_b] = 1
		M[nod_b][nod_a] = 1

	return( M )

def get_weights( edge_dict, M ):
	weight_dict = {}
	for i in edge_dict.keys():
		node_a = edge_dict[i][0]
		node_b = edge_dict[i][1]
		norm = np.linalg.norm( M[node_a] - M[node_b], 1 )
		weight_dict[ i ] = norm
	sorted_dict = {k: v for k, v in sorted(weight_dict.items(), key=lambda item: item[1])}
	return( sorted_dict )


def get_nodals( G, filename, k, tolerance, normed=False ):
	if normed:
		lap_G = nx.normalized_laplacian_matrix(G,weight="weight").asfptype()
	else:
		lap_G = nx.laplacian_matrix(G,weight="weight").asfptype()

	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k+1, which="SM", ncv=4*k+20, tol=tolerance )
	#evals_G,evecs_G = sp.linalg.eigh( lap_G, k+1, which="SM" )
	idx = evals_G.argsort()[::1]
	evals_G = evals_G[idx]
	evecs_G = evecs_G[:,idx]
	signs = np.sign( np.array(evecs_G) )
	np.savetxt( filename, signs.astype(int), fmt='%i', delimiter=',' )
	return( signs )

def get_nodal_dict( signs ):
	nodal_dict = {}
	row_names = []
	count = 0
	for row in signs:
		row_name = np.array2string( row.astype(int) )
		if row_name in row_names:
			nodal_dict[ row_name ].append( count )
		else:
			row_names .append( row_name )
			nodal_dict[ row_name ] = [ count ]
		count += 1
	return( nodal_dict )

def get_connected_nodal_dict( G, signs ):
	nodal_dict = {}
	row_names = []
	count = 0
	for row in signs:
		row_name = np.array2string( row.astype(int) )
		if row_name in row_names:
			nodal_dict[ row_name ].append( count )
		else:
			row_names .append( row_name )
			nodal_dict[ row_name ] = [ count ]
		count += 1

	new_dict = {}
	curr_key = 0
	for key in nodal_dict:
		components = [ list(c) for c in sorted(nx.connected_components(G.subgraph(nodal_dict[key])), key=len, reverse=True) ]
		for comp in components:
			new_dict[curr_key] = sorted( comp )
			curr_key += 1
	return( new_dict )

def nodal_merges( G, nodal_dict ):
	H = G.copy()
	merge_num = len(G.nodes()) - len(nodal_dict.keys())
	count = 0
	for key in nodal_dict:
		core_node = nodal_dict[ key ][ 0 ]
		for node in nodal_dict[ key ]:
			if node != core_node:
				#H = nx.contracted_nodes( H, core_node, node )
				H = single_merge( H, core_node, node )
				print( "	merge "+str( count )+" out of "+str( merge_num )+" performed...")
				count += 1
	return( H )

def nodal_filter_merges( G, nodal_dict, p ):
	nodes = len( G.nodes() )
	merge_num = math.ceil( nodes*p )
	H = G.copy()
	D = np.diag( np.array([ (1/H.degree(v,weight="weight")) for v in H.nodes() ]) )
	A = nx.adjacency_matrix( H ).todense()
	P = np.matmul( D, A )
	merge_pairs = []
	merge_weights = []
	for key in nodal_dict:
		K = G.subgraph( nodal_dict[ key ] )
		for (u,v) in K.edges():
			merge_pairs.append( (u,v) )
			merge_weights.append( np.linalg.norm( P[u,:] - P[v,:] ) )

	merge_pairs = np.array( merge_pairs )
	merge_weights = np.array( merge_weights )

	idx = merge_weights.argsort()[::1]
	merge_pairs = merge_pairs[idx]
	merge_weights = merge_weights[idx]

	merged = 0
	index = 0
	mapping = { i:i for i in H.nodes() }
	while merged < merge_num:
		a,b = np.argsort( np.array([mapping[merge_pairs[index][0]],mapping[merge_pairs[index][1]]]) )
		u = mapping[merge_pairs[index][a]]
		v = mapping[merge_pairs[index][b]]

		if u != v:
			H = single_merge( H, u, v )
			for key in mapping.keys():
				if mapping[key] == v:
					mapping[key] = u
			merged += 1

		index += 1
		print( "	merge "+str( merged )+" out of "+str( merge_num )+" performed...")
		
	return( H, mapping )

def get_filtered_lifted_graph( G, H, mapping ):
	#initialize graph
	K = nx.Graph()
	K.add_nodes_from( list( G.nodes() ) )
	# initialize a dictionary of clusters
	clust_dict = {}
	for key in mapping.keys():
		if mapping[key] in list( clust_dict.keys() ):
			clust_dict[ mapping[key] ].append( key )
		else:
			clust_dict[ mapping[key] ] = [ key ]

	keys = list( clust_dict.keys() )
	for clust_src in clust_dict.keys():
		for clust_dest in clust_dict.keys():
			clust_a = set( clust_dict[ clust_src ] )
			clust_b = set( clust_dict[ clust_dest ] )
			weight = 0
			edges = { (u,v) for u in clust_a for v in clust_b.intersection(G.adj[u]) }
			for edge in edges:
					weight += G[edge[0]][edge[1]]["weight"]
			btwn_cluster_weight = weight / ( len(clust_a) * len(clust_b) )
			if btwn_cluster_weight != 0:
				for node in clust_a:
					for neighbor in clust_b:
						K.add_edge( node, neighbor, weight=btwn_cluster_weight )

	return(K)

def get_nodal_lifted_graph( G, H, nodal_dict ):

	# initialize graph
	K = nx.Graph()
	K.add_nodes_from( list( G.nodes() ) )

	# calculate weights and add edges
	keys = list( nodal_dict.keys() )
	for clust_src in nodal_dict.keys():
		for clust_dest in nodal_dict.keys():
			clust_a = set( nodal_dict[ clust_src ] )
			clust_b = set( nodal_dict[ clust_dest ] )
			weight = 0
			edges = { (u,v) for u in clust_a for v in clust_b.intersection(G.adj[u]) }
			for edge in edges:
					weight += G[edge[0]][edge[1]]["weight"]
			btwn_cluster_weight = weight / ( len(clust_a) * len(clust_b) )
			if btwn_cluster_weight != 0:
				for node in clust_a:
					for neighbor in clust_b:
						K.add_edge( node, neighbor, weight=btwn_cluster_weight )
	return( K )

def fast_nodal_merge( G, nodal_dict ):
	H = G.copy()
	merge_num = len(G.nodes()) - len(nodal_dict.keys())
	count = 0
	for key in nodal_dict:
		node_a = nodal_dict[ key ][ 0 ]
		for node_b in nodal_dict[ key ]:
			if node_b != node_a:
				#H = nx.contracted_nodes( H, core_node, node )
				#H = single_merge( H, core_node, node )
				neighbors = {}
				for node in H.neighbors(node_a):
					if node not in neighbors.keys():
						neighbors[node] = H[node_a][node]["weight"]
					else:
						neighbors[node] += H[node_a][node]["weight"]
				for node in H.neighbors(node_b):
					if node not in neighbors.keys():
						neighbors[node] = H[node_b][node]["weight"]
					else:
						neighbors[node] += H[node_b][node]["weight"]

				neighbor_list = list( H.neighbors( node_a ) )
				for neighbor in neighbor_list:
					H.remove_edge( node_a, neighbor )
				H.remove_node( node_b )

				for neighbor in neighbors.keys():
					if neighbor != node_b:
						H.add_edge( node_a, neighbor, weight=neighbors[neighbor] )

				print( "	merge "+str( count )+" out of "+str( merge_num )+" performed...")
				count += 1
	return( H )

def single_merge( G, node_a, node_b ):
	H = G.copy()

	neighbors = {}
	for node in G.neighbors(node_a):
		if node not in neighbors.keys():
			neighbors[node] = G[node_a][node]["weight"]
		else:
			neighbors[node] += G[node_a][node]["weight"]
	for node in G.neighbors(node_b):
		if node not in neighbors.keys():
			neighbors[node] = G[node_b][node]["weight"]
		else:
			neighbors[node] += G[node_b][node]["weight"]

	neighbor_list = list( H.neighbors( node_a ) )
	for neighbor in neighbor_list:
		H.remove_edge( node_a, neighbor )
	H.remove_node( node_b )

	for neighbor in neighbors.keys():
		if neighbor != node_b:
			H.add_edge( node_a, neighbor, weight=neighbors[neighbor] )

	return( H )

def perform_merges( G, edge_dict, edge_fitness, threshold ):
	H = G.copy()
	contractions = 0
	index = 0
	mapping = {i:i for i in G.nodes()}
	for i in edge_fitness.keys():
		a = edge_dict[ i ][0]
		b = edge_dict[ i ][1]
		node_a = mapping[ a ]
		node_b = mapping[ b ]
		if node_a != node_b:
			print( "performing contraction..."+str(index))
			index += 1
			#H = nx.contracted_nodes( H, node_a, node_b )
			H = single_merge( H, node_a, node_b )
			for i in mapping.keys():
				if mapping[i] == node_b or mapping[i] == b:
					mapping[i] = node_a
			contractions += 1
		if contractions > threshold:
			break

	return( H )

def get_eigens( G, k, tolerance, normed=False ):
	if normed:
		lap_G = nx.normalized_laplacian_matrix(G,weight="weight").asfptype()
	else:
		lap_G = nx.laplacian_matrix(G,weight="weight").asfptype()

	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k+1, which="SM", ncv=4*k+20, tol=tolerance)
	idx1 = evals_G.argsort()[::1]
	evals_G = evals_G[idx1]
	evecs_G = evecs_G[:,idx1]

	return( evals_G, evecs_G )

def get_eigen_variances( G, k, tolerance, normed=False ):
	if normed:
		lap_G = nx.normalized_laplacian_matrix(G,weight="weight").asfptype()
	else:
		lap_G = nx.laplacian_matrix(G,weight="weight").asfptype()

	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k+1, which="SM", ncv=4*k+20, tol=tolerance)
	idx1 = evals_G.argsort()[::1]
	evals_G = evals_G[idx1]
	evecs_G = evecs_G[:,idx1]

	var_arr = np.zeros((k,2))
	index = 0
	for i in range( k ):
		vect = evecs_G[ :, i+1 ]
		vect = vect / sp.linalg.norm( vect, ord=2 )
		num_a = 0
		num_b = 0
		sum_a = 0
		sum_b = 0 
		for entry in vect:
			if entry >= 0:
				sum_a += entry
				num_a += 1
			else:
				sum_b += entry
				num_b += 1
		mean_a = sum_a / num_a
		mean_b = sum_b / num_b
		var_a = 0
		var_b = 0
		for entry in vect:
			if entry >= 0:
				var_a += (entry - mean_a)**2
			else:
				var_b += (entry - mean_b)**2
		var_arr[ index, : ] = [ var_a / num_a, var_b / num_b ]
		index += 1

	return( var_arr )

#----------------------------------------------------------
# Plotting functions
#----------------------------------------------------------

def plot_nodal_components( G, components, pos, fignum, filepath, k ):
	node_size = np.zeros( len(G.nodes()) )
	for i in range( len(G.nodes()) ):
		node_size[i] = 20

	short_color_list = np.zeros( len(components.keys()) )
	index = 0
	for key in components.keys():
		for node in components[key]:
			short_color_list[index] += index
		index += 1

	colors = np.zeros( len(G.nodes()) )
	index = 0
	for key in components.keys():
		for node in components[key]:
			#colors[node] = np.abs( eig[node] - means[index] )
			colors[node] = short_color_list[ index ]
		index += 1

	plt.figure( fignum )
	nx.draw_networkx_nodes( G, pos, node_size=node_size, node_color=colors, cmap= "tab20" )
	nx.draw_networkx_edges( G, pos, width=0.5, alpha=0.4 )
	plt.title("nodal partitions, "+str(k)+" eigenvectors",fontsize=16)
	plt.savefig( filepath )

def plot_component_variances( G, components, pos, eig, fignum, filepath ):
	node_size = np.zeros( len(G.nodes()) )
	for i in range( len(G.nodes()) ):
		node_size[i] = 20
	means = []
	index = 0
	for key in components.keys():
		means.append( 0 )
		for node in components[key]:
			means[index] += eig[node]
		means[index] /= len(components[key])
		index += 1

	short_color_list = np.zeros( len(means) )
	index = 0
	for key in components.keys():
		for node in components[key]:
			short_color_list[index] += ( eig[node] - means[index] )**2
		index += 1

	colors = np.zeros( len(G.nodes()) )
	index = 0
	for key in components.keys():
		for node in components[key]:
			#colors[node] = np.abs( eig[node] - means[index] )
			colors[node] = short_color_list[ index ]
		index += 1

	plt.figure( fignum )
	nx.draw_networkx_nodes( G, pos, node_size=node_size, node_color=colors, cmap= "coolwarm" )
	nx.draw_networkx_edges( G, pos, width=0.5, alpha=0.4 )
	vmin = np.min(colors)
	vmax = np.max(colors)
	cmap = plt.cm.coolwarm
	bar = plt.cm.ScalarMappable( cmap=cmap, norm=plt.Normalize(vmin=vmin, vmax=vmax))
	plt.colorbar( bar )
	plt.title("squared variance, eigenvector: "+str( fignum ),fontsize=16)
	plt.savefig( filepath )

def plot_eigen_variances( k, var_arr ):
	fig, ax = plt.subplots(1,1)
	X = np.arange( k )
	ax.bar( X, height=var_arr[ :, 0 ], color="black", width=0.4 )
	ax.bar( X + 0.4, height=var_arr[ :, 1 ], fill=False, edgecolor="black", width=0.4 )
	ax.set_xticks( np.arange( k ) )
	ax.set_title("nodal variances", fontsize=18)
	ax.set_xlabel("non-trivial eigenvalue index", fontsize=18)
	ax.set_ylabel("variance", fontsize=18)
	ax.legend(labels=["positive","negative"],fontsize=14)
	plt.show()

def compare_spectrum( G, H, k, tolerance, r, normed=False ):
	xg = range( k+1 )
	xh = range( k+1 )
	if normed:
		lap_G = nx.normalized_laplacian_matrix(G,weight="weight").asfptype()
		lap_H = nx.normalized_laplacian_matrix(H,weight="weight").asfptype()
	else:
		lap_G = nx.laplacian_matrix(G,weight="weight").asfptype()
		lap_H = nx.laplacian_matrix(H,weight="weight").asfptype()

	print( "lap_G trace: " + str( lap_G.diagonal().sum() ) + " lap_H trace: " + str( lap_H.diagonal().sum() ) )
	print( lap_G )
	print()
	print( lap_H )
	print()
	print( "IN: compare_spectra() -- computing eigen values / vectors..." )
	evals_G,evecs_G = sp.sparse.linalg.eigs( lap_G, k+1, which="SM", ncv=4*k+20, tol=tolerance )
	print( "	computed eigenpairs of G..." )
	evals_H,evecs_H = sp.sparse.linalg.eigs( lap_H, k+1, which="SM", ncv=4*k+20, tol=tolerance )
	print( evecs_G )
	print( evecs_H )
	print( "	computed eigenpairs of H..." )
	#evals_G,evecs_G = sp.linalg.eigh( lap_G, k+1, which="SM" )
	#evals_H,evecs_H = sp.linalg.eigh( lap_H, k+1, which="SM" )

	idx1 = evals_G.argsort()[::1]
	evals_G = evals_G[idx1]
	evecs_G = evecs_G[:,idx1]
	idx2 = evals_H.argsort()[::1]
	evals_H = evals_H[idx2]
	evecs_H = evecs_H[:,idx2]

	print( "G eigenvals:" )
	print(evals_G)
	print( "H eigenvals:" )
	print(evals_H)

	Gvals = np.array(evals_G)
	Hvals = np.array(evals_H)
	norm_diff = np.linalg.norm( Gvals - Hvals, 2 )
	print( "norm diff:" )
	print(norm_diff/ np.linalg.norm(Gvals,2))

	print( "plotting..." )
	fig, ( ax1, ax2 ) = plt.subplots(1,2)
	ax1.plot( xg, evals_G, '--', c='c', linewidth=2, label='original (n)')
	ax1.scatter( xh, evals_H, s=60, c='firebrick', marker='.', label='coarsened')

	ax1.legend(loc='upper left',fontsize=14)
	ax1.set_title("coarsened spectra, prop. size: "+str(r),fontsize=18)
	ax1.set_xlabel("eigenvalue index",fontsize=18)
	ax1.set_ylabel("value",fontsize=18)

	arr_comp1 = np.abs( np.matmul( evecs_G.transpose(), evecs_H ) )
	
	ax2.set_title("eigenvector comparison",fontsize=18)
	ax2.imshow(arr_comp1,cmap="YlGnBu")

	ax1.set_box_aspect(1)
	ax2.set_box_aspect(1)
	
	plt.show()
